
#ifndef _JSNET_DLK_CMD_H
#define _JSNET_DLK_CMD_H

#include "jsnet_dlk.h"

/*+ VerfCode(limited time, dynamic)*/
//B:BROADCAST;	T:TARGET;	L:LOCAL;
#define FRAME_CMD_OPEN_NODE_REQUEST		0X01//SAddr:L TAddr:B
#define FRAME_CMD_OPEN_NODE_RESPONSE	0X02//SAddr:L TAddr:T 											->NoACK response when node was opne

#define FRAME_CMD_LINK_REQUEST			0X03//SAddr:L TAddr:T + DeviceType
#define FRAME_CMD_LINK_RESPONSE			0X04//SAddr:L TAddr:T + Key										->NoACK response when node was opne

#define FRAME_CMD_DEVICE_NOTIFY			0X05//SAddr:L TAddr:L/B + ShortAddr + DeviceType				->NoACK encrypted

#define FRAME_CMD_UNLINK_NOTIFY			0X08//SAddr:L TAddr:T											->NoACK encrypted

#define FRAME_CMD_DATA_REQUEST			0X09//SAddr:L(short/long) TAddr:T(short/long)					->NeedACK encrypted

#define FRAME_CMD_ACK					0X0E//SAddr:L(short/long) TAddr:T(short/long) + RecFrameCount	->NoACK encrypted, match by sequence num

#define FRAME_DATA						0X00//SAddr:L(short/long) TAddr:T(short/long) + DataPayload		->NeedACK encrypted


uint8_t dlk_cmd_open_node_request(void);
#if(JSNET_DLK_DEVICE_TYPE == JSNET_DLK_DEVICE_TYPE_STD_NODE)
uint8_t dlk_cmd_open_node_response(uint8_t* target_node_addr, uint8_t target_node_addr_type);
#endif

uint8_t dlk_cmd_link_request(uint8_t* target_node_addr, uint8_t target_node_addr_type);
#if(JSNET_DLK_DEVICE_TYPE == JSNET_DLK_DEVICE_TYPE_STD_NODE)
uint8_t  dlk_cmd_link_response(uint8_t* target_node_addr, uint8_t target_node_addr_type,
							uint8_t key);
#endif

uint8_t dlk_cmd_device_notify(uint8_t* target_node_addr, uint8_t target_node_addr_type,
							uint8_t* short_local_node_addr, uint8_t local_device_type);

uint8_t dlk_cmd_unlink_notify(void);

uint8_t dlk_cmd_data_request(uint8_t* target_node_addr, uint8_t target_node_addr_type);

uint8_t dlk_cmd_ack(uint8_t* target_node_addr, uint8_t target_node_addr_type,
							uint8_t rec_frame_count);

uint8_t dlk_data_send(uint8_t* target_node_addr, uint8_t target_node_addr_type,
							uint8_t* send_data, uint8_t send_data_len);
							
#endif
