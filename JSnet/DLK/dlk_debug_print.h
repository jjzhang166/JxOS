

#ifndef _JSNET_DLK_DEBUG_PRINT_H
#define _JSNET_DLK_DEBUG_PRINT_H

#include "jxos_public.h"
#include "jsnet_dlk.h"

#if (JSNET_DLK_DEBUG_PRINT_ENABLE == 1)
//#define dlk_debug_print_str(s)							sys_debug_print_task_print_str(s,0)
//#define dlk_debug_print_int(v)							sys_debug_print_task_print_int(v,0)
//#define dlk_debug_print_uint(v)							sys_debug_print_task_print_uint(v,0)
//#define dlk_debug_print_hex(v)							sys_debug_print_task_print_hex(v,0)
//#define dlk_debug_print_bin(v)							sys_debug_print_task_print_bin(v,0)
//#define dlk_debug_print_data_stream_in_hex(p,l)			sys_debug_print_task_print_data_stream_in_hex(p,l,0)

#define dlk_debug_print_str(s)								printf(s)
#define dlk_debug_print_int(v)								printf("%d", v)
#define dlk_debug_print_uint(v)								printf("%d", v)
#define dlk_debug_print_hex(v)								printf("%x", v)
#define dlk_debug_print_bin(v)
#define dlk_debug_print_data_stream_in_hex(p,l)
#else
#define dlk_debug_print_str(s)
#define dlk_debug_print_int(v)
#define dlk_debug_print_uint(v)
#define dlk_debug_print_hex(v)
#define dlk_debug_print_bin(v)
#define dlk_debug_print_data_stream_in_hex(p,l)
#endif

#endif
