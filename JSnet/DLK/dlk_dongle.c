

#include "jsnet_dlk.h"

void dongle_init(void)
{
}

//dongle
#if(0)
#include "stdio.h"
#include "jsnet_phy.h"
#include "dlk_frame.h"
#include "dlk_cmd.h"
void dongle_handler(uint8_t* receive_data, uint8_t data_len)
{
	uint8_t buff[19];
    uint8_t frame_len;
    uint8_t frame_data_len;
    uint8_t pan_id;
    uint8_t frame_type;
    uint8_t source_node_addr;
    uint8_t target_node_addr;
    uint8_t frame_count;
    uint8_t frame_ask_ack;

	uint8_t i;
	memcpy(buff, receive_data, data_len);
	for(i = 0; i < data_len; i++){
		printf("%x ", (int)buff[i]);
	}
	printf("\r\n");

    if(deframe(buff, &frame_len,
			   receive_data, &frame_data_len,
			   &pan_id,
			   &frame_type,
			   &target_node_addr,
			   &source_node_addr,
			   &frame_count,
	&frame_ask_ack) == 0){
        return;
    }

//	printf("ReLen: %d\tFrLen :%d\r\n", (int)data_len, (int)frame_len);
	printf("PanId: %d  |", (int)pan_id);
	printf("SAddr: 0x%x |TAddr: 0x%x |", (int)source_node_addr, (int)target_node_addr);
	printf("FrNum: %d |", (int)frame_count);
	printf("FrAck: ");
	if(frame_ask_ack == FRAME_NEED_ACK){
		printf("NeedACK");
	}
	else{
		printf("NoACK  ");
	}
	printf(" |");

	printf("FrTyp: ");
	switch(frame_type){
		case FRAME_TYPE_CMD:
			printf("CMD:");
			switch(receive_data[0]){
				case FRAME_CMD_ACK:
					printf("ACK ");
					break;

				case FRAME_CMD_PANID_REQUEST:
					printf("PANID REQ");
					break;
				case FRAME_CMD_PANID_RESPONSE:
					printf("PANID RSP");
					break;
				case FRAME_CMD_ASSOC_REQUEST:
					printf("ASSOC REQ; VrefCode: %x %x, DeviceType: ",
						   (int)receive_data[1], (int)receive_data[2]);
					if((int)receive_data[3] == JSNET_DLK_DEVICE_TYPE_STD_NODE){
						printf("COORDINATOR");
					}
					else if((int)receive_data[3] == JSNET_DLK_DEVICE_TYPE_STD_NODE){
						printf("WAKE_END");
					}
					else if((int)receive_data[3] == JSNET_DLK_DEVICE_TYPE_REDUCED_NODE){
						printf("SLEEP_END");
					}
					else{
						printf("Unknow");
					}
					break;
				case FRAME_CMD_ASSOC_RESPONSE:
					printf("ASSOC RSP; VrefCode: %x %x, PanKey: %x %x",
						   receive_data[1], receive_data[2], receive_data[3], receive_data[4]);
					break;
				case FRAME_CMD_DISASSOC_NOTIFY:
					printf("DISASSOC NTF");
					break;
				case FRAME_CMD_NEWPAN_NOTIFY:
					printf("NEWPAN NTF");
					break;
				case FRAME_CMD_PANCONFLICT_NOTIFY:
					printf("PANCONFLICT NTF");
					break;
				case FRAME_CMD_DATA_REQUEST:
					printf("DATA REQ");
					break;
				case FRAME_CMD_REJION_REQUEST:
					printf("REJION REQ VrefCode: %x %x, PanKey: %x %x DeviceType: ",
						   receive_data[1], receive_data[2], receive_data[3], receive_data[4]);
					if((int)receive_data[5] == JSNET_DLK_DEVICE_TYPE_STD_NODE){
						printf("COORDINATOR");
					}
					else if((int)receive_data[3] == JSNET_DLK_DEVICE_TYPE_STD_NODE){
						printf("WAKE_END");
					}
					else if((int)receive_data[3] == JSNET_DLK_DEVICE_TYPE_REDUCED_NODE){
						printf("SLEEP_END");
					}
					else{
						printf("Unknow");
					}
					break;
				case FRAME_CMD_REJION_RESPONSE:
					printf("REJION RSP");
					break;
				default:
					printf("Unknow Cmd");
					break;
			}
			break;

		case FRAME_TYPE_DATA:
			printf("DATA");
			break;

		default:
			printf("Unknow");
			break;
	}

	printf("\r\n----------------------------------------------------------------------------------------\r\n");
}

#include "soc.h"


/*
char putchar(char c)
{
//	Send_Data_To_UART0(c);
    TI = 0;
    SBUF = c;
    while(TI==0);
	return 0;
}
*/
#endif
