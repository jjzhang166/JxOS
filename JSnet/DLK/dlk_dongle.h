
#ifndef _JSNET_DLK_DONGLE_H
#define _JSNET_DLK_DONGLE_H


#if(JSNET_DLK_DEVICE_TYPE == JSNET_DLK_DEVICE_TYPE_DONGLE)
void dongle_handler(uint8_t* receive_data, uint8_t data_len);
void dongle_init(void);
#endif


#endif
