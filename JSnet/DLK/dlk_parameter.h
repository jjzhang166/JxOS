
#ifndef _JSNET_DLK_PARAMETER_H
#define _JSNET_DLK_PARAMETER_H

#include "lib/type.h"
#include "jsnet_dlk.h"

uint8_t node_addr_match(uint8_t* node_addr_1, uint8_t* node_addr_2,
					uint8_t node_addr_type);
uint8_t node_addr_set(uint8_t* t_node_addr, uint8_t* s_node_addr,
					uint8_t node_addr_type);		//s->t

uint8_t broadcast_addr_check(uint8_t* node_addr, uint8_t node_addr_type);
uint8_t* broadcast_addr_get(void);

//NEIGHBOR_NODE_STRUCT* neighbor_node_map_get(uint8_t index);
//NEIGHBOR_NODE_STRUCT* neighbor_node_map_find_by_long_addr(uint8_t* long_node_addr);
//NEIGHBOR_NODE_STRUCT* neighbor_node_map_find_by_short_addr(uint8_t* short_node_addr);
//uint8_t neighbor_node_map_add(NEIGHBOR_NODE_STRUCT* new_neighbor_node);
//void neighbor_node_map_del(uint8_t index);
//uint8_t neighbor_node_map_find_empty(void);

uint8_t* local_node_addr_get(uint8_t node_addr_type);
void local_node_addr_set(uint8_t* node_addr, uint8_t node_addr_type);

uint8_t frame_count_get(void);
void frame_count_add(void);

uint8_t discoverable_falg_get(void);
void discoverable_falg_set(uint8_t state_new);

uint8_t dlk_state_get(void);
void dlk_state_set(uint8_t state_new);
void dlk_state_turn_back(void);

uint8_t key_get(void);
void key_set(uint8_t in_key);

void dlk_parameter_init(void);

#endif
