
#ifndef _JSNET_DLK_H
#define _JSNET_DLK_H

#include "lib/type.h"

/****************************************************/
#define JSNET_DLK_DEBUG_PRINT_ENABLE			1

#define JSNET_DLK_DEVICE_TYPE_STD_NODE		0
#define JSNET_DLK_DEVICE_TYPE_REDUCED_NODE		1	//can't be a open node; connet to STD_NODE only
#define JSNET_DLK_DEVICE_TYPE_DONGLE			3

#define JSNET_DLK_DEVICE_TYPE					JSNET_DLK_DEVICE_TYPE_STD_NODE

#define DLK_STATE_SCANING					1
#define DLK_STATE_CONNECTING				2
#define DLK_STATE_DATA_POLLING				4
#define DLK_STATE_DATA_SENDING				5
#if (JSNET_DLK_DEVICE_TYPE == JSNET_DLK_DEVICE_TYPE_STD_NODE)
#define DLK_STATE_STARTED					0XFF
#elif (JSNET_DLK_DEVICE_TYPE == JSNET_DLK_DEVICE_TYPE_REDUCED_NODE)
#define DLK_STATE_CONNECTED					0XFF
#define DLK_STATE_NOT_CONNECTED				0XFE
#else//err
#endif

/****************************************************/
#if (JSNET_DLK_DEVICE_TYPE == JSNET_DLK_DEVICE_TYPE_STD_NODE)
	#define JSNET_DLK_NEIGHBOR_MAP_LEN		5
#elif (JSNET_DLK_DEVICE_TYPE == JSNET_DLK_DEVICE_TYPE_REDUCED_NODE)
	#define JSNET_DLK_NEIGHBOR_MAP_LEN		1
#else//err
#endif

/****************************************************/
//param configed by n+1 layer
uint8_t jsnet_dlk_parameter_get_node_state(void);

uint8_t jsnet_dlk_parameter_get_local_node_addr(uint8_t* local_node_addr, uint8_t* local_node_addr_len);
uint8_t jsnet_dlk_parameter_set_local_node_addr(uint8_t* local_node_addr, uint8_t local_node_addr_len);

uint8_t jsnet_dlk_parameter_get_local_key(uint8_t* key, uint8_t* key_len);
uint8_t jsnet_dlk_parameter_set_local_key(uint8_t* key, uint8_t key_len);

/****************************************************/
//interface called by n+1 layer
void jsnet_dlk_request_reset(void);

uint8_t jsnet_dlk_request_receive_enable(void);
uint8_t jsnet_dlk_request_receive_disable(void);

#if(JSNET_DLK_DEVICE_TYPE == JSNET_DLK_DEVICE_TYPE_STD_NODE)
uint8_t jsnet_dlk_request_discoverable(void);
uint8_t jsnet_dlk_request_undiscoverable(void);
#endif

uint8_t jsnet_dlk_request_scan(void);
extern void (*jsnet_dlk_confirm_scan)(uint8_t* open_node_adddr, uint8_t open_node_adddr_len); //issr?
//open_node_adddr = 0; open_node_adddr_len = 0; -->finish

uint8_t jsnet_dlk_request_connect(uint8_t* target_node_addr, uint8_t target_node_addr_len);
extern void (*jsnet_dlk_confirm_connect)(uint8_t* confirm_node_adddr, uint8_t confirm_node_adddr_len, uint8_t success);
//success = 0 -->fail
#if(JSNET_DLK_DEVICE_TYPE == JSNET_DLK_DEVICE_TYPE_STD_NODE)
extern uint8_t (*jsnet_dlk_indication_connect)(uint8_t* req_connect_node_adddr, uint8_t req_connect_node_adddr_len);
#endif

uint8_t jsnet_dlk_request_announce(uint8_t* target_node_addr, uint8_t target_node_addr_len);	//short addr
extern void (*jsnet_dlk_confirm_announce)(uint8_t* announce_node_adddr, uint8_t announce_node_adddr_len); //issr?
//announce_node_adddr = 0; announce_node_adddr_len = 0; -->finish
#if(JSNET_DLK_DEVICE_TYPE == JSNET_DLK_DEVICE_TYPE_STD_NODE)
extern void (*jsnet_dlk_indication_announce)(uint8_t* announce_node_adddr, uint8_t announce_node_adddr_len);
#endif

uint8_t jsnet_dlk_request_disconnect(void);
extern void (*jsnet_dlk_indication_disconnect)(uint8_t* disconnect_node_adddr, uint8_t disconnect_node_adddr_len);

uint8_t jsnet_dlk_request_data_poll(uint8_t* target_node_addr, uint8_t target_node_addr_len);
extern void (*jsnet_dlk_confirm_data_poll)(uint8_t* target_node_addr, uint8_t target_node_addr_len, uint8_t success);
//success = 0 -->fail
extern void (*jsnet_dlk_indication_data_poll)(uint8_t* req_node_addr, uint8_t req_node_addr_len);

uint8_t jsnet_dlk_request_data_send(uint8_t* target_node_addr, uint8_t target_node_addr_len,
								uint8_t* send_data, uint8_t send_data_len);
extern void (*jsnet_dlk_confirm_data_send)(uint8_t* target_node_addr, uint8_t target_node_addr_len, uint8_t success);
//success = 0 -->fail
extern void (*jsnet_dlk_indication_data_receive)(uint8_t* source_node_addr, uint8_t source_node_addr_len,
											uint8_t* receive_playload, uint8_t receive_playload_len);

/****************************************************/
void dlk_task_init(void);

#endif


/********************************
typedef struct
{
	uint8_t device_type;
	uint8_t short_node_addr[1];	//not necessary
	uint8_t long_node_addr[2];	//necessary
	//issr
} NEIGHBOR_NODE_STRUCT;
//when short_node_addr is not FRAME_ADDR_BROADCAST, always prefer using short_node_addr

uint8_t* jsnet_dlk_parameter_get_neighbor_node_map(NEIGHBOR_NODE_STRUCT* neighbor_node_map, uint8_t neighbor_node_map_len);
********************************/