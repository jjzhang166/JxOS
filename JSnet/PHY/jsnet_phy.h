
#ifndef _JSNET_PHY_H
#define _JSNET_PHY_H

#include "lib\type.h"

//CONFIG
#define JXNET_PHY_REC_BYFF_LEN              256
#define JXNET_PHY_SEND_BYFF_LEN             256
#define JXNET_PHY_SEND_OVERTIME_TIME_MS     50
#define JXNET_PHY_SEND_RETRY_TIME_MS        5
#define JXNET_PHY_SEND_RETRY_COUNT          3

uint8_t jsnet_phy_request_reset(void);
uint8_t jsnet_phy_request_check_tx_busy(void);      //0:not busy; 1:busy
uint8_t jsnet_phy_request_tx_enable(void);
uint8_t jsnet_phy_request_tx_disable(void);
uint8_t jsnet_phy_request_rx_enable(void);
uint8_t jsnet_phy_request_rx_disable(void);
uint8_t jsnet_phy_request_set_channle(uint8_t set_channel);


uint8_t jsnet_phy_request_data_send(uint8_t* send_data, uint8_t data_len);  //there is an internal data buffer
extern void (*jsnet_phy_confirm_data_send)(uint8_t ok_fail);    //called after the data is written into the buffer and delay JXNET_PHY_SEND_OVERTIME_TIME_MS

extern void (*jsnet_phy_indication_data_receive)(uint8_t* receive_data, uint8_t data_len);

uint8_t jsnet_phy_request_channel_idle_assessment(void);
extern void (*jsnet_phy_confirm_channel_idle_assessment)(uint8_t idle_busy);

/**************************************************/
void phy_data_send_finish_handler(void);
void phy_data_receive_handler(uint8_t* receive_data, uint8_t data_len);

void phy_check_tx_busy_callback_register(uint8_t (*callback_handler)(void));
void phy_rx_disable_callback_register(uint8_t (*callback_handler)(void));
void phy_tx_disable_callback_register(uint8_t (*callback_handler)(void));
void phy_rx_enable_callback_register(uint8_t (*callback_handler)(void));
void phy_tx_enable_callback_register(uint8_t (*callback_handler)(void));
void phy_data_send_callback_register(uint8_t (*callback_handler)(uint8_t* send_data, uint8_t data_len));

void phy_task_init(void);

#endif
