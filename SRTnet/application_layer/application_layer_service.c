
/*
帧结构:
service cmd param len	: the param len of this cmd (byte)
3 bit: 111XXXXX (0~7)

service id				: the id of this service (service type)
5 bit: XXX11111 (0~37)

service cmd				: the cmd of this service (service's cmd)
4 bit: 1111XXXX (0~15)

service serila mun		: the serila mun in the service list of this service (end point/port)
4 bit: XXXX1111 (0~15)

service cmd param		: the param of this cmd 
n byte (Big-Endian)
*/
/********************************************
MASTER								SLAVE
service serila mun  ---------->		service serila mun
				^					|
				|					v
		service id             	   service id
********************************************/

#include <string.h>
#include "lib/bit.h"
#include "jxos_public.h"
#include <srtnet_config.h>
#include "../communication_layer/transmitter.h"
#include "../communication_layer/receiver.h"
#include "application_layer_service.h"
#include "service_define.h"
#include "service_cmd_handler.h"

uint8_t service_config_table[][2] = {SERVICE_CONFIG_TABLE_INIT};
uint8_t SERVICE_CONFIG_TABLE_LEN  = (sizeof(service_config_table))/2;

#if (SRTNET_COMM_DEVICE_TYPE == SRTNET_COMM_DEVICE_TYPE_RECEIVER)\
    ||(SRTNET_COMM_DEVICE_TYPE == SRTNET_COMM_DEVICE_TYPE_REPEATER)
static uint16_t	active_service_mark = 0;

void srtnet_app_layer_service_slave_receive_cmd_handler(uint8_t slave_service_serila_mun, uint8_t service_id, uint8_t service_cmd_id, uint8_t* service_cmd_param)
{
	uint32_t temp_param = 0;
	switch(service_id){
#if (SERVICE_ONOFF_SLAVE_ENABLE == 1)
	case SERVICE_ONOFF_ID:
		switch(service_cmd_id){
		case SERVICE_ONOFF_CMD_ON:
			service_onoff_rec_cmd_on_handler(slave_service_serila_mun);
			break;
		case SERVICE_ONOFF_CMD_OFF:
			service_onoff_rec_cmd_off_handler(slave_service_serila_mun);
			break;
		case SERVICE_ONOFF_CMD_TOGGLE:
			service_onoff_rec_cmd_toggle_handler(slave_service_serila_mun);
			break;
		default:
			break;
		}
		break;
#endif
#if (SERVICE_TEMPERATURE_SLAVE_ENABLE == 1)
	case SERVICE_TEMPERATURE_ID:
		switch(service_cmd_id){
		case SERVICE_TEMPERATURE_CMD_REPORTVAL:
			temp_param = service_cmd_param[1];
			temp_param <<= 8;
			temp_param |= service_cmd_param[2];
			service_temperature_rec_cmd_report_val(slave_service_serila_mun, (uint16_t)temp_param);
		break;
		default:
			break;
		}
		break;
#endif

/*******************************************************************/
//default service
	case SERVICE_BUNDING_ID:
		switch(service_cmd_id){
		case SERVICE_BUNDING_CMD_BUND:
			service_binding_rec_cmd_bind_handler(service_cmd_param);
			break;
		case SERVICE_BUNDING_CMD_UNBUND:
			service_binding_rec_cmd_unbind_handler(service_cmd_param);
			break;
		default:
			break;
		}
		break;

	default:
		break;
	}
}

uint8_t srtnet_app_layer_service_set_active_service(uint8_t serila_mun)
{
#if (SRTNET_APP_LAYER_DEBUG_PRINT_ENABLE == 1)
	sys_debug_print_task_user_print_str("set_active_service:");
	sys_debug_print_task_user_print_uint(serila_mun);
	sys_debug_print_task_user_print_str("\r\n");
#endif
	if((serila_mun < SERVICE_CONFIG_TABLE_LEN)
		&&(service_config_table[serila_mun][SERVICE_CONFIG_TABLE_SERVICE_M_S_OBJECT] == SERVICE_SLAVE)){
		active_service_mark = set_bit_32(active_service_mark, serila_mun);
#if (SRTNET_APP_LAYER_DEBUG_PRINT_ENABLE == 1)
		sys_debug_print_task_user_print_str("success\r\n");
#endif
		return 1;
	}
	else{
#if (SRTNET_APP_LAYER_DEBUG_PRINT_ENABLE == 1)
		sys_debug_print_task_user_print_str("serila_mun error\r\n");
#endif
		return 0;
	}
}

uint16_t srtnet_app_layer_service_get_active_service_mark(void)
{
	return active_service_mark;
}

void srtnet_app_layer_service_no_active_service(void)
{
#if (SRTNET_APP_LAYER_DEBUG_PRINT_ENABLE == 1)
	sys_debug_print_task_user_print_str("no_active_service\r\n");
#endif
	active_service_mark = 0;
}
#endif

#if (SRTNET_COMM_DEVICE_TYPE == SRTNET_COMM_DEVICE_TYPE_TRANSMITTER)\
    ||(SRTNET_COMM_DEVICE_TYPE == SRTNET_COMM_DEVICE_TYPE_REPEATER)
static uint8_t service_cmd_send_buff[SERVICE_CMD_PARAME_LEN_MAX+SERVICE_CMD_PARAME_HEARD_LEN];
uint8_t srtnet_app_layer_service_master_send_cmd(uint8_t is_broadcast, uint8_t master_service_serila_num, uint8_t service_id, uint8_t service_cmd_id,
										uint8_t* service_cmd_param, uint8_t service_cmd_param_len)
{
	memset(service_cmd_send_buff, 0 , SERVICE_CMD_PARAME_LEN_MAX+SERVICE_CMD_PARAME_HEARD_LEN);
	service_cmd_send_buff[0] = service_cmd_param_len<<5;
	service_cmd_send_buff[0] |= service_id;
	service_cmd_send_buff[1] = service_cmd_id<<4;
	service_cmd_send_buff[1] |= master_service_serila_num;
	memcpy(&(service_cmd_send_buff[SERVICE_CMD_PARAME_HEARD_LEN]), service_cmd_param, service_cmd_param_len);
#if (SRTNET_APP_LAYER_DEBUG_PRINT_ENABLE == 1)
	sys_debug_print_task_user_print_str("srtapp snum: ");
	sys_debug_print_task_user_print_uint(master_service_serila_num);
	sys_debug_print_task_user_print_str("\r\n");

	sys_debug_print_task_user_print_str("srtapp sid: ");
	sys_debug_print_task_user_print_uint(service_id);
	sys_debug_print_task_user_print_str("\r\n");

	sys_debug_print_task_user_print_str("srtapp cmd: ");
	sys_debug_print_task_user_print_uint(service_cmd_id);
	sys_debug_print_task_user_print_str("\r\n");

	sys_debug_print_task_user_print_str("srtapp param: ");
	sys_debug_print_task_user_print_data_stream_in_hex(service_cmd_param, service_cmd_param_len);
	sys_debug_print_task_user_print_str("\r\n");

	sys_debug_print_task_user_print_str("srtapp send: ");
	sys_debug_print_task_user_print_data_stream_in_hex(service_cmd_send_buff, service_cmd_param_len+SERVICE_CMD_PARAME_HEARD_LEN);
	sys_debug_print_task_user_print_str("\r\n");
#endif
	return srtnet_comm_layer_transmitter_device_request_data_send(is_broadcast, service_cmd_send_buff, service_cmd_param_len+SERVICE_CMD_PARAME_HEARD_LEN);
}
#endif


/************************************************************
//SERVICE_MASTER --- SERVICE_SERILA_NUM + SERVICE_ID + SERVICE_CMD_ID + SERVICE_CMD_PARAM --> SERVICE_SLAVE
#define SERVICE_MASTER				0
#define SERVICE_SLAVE				1

#define SERVICE_ONOFF_ID            0

uint8_t service_init_config[][3] =
{
//  SERIAL NUMBER   ID                  SERVER/CLIENT
	{0,				SERVICE_ONOFF_ID,	SERVICE_SLAVE},
	{1,				SERVICE_ONOFF_ID,	SERVICE_MASTER},
	{2,				SERVICE_ONOFF_ID,	SERVICE_SLAVE}
};

typedef void (*SERVICE_CMD_FUNCTION)(uint8_t serila_mun, void* param);


#define SERVICE_ONOFF_CMD_ON              0
#define SERVICE_ONOFF_CMD_OFF             1
#define SERVICE_ONOFF_CMD_TOGGLE          2
void service_onoff_cmd_on(uint8_t mun, void* p)
{
    printf("cmd on\r\n");
}
void service_onoff_cmd_off(uint8_t mun, void* p)
{
    printf("cmd off\r\n");
}
void service_onoff_cmd_toggle(uint8_t mun, void* p)
{
    printf("toggle\r\n");
}
#define SERVICE_ONOFF_CMD_ID_MAX  3
SERVICE_CMD_FUNCTION service_onoff_fun_list[] = {
    service_onoff_cmd_on,
    service_onoff_cmd_off,
    service_onoff_cmd_toggle
};

SERVICE_CMD_FUNCTION* service_fun_list[][2] = {
	service_onoff_fun_list,
};


#define cmd_fum(SERIAL_NUMBER, CMD_ID, )
 	(service_fun_list[(service_init_config[SERIAL_NUMBER][1])][(service_init_config[SERIAL_NUMBER][2])])[CMD_ID](SERIAL_NUMBER);
uint8_t srtnet_app_service_init(void)
{
//	(service_fun_list[SERVICE_ONOFF_ID][SERVICE_SERVER])[SERVICE_ONOFF_CMD_ON](0);
	cmd_fum(0, 0);
	cmd_fum(0, 2);
}

**********************************
COMM_LAYE(source_node_addr, MASTER_S_S/N, MASTER_S_ID, MASTER_S_CMD, MASTER_S_PARAM)
--> BANDING TABLE -> Y/N - SLAVE_S_S/N, MASTER_S_ID,

****************************************************************
static SERVICES_STRUCT service_list[SRTNET_APP_SERVICE_MAX];
static uint8_t service_list_count = 0;
SERVICES_STRUCT* srtnet_app_service_new(uint8_t service_id, uint8_t* service_attribute,
                                        SERVICE_CMD_FUNCTION* service_cmd_fun)
{
    if(service_list_count <= SRTNET_APP_SERVICE_MAX){
        service_list[service_list_count].id = service_id;
        service_list[service_list_count].attribute = service_attribute;
        service_list[service_list_count].cmd_fun = service_cmd_fun;
        service_list_count++;
        return &(service_list[service_list_count-1]);
    }
    else{
        return 0;
    }
}

SERVICES_STRUCT* srtnet_app_service_num_index(uint8_t service_num)
{
    if(service_num < service_list_count){
        return &(service_list[service_num]);
    }
    else{
        return 0;
    }
}

uint8_t srtnet_app_service_struct_index(SERVICES_STRUCT* service)
{
    uint8_t i;
    if(service == 0){
        return 0XFF;
    }
    for(i = 0; i < service_list_count; i++){
        if(&(service_list[i]) == service){
            return i;
        }
    }
    return 0XFF;
}

void srtnet_app_service_id_to_num(uint8_t service_id, uint8_t* service_num_list, uint8_t* service_num_list_len)
{
    uint8_t i;
    if((service_num_list == 0)
       ||(service_num_list_len == 0)){
        return;
    }
    for(i = 0; i < service_list_count; i++){
        if(service_list[i].id == service_id){
            service_num_list[*service_num_list_len] = i;
            (*service_num_list_len)++;
        }
    }
}


void srtnet_app_service_cmd(uint8_t service_num,
                            uint8_t service_id,
                            uint8_t service_cmd_id,
                            uint8_t* service_cmd_param
                            uint8_t service_cmd_param_len)
{

}
****************************************************************/



/********************************************************
#if (SERVICE_ONOFF_SERVER == 1)
void* service_onoff_cmd_on(SERVICES_STRUCT* p, void* q)
{
    printf("cmd on\r\n");
    p->attribute = true;
    return 0;
}

void* service_onoff_cmd_off(SERVICES_STRUCT* p, void* q)
{
    printf("cmd off\r\n");
    p->attribute = false;
    return 0;
}

void* service_onoff_cmd_toggle(SERVICES_STRUCT* p, void* q)
{
    printf("toggle\r\n");
    if(p->attribute == false){
        p->attribute = true;
    }
    else{
        p->attribute = false;
    }
    return 0;
}

SERVICE_CMD_FUNCTION service_onoff_fun_list[SERVICE_FUN_LIST_MAX] = {
    service_onoff_cmd_on,
    service_onoff_cmd_off,
    service_onoff_cmd_toggle
};

uint8_t service_onoff_attribute_list[SERVICE_ATTR_LIST_MAX];

uint8_t srtnet_app_service_onoff_server_new(SERVICES_STRUCT*)
{
    SERVICES_STRUCT* ONOFF;
    ONOFF = srtnet_app_service_new(SERVICE_ONOFF_ID, service_onoff_attribute_list, service_onoff_fun_list);
    ONOFF->attribute[SERVICE_ONOFF_ATTR_ONOFF] = false;
    return ONOFF;
}
#endif

#if (SERVICE_ONOFF_CLIENT == 1)
void srtnet_app_service_onoff_cmd_on(uint8_t service_num)
{
    uint8_t buff[3];
    buff[0] = service_num;
    buff[1] = SERVICE_ONOFF_ID;
    buff[2] = SERVICE_ONOFF_CMD_ON;
    srtnet_comm_request_data_send(buff, 3);
}
#endif

void test_onoff(void)
{
    SERVICES_STRUCT* ONOFF;
    uint8_t list[5];
    uint8_t list_len = 0;

    srtnet_app_service_onoff_server_new();
    srtnet_app_service_onoff_server_new();
//    srtnet_app_service_onoff_server_new();

    srtnet_app_service_id_to_num(SERVICE_ONOFF_ID, list, &list_len);
    printf("list_len %d\r\n", list_len);

    ONOFF = srtnet_app_service_num_index(list[0]);
    printf("srtnet_app_service_num_index %d\r\n", list[0]);

    ONOFF->cmd_fun[0](ONOFF, 0);
}
*****************************************************************/

/*
#define SRTNET_APP_SERVICE_MAX 5

typedef struct _service_struct_ SERVICES_STRUCT;
typedef void* (*SERVICE_CMD_FUNCTION)(SERVICES_STRUCT* service, void* param);
struct _service_struct_{
    uint8_t id;
    uint8_t* attribute;
    //void* (*cmd_fun)(SERVICES_STRUCT* service, void* param);      //指向函数的指针
    //void* (*cmd_fun[])(SERVICES_STRUCT* service, void* param);    //（指向函数的指针）组成的数组
    //void* (*(*cmd_fun[]))(SERVICES_STRUCT* service, void* param);   //指向【（指向函数的指针）组成的数组]】的指针
    SERVICE_CMD_FUNCTION* cmd_fun;
};

SERVICES_STRUCT* srtnet_app_service_new(uint8_t service_id, uint8_t* service_attribute,
                                        SERVICE_CMD_FUNCTION* service_cmd_fun);
SERVICES_STRUCT* srtnet_app_service_num_index(uint8_t service_num);
uint8_t srtnet_app_service_struct_index(SERVICES_STRUCT* service);
void srtnet_app_service_id_to_num(uint8_t service_id,
                                  uint8_t* service_num_list, uint8_t* service_num_list_len);
*/

