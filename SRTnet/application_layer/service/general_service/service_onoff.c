#include "jxos_public.h"
#include <srtnet_config.h>
#include "../../application_layer_service.h"
#include "../../service_define.h"

#if (SRTNET_COMM_DEVICE_TYPE == SRTNET_COMM_DEVICE_TYPE_TRANSMITTER)\
    ||(SRTNET_COMM_DEVICE_TYPE == SRTNET_COMM_DEVICE_TYPE_REPEATER)
#if (SERVICE_ONOFF_MASTER_ENABLE == 1)
void srtnet_app_layer_service_id_onoff_send_cmd_on(uint8_t master_service_serila_mun)
{
	uint8_t param = 0;
#if (SRTNET_APP_LAYER_DEBUG_PRINT_ENABLE == 1)
	sys_debug_print_task_user_print_str("srtapp onoff on\r\n");
#endif
    srtnet_app_layer_service_master_send_cmd(false, master_service_serila_mun,
									SERVICE_ONOFF_ID,
									SERVICE_ONOFF_CMD_ON,
									&param, 0);
}

void srtnet_app_layer_service_id_onoff_send_cmd_off(uint8_t master_service_serila_mun)
{
	uint8_t param = 0;
#if (SRTNET_APP_LAYER_DEBUG_PRINT_ENABLE == 1)
	sys_debug_print_task_user_print_str("srtapp onoff off\r\n");
#endif
    srtnet_app_layer_service_master_send_cmd(false, master_service_serila_mun,
									SERVICE_ONOFF_ID,
									SERVICE_ONOFF_CMD_OFF,
									&param, 0);
}

void srtnet_app_layer_service_id_onoff_send_cmd_toggle(uint8_t master_service_serila_mun)
{
	uint8_t param = 0;
#if (SRTNET_APP_LAYER_DEBUG_PRINT_ENABLE == 1)
	sys_debug_print_task_user_print_str("srtapp onoff toggle\r\n");
#endif
    srtnet_app_layer_service_master_send_cmd(false, master_service_serila_mun,
									SERVICE_ONOFF_ID,
									SERVICE_ONOFF_CMD_TOGGLE,
									&param, 0);
}
#endif
#endif

#if (SRTNET_COMM_DEVICE_TYPE == SRTNET_COMM_DEVICE_TYPE_RECEIVER)\
    ||(SRTNET_COMM_DEVICE_TYPE == SRTNET_COMM_DEVICE_TYPE_REPEATER)
#if (SERVICE_ONOFF_SLAVE_ENABLE == 1)
void (*srtnet_app_layer_service_id_onoff_rec_cmd_on_callback)(uint8_t serila_mun) = 0;
void (*srtnet_app_layer_service_id_onoff_rec_cmd_off_callback)(uint8_t serila_mun) = 0;
void (*srtnet_app_layer_service_id_onoff_rec_cmd_toggle_callback)(uint8_t serila_mun) = 0;
void service_onoff_rec_cmd_on_handler(uint8_t slave_service_serila_mun)
{
#if (SRTNET_APP_LAYER_DEBUG_PRINT_ENABLE == 1)
	sys_debug_print_task_user_print_str("srtapp onoff rec on\r\n");
#endif
	if(srtnet_app_layer_service_id_onoff_rec_cmd_on_callback != 0){
		srtnet_app_layer_service_id_onoff_rec_cmd_on_callback(slave_service_serila_mun);
	}
}
void service_onoff_rec_cmd_off_handler(uint8_t slave_service_serila_mun)
{
#if (SRTNET_APP_LAYER_DEBUG_PRINT_ENABLE == 1)
	sys_debug_print_task_user_print_str("srtapp onoff rec off\r\n");
#endif
	if(srtnet_app_layer_service_id_onoff_rec_cmd_off_callback != 0){
		srtnet_app_layer_service_id_onoff_rec_cmd_off_callback(slave_service_serila_mun);
	}
}
void service_onoff_rec_cmd_toggle_handler(uint8_t slave_service_serila_mun)
{
#if (SRTNET_APP_LAYER_DEBUG_PRINT_ENABLE == 1)
	sys_debug_print_task_user_print_str("srtapp onoff rec toggle\r\n");
#endif
	if(srtnet_app_layer_service_id_onoff_rec_cmd_toggle_callback != 0){
		srtnet_app_layer_service_id_onoff_rec_cmd_toggle_callback(slave_service_serila_mun);
	}
}
#endif
#endif
