#ifndef __SRTNET_SERCICE_DEFINE_H
#define __SRTNET_SERCICE_DEFINE_H

/************************************************************************/
/********************SERVICE COMMON**************************************/
/************************************************************************/
#define SERVICE_CMD_PARAME_HEARD_LEN			2UL
#define SERVICE_CMD_PARAME_LEN_MAX				7UL
enum
{
	SERVICE_MASTER,
	SERVICE_SLAVE,
};

//defualt service
#define DEFAULT_SERVICE_SERILA_NUM_START		0x08UL
#define SERVICE_SERILA_NUM_MAX					0x0FUL
enum
{
	DEFAULT_SERVICE_BUNDING_SERILA_NUM = DEFAULT_SERVICE_SERILA_NUM_START,
	DEFAULT_SERVICE_1_SERILA_NUM,
	DEFAULT_SERVICE_2_SERILA_NUM,
	//...
	//0x0F	SERVICE_SERILA_NUM_MAX
};
/************************************************************************/


/************************************************************************/
/********************SERVICE ID CMD DEFINE*******************************/
/************************************************************************/
//general service id: 0x00 ~ 0x0f
#define SERVICE_ONOFF_ID				0
enum
{
	SERVICE_ONOFF_CMD_ON = 0,
	SERVICE_ONOFF_CMD_OFF,
	SERVICE_ONOFF_CMD_TOGGLE,
};

/************************************************************************/
#define SERVICE_TEMPERATURE_ID			1
enum
{
	SERVICE_TEMPERATURE_CMD_REPORTVAL = 0,
};

/************************************************************************/
//general service id: 0x10 ~ 0x1f
#define SERVICE_BUNDING_ID				0x10
enum
{
	SERVICE_BUNDING_CMD_BUND = 0,
	SERVICE_BUNDING_CMD_UNBUND,
};

/************************************************************************/


#endif
