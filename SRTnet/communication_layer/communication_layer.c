

/***************************************
Service primitives
N layer						N+1 layer
-----------------------------
	<-----------request			//N+1 req to N
	confirm----------->
	...................
	...................
	indication-------->
	<----------response
-----------------------------
***************************************/

#include "bsp/bsp_rf_driver.h"
#include "jxos_public.h"
#include <srtnet_config.h>
#include "transmitter.h"
#include "receiver.h"
#include "repeater.h"

void (*srtnet_comm_layer_task_hal_init_callback)(void) = 0;

static void srtnet_comm_task(uint8_t task_id, void * parameter)
{
#if (SRTNET_COMM_DEVICE_TYPE == SRTNET_COMM_DEVICE_TYPE_RECEIVER)\
    ||(SRTNET_COMM_DEVICE_TYPE == SRTNET_COMM_DEVICE_TYPE_REPEATER)
    srtnet_comm_layer_receiver_device_handler();
#endif

#if (SRTNET_COMM_DEVICE_TYPE == SRTNET_COMM_DEVICE_TYPE_REPEATER)
    srtnet_comm_layer_repeater_device_handler();
#endif

#if (SRTNET_COMM_DEVICE_TYPE == SRTNET_COMM_DEVICE_TYPE_TRANSMITTER)\
    ||(SRTNET_COMM_DEVICE_TYPE == SRTNET_COMM_DEVICE_TYPE_REPEATER)
    srtnet_comm_layer_transmitter_device_handler();
#endif
}

void srtnet_comm_layer_tick_handler(void)
{
#if (SRTNET_COMM_DEVICE_TYPE == SRTNET_COMM_DEVICE_TYPE_TRANSMITTER)\
    ||(SRTNET_COMM_DEVICE_TYPE == SRTNET_COMM_DEVICE_TYPE_REPEATER)
	if(srtnet_comm_layer_transmitter_device_enable_flag == 1){
		bsp_rf_send_tick_handler();
	}
#endif
#if (SRTNET_COMM_DEVICE_TYPE == SRTNET_COMM_DEVICE_TYPE_RECEIVER)\
    ||(SRTNET_COMM_DEVICE_TYPE == SRTNET_COMM_DEVICE_TYPE_REPEATER)
#if (SRTNET_COMM_DEVICE_TYPE_RECEIVER_IDLE_MODE_ENBALE == 1)
	if(srtnet_comm_layer_receiver_device_enable_flag == 1){
		bsp_rf_receive_tick_handler();	
	}
#else
	bsp_rf_receive_tick_handler();	
#endif
#endif
}

void srtnet_comm_layer_task_init(void)
{
    //hal
	if(srtnet_comm_layer_task_hal_init_callback != 0){
		srtnet_comm_layer_task_hal_init_callback();
	}

    //os
    jxos_task_create(srtnet_comm_task, "srtnet_comm", 0);
#if (SRTNET_COMM_DEVICE_TYPE == SRTNET_COMM_DEVICE_TYPE_TRANSMITTER)\
    ||(SRTNET_COMM_DEVICE_TYPE == SRTNET_COMM_DEVICE_TYPE_REPEATER)
    srtnet_comm_layer_transmitter_device_init();
#endif
#if (SRTNET_COMM_DEVICE_TYPE == SRTNET_COMM_DEVICE_TYPE_RECEIVER)\
    ||(SRTNET_COMM_DEVICE_TYPE == SRTNET_COMM_DEVICE_TYPE_REPEATER)
    srtnet_comm_layer_receiver_device_init();
#endif

}


