
/************************************************************
帧结构:
帧头1byte

源地址
2byte

载荷长度4bit + 帧序号4bit
0-15		0-15

载荷

校验1byte
前面所有字节
************************************************************/


#include <string.h>
#include "lib/type.h"
#include "jxos_public.h"
#include <srtnet_config.h>
#include "communication_layer_frame.h"

static uint8_t local_frame_head = 0;

uint8_t srtnet_comm_layer_request_local_frame_head_read(void)
{
    return local_frame_head;
}

void srtnet_comm_layer_request_local_frame_head_write(uint8_t frame_head_new)
{
    if(frame_head_new != FRAME_HEAD_BROADCAST){
        local_frame_head = frame_head_new;
    }
}

static uint8_t XOR_Check_Sum(uint8_t *dat,uint8_t length)
{
	uint8_t result;
	result = 0;
	for(; length > 0; length--)
	{
		result = result ^ *dat;
		dat++;
	}
	return result;
}

uint8_t deframe(uint8_t* frame, uint8_t frame_len,
			uint8_t* frame_playload, uint8_t* frame_playload_len,
			uint16_t* source_node_addr)
{
//    if(frame == 0){
//        return 0;
//    }
//    if(frame_len == 0){
//        return 0;
//    }
//    if(frame_data == 0){
//        return 0;
//    }
//    if(frame_data_len == 0){
//        return 0;
//    }
//    if(pan_id == 0){
//        return 0;
//    }
//    if(frame_type == 0){
//        return 0;
//    }
//    if(source_node_addr == 0){
//        return 0;
//    }
//    if(target_node_addr == 0){
//        return 0;
//    }
//    if(frame_count == 0){
//        return 0;
//    }
//    if(frame_ask_ack == 0){
//        return 0;
//    }

	if((frame[0] != local_frame_head)&&
        (frame[0] != FRAME_HEAD_BROADCAST)){
		return 0;
	}
	*frame_playload_len = (frame[3]&0xf0)>>4;
	if(XOR_Check_Sum(frame, (*frame_playload_len)+4) != frame[(*frame_playload_len)+4]){
		return 0;
	}
	*source_node_addr = frame[1];
    *source_node_addr <<= 8;
	*source_node_addr |= frame[2];

//	*frame_count = (frame[3]&0x0f);

    *frame_playload_len = frame_len-5;

    memcpy(frame_playload, &frame[4], *frame_playload_len);

	return 1;
}

void framing(uint8_t* frame, uint8_t* frame_len,
			uint8_t* frame_playload, uint8_t frame_playload_len,
			uint16_t source_node_addr, uint8_t frame_count)
{
//	if(frame_data == 0){
//		return 0;
//	}
//	if(frame == 0){
//		return 0;
//	}
//	if(frame_len == 0){
//		return 0;
//	}

//#if (SRTNET_COMM_LAYER_DEBUG_PRINT_ENABLE == 1)
//	sys_debug_print_task_user_print_str("srtcomm frpl: ");
//	sys_debug_print_task_user_print_data_stream_in_hex(frame_playload, frame_playload_len);
//	sys_debug_print_task_user_print_str("\r\n");
//#endif

	memset(frame, 0, frame_playload_len+5);
    *frame_len = frame_playload_len+5;

	frame[0] = local_frame_head;

	frame[1] = (source_node_addr&0xff00)>>8;
	frame[2] = source_node_addr&0x00ff;

	frame[3] = (frame_playload_len<<4);
	frame[3] |= frame_count;

	memcpy(&(frame[4]), frame_playload, frame_playload_len);

	frame[frame_playload_len+4] = XOR_Check_Sum(frame, frame_playload_len+4);
}
