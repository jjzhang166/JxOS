
#ifndef _SRTNET_FRAME_H
#define _SRTNET_FRAME_H

#include "lib/type.h"

#define FRAME_HEAD_BROADCAST        0XFF

#define ADDR_MAX			        0XFFFF
#define ADDR_BROADCAST		        0XFFFF

#define FRAME_PAYLOAD_MAX_LEN	    0X0F
#define FRAME_COUNT_MAX			    0X0F

uint8_t deframe(uint8_t* frame, uint8_t frame_len,
			uint8_t* frame_playload, uint8_t* frame_playload_len,
			uint16_t* source_node_addr);

void framing(uint8_t* frame, uint8_t* frame_len,
			uint8_t* frame_playload, uint8_t frame_playload_len,
			uint16_t source_node_addr, uint8_t frame_count);

#endif
