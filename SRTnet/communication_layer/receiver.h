#ifndef _COMMUNICATION_RECEIVER_H
#define _COMMUNICATION_RECEIVER_H

#include "lib/type.h"

extern uint8_t srtnet_comm_layer_receiver_device_enable_flag;
extern uint8_t srtnet_comm_layer_receiver_device_receiver_ok_falg;
extern uint8_t srtnet_comm_layer_receiver_device_receiver_buffer[];
extern uint8_t srtnet_comm_layer_receiver_device_receiver_frame_len;

void srtnet_comm_layer_receiver_device_init(void);
void srtnet_comm_layer_receiver_device_handler(void);

typedef struct {
	uint16_t source_node_addr;
	uint8_t receive_playload_len;
	uint8_t* receive_playload;
} SRTNET_COMM_REC_DATA_STRUCT;
extern void (*srtnet_comm_layer_receiver_device_indication_data_receive)(SRTNET_COMM_REC_DATA_STRUCT* receive_data); 

#endif
