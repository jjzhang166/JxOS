
#include <string.h>

#include "jxos_public.h"
#include "bsp/bsp_rf_driver.h"
#include <srtnet_config.h>
#include "communication_layer_frame.h"
#include "transmitter.h"
#include "receiver.h"
#include "repeater.h"

#if (SRTNET_COMM_DEVICE_TYPE == SRTNET_COMM_DEVICE_TYPE_TRANSMITTER)\
    ||(SRTNET_COMM_DEVICE_TYPE == SRTNET_COMM_DEVICE_TYPE_REPEATER)

static JXOS_EVENT_HANDLE event_rf_send_ok;

static uint16_t local_addr = 0;
static uint8_t frame_count = 0;			//sequence number； serial number ; series number ; serial-number
static swtime_type swt_transmitter_send;
static uint8_t transmitter_buffer[FRAME_PAYLOAD_MAX_LEN+5] = {0};
static uint8_t transmitter_frame_len = 0;
static uint8_t transmitter_device_send_count = 0;
static uint8_t transmitter_device_send_finish_flag = 0;

uint8_t srtnet_comm_layer_transmitter_device_enable_flag = 0;

void (*srtnet_comm_layer_transmitter_device_confirm_data_send)(void) = 0;
void (*srtnet_comm_layer_transmitter_device_enable_callback)(void) = 0;
void (*srtnet_comm_layer_transmitter_device_disable_callback)(void) = 0;

uint16_t srtnet_comm_layer_transmitter_device_request_local_addr_read(void)
{
    return local_addr;
}

void srtnet_comm_layer_transmitter_device_request_local_addr_write(uint16_t addr_new)
{
    if(addr_new != ADDR_BROADCAST)
        local_addr = addr_new;
}

uint8_t srtnet_comm_layer_transmitter_device_request_data_send(uint8_t is_broadcast, uint8_t* send_playload, uint8_t send_playload_len)
{
    if(transmitter_device_send_count == 0){
        transmitter_device_send_count++;
		sys_software_timer_task_set_timer(swt_transmitter_send, 1);
        sys_software_timer_task_restart_timer(swt_transmitter_send);
        transmitter_device_send_finish_flag = 0;
        
        if(srtnet_comm_layer_transmitter_device_enable_flag == 0){}
            srtnet_comm_layer_transmitter_device_enable_flag = 1;
            if(srtnet_comm_layer_transmitter_device_enable_callback != 0){
                srtnet_comm_layer_transmitter_device_enable_callback();
            }
        }

		if(is_broadcast == false){
			framing(transmitter_buffer, &transmitter_frame_len, send_playload, send_playload_len, local_addr, frame_count);
		}
		else{
			framing(transmitter_buffer, &transmitter_frame_len, send_playload, send_playload_len, ADDR_BROADCAST, frame_count);
		}

        frame_count++;
        if(frame_count > FRAME_COUNT_MAX){
            frame_count = 0;
        }
        return 1;
    }
    else{
        return 0;
    }
}

static void srtnet_comm_rf_send_ok_callback_handler(void)
{
    jxos_event_set(event_rf_send_ok);
}

void srtnet_comm_layer_transmitter_device_init(void)
{
    frame_count = 0;

    srtnet_comm_layer_transmitter_device_confirm_data_send = 0;
    srtnet_comm_layer_transmitter_device_enable_callback = 0;
    srtnet_comm_layer_transmitter_device_disable_callback = 0;

    memset(transmitter_buffer, 0, FRAME_PAYLOAD_MAX_LEN+5);
    transmitter_frame_len = 0;

    transmitter_device_send_count = 0;
	transmitter_device_send_finish_flag = 0;

	srtnet_comm_layer_transmitter_device_enable_flag = 0;
	if(srtnet_comm_layer_transmitter_device_disable_callback != 0){
		srtnet_comm_layer_transmitter_device_disable_callback();
	}

    event_rf_send_ok = jxos_event_create();
    swt_transmitter_send = sys_software_timer_task_new_timer();
    
    bsp_rf_send_init();
    bsp_rf_send_ok_call_back = srtnet_comm_rf_send_ok_callback_handler;
}

void srtnet_comm_layer_transmitter_device_handler(void)
{
    if(sys_software_timer_task_check_overtime_timer(swt_transmitter_send) == 1){
        sys_software_timer_task_stop_timer(swt_transmitter_send);
        if(transmitter_device_send_count < SRTNET_COMM_TRANSMITTER_SEND_REPEAT_MAX+1){
            if(bsp_rf_send_busy_check() == 0){
                bsp_rf_send(transmitter_buffer, transmitter_frame_len);
                transmitter_device_send_count++;
				if(transmitter_device_send_count == SRTNET_COMM_TRANSMITTER_SEND_REPEAT_MAX+1){
					transmitter_device_send_finish_flag = 1;
				}
#if (SRTNET_COMM_LAYER_DEBUG_PRINT_ENABLE == 1)
				sys_debug_print_task_user_print_str("srtcomm rfsend: ");
                sys_debug_print_task_user_print_data_stream_in_hex(transmitter_buffer, transmitter_frame_len);
				sys_debug_print_task_user_print_str("\r\n");
#endif
            }
            else{
//             sys_software_timer_task_set_timer(swt_transmitter_send, rand());   //随机延时(单帧时间*n)
                sys_software_timer_task_set_timer(swt_transmitter_send, SRTNET_COMM_TRANSMITTER_SEND_REPEAT_INTERVAL_TIME_MS);
                sys_software_timer_task_restart_timer(swt_transmitter_send);
#if (SRTNET_COMM_LAYER_DEBUG_PRINT_ENABLE == 1)
				sys_debug_print_task_user_print_str("srtcomm busy\r\n");
#endif
            }
        }
        else{
			transmitter_device_send_count = 0;
            if(srtnet_comm_layer_transmitter_device_confirm_data_send != 0){
                srtnet_comm_layer_transmitter_device_confirm_data_send();
            }
        }
    }

    if(jxos_event_wait(event_rf_send_ok) == 1){
//      sys_software_timer_task_set_timer(swt_transmitter_send, rand());   //随机延时(单帧时间*n)
        sys_software_timer_task_set_timer(swt_transmitter_send, SRTNET_COMM_TRANSMITTER_SEND_REPEAT_INTERVAL_TIME_MS);
        sys_software_timer_task_restart_timer(swt_transmitter_send);
		if(transmitter_device_send_finish_flag == 1){
			transmitter_device_send_finish_flag = 0;
			srtnet_comm_layer_transmitter_device_enable_flag = 0;
			if(srtnet_comm_layer_transmitter_device_disable_callback != 0){
				srtnet_comm_layer_transmitter_device_disable_callback();
			}
		}
    }
}

#endif
