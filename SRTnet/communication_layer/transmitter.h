
#ifndef _COMMUNICATION_TRANSMITTER_H
#define _COMMUNICATION_TRANSMITTER_H

#include "lib/type.h"

extern uint8_t srtnet_comm_layer_transmitter_device_enable_flag;

uint16_t srtnet_comm_layer_transmitter_device_request_local_addr_read(void);
void srtnet_comm_layer_transmitter_device_request_local_addr_write(uint16_t addr_new);
uint8_t srtnet_comm_layer_transmitter_device_request_data_send(uint8_t is_broadcast, uint8_t* send_playload, uint8_t send_playload_len);

void srtnet_comm_layer_transmitter_device_init(void);
void srtnet_comm_layer_transmitter_device_handler(void);

extern void (*srtnet_comm_layer_transmitter_device_confirm_data_send)(void);

#endif
