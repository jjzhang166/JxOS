#ifndef _SRTNET_PUBLIC_H
#define _SRTNET_PUBLIC_H

#include "lib/type.h"
#include <srtnet_config.h>

/****************************************************
the comm layer function is based on timer interrupt, 
so we should care about the other interrupts 
inerference whit the comm layer timer interrupt
*****************************************************/

/**********************************************************************/
//comm_layer
void srtnet_comm_layer_task_init(void);
void srtnet_comm_layer_tick_handler(void);
extern void (*srtnet_comm_layer_task_hal_init_callback)(void);

uint8_t srtnet_comm_layer_request_local_frame_head_read(void);
void srtnet_comm_layer_request_local_frame_head_write(uint8_t frame_head_new);

#if (SRTNET_COMM_DEVICE_TYPE == SRTNET_COMM_DEVICE_TYPE_TRANSMITTER)\
    ||(SRTNET_COMM_DEVICE_TYPE == SRTNET_COMM_DEVICE_TYPE_REPEATER)
uint16_t srtnet_comm_layer_transmitter_device_request_local_addr_read(void);
void srtnet_comm_layer_transmitter_device_request_local_addr_write(uint16_t addr_new);
uint8_t srtnet_comm_layer_transmitter_device_request_data_send(uint8_t broadcast, uint8_t* send_playload, uint8_t send_playload_len);
extern void (*srtnet_comm_layer_transmitter_device_confirm_data_send)(void);
extern void (*srtnet_comm_layer_transmitter_device_enable_callback)(void);
extern void (*srtnet_comm_layer_transmitter_device_disable_callback)(void);
#endif

#if (SRTNET_COMM_DEVICE_TYPE == SRTNET_COMM_DEVICE_TYPE_RECEIVER)\
    ||(SRTNET_COMM_DEVICE_TYPE == SRTNET_COMM_DEVICE_TYPE_REPEATER)
extern uint8_t srtnet_comm_layer_receiver_device_receiver_ok_falg;
extern uint8_t srtnet_comm_layer_receiver_device_receiver_buffer[];
extern uint8_t srtnet_comm_layer_receiver_device_receiver_frame_len;
void srtnet_comm_layer_receiver_device_keep_enable(uint8_t keep_enable);
typedef struct {
	uint16_t source_node_addr;
	uint8_t receive_playload_len;
	uint8_t* receive_playload;
} SRTNET_COMM_REC_DATA_STRUCT;
extern void (*srtnet_comm_layer_receiver_device_indication_data_receive)(SRTNET_COMM_REC_DATA_STRUCT* receive_data);
extern void (*srtnet_comm_layer_receiver_device_enable_callback)(void);
extern void (*srtnet_comm_layer_receiver_device_disable_callback)(void);
#endif

/**********************************************************************/
//app_layer
void srtnet_app_layer_task_init(void);

extern uint8_t SERVICE_CONFIG_TABLE_LEN;
uint8_t srtnet_app_layer_service_set_active_service(uint8_t serila_mun);
void srtnet_app_layer_service_no_active_service(void);

void srtnet_app_layer_binding_table_clear(uint8_t slave_service_serila_mun);
uint8_t srtnet_app_layer_binding_table_is_empty(void);
void srtnet_app_layer_binding_table_print(void);
uint8_t srtnet_app_layer_binding_table_store(void);
extern uint8_t (*srtnet_app_layer_binding_table_store_callback)(uint8_t* binding_table, uint8_t binding_table_len);
extern uint8_t (*srtnet_app_layer_binding_table_recover_callback)(uint8_t* binding_table, uint8_t binding_table_len);

//comm -> ADDR_BROADCAST -> default service
//comm -> other addr -> user service
//service_id_onoff
void srtnet_app_layer_service_id_onoff_send_cmd_on(uint8_t master_service_serila_mun);
void srtnet_app_layer_service_id_onoff_send_cmd_off(uint8_t master_service_serila_mun);
void srtnet_app_layer_service_id_onoff_send_cmd_toggle(uint8_t master_service_serila_mun);
extern void (*srtnet_app_layer_service_id_onoff_rec_cmd_on_callback)(uint8_t serila_mun);
extern void (*srtnet_app_layer_service_id_onoff_rec_cmd_off_callback)(uint8_t serila_mun);
extern void (*srtnet_app_layer_service_id_onoff_rec_cmd_toggle_callback)(uint8_t serila_mun);

//service_id_binding
void srtnet_app_layer_service_id_binding_send_cmd_bind(uint8_t need_bind_service_serila_mun);
void srtnet_app_layer_service_id_binding_send_cmd_unbind(uint8_t need_bind_service_serila_mun);
typedef struct {
	uint16_t master_addr;
	uint8_t master_service_serila_mun;
	uint8_t master_service_id;
	uint8_t slave_service_serila_mun;
} SRTNET_BINDING_STRUCT;
extern void (*srtnet_app_layer_service_id_binding_rec_cmd_bind_callback)(SRTNET_BINDING_STRUCT* param);
extern void (*srtnet_app_layer_service_id_binding_rec_cmd_unbind_callback)(SRTNET_BINDING_STRUCT* param);

#endif
