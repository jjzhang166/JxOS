/*
 * jxos_printf.h
 *
 *  Created on: 2015��9��1��
 *      Author: ASUS
 */

#ifndef _MY_PRINTF_H_
#define _MY_PRINTF_H_

#include "jxos_stdint.h"

void printf_putc_callback_regist(int (*putc)(int ch));

//void    _trace(const u8* pstr);
//void    _hexstrTrace(u8* pstr, u8* pnum, u16 size);
//void    _my_printf(const s8* str, ...);

void printf_string(s8 * str);
void printf_char(s8 c);
void printf_byte_hex(u8 a);
void printf_bytes_hex(u8 * str, u32 n);
void printf_byte_decimal(s8 a);
void printf_16bit_hex(u16 aHalfWord);
void printf_32bit_hex(u32 aWord);

#endif /* _MY_PRINTF_H_ */
