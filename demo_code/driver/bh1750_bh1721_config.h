/**
 *
 * Leedarson
 * All Rights Reserved
 *
 * @file mod_light_bh1750.h
 *
 * @brief Header file for mod_light_bh1750.c.
 *
 *
 * Author: xuexiaofei 
 *
 * Last Changed By: $Author: xuexiaofei $
 * Revision: $Revision: 1.00 $
 * Last Changed: $Date: 2017/03/30 10:18:01 $
 *
 */

#ifndef __MOD_LIGHT_BH1750_H
#define __MOD_LIGHT_BH1750_H

#include "stdint.h"

void bh1750_hal_init(void);
uint8_t	bh1750_hal_i2c_write_device(uint8_t addr, uint8_t *data, uint8_t len);
uint8_t	bh1750_hal_i2c_read_device(uint8_t addr, uint8_t *data, uint8_t len);

#endif

