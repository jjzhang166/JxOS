
#include "cc1101_config.h"

/***********************************************************
CC1101的寄存器的地址有效值为 BIT0 ~ BIT5。

BIT7 为读写控制位，为1，表示读；为0，表示写。（最高位）
BIT6 为是否连续操作（突发访问）， 为1，表示连续读写；为0，表示单个读写。

CC1101的寄存器有 命令/状态寄存器、 控制（配置）寄存器 两种类型。

特别说明 命令/状态寄存器 的操作方式如下：
当读（BIT7 = 1） 命令/状态寄存器 地址时，即为读状态；
当写（BIT7 = 0） 命令/状态寄存器 地址时，即为写命令。

如发送0x30表示写复位命令，发送0XF0（0X30 bit7为1，表示读，
bit6为1，表示连续）则表示读 PARTNUM 状态寄存器
似乎读状态寄存器只能连续读？（bit6必须为1？）
***********************************************************/
/***********************************************************
//关于地址滤波设置与FIFO数据和有效数据长度问题

if( ( GS_CC1101ReadReg( CC1101_PKTCTRL1 ) & ~0x03 ) != 0 )	//开启地址滤波
{
	CC1101WriteReg( CC1101_TXFIFO, size + 1 );	//注意有效数据长度值
	CC1101WriteReg( CC1101_TXFIFO, address );
}
else														//未开启地址滤波
{
	CC1101WriteReg( CC1101_TXFIFO, size );		//注意有效数据长度值
}
CC1101WriteMultiReg( CC1101_TXFIFO, txbuffer, size );

***********************************************************/

/*********************************************/
//控制寄存器地址
#define CC1101_IOCFG2       0x00        // GDO2 output pin configuration
#define CC1101_IOCFG1       0x01        // GDO1 output pin configuration
#define CC1101_IOCFG0       0x02        // GDO0 output pin configuration
#define CC1101_FIFOTHR      0x03        // RX FIFO and TX FIFO thresholds
#define CC1101_SYNC1        0x04        // Sync word, high uint8_t
#define CC1101_SYNC0        0x05        // Sync word, low uint8_t
#define CC1101_PKTLEN       0x06        // Packet length
#define CC1101_PKTCTRL1     0x07        // Packet automation control
#define CC1101_PKTCTRL0     0x08        // Packet automation control
#define CC1101_ADDR         0x09        // Device address
#define CC1101_CHANNR       0x0A        // Channel number
#define CC1101_FSCTRL1      0x0B        // Frequency synthesizer control
#define CC1101_FSCTRL0      0x0C        // Frequency synthesizer control
#define CC1101_FREQ2        0x0D        // Frequency control word, high uint8_t
#define CC1101_FREQ1        0x0E        // Frequency control word, middle uint8_t
#define CC1101_FREQ0        0x0F        // Frequency control word, low uint8_t
#define CC1101_MDMCFG4      0x10        // Modem configuration
#define CC1101_MDMCFG3      0x11        // Modem configuration
#define CC1101_MDMCFG2      0x12        // Modem configuration
#define CC1101_MDMCFG1      0x13        // Modem configuration
#define CC1101_MDMCFG0      0x14        // Modem configuration
#define CC1101_DEVIATN      0x15        // Modem deviation setting
#define CC1101_MCSM2        0x16        // Main Radio Control State Machine configuration
#define CC1101_MCSM1        0x17        // Main Radio Control State Machine configuration
#define CC1101_MCSM0        0x18        // Main Radio Control State Machine configuration
#define CC1101_FOCCFG       0x19        // Frequency Offset Compensation configuration
#define CC1101_BSCFG        0x1A        // Bit Synchronization configuration
#define CC1101_AGCCTRL2     0x1B        // AGC control
#define CC1101_AGCCTRL1     0x1C        // AGC control
#define CC1101_AGCCTRL0     0x1D        // AGC control
#define CC1101_WOREVT1      0x1E        // High uint8_t Event 0 timeout
#define CC1101_WOREVT0      0x1F        // Low uint8_t Event 0 timeout
#define CC1101_WORCTRL      0x20        // Wake On Radio control
#define CC1101_FREND1       0x21        // Front end RX configuration
#define CC1101_FREND0       0x22        // Front end TX configuration
#define CC1101_FSCAL3       0x23        // Frequency synthesizer calibration
#define CC1101_FSCAL2       0x24        // Frequency synthesizer calibration
#define CC1101_FSCAL1       0x25        // Frequency synthesizer calibration
#define CC1101_FSCAL0       0x26        // Frequency synthesizer calibration
#define CC1101_RCCTRL1      0x27        // RC oscillator configuration
#define CC1101_RCCTRL0      0x28        // RC oscillator configuration
#define CC1101_FSTEST       0x29        // Frequency synthesizer calibration control
#define CC1101_PTEST        0x2A        // Production test
#define CC1101_AGCTEST      0x2B        // AGC test
#define CC1101_TEST2        0x2C        // Various test settings
#define CC1101_TEST1        0x2D        // Various test settings
#define CC1101_TEST0        0x2E        // Various test settings
/*********************************************/
/*********************************************/
//状态寄存器地址
#define CC1101_PARTNUM      0x30
#define CC1101_VERSION      0x31
#define CC1101_FREQEST      0x32
#define CC1101_LQI          0x33
#define CC1101_RSSI         0x34
#define CC1101_MARCSTATE    0x35
#define CC1101_WORTIME1     0x36
#define CC1101_WORTIME0     0x37
#define CC1101_PKTSTATUS    0x38
#define CC1101_VCO_VC_DAC   0x39
#define CC1101_TXBYTES      0x3A
#define CC1101_RXBYTES      0x3B

#define CC1101_PATABLE      0x3E
#define CC1101_TXFIFO       0x3F
#define CC1101_RXFIFO       0x3F

#define NUM_RXBYTES		    0x7F  						//接收缓冲区的有效字节数
#define CRC_OK              0x80 						//CRC校验通过位标志
/*********************************************/
/*********************************************/
//命令
#define CC1101_SRES         0x30        // Reset chip.
#define CC1101_SFSTXON      0x31        // Enable and calibrate frequency synthesizer (if MCSM0.FS_AUTOCAL=1).
// If in RX/TX: Go to a wait state where only the synthesizer is
// running (for quick RX / TX turnaround).
#define CC1101_SXOFF        0x32        // Turn off crystal oscillator.
#define CC1101_SCAL         0x33        // Calibrate frequency synthesizer and turn it off
// (enables quick start).
#define CC1101_SRX          0x34        // Enable RX. Perform calibration first if coming from IDLE and
// MCSM0.FS_AUTOCAL=1.
#define CC1101_STX          0x35        // In IDLE state: Enable TX. Perform calibration first if
// MCSM0.FS_AUTOCAL=1. If in RX state and CCA is enabled:
// Only go to TX if channel is clear.
#define CC1101_SIDLE        0x36        // Exit RX / TX, turn off frequency synthesizer and exit
// Wake-On-Radio mode if applicable.
#define CC1101_SAFC         0x37        // Perform AFC adjustment of the frequency synthesizer
#define CC1101_SWOR         0x38        // Start automatic RX polling sequence (Wake-on-Radio)
#define CC1101_SPWD         0x39        // Enter power down mode when CSn goes high.
#define CC1101_SFRX         0x3A        // Flush the RX FIFO buffer.
#define CC1101_SFTX         0x3B        // Flush the TX FIFO buffer.
#define CC1101_SWORRST      0x3C        // Reset real time clock.
#define CC1101_SNOP         0x3D        // No operation. May be used to pad strobe commands to two
// uint8_ts for simpler software.
/*********************************************/
/*********************************************/
//预设控制寄存器参
// Sync word qualifier mode = 30/32 sync word bits detected
// CRC autoflush = false
// Channel spacing = 199.951172
// Data format = Normal mode
// Data rate = 249.939
// RX filter BW = 541.666667
// PA ramping = false
// Preamble count = 4
// Whitening = false
// Address config = enable 0xff & 0x00
// Carrier frequency = 432.999817
// Device address = 0
// TX power = 10 ?
// Manchester enable = false
// CRC enable = true
// Deviation = 126.953125
// Packet length mode = Variable packet length mode. Packet length configured by the first byte after sync word
// Packet length = 255
// Modulation format = GFSK
// Base frequency = 432.999817
// Modulated = true
// Channel number = 0
// PA table
static const uint8_t CC1101CtrlRegInit[][2] = {
	{CC1101_FIFOTHR,    	0x07},		//0x47 ?
	{CC1101_SYNC1,			0xD3},		//同步字
	{CC1101_SYNC0,			0x91},		//同步字
	{CC1101_PKTCTRL1,		0x07},		//地址滤波使能
	{CC1101_PKTCTRL0,   	0x05},		//可变数据包长度（FIFO.Length决定)
	{CC1101_CHANNR,     	0x00},
	{CC1101_FSCTRL1,    	0x0C},	//？？
	{CC1101_FREQ2,      	0x10},	//基频
	{CC1101_FREQ1,      	0xA7},	//基频
	{CC1101_FREQ0,      	0x62},	//基频
	{CC1101_MDMCFG4,    	0x2D},	//速率
	{CC1101_MDMCFG3,    	0x3B},	//速率
	{CC1101_MDMCFG2,    	0x13},	//速率	//??93//同步字重复
	{CC1101_MDMCFG1,    	0x22},	//前导长度
	{CC1101_DEVIATN,    	0x62},
	{CC1101_MCSM1,      	0x30},
	{CC1101_MCSM0,      	0x08},	//校准模式
	{CC1101_FOCCFG,     	0x1D},
	{CC1101_WORCTRL,    	0xFB},
	{CC1101_FSCAL3,     	0xEA},
	{CC1101_FSCAL2,     	0x2A},
	{CC1101_FSCAL1,     	0x00},
	{CC1101_FSCAL0,     	0x1F},
	{CC1101_TEST2,      	0x81},
	{CC1101_TEST1,      	0x35},
	
	{CC1101_IOCFG0,     	0x46},	//写前面某个寄存器会影响这个寄存器的值，所以放在最后写
};
/**
static const registerSetting_t preferredSettings[] = {
	{CC1101_IOCFG2,        	0x29},
	{CC1101_IOCFG1,        	0x2E},
	{CC1101_IOCFG0,        	0x06},
	{CC1101_FIFOTHR,       	0x07},
	{CC1101_SYNC1,         	0xD3},
	{CC1101_SYNC0,         	0x91},
	{CC1101_PKTLEN,        	0xFF},
	{CC1101_PKTCTRL1,      	0x04},
	{CC1101_PKTCTRL0,      	0x05},
	{CC1101_ADDR,          	0x00},
	{CC1101_CHANNR,        	0x00},
	{CC1101_FSCTRL1,       	0x0C},
	{CC1101_FSCTRL0,       	0x00},
	{CC1101_FREQ2,         	0x10},
	{CC1101_FREQ1,         	0xA7},
	{CC1101_FREQ0,         	0x62},
	{CC1101_MDMCFG4,       	0x2D},
	{CC1101_MDMCFG3,       	0x3B},
	{CC1101_MDMCFG2,       	0x13},
	{CC1101_MDMCFG1,       	0x22},
	{CC1101_MDMCFG0,       	0xF8},
	{CC1101_DEVIATN,       	0x62},
	{CC1101_MCSM2,         	0x07},
	{CC1101_MCSM1,         	0x30},
	{CC1101_MCSM0,         	0x18},
	{CC1101_FOCCFG,        	0x1D},
	{CC1101_BSCFG,         	0x1C},
	{CC1101_AGCCTRL2,      	0xC7},
	{CC1101_AGCCTRL1,      	0x00},
	{CC1101_AGCCTRL0,      	0xB0},
	{CC1101_WOREVT1,       	0x87},
	{CC1101_WOREVT0,       	0x6B},
	{CC1101_WORCTRL,       	0xFB},
	{CC1101_FREND1,        	0xB6},
	{CC1101_FREND0,        	0x10},
	{CC1101_FSCAL3,        	0xEA},
	{CC1101_FSCAL2,        	0x2A},
	{CC1101_FSCAL1,        	0x00},
	{CC1101_FSCAL0,        	0x1F},
	{CC1101_RCCTRL1,       	0x41},
	{CC1101_RCCTRL0,       	0x00},
	{CC1101_FSTEST,        	0x59},
	{CC1101_PTEST,         	0x7F},
	{CC1101_AGCTEST,       	0x3F},
	{CC1101_TEST2,         	0x88},
	{CC1101_TEST1,         	0x31},
	{CC1101_TEST0,         	0x09},
	{CC1101_PARTNUM,       	0x00},
	{CC1101_VERSION,       	0x04},
	{CC1101_FREQEST,       	0x00},
	{CC1101_LQI,           	0x00},
	{CC1101_RSSI,          	0x00},
	{CC1101_MARCSTATE,     	0x00},
	{CC1101_WORTIME1,      	0x00},
	{CC1101_WORTIME0,      	0x00},
	{CC1101_PKTSTATUS,     	0x00},
	{CC1101_VCO_VC_DAC,    	0x00},
	{CC1101_TXBYTES,       	0x00},
	{CC1101_RXBYTES,       	0x00},
	{CC1101_RCCTRL1_STATUS,	0x00},
	{CC1101_RCCTRL0_STATUS,	0x00},
};
**/
/*********************************************/
//参数寄存器值
//10, 7, 5, 0, -5, -10, -15, -20, dbm output power, 0x12 == -30dbm
uint8_t PaTabel[] = { 0xc0, 0xC8, 0x84, 0x60, 0x68, 0x34, 0x1D, 0x0E};
#define PA_TABLE {0xc2,0x00,0x00,0x00,0x00,0x00,0x00,0x00,}
/*********************************************/

/*************************************************
//标准流程
uint8_t CC1101( uint8_t addr )
{
    uint8_t i, cc1101_chip_status_byte;
    cc1101_spi_cs_low( );

	cc1101_blocking_delay_us(x);		//等待芯片响应，等待时间<=1us 也可以通过判断cc1101的SO脚是否变低。当SO为低时，表示芯片已经准备好
    cc1101_chip_status_byte = cc1101_spi_transfer( addr | 0X80);	//0X80：读 当向CC1101发送第一个字节时，芯片将同时传出芯片状态字
	if((cc1101_chip_status_byte & 0x80) != 0x00){	//如果芯片状态字的最高位不为零，表示芯片没有准备好，传输失败
	  return 0;
	}

    i = cc1101_spi_transfer( 0xFF );
    cc1101_spi_cs_high( );
    return i;
}
*************************************************/

//读取单个控制寄存器的值
uint8_t CC1101ReadCtrlReg( uint8_t addr )
{
	uint8_t i;
	cc1101_spi_cs_low( );
	cc1101_spi_transfer( addr | 0X80);	//0X80 读
	i = cc1101_spi_transfer( 0xFF );
	cc1101_spi_cs_high( );
	return i;
}

//burst
//读取多个控制寄存器的值
void CC1101ReadMultiCtrlReg( uint8_t addr, uint8_t *buff, uint8_t size )
{
	uint8_t i;
	cc1101_spi_cs_low( );
	cc1101_spi_transfer( addr | 0X80 | 0X40);	//0X80：读；0X40:突发访问
	for( i = 0; i < size; i ++ ) {
		*( buff + i ) = cc1101_spi_transfer( 0xFF );
	}
	cc1101_spi_cs_high( );
}

void CC1101WriteCtrlReg( uint8_t addr, uint8_t value )
{
	cc1101_spi_cs_low( );
	cc1101_spi_transfer( addr );
	cc1101_spi_transfer( value );
	cc1101_spi_cs_high( );
}

void CC1101WriteMultiCtrlReg( uint8_t addr, uint8_t *buff, uint8_t size )
{
	uint8_t i;
	cc1101_spi_cs_low( );
	cc1101_spi_transfer(addr | 0X40);
	for( i = 0; i < size; i ++ ) {
		cc1101_spi_transfer( *( buff + i ) );
	}
	cc1101_spi_cs_high( );
}

//读取状态寄存器的值
uint8_t CC1101ReadStatusReg( uint8_t addr )
{
	uint8_t i;
	cc1101_spi_cs_low( );
	cc1101_spi_transfer( addr | 0X80 | 0X40);	//为什么要突发访问？
	i = cc1101_spi_transfer( 0xFF );
	cc1101_spi_cs_high( );
	return i;
}

//写命令
void CC1101WriteCmd( uint8_t command )
{
	cc1101_spi_cs_low( );
	cc1101_spi_transfer( command );
	cc1101_spi_cs_high( );
}

/**************************************************************************/
/*************************************************
???
void  GS_CC1101WORInit( void )
{
    CC1101WriteCtrlReg(CC1101_MCSM0,0x18);
    CC1101WriteCtrlReg(CC1101_WORCTRL,0x78); //Wake On Radio Control
    CC1101WriteCtrlReg(CC1101_MCSM2,0x00);
    CC1101WriteCtrlReg(CC1101_WOREVT1,0x8C);
    CC1101WriteCtrlReg(CC1101_WOREVT0,0xA0);

	CC1101WriteCmd( CC1101_SWORRST );
}
*************************************************/
//当开启地址滤波时，只有地址正确的包才会写入FIFO，错误的包将会丢弃(CC1101ReadStatusReg( CC1101_RXBYTES )  & NUM_RXBYTES == 0)
//当开启地址滤波时，只要收到正确的同步字，即使地址错误，仍会产生GDO下降沿。
//但是由于数据包被丢弃，所以很快将产生GDO上升沿(数据包结束)
//所以地址正确和错误的数据包产生的GDO 低方波宽度不同，正确的方波较宽，错误的方波较窄。
//可以考虑同步配置不同的同步字来代替地址滤波功能来与多设备通信，由于同步字不匹配将不会产生GDO方波。
void CC1101SetAddress(uint8_t address)
{
	CC1101WriteCtrlReg(CC1101_ADDR, address);
}

void CC1101SetChannel(uint8_t channel)
{
	CC1101WriteCtrlReg(CC1101_CHANNR, channel);
}

void C1101WriteTxFIFO(uint8_t data_len, uint8_t addr, uint8_t *data)
{
	/********************************************************

	uint8_t txfifo_buffer[24];
	txfifo_buffer[0] = (data_len + 1);
	txfifo_buffer[1] = addr;
	memcpy(&(txfifo_buffer[2]), data, data_len);
	CC1101WriteMultiCtrlReg(CC1101_TXFIFO, txfifo_buffer, data_len + 2);
	
	********************************************************/

	CC1101WriteCtrlReg(CC1101_TXFIFO, (data_len + 1));
	CC1101WriteCtrlReg(CC1101_TXFIFO, addr);
	CC1101WriteMultiCtrlReg(CC1101_TXFIFO, data, data_len);
	
	/********************************************************/
}

uint8_t C1101ReadRxFIFO(uint8_t *data_len, uint8_t *addr,\
		uint8_t *data, uint8_t *rssi, uint8_t *crc_ok_lqi)
{
	/********************************************************

	uint8_t ret = 0;
	uint8_t rxfifo_buffer[24];
	*data_len = (CC1101ReadStatusReg( CC1101_RXBYTES ) & NUM_RXBYTES);
	if(*data_len > 0) {
		CC1101ReadMultiCtrlReg(CC1101_RXFIFO, rxfifo_buffer, *data_len);
		*data_len = (rxfifo_buffer[0] - 1);		// *data_len  = (GS_CC1101GetRXCnt() - 4);
		*addr = rxfifo_buffer[1];
		*rssi = rxfifo_buffer[rxfifo_buffer[0] + 1];
		*crc_ok_lqi = rxfifo_buffer[rxfifo_buffer[0] + 2];
		memcpy(data, &(rxfifo_buffer[2]), *data_len);
		ret = 1;
	}
	return ret;
	
	********************************************************/
	uint8_t ret = 0;
	ret = (CC1101ReadStatusReg( CC1101_RXBYTES )  & NUM_RXBYTES);
	if(ret > 0) {
		*data_len = (CC1101ReadCtrlReg(CC1101_RXFIFO) - 1);
		*addr  = CC1101ReadCtrlReg(CC1101_RXFIFO);
		CC1101ReadMultiCtrlReg(CC1101_RXFIFO, data, *data_len);
		*rssi  = CC1101ReadCtrlReg(CC1101_RXFIFO);
		*crc_ok_lqi  = CC1101ReadCtrlReg(CC1101_RXFIFO);		
	}
	return ret;
	/********************************************************/
	
}


uint8_t CC1101Init( void )
{
	volatile uint8_t i;

	/******************************************/
	//复位
	cc1101_spi_cs_high( );
	cc1101_spi_cs_low( );
	cc1101_spi_cs_high( );
	cc1101_blocking_delay_us(255);		//等待？？
	CC1101WriteCmd( CC1101_SRES );
	/******************************************/

	/******************************************/
	//批量设置控制寄存器
	for( i = 0; i < (sizeof(CC1101CtrlRegInit)/2); i++ ) {
		CC1101WriteCtrlReg(CC1101CtrlRegInit[i][0], CC1101CtrlRegInit[i][1]);
	}
	/******************************************/

	/******************************************/
	//初始化为广播地址
//	CC1101WriteCtrlReg(CC1101_ADDR, 0x00);
	/******************************************/

	/******************************************/
	//配置常数寄存器
	CC1101WriteMultiCtrlReg(CC1101_PATABLE, PaTabel, 8 );
	/******************************************/

	/******************************************/
	//清除FIFO
	CC1101WriteCmd(CC1101_SIDLE);
	cc1101_blocking_delay_us(5);

	CC1101WriteCmd( CC1101_SFTX );
	CC1101WriteCmd( CC1101_SFRX );
	cc1101_blocking_delay_us(5);
	/******************************************/

	/******************************************/
	//校准
	CC1101WriteCmd( CC1101_SCAL );
	cc1101_blocking_delay_us(100);
	CC1101WriteCmd(CC1101_SIDLE);	//空闲
	//CC1101WriteCmd(CC1101_SPWD);	//休眠
	/******************************************/
	
	if((CC1101ReadStatusReg( CC1101_PARTNUM ) == 0X00)&&
		(CC1101ReadStatusReg( CC1101_VERSION ) ==  0x14)){
		i = 1;
	}
	else{
		i = 0;
	}

	return i;
}

