#ifndef CC1101_CONFIG
#define CC1101_CONFIG

#include "stm32f10x.h"
uint8_t SPI_ExchangeByte(uint8_t input);
void delay_ms(uint8_t n);

//�ӿ�
#define cc1101_spi_cs_low()    			GPIO_ResetBits( GPIOB, GPIO_Pin_12 )//while( GPIO_ReadInputDataBit( GPIOB, GPIO_Pin_7 ) != 0 );
#define cc1101_spi_cs_high()    		GPIO_SetBits( GPIOB, GPIO_Pin_12 )
#define cc1101_spi_transfer(n)    		SPI_ExchangeByte(n)
#define cc1101_blocking_delay_us(n) 	delay_ms(n)

#endif
