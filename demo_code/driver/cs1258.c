#include "stm32f10x.h"

void CS1258_CS_HIGH(void)
{
	GPIO_SetBits(GPIOB, GPIO_Pin_12);
}

void CS1258_CS_LOW(void)
{
	GPIO_ResetBits(GPIOB, GPIO_Pin_12);
}

void CS1258_CLK_HIGH(void)
{
	GPIO_SetBits(GPIOB, GPIO_Pin_13);
}

void CS1258_CLK_LOW(void)
{
	GPIO_ResetBits(GPIOB, GPIO_Pin_13);
}

void CS1258_OUT_HIGH(void)
{
	GPIO_SetBits(GPIOB, GPIO_Pin_14);
}

void CS1258_OUT_LOW(void)
{
	GPIO_ResetBits(GPIOB, GPIO_Pin_14);
}

uint8_t CS1258_IN(void)
{
	return 	GPIO_ReadInputDataBit(GPIOB, GPIO_Pin_14);
}

void CS1258_PIN_INIT(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE); 
  	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_12|GPIO_Pin_13;
  	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
  	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  	GPIO_Init(GPIOB, &GPIO_InitStructure); 

  	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_14;
  	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_OD;
  	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  	GPIO_Init(GPIOB, &GPIO_InitStructure); 
}

void CC1258_delay(volatile uint32_t nCount)
{
	volatile uint32_t i;
	for(; nCount != 0; nCount--);
}

void CS1258_write(uint8_t send_data)
{
    uint8_t i;

    for(i=0; i<8; i++){
		if((send_data & (0x80>>i)) != 0){
			CS1258_OUT_HIGH();
		}
		else{
			CS1258_OUT_LOW();
		}
		CC1258_delay(2);
		
		CS1258_CLK_HIGH();
		CC1258_delay(6);

		CS1258_CLK_LOW();
		CC1258_delay(4);
    }
	CC1258_delay(10);
}

uint8_t CS1258_read(void)
{
    uint8_t i;
    uint8_t read_data = 0;

	CS1258_OUT_HIGH();

    for(i=0; i<8; i++){
		CS1258_CLK_HIGH();
		CC1258_delay(6);

		read_data <<= 1;
		if(CS1258_IN() != 0){
			read_data |= 0x01;
		}

		CS1258_CLK_LOW();
		CC1258_delay(6);

    }
	CC1258_delay(10);
	return read_data;
}

void CC1258_init(void)
{
	uint8_t WByte_zwh;

	WByte_zwh=0x81;                //adc0??????
	CS1258_write(WByte_zwh);  
	WByte_zwh=0XC8;
	CS1258_write(WByte_zwh);  

	WByte_zwh=0x82;                //adc1??????            
	CS1258_write(WByte_zwh);  
	WByte_zwh=0X8A;
	CS1258_write(WByte_zwh);  

	WByte_zwh=0x83;               //adc2??????
	CS1258_write(WByte_zwh);   
	WByte_zwh=0X0;
	CS1258_write(WByte_zwh); 

	WByte_zwh=0x84;              //adc3??????
	CS1258_write(WByte_zwh);      
	WByte_zwh=0X0;
	CS1258_write(WByte_zwh);

	WByte_zwh=0x85;                //adc4??????
	CS1258_write(WByte_zwh);  
	WByte_zwh=0X40;
	CS1258_write(WByte_zwh);  

	WByte_zwh=0x86;                //adc5??????            
	CS1258_write(WByte_zwh);  
	WByte_zwh=0X0;
	CS1258_write(WByte_zwh);  

	WByte_zwh=0x87;               //bim0??????
	CS1258_write(WByte_zwh);   
	WByte_zwh=0X14;
	CS1258_write(WByte_zwh); 

	WByte_zwh=0x88;               //bim1??????
	CS1258_write(WByte_zwh);   
	WByte_zwh=0X03;
	CS1258_write(WByte_zwh); 

	WByte_zwh=0x80;               //SYS????????
	CS1258_write(WByte_zwh);  
	WByte_zwh=0XDF;
	CS1258_write(WByte_zwh);  
}

