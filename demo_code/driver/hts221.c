/**
 ******************************************************************************
 * @file    hts221.c
 * @author  AST Robotics Team
 * @version V0.0.1
 * @date    08-April-2014
 * @brief   This file provides a set of functions needed to manage the hts221.
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; COPYRIGHT(c) 2014 STMicroelectronics</center></h2>
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *   1. Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 *   3. Neither the name of STMicroelectronics nor the names of its contributors
 *      may be used to endorse or promote products derived from this software
 *      without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************
 */
/* Includes ------------------------------------------------------------------*/
#include "hts221_config.h"
#include "hts221_X.h"
#include <math.h>
#include <stdint.h>

uint8_t HUM_TEMP_IO_Read(uint8_t* pu8Data, uint8_t u8Address, uint8_t reg_addr, uint8_t u8Length)
{
	return hts221_hal_i2c_read_device_reg(u8Address, reg_addr, pu8Data, u8Length);
}

uint8_t HUM_TEMP_IO_Write(uint8_t* pu8Data, uint8_t u8Address, uint8_t u8Command, uint8_t u8Length)
{
	return hts221_hal_i2c_write_device_reg(u8Address, u8Command, pu8Data, u8Length);
}
 
/** @addtogroup BSP
 * @{
 */
 
/** @addtogroup STM32F439_SENSITRON
 * @{
 */
 
/** @addtogroup HTS221
 * @{
 */
 
 
/** @defgroup HTS221_Private_TypesDefinitions
 * @{
 */
 
/**
 * @}
 */
 
/** @defgroup HTS221_Private_Defines
 * @{
 */
 
/**
 * @}
 */
 
/** @defgroup HTS221_Private_Macros
 * @{
 */
 
/**
 * @}
 */
 
/** @defgroup HTS221_Private_Variables
 * @{
 */
 
 
/* Temperature in degree for calibration  */
float T0_degC, T1_degC;
 
/* Output temperature value for calibration */
int16_t T0_out, T1_out;
 
 
/* Humidity for calibration  */
float H0_rh, H1_rh;
 
/* Output Humidity value for calibration */
int16_t H0_T0_out, H1_T0_out;

float Temp_adjusted_parameter1 = 0.0;
float Humi_adjusted_parameter1 = 0.0;

 
/**
 * @}
 */
 
/** @defgroup HTS221_Private_FunctionPrototypes
 * @{
 */
static void HTS221_Power_On(void);
 
static void HTS221_Calibration(void);
/**
 * @}
 */
 
/** @defgroup HTS221_Private_Functions
 * @{
 */
 
 
 
/**
 * @brief  HTS221 Calibration procedure.
 * @param  None
 * @retval None
 */

static void HTS221_Calibration(void)
{
    /* Temperature Calibration */
    /* Temperature in degree for calibration ( "/8" to obtain float) */
    uint16_t T0_degC_x8_L, T0_degC_x8_H, T1_degC_x8_L, T1_degC_x8_H;
    uint8_t H0_rh_x2, H1_rh_x2;
    uint8_t tempReg[2] = {0,0};
 
    HUM_TEMP_IO_Read(tempReg, HTS221_ADDRESS, HTS221_T0_degC_X8_ADDR, 1);
    T0_degC_x8_L = (uint16_t)tempReg[0];
 
    HUM_TEMP_IO_Read(tempReg, HTS221_ADDRESS, HTS221_T1_T0_MSB_X8_ADDR, 1);
    T0_degC_x8_H = (uint16_t) (tempReg[0] & 0x03);
 
    T0_degC = ((float)((T0_degC_x8_H<<8) | (T0_degC_x8_L)))/8;
 
    HUM_TEMP_IO_Read(tempReg, HTS221_ADDRESS, HTS221_T1_degC_X8_ADDR, 1);
    T1_degC_x8_L = (uint16_t)tempReg[0];
 
    HUM_TEMP_IO_Read(tempReg, HTS221_ADDRESS, HTS221_T1_T0_MSB_X8_ADDR, 1);
    T1_degC_x8_H = (uint16_t) (tempReg[0] & 0x0C);
    T1_degC_x8_H = T1_degC_x8_H >> 2;
 
    T1_degC = ((float)((T1_degC_x8_H<<8) | (T1_degC_x8_L)))/8;
 
    HUM_TEMP_IO_Read(tempReg, HTS221_ADDRESS, HTS221_T0_OUT_L_ADDR + 0x80, 2);
    T0_out = ((((int16_t)tempReg[1]) << 8)+(int16_t)tempReg[0]);
 
    HUM_TEMP_IO_Read(tempReg, HTS221_ADDRESS, HTS221_T1_OUT_L_ADDR + 0x80, 2);
    T1_out = ((((int16_t)tempReg[1]) << 8)+(int16_t)tempReg[0]);
 
    /* Humidity Calibration */
    /* Humidity in degree for calibration ( "/2" to obtain float) */
 
    HUM_TEMP_IO_Read(&H0_rh_x2, HTS221_ADDRESS, HTS221_H0_RH_X2_ADDR, 1);
 
    HUM_TEMP_IO_Read(&H1_rh_x2, HTS221_ADDRESS, HTS221_H1_RH_X2_ADDR, 1);
 
    HUM_TEMP_IO_Read(&tempReg[0], HTS221_ADDRESS, HTS221_H0_T0_OUT_L_ADDR + 0x80, 2);
    H0_T0_out = ((((int16_t)tempReg[1]) << 8)+(int16_t)tempReg[0]);
 
    HUM_TEMP_IO_Read(&tempReg[0], HTS221_ADDRESS, HTS221_H1_T0_OUT_L_ADDR  + 0x80, 2);
    H1_T0_out = ((((int16_t)tempReg[1]) << 8)+(int16_t)tempReg[0]);
 
    H0_rh = ((float)H0_rh_x2)/2;
    H1_rh = ((float)H1_rh_x2)/2;
}
 
 
/**
 * @brief  Set HTS221 Initialization.
 * @param  InitStruct: it contains the configuration setting for the HTS221.
 * @retval None
 */
void HTS221_Init( uint8_t read_freq)
{  
    uint8_t tmp = 0x00;
 
    HTS221_Power_On();
 
    HTS221_Calibration();
 
    HUM_TEMP_IO_Read(&tmp, HTS221_ADDRESS, HTS221_CTRL_REG1_ADDR, 1);
 
    /* Output Data Rate selection */
    tmp &= ~(HTS221_ODR_MASK);
    tmp |= read_freq;
    //tmp |= HTS221_ODR_1Hz;
 
    HUM_TEMP_IO_Write(&tmp, HTS221_ADDRESS, HTS221_CTRL_REG1_ADDR, 1);
}
 
/**
 * @brief  Read ID address of HTS221
 * @param  Device ID address
 * @retval ID name
 */
uint8_t HTS221_ReadID(void)
{
    uint8_t tmp;
 
    /* Read WHO I AM register */
    HUM_TEMP_IO_Read(&tmp, HTS221_ADDRESS, HTS221_WHO_AM_I_ADDR, 1);
 
    /* Return the ID */
    return (uint8_t)tmp;
}
 
/**
 * @brief  Reboot memory content of HTS221
 * @param  None
 * @retval None
 */
void HTS221_RebootCmd(void)
{
    uint8_t tmpreg;
 
    /* Read CTRL_REG2 register */
    HUM_TEMP_IO_Read(&tmpreg, HTS221_ADDRESS, HTS221_CTRL_REG2_ADDR, 1);
 
    /* Enable or Disable the reboot memory */
    tmpreg |= HTS221_BOOT_REBOOTMEMORY;
 
    /* Write value to MEMS CTRL_REG2 regsister */
    HUM_TEMP_IO_Write(&tmpreg, HTS221_ADDRESS, HTS221_CTRL_REG2_ADDR, 1);
}
 
/**
 * @brief Set HTS221 Interrupt INT1 configuration
 * @param  HTS221_InterruptConfig_TypeDef: pointer to a HTS221_InterruptConfig_TypeDef
 *         structure that contains the configuration setting for the HTS221 Interrupt.
 * @retval None
 */
void HTS221_INT1InterruptConfig(uint16_t Int1Config)
{
 
}
 
/**
 * @brief  Enable INT1
 * @retval None
 */
void HTS221_EnableIT(uint8_t IntPin)
{  
 
}
 
/**
 * @brief  Disable  INT1
 * @retval None
 */
void HTS221_DisableIT(uint8_t IntPin)
{  
 
}
 

/**
 * @brief  Adjust temp or hum
 * @retval None
 */
float HTS221_CalibrationAlgorithm(float start_point, float end_point, float point, float start_adjust, float end_adjust)
{
    float calibration_value = 0.0;
    calibration_value = start_adjust + (point - start_point)/(end_point - start_point)*(end_adjust - start_adjust);
    return calibration_value;
}

/****************************************************************************
 *
 * NAME: HTS221_Get_AdjustTmp
 *
 * DESCRIPTION:
 * 2016-08-29.adjust the Temperature-data
 *
 ****************************************************************************/
float HTS221_Get_AdjustTmp(float temp)
{
    float temperature = temp;
    float adjust_value = 0.00;
    if(0 <= temperature && temperature < 5){
        adjust_value = HTS221_CalibrationAlgorithm(0, 5, temperature, -1.8, 0.45);
    }
    else if(5 <= temperature && temperature < 10){
        adjust_value = HTS221_CalibrationAlgorithm(5, 10, temperature, 0.45, 0.15);
    }
    else if(10 <= temperature && temperature < 15){
        adjust_value = HTS221_CalibrationAlgorithm(10, 15, temperature, 0.15, -0.13);
    }
    else if(15 <= temperature && temperature < 20){
        adjust_value = HTS221_CalibrationAlgorithm(15, 20, temperature, -0.13, -0.95);
    }
    else if(20 <= temperature && temperature < 25){
        adjust_value = HTS221_CalibrationAlgorithm(20, 25, temperature, -0.95, -0.41);
    }
    else if(25 <= temperature && temperature < 30){
        adjust_value = HTS221_CalibrationAlgorithm(25, 30, temperature, -0.41, -1.01);
    }
    else if(30 <= temperature && temperature < 35){
        adjust_value = HTS221_CalibrationAlgorithm(30, 35, temperature, -1.01, -0.75);
    }
    else if(35 <= temperature && temperature < 40){
        adjust_value = HTS221_CalibrationAlgorithm(35, 40, temperature, -0.75, -0.45);
    }
    else{
        //do none
        adjust_value = 0;
    }
    return adjust_value;
}

/****************************************************************************
 *
 * NAME: HTS221_Get_AdjustHum
 *
 * DESCRIPTION:
 * 2016-08-29.adjust the humidity-data
 *
 ****************************************************************************/
float HTS221_Get_AdjustHum(float hum)
{
    float humidity = hum;
    float adjust_value = 0.00;
    if(0 <= humidity && humidity < 15){
        adjust_value = HTS221_CalibrationAlgorithm(0, 15, humidity, 0, -4);
    }
    else if(15 <= humidity && humidity < 25){
        adjust_value = HTS221_CalibrationAlgorithm(15, 25, humidity, -4, -4);//-9,-8
    }
    else if(25 <= humidity && humidity < 35){
        adjust_value = HTS221_CalibrationAlgorithm(25, 35, humidity, -6, -7);//-8,-13
    }
    else if(35 <= humidity && humidity < 45){
        adjust_value = HTS221_CalibrationAlgorithm(35, 45, humidity, -6, -3);
    }
    else if(45 <= humidity && humidity < 55){
        adjust_value = HTS221_CalibrationAlgorithm(45, 55, humidity, -2, -2);
    }
    else if(55 <= humidity && humidity < 65){
        adjust_value = HTS221_CalibrationAlgorithm(55, 65, humidity, -2, -3);
    }
    else if(65 <= humidity && humidity < 75){
        adjust_value = HTS221_CalibrationAlgorithm(65, 75, humidity, -3, -8);//add 20161101,all add 2 below 75%
    }
    else if(75 <= humidity && humidity < 85){
        adjust_value = HTS221_CalibrationAlgorithm(75, 85, humidity, -10, -9);
    }
    else if(85 <= humidity && humidity < 90){
        adjust_value = HTS221_CalibrationAlgorithm(85, 90, humidity, -13, -15);
    }
    else if(90 <= humidity && humidity <= 95){
        adjust_value = HTS221_CalibrationAlgorithm(90, 95, humidity, -13, -12);
    }
    else if(95 <= humidity && humidity < 100){
        adjust_value = HTS221_CalibrationAlgorithm(95, 100, humidity, -14, -15);//13,10
    }
    else if(100 <= humidity && humidity < 110){
        adjust_value = HTS221_CalibrationAlgorithm(100, 110, humidity, -16, -13);//18,14
    }
    else if(110 <= humidity && humidity < 120){
        adjust_value = HTS221_CalibrationAlgorithm(110, 120, humidity, -18, -23);//13,10
    }
    else if(humidity >=120){
        adjust_value = -23;
    }

/*
    else if(85 <= humidity && humidity < 95){
        adjust_value = HTS221_CalibrationAlgorithm(85, 95, humidity, -16, -8.7);
    }
    */
    else{
        //do none
        adjust_value = 0;
    }
    return adjust_value;
}
 
/**
 * @brief  Read HTS221 output register, and calculate the humidity.
 * @param  pfData : Data out pointer
 * @retval None
 */	
void HTS221_GetHumidity(float* pfData)
{
    int16_t H_T_out, humidity_t;
    uint8_t tempReg[2] = {0,0};
    uint8_t tmp = 0x00;
    float H_rh;
    
    float humidity_value = 0.0;;
    float adjust_value = 0.0;
	
    HUM_TEMP_IO_Read(&tmp, HTS221_ADDRESS, HTS221_CTRL_REG1_ADDR, 1);
 
    /* Output Data Rate selection */
    tmp &= (HTS221_ODR_MASK);
    
    if(tmp == 0x00){
    
      HUM_TEMP_IO_Read(&tmp, HTS221_ADDRESS, HTS221_CTRL_REG2_ADDR, 1);
 
      /* Serial Interface Mode selection */
      tmp &= ~(HTS221_ONE_SHOT_MASK);
      tmp |= HTS221_ONE_SHOT_START;
 
      HUM_TEMP_IO_Write(&tmp, HTS221_ADDRESS, HTS221_CTRL_REG2_ADDR, 1);
    
      do{
      
        HUM_TEMP_IO_Read(&tmp, HTS221_ADDRESS, HTS221_STATUS_REG_ADDR, 1);
         
      }while(!(tmp&&0x02));
    
    }
    
    HUM_TEMP_IO_Read(&tempReg[0], HTS221_ADDRESS, HTS221_HUMIDITY_OUT_L_ADDR + 0x80, 2);
    H_T_out = ((((int16_t)tempReg[1]) << 8)+(int16_t)tempReg[0]);
 
    H_rh = ((float)(H_T_out - H0_T0_out))/(H1_T0_out - H0_T0_out) * (H1_rh - H0_rh) + H0_rh;
 
    humidity_t = (uint16_t)(H_rh * pow(10,HUM_DECIMAL_DIGITS));
    
    /*add by xd 2016年8月8日19:19:56*/
    humidity_value = ((float)humidity_t)/pow(10,TEMP_DECIMAL_DIGITS);
    if(humidity_value>=120)
		humidity_value=120;
    adjust_value = HTS221_Get_AdjustHum(humidity_value);
    /**********/

    *pfData = humidity_value+adjust_value+Humi_adjusted_parameter1;//*(Humi_adjusted_parameter/100);//add 2016-09-05.liu

    //*pfData = ((((float)humidity_t)/pow(10,HUM_DECIMAL_DIGITS)+adjust_value)+Humi_adjusted_parameter1);//*(Humi_adjusted_parameter/100);//add 2016-09-05.liu
}
 
/**
 * @brief  Read HTS221 output register, and calculate the temperature.
 * @param  pfData : Data out pointer
 * @retval None
 */
void HTS221_GetTemperature(float* pfData)
{
    int16_t T_out, temperature_t;
    uint8_t tempReg[2] = {0,0};
    uint8_t tmp = 0x00;
    float T_degC;
    
    float temp_value = 0.0;
    float adjust_value = 0.0;
    HUM_TEMP_IO_Read(&tmp, HTS221_ADDRESS, HTS221_CTRL_REG1_ADDR, 1);
 
    /* Output Data Rate selection */
    tmp &= (HTS221_ODR_MASK);
    
    if(tmp == 0x00){
    
      HUM_TEMP_IO_Read(&tmp, HTS221_ADDRESS, HTS221_CTRL_REG2_ADDR, 1);
 
      /* Serial Interface Mode selection */
      tmp &= ~(HTS221_ONE_SHOT_MASK);
      tmp |= HTS221_ONE_SHOT_START;
 
      HUM_TEMP_IO_Write(&tmp, HTS221_ADDRESS, HTS221_CTRL_REG2_ADDR, 1);
    
      do{

        HUM_TEMP_IO_Read(&tmp, HTS221_ADDRESS, HTS221_STATUS_REG_ADDR, 1);

      }while(!(tmp&&0x01));
    
    }
 
    HUM_TEMP_IO_Read(&tempReg[0], HTS221_ADDRESS, HTS221_TEMP_OUT_L_ADDR + 0x80, 2);
    T_out = ((((int16_t)tempReg[1]) << 8)+(int16_t)tempReg[0]);
 
    T_degC = ((float)(T_out - T0_out))/(T1_out - T0_out) * (T1_degC - T0_degC) + T0_degC;
 
    temperature_t = (int16_t)(T_degC * pow(10,TEMP_DECIMAL_DIGITS));

    /*add by xd 2016年8月8日19:20:00*/
    temp_value = ((float)temperature_t)/pow(10,TEMP_DECIMAL_DIGITS);
    adjust_value = HTS221_Get_AdjustTmp(temp_value);
    /**********/

    *pfData = ((((float)temperature_t)/pow(10,TEMP_DECIMAL_DIGITS) + adjust_value)+Temp_adjusted_parameter1);//*(Temp_adjusted_parameter/100);//change 20160905.liu
}
 
 
/**
 * @brief  Exit the shutdown mode for HTS221.
 * @retval None
 */
static void HTS221_Power_On()
{
    uint8_t tmpReg;
 
    /* Read the register content */
    HUM_TEMP_IO_Read(&tmpReg, HTS221_ADDRESS, HTS221_CTRL_REG1_ADDR, 1);
 
    /* Set the power down bit */
    tmpReg |= HTS221_MODE_ACTIVE;
 
    /* Write register */
    HUM_TEMP_IO_Write(&tmpReg, HTS221_ADDRESS, HTS221_CTRL_REG1_ADDR, 1);
}
 
/**
 * @brief  Enter the shutdown mode for HTS221.
 * @retval None
 */
void HTS221_Power_OFF()
{
    uint8_t tmpReg;
 
    /* Read the register content */
    HUM_TEMP_IO_Read(&tmpReg, HTS221_ADDRESS, HTS221_CTRL_REG1_ADDR, 1);
 
    /* Reset the power down bit */
    tmpReg &= ~(HTS221_MODE_ACTIVE);
 
    /* Write register */
    HUM_TEMP_IO_Write(&tmpReg, HTS221_ADDRESS, HTS221_CTRL_REG1_ADDR, 1);
}
 
/**
 * @}
 */
 
/**
 * @}
 */
 
/**
 * @}
 */
 
/**
 * @}
 */
 
void hts221_init_one_shot( void )
{
	HTS221_Init(HTS221_ODR_ONE_SHOT);
}

int16_t hts221_read_temp(void)
{
    int16_t T_out;
    uint8_t tempReg[2] = {0,0};
    uint8_t tmp = 0x00;
    float T_degC;
    
    float temp_value = 0.0;
    float adjust_value = 0.0;
    HUM_TEMP_IO_Read(&tmp, HTS221_ADDRESS, HTS221_CTRL_REG1_ADDR, 1);
 
    /* Output Data Rate selection */
    tmp &= (HTS221_ODR_MASK);
    
    if(tmp == 0x00){
    
      HUM_TEMP_IO_Read(&tmp, HTS221_ADDRESS, HTS221_CTRL_REG2_ADDR, 1);
 
      /* Serial Interface Mode selection */
      tmp &= ~(HTS221_ONE_SHOT_MASK);
      tmp |= HTS221_ONE_SHOT_START;
 
      HUM_TEMP_IO_Write(&tmp, HTS221_ADDRESS, HTS221_CTRL_REG2_ADDR, 1);
    
      do{

        HUM_TEMP_IO_Read(&tmp, HTS221_ADDRESS, HTS221_STATUS_REG_ADDR, 1);

      }while(!(tmp&&0x01));
    
    }
 
    HUM_TEMP_IO_Read(&tempReg[0], HTS221_ADDRESS, HTS221_TEMP_OUT_L_ADDR + 0x80, 2);
    T_out = ((((int16_t)tempReg[1]) << 8)+(int16_t)tempReg[0]);
 
    T_degC = ((float)(T_out - T0_out))/(T1_out - T0_out) * (T1_degC - T0_degC) + T0_degC;
	
	return (int16_t)(T_degC * pow(10,TEMP_DECIMAL_DIGITS));
}

uint16_t hts221_read_humi(void)
{
    int16_t H_T_out;
    uint8_t tempReg[2] = {0,0};
    uint8_t tmp = 0x00;
    float H_rh;
    
    float humidity_value = 0.0;;
    float adjust_value = 0.0;
	
    HUM_TEMP_IO_Read(&tmp, HTS221_ADDRESS, HTS221_CTRL_REG1_ADDR, 1);
 
    /* Output Data Rate selection */
    tmp &= (HTS221_ODR_MASK);
    
    if(tmp == 0x00){
    
      HUM_TEMP_IO_Read(&tmp, HTS221_ADDRESS, HTS221_CTRL_REG2_ADDR, 1);
 
      /* Serial Interface Mode selection */
      tmp &= ~(HTS221_ONE_SHOT_MASK);
      tmp |= HTS221_ONE_SHOT_START;
 
      HUM_TEMP_IO_Write(&tmp, HTS221_ADDRESS, HTS221_CTRL_REG2_ADDR, 1);
    
      do{
      
        HUM_TEMP_IO_Read(&tmp, HTS221_ADDRESS, HTS221_STATUS_REG_ADDR, 1);
         
      }while(!(tmp&&0x02));
    
    }
    
    HUM_TEMP_IO_Read(&tempReg[0], HTS221_ADDRESS, HTS221_HUMIDITY_OUT_L_ADDR + 0x80, 2);
    H_T_out = ((((int16_t)tempReg[1]) << 8)+(int16_t)tempReg[0]);
 
    H_rh = ((float)(H_T_out - H0_T0_out))/(H1_T0_out - H0_T0_out) * (H1_rh - H0_rh) + H0_rh;
 
    return (uint16_t)(H_rh * pow(10,HUM_DECIMAL_DIGITS));	
} 



/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/     

