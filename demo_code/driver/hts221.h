#ifndef __HTS221_H
#define __HTS221_H

#include "stdint.h"

void hts221_init_one_shot( void );
int16_t hts221_read_temp(void);
uint16_t hts221_read_humi(void);

#endif
