#include <reg52.h>
#include <intrins.h>//for:"_nop_()"

#define data_port P2

sbit rs=P0^7;
sbit rw=P0^6;
sbit e =P0^5;


void lcd1602_busy(void)
{
	unsigned char	busybit = 0xff;
	unsigned char	time = 0;
	data_port = 0xff;
	do{
	rs=0;           // 读忙信号
	rw=1;
	e=0;
	_nop_();
	_nop_();
	e=1;
	_nop_();
	_nop_();
	_nop_();
	_nop_();
	busybit = data_port&0x80;
	e=0;
	time++;
	}while((busybit != 0)&&(time <= 50));
}

void lcd1602_wcmd (unsigned char cmd)
{
	lcd1602_busy();
	rs=0;
	rw=0;
	e=0;
	data_port=cmd ;
	_nop_();
	_nop_();
	e=1;
	_nop_();
	_nop_();
	_nop_();
	_nop_();
	e=0;
}
/*
unsigned char lcd1602_rreg (void)
{
	unsigned char temp;
	lcd1602_busy();
	data_port = 0xff;
	rs=0;          
	rw=1;
	e=0;
	_nop_();
	_nop_();
	e=1;
	_nop_();
	_nop_();
	_nop_();
	_nop_();
	temp = data_port;
	e=0;
	return temp;
}
*/
void lcd1602_wdat (unsigned char dat)
{
	lcd1602_busy();
	rs=1;
	rw=0;
	e=0;
	data_port=dat ;
	_nop_();
	_nop_();
	e=1;
	_nop_();
	_nop_();
	_nop_();
	_nop_();
	e=0;
}
/*
unsigned char lcd1602_rdat (void)
{
	unsigned char temp;
	lcd1602_busy();
	data_port = 0xff;
	rs=1;          
	rw=1;
	e=0;
	_nop_();
	_nop_();
	e=1;
	_nop_();
	_nop_();
	_nop_();
	_nop_();
	temp = data_port;
	e=0;
	return temp;
}
*/
void lcd1602_init(void)
{
	lcd1602_wcmd(0x38);
	lcd1602_wcmd(0x0c);
	lcd1602_wcmd(0x06);
	lcd1602_wcmd(0x01);
}

void lcd1602_clr(void)
{
	lcd1602_wcmd(0x01);	
	lcd1602_wdat(0x01);
}
/*
void lcd1602_01(void)
{
	lcd1602_wcmd(0xc0);
}
*/
void lcd1602_02(void)
{
	lcd1602_wcmd(0xc0);
}
/*
void lcd1602_changeline(void)//复制第二行数据到第一行。
{
	unsigned char i = 0;
	unsigned char temp[16];
	lcd1602_wcmd(0x06);	//地址自动加1
	lcd1602_wcmd(0xc0);	//地址回到行首
	for(i = 0; i < 16; i ++){
		temp[i] = lcd1602_rdat();
	}
	lcd1602_wcmd(0x01);	//清屏	
	for(i = 0; i < 16; i ++){
		lcd1602_wdat(temp[i]);
	}
}

void printf_setbel_lcd1602(unsigned char *words,unsigned char num)
{
	unsigned char i;
	for(i = 0;i < num;words++){
		i++;
		lcd1602_wdat(*words);					
	}
}
*/
void printf_lcd1602(unsigned char *words)
{
	for(;*words != '\0';words++){
		lcd1602_wdat(*words);
	}
}
/*
void printf_lcd1602(unsigned char *words)	//输出字符串
{
	unsigned char i = 0;

	if(lcd1602_rreg() > 0x00){		//如果指针不在第一行首（第一行有数据）
		if((lcd1602_rreg() > 0x40))	//如果第二行有数据，复制第二行数据到第一行。
		{
			lcd1602_changeline();
		}
		lcd1602_wcmd(0xc0);			//写到第二行
		for(;*words != '\0';words++){
			i++;
			lcd1602_wdat(*words);
			if(i >= 16){				//超过16个字符，换行
				lcd1602_changeline();
				lcd1602_wcmd(0xc0);
				i = 0;
			}					
		}
	}
	else{
		for(;*words != '\0';words++){
			i++;
			lcd1602_wdat(*words);
			if(i >= 16){				//超过16个字符，换行
				lcd1602_wcmd(0xc0);
				i = 0;
			}					
		}		
	}
}

void dis_num_lcd1602(unsigned int num,unsigned char n,unsigned char disall)//数值，位数（从后到前），显示所有0（1显示，0不显示）
{
	unsigned char temp;
	unsigned char undis = 0;
	switch(n){
		case 5:
		temp = num/10000;
		if((temp == 0)&&(disall == 0)){
			undis = 1;
		}
		else
			lcd1602_wdat(temp+0x30);
		case 4:
		temp = num/1000%10;
		if((temp == 0)&&(undis == 1)&&(disall == 0)){
			undis = 1;
		}
		else{
			undis = 0;
			lcd1602_wdat(temp+0x30);
		}
		case 3:	
		temp = num/100%10;
		if((temp == 0)&&(undis == 1)&&(disall == 0)){
			undis = 1;
		}
		else{
			undis = 0;
			lcd1602_wdat(temp+0x30);
		}
		case 2:	
		temp = num/10%10;
		if((temp == 0)&&(undis == 1)&&(disall == 0)){
			undis = 1;
		}
		else{
			undis = 0;
			lcd1602_wdat(temp+0x30);
		}
		case 1:	
		temp = num/1%10;
		lcd1602_wdat(temp+0x30);
		default:
		break;
	}
}
*/
