/**
 *
 * Leedarson
 * All Rights Reserved
 *
 * @file mod_light_bh1750.h
 *
 * @brief Header file for mod_light_bh1750.c.
 *
 *
 * Author: xuexiaofei 
 *
 * Last Changed By: $Author: xuexiaofei $
 * Revision: $Revision: 1.00 $
 * Last Changed: $Date: 2017/03/30 10:18:01 $
 *
 */

#ifndef __MOD_LIGHT_BH1750_H
#define __MOD_LIGHT_BH1750_H

#include "stdint.h"

void modLightStartConvert( void );


void modLightConvertMul( void ); // 进行转换


uint16_t modLightValGet( void ); // 直接读取值


void StartMeasureLightProcess(void);

void StartMeasureLight(void);

uint8_t GetMeasureLight(void);


uint8_t GetLXMeasureStatus(void);

extern uint8_t    LX_State;

#endif

