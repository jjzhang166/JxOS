#ifndef _transfer_control_h_
#define _transfer_control_h_

#include "net_stdint.h"

#define  NOP_ADDR		0xff

/*************************************/
//LINK 层回调
extern void (*link_start_cb)(void);
extern void (*link_fail_cb)(void);
extern void (*link_finish_cb)(void);

/*************************************/
//LINK 层输入变量
//输入变量
extern uint8_t link_force_sleep_enable;

extern uint8_t link_channel;
extern uint8_t link_target_addr;
extern uint8_t* link_tx_data_buffer;
extern uint8_t link_tx_data_buffer_len;

//输出变量
extern uint8_t* link_rssi;
extern uint8_t* link_rx_data_buffer;
extern uint8_t* link_rx_data_buffer_len;


/*************************************/
//LINK 层接口
void link_enable(void);
void link_init(void);



//为提高效率，LINK 层接口可将变量传导方式优化为指针方式
/*************************************
void trans_load_param(uint8_t channel,
                      uint8_t addr,
                      uint8_t *tx_buffer_p,
                      uint8_t *tx_buffer_len,
                      uint8_t *rx_buffer_p,
                      uint8_t *rx_buffer_len,
                      uint8_t *rssi);
*************************************/



#endif // _transfer_control_h_

