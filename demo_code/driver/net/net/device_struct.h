
#ifndef _device_struct_h_
#define _device_struct_h_

#include "net_stdint.h"


#define DEVICE_DATA_BUFFER_LEN 8
//#if(DEVICE_DATA_BUFFER_LEN < 24)
//  #define DEVICE_DATA_BUFFER_LEN 24
//#endif

//state
enum {
	DEVICE_STATE_FREE,
	DEVICE_STATE_BUZY,
	DEVICE_STATE_OK,
	DEVICE_STATE_ERR
};

typedef struct{

	uint8_t addr;
	uint8_t channel;
	uint8_t state;
	uint8_t rssi;
	uint8_t finish_count;
	uint8_t fail_count;
	uint8_t tx_buffer[DEVICE_DATA_BUFFER_LEN];
	uint8_t tx_buffer_len;
	uint8_t rx_buffer[DEVICE_DATA_BUFFER_LEN];
	uint8_t rx_buffer_len;
	
}DEVICE_STRUCT;

DEVICE_STRUCT* device_struct_init(uint8_t addr, uint8_t channel, DEVICE_STRUCT* device_struct);
void device_struct_writer_tx_buffer(DEVICE_STRUCT* device_struct, uint8_t* tx_data, uint8_t tx_data_len);
void device_struct_read_tx_buffer(DEVICE_STRUCT* device_struct, uint8_t* rx_data, uint8_t* rx_data_len);

#endif

