

#include "net_stdint.h"

uint8_t framing(uint8_t cmd, uint8_t* dat, uint8_t data_len, uint8_t* frame, uint8_t* frame_len);
uint8_t unframing(uint8_t* frame, uint8_t frame_len, uint8_t* dat, uint8_t* data_len, uint8_t* cmd);

