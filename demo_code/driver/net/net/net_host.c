
#include <string.h>
#include "link_control.h"

#include "frame.h"
#include "time_slice_list.h"
#include "general_interface.h"
#include "broadcast_logon.h"

#include "link_control_config.h"

#ifdef HOST_DEVICE

#define HOST_NAME 			"HOST"
#define HOST_NAME_LEN 		4

DEVICE_STRUCT test_1;
DEVICE_STRUCT test_2;
//DEVICE_STRUCT test_3;

void net_host_init(void)
{	
	link_init();
	link_start_cb = net_transfer_start_interface;
	link_fail_cb = net_transfer_fail_interface;
	link_finish_cb = net_transfer_finish_interface;
	
	time_slice_list_init(); 
	
//	broadcast_sync_init();
//	broadcast_reply_init();
//	broadcast_register_init();
	device_struct_init(BROADCAST_ADDR, BROADCAST_SYNC_CHANNEL, &broadcast_sync);
	time_slice_list_logon(&broadcast_sync, BROADCAST_SYNC_TIME_SLICE);
	framing(FRAME_TYPE_HOST_BROADCAST_SYNC, HOST_NAME, HOST_NAME_LEN,\
			broadcast_sync.tx_buffer, &(broadcast_sync.tx_buffer_len));
//	memcpy(broadcast_sync.tx_buffer, , 8);
//	broadcast_sync.tx_buffer_len = 8;

	

	//	slave_device_list_init();	
	device_struct_init(0x05, 0x00, &test_1);
	memcpy(test_1.tx_buffer, "h\r\n", 3);
	test_1.tx_buffer_len = 3;
	time_slice_list_logon(&test_1, 10);
	  
	device_struct_init(0x06, 0x00, &test_2);
	memcpy(test_2.tx_buffer, "x\r\n", 3);
	test_2.tx_buffer_len = 3;
	time_slice_list_logon(&test_2, 5);

}

void logon_manage(uint8_t* msg, uint8_t len)
{
//	static logon_manage_state = 0;
	uint8_t cmd;
	uint8_t data_len;
	uint8_t data[8];
	
	memset(data, 0x00, 8);
	if(1 == unframing(msg, len, data, &data_len, &cmd)){
		if((cmd == FRAME_TYPE_SLAVE_LOGON_APPLY)
				&&(data_len == 2)){
//			logon_manage_state = 1;
//			分配时间片号，分配地址		
//			data[2] = 01;		//01：申请成功 00申请失败
//			data[3] = 时间片号;
//			data[4] = 时间片号;			
			framing(FRAME_TYPE_HOST_LOGON_REPONSE, data, data_len+3,\
					broadcast_sync.tx_buffer, &(broadcast_sync.tx_buffer_len));
		}
		else{
			framing(FRAME_TYPE_HOST_BROADCAST_SYNC, HOST_NAME, HOST_NAME_LEN,\
					broadcast_sync.tx_buffer, &(broadcast_sync.tx_buffer_len));	
		}
	}
	
}

void rxdata_analysis(void)
{
  
	uint8_t i;
	uint8_t rxdata_buffer[24];
	uint8_t rxdata_len;
	
	//轮询所有回复
	for(i = 0; i < TIME_SLICE_TOTAL; i++){	
//		if((time_slice_list[i]->state != DEVICE_STATE_BUZY)
//			&&(time_slice_list[i]->state != DEVICE_STATE_FREE)){
		//有效回复
		if(time_slice_list[i]->state == DEVICE_STATE_OK){
			//处理广播回复
			if(i == BROADCAST_SYNC_TIME_SLICE){
				//注册申请管理
				logon_manage(time_slice_list[i]->rx_buffer,\
							 time_slice_list[i]->rx_buffer_len);
				time_slice_list[i]->rx_buffer_len = rxdata_len;
				time_slice_list[i]->state = DEVICE_STATE_FREE;			
			}	
			//处理其他回复
			else{
				memcpy(rxdata_buffer, time_slice_list[i]->rx_buffer,\
					   time_slice_list[i]->rx_buffer_len);	
				time_slice_list[i]->rx_buffer_len = rxdata_len;
				time_slice_list[i]->state = DEVICE_STATE_FREE;							
			}
		}
	}
	
}

/*
 if(time_slice_list[BROADCAST_SYNC_TIME_SLICE]->state == 1){
  unframing(time_slice_list[BROADCAST_SYNC_TIME_SLICE]->rx_buffer,
  data, &cmd, &len);
  uart_print("bor get ok\r\n");
  if(time_slice_list[BROADCAST_SYNC_TIME_SLICE]->rx_buffer[0] == 0x80){
   random_code[0] = time_slice_list[BROADCAST_SYNC_TIME_SLICE]->rx_buffer[2];
   random_code[1] = time_slice_list[BROADCAST_SYNC_TIME_SLICE]->rx_buffer[3];
   uart_print("get ask\r\n");			
  }
   
 }
*/

void net_host_task(void)	//执行时间 <= 一个循环时间
{
	//个时间片的RX数据处理（搬运到缓存池）
	//准备需要写入的数据
	//注册申请处理
	
	//处理回复数据
	rxdata_analysis();



	
//	net_host_logon_manager();
//	time_slice_list_print();
}

#endif
