#include <string.h>

#include "link_control.h"
#include "net_control.h"
#include "time_slice_list.h"
#include "general_interface.h"
#include "broadcast_logon.h"

#include "link_control_config.h"


#ifdef SLAVE_DEVICE

DEVICE_STRUCT test_1;
//DEVICE_STRUCT test_2;
//DEVICE_STRUCT test_3;

void snet_transfer_start_interface(void)
{
	link_channel = time_slice_list[time_slice_list_pionter]->channel;
	link_target_addr = time_slice_list[time_slice_list_pionter]->addr;
	link_tx_data_buffer = time_slice_list[time_slice_list_pionter]->tx_buffer;
	link_tx_data_buffer_len = time_slice_list[time_slice_list_pionter]->tx_buffer_len;
	
	link_rssi = &(time_slice_list[time_slice_list_pionter]->rssi);
	link_rx_data_buffer = time_slice_list[time_slice_list_pionter]->rx_buffer;
	link_rx_data_buffer_len = &(time_slice_list[time_slice_list_pionter]->rx_buffer_len);

}


void snet_transfer_fail_interface(void)
{
//	uart_print("w\r\n");
}

void snet_transfer_finish_interface(void)
{
//	uart_print("ok\r\n");
//	uart_print(time_slice_list[time_slice_list_pionter]->rx_buffer);
	
	link_start_cb = net_transfer_start_interface;
	link_fail_cb = net_transfer_fail_interface;
	link_finish_cb = net_transfer_finish_interface;
	
	time_slice_list_pionter_run();
	
	time_slice_list_logon(&test_1, 10);

	time_slice_list_logoff(0);
}

void net_slave_scan_broadcast_sync(void)
{
  	link_init();
	link_start_cb = snet_transfer_start_interface;
	link_fail_cb = snet_transfer_fail_interface;
	link_finish_cb = snet_transfer_finish_interface;
	
	time_slice_list_init();  
	
	device_struct_init(BROADCAST_ADDR, BROADCAST_SYNC_CHANNEL, &broadcast_sync);
	time_slice_list_logon(&broadcast_sync, BROADCAST_SYNC_TIME_SLICE);
	

	device_struct_init(0xFE, 0x00, &test_1);
	memcpy(test_1.tx_buffer, "s\r\n", 3);
	test_1.tx_buffer_len = 3;
}

void net_slave_task(void)	//执行时间 <= 一个循环时间
{
	uint8_t i;
	uint8_t rxdata_buffer[24];
	uint8_t rxdata_len;
/*	
	if(time_slice_list_pionter == 11){
		if(time_slice_list[10]->fail_count > 20){
			time_slice_list[10]->fail_count = 0;
			time_slice_list[10]->finish_count = 0;
			uart_print("DS\r\n");
			link_force_sleep_enable = 0;	
		}
		if(time_slice_list[10]->finish_count > 20){
			time_slice_list[10]->fail_count = 0;
			time_slice_list[10]->finish_count = 0;
			uart_print("EN\r\n");
			link_force_sleep_enable = 1;	
		}	
	}
*/
	//轮询所有回复
	for(i = 0; i < TIME_SLICE_TOTAL; i++){	
		//有效回复
		if(time_slice_list[i]->state == DEVICE_STATE_OK){
			memcpy(rxdata_buffer, time_slice_list[i]->rx_buffer,\
				   time_slice_list[i]->rx_buffer_len);	
			time_slice_list[i]->rx_buffer_len = rxdata_len;
			time_slice_list[i]->state = DEVICE_STATE_FREE;	
//			uart_send_data(rxdata_buffer, rxdata_len);
		  if(i == 10){
//			uart_print("OK!\r\n");
		  }		
		}
	}

//	uint8_t st;
//	st = time_slice_list[0]->state;
//	if(st == 1){
//		framing(0x81, "12", 2, broadcast_sync.tx_buffer);
//		broadcast_sync.tx_buffer_len = 2+3;
//		time_slice_list[0]->state = 0;
//		uart_print("get host bro\r\n");
//		uart_print("host name:");
//		uart_send_data(broadcast_sync.rx_buffer, broadcast_sync.rx_buffer_len);
//		uart_print("  \r\n");
//	}
//	net_host_logon_manager();
//	time_slice_list_print();
}

#endif
