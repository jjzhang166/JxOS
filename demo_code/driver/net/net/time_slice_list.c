

#include "time_slice_list.h"


/*************************************************************************************/
//以下为时间片列表控制接口

DEVICE_STRUCT* time_slice_list[TIME_SLICE_TOTAL];
uint8_t time_slice_list_pionter = 0;	//volatile
uint8_t time_slice_list_init_ok = 0;
void time_slice_list_init(void)
{
	uint8_t i;
	for(i = 0; i < TIME_SLICE_TOTAL; i++) {
		time_slice_list[i] = 0;
	}
	time_slice_list_init_ok = 1;
}

uint8_t time_slice_list_search_null(void)
{
	uint8_t i;
	for(i = 0; i < TIME_SLICE_TOTAL; i++) {
		if(time_slice_list[i] == 0){
			break;
		}
	}
	return i;
}

void time_slice_list_pionter_run(void)
{
	time_slice_list_pionter++;
	if(time_slice_list_pionter >= TIME_SLICE_TOTAL) {
		time_slice_list_pionter = 0;
	}
}

void time_slice_list_pionter_set(uint8_t mun)
{
	time_slice_list_pionter = mun;
}

void time_slice_list_logon(DEVICE_STRUCT* device_info, uint8_t time_slice_list_pionter_mun)
{
	if(device_info != 0){
		time_slice_list[time_slice_list_pionter_mun] = device_info;
//		printf("longon @ %d\r\n", time_slice_list_pionter_mun);
	}
}

void time_slice_list_logoff(uint8_t time_slice_list_pionter_mun)
{
	time_slice_list[time_slice_list_pionter_mun] = 0;
//	printf("longon @ %d\r\n", time_slice_list_pionter_mun);
}

