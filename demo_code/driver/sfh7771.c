
#include "typedef.h"
#include "sw_i2c.h"

#define SFH771_ID					0X39	//0X38

#define SFH771_SYSTEM_CONTROL_REG	0X40
#define SFH771_MODE_CONTROL_REG		0X41
#define SFH771_ALS_PS_CONTROL_REG	0X42
#define SFH771_PERSISTENCE_REG		0X43

#define SFH771_PS_DATA_LSB_REG		0X44
#define SFH771_PS_DATA_MSB_REG		0X45
#define SFH771_ALS_VIS_DATA_LSB_REG	0X46
#define SFH771_ALS_VIS_DATA_MSB_REG	0X47
#define SFH771_ALS_IR_DATA_LSB_REG	0X48
#define SFH771_ALS_IR_DATA_MSB_REG	0X49

#define SFH771_INT_CONTROL_REG		0X4A

#define SFH771_PS_TH_LSB_REG		0X4B
#define SFH771_PS_TH_MSB_REG		0X4C

void sfh7771_init(void)
{
	uint8 reg_data;
	
	reg_data = 0x3C;		//0X3F
	i2c_burst_write(SFH771_ID, SFH771_ALS_PS_CONTROL_REG, 1, &reg_data);
	
	reg_data = 0x0B;		//0x06
	i2c_burst_write(SFH771_ID, SFH771_MODE_CONTROL_REG, 1, &reg_data);
	
	reg_data = 0x07;
	i2c_burst_write(SFH771_ID, SFH771_PS_TH_LSB_REG, 1, &reg_data);
	reg_data = 0x00;
	i2c_burst_write(SFH771_ID, SFH771_PS_TH_MSB_REG, 1, &reg_data);
	
	reg_data = 0x05;		//当PS高于PSTH时产生一个下降沿中断
	i2c_burst_write(SFH771_ID, SFH771_INT_CONTROL_REG, 1, &reg_data);
}

void sfh7771_ps_th_set(uint16 th)
{
	uint8 reg_data;
	reg_data = (uint8)th;
	i2c_burst_write(SFH771_ID, SFH771_PS_TH_LSB_REG, 1, &reg_data);
	reg_data = (uint8)(th>>8);
	i2c_burst_write(SFH771_ID, SFH771_PS_TH_MSB_REG, 1, &reg_data);
}

uint8 sfh7771_read_id(void)
{
	uint8 temp;
	i2c_burst_read(SFH771_ID, SFH771_SYSTEM_CONTROL_REG, 1, &temp);
	return temp;
}

uint16 sfh7771_read_ps_data(void)
{
	uint16 ps_data;
	uint8 temp;
	i2c_burst_read(SFH771_ID, SFH771_PS_DATA_MSB_REG, 1, &temp);
	ps_data = temp;
	i2c_burst_read(SFH771_ID, SFH771_PS_DATA_LSB_REG, 1, &temp);
	ps_data <<= 8;
	ps_data |= temp;
	
	return ps_data;
}

uint16 sfh7771_read_ir_data(void)
{
	uint16 ps_data;
	uint8 temp;
	i2c_burst_read(SFH771_ID, SFH771_ALS_IR_DATA_LSB_REG, 1, &temp);
	ps_data = temp;
	i2c_burst_read(SFH771_ID, SFH771_ALS_IR_DATA_MSB_REG, 1, &temp);
	ps_data <<= 8;
	ps_data |= temp;
	
	return ps_data;
}

uint16 sfh7771_read_vis_data(void)
{
	uint16 ps_data;
	uint8 temp;
	i2c_burst_read(SFH771_ID, SFH771_ALS_VIS_DATA_LSB_REG, 1, &temp);
	ps_data = temp;
	i2c_burst_read(SFH771_ID, SFH771_ALS_VIS_DATA_MSB_REG, 1, &temp);
	ps_data <<= 8;
	ps_data |= temp;
	
	return ps_data;
}

uint8 sfh7771_read_int_data(void)
{
	uint8 temp;
	i2c_burst_read(SFH771_ID, SFH771_INT_CONTROL_REG, 1, &temp);

	return temp;
}

void sfh7771_set_led_current(uint8 current)
{
	uint8 reg_data = 0x3C;
		
	switch(current){
		case 0:		//25mA
		reg_data = 0x3C+current;	
		i2c_burst_write(SFH771_ID, SFH771_ALS_PS_CONTROL_REG, 1, &reg_data);	
		reg_data = 0x0B;
		i2c_burst_write(SFH771_ID, SFH771_MODE_CONTROL_REG, 1, &reg_data);
		break;
		
		case 1:		//50mA
		reg_data = 0x3C+current;	
		i2c_burst_write(SFH771_ID, SFH771_ALS_PS_CONTROL_REG, 1, &reg_data);
		reg_data = 0x0B;
		i2c_burst_write(SFH771_ID, SFH771_MODE_CONTROL_REG, 1, &reg_data);
		break;
		
		case 2:		//100mA
		reg_data = 0x3C+current;	
		i2c_burst_write(SFH771_ID, SFH771_ALS_PS_CONTROL_REG, 1, &reg_data);
		reg_data = 0x0B;
		i2c_burst_write(SFH771_ID, SFH771_MODE_CONTROL_REG, 1, &reg_data);
		break;		
		
		case 3:		//200mA
		reg_data = 0x3C+current;	
		i2c_burst_write(SFH771_ID, SFH771_ALS_PS_CONTROL_REG, 1, &reg_data);
		reg_data = 0x0B;
		i2c_burst_write(SFH771_ID, SFH771_MODE_CONTROL_REG, 1, &reg_data);
		break;	
		
		default:	//off
		reg_data = 0x0A;
		i2c_burst_write(SFH771_ID, SFH771_MODE_CONTROL_REG, 1, &reg_data);
		break;		
	}	
}

/*
void pir_state_handler(void)
{
	if(sfh7771_read_ps_data() > 10){
		
	}
}
*/
