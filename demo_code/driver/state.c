/*
 * state.c
 *
 *  Created on: 2017��4��24��
 *      Author: zengjie
 */
#include "state.h"


void _my_printf(const int8_t *str, ...);
//#define STATE_EVENT_ENTER 0
//typedef int STATE_ID;
static STATE_ST* current_state = 0;


static STATE_EVENT_HANDLER *state_event_handler_callback_list[] = {
//	&key_eco_short_handler,
//	&key_eco_long_handler,
//	&key_boost_short_handler,
//	&key_boost_long_handler,
//	&key_auto_short_handler,
//	&key_auto_long_handler,
//	&key_all_long_handler,
//	&rotary_encoder_cw_handler,
//	&rotary_encoder_ccw_handler,
//	&rtc_1sec_handler
};
#define STATE_EVENT_HANDLER_CALLBACK_LIST_LEN  	sizeof(state_event_handler_callback_list)/sizeof(STATE_EVENT_HANDLER)

static void null_fun(void) {
	//_my_printf("null_fun\r\n");
}

static void state_event_handler_callback_list_init(void) {
	uint32_t i;
	for (i = 0; i < STATE_EVENT_HANDLER_CALLBACK_LIST_LEN; i++) {
		*(state_event_handler_callback_list[i]) = null_fun;
	}
}

static uint8_t state_struct_event_handler_init(STATE_ST* state) {
	uint32_t i;
	if(state->event_hendler_table_len > STATE_EVENT_HANDLER_CALLBACK_LIST_LEN){
		//state->event_hendler_table_len = STATE_EVENT_HANDLER_CALLBACK_LIST_LEN;
		return 0;
	}
	else{
		state_event_handler_callback_list_init();
		for (i = 0; i < state->event_hendler_table_len; i++) {
			if (state->event_hendler_table[i] != 0) {
				*(state_event_handler_callback_list[i]) =
						state->event_hendler_table[i];
			}
		}
		return 1;
	}
}

uint8_t state_jump_to_state(STATE_ST* state) {
	if(state_struct_event_handler_init(state) == 1){
		if(current_state != 0){
			current_state->exit();
		}
		state->enter();
		current_state = state;
		return 1;
	}
	else{
		return 0;
	}
}

STATE_ST* state_current_state_get(void)
{
	return current_state;
}
