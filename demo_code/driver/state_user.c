/*
 * state_user.c
 *
 *  Created on: 2017��4��24��
 *      Author: zengjie
 */



#include "state.h"


void _my_printf(const int8_t *str, ...);


extern STATE_ST AUTO_STATE_STRUCT;
extern STATE_ST MANU_STATE_STRUCT;
extern STATE_ST LOCK_STATE_STRUCT;
extern STATE_ST HOLIDAY_STATE_STRUCT;
extern STATE_ST TIME_DATE_STATE_STRUCT;
extern STATE_ST CONFIG_STATE_STRUCT;

static STATE_ST* LAST_STATE;

void state_default(void){
	LAST_STATE = &AUTO_STATE_STRUCT;
	state_jump_to_state(&AUTO_STATE_STRUCT);
}

/********************************************************************/
static void delay_ms(uint32_t n) {
	volatile uint32_t i = 0, j = 0;
	for (i = n; i > 0; i--)
		for (j = 0; j < 1050; j++)
			;
}

static void boost_switch(void)
{
	if(bst_flag == BST_OFF){
		bst_flag = BST_ON;
	}
	else{
		bst_flag = BST_OFF;
	}
	bst_display_handler();
}

static void paired_switch(void)
{
	static uint8_t paired_enable = 0;
	if(paired_enable == 0){
		paired_enable = 1;
		ui_paired_display();
		ui_show();
	}
	else{
		paired_enable = 0;
		ui_paired_clear();
		ui_show();
	}
}

static void eco_switch(void)
{
	eco_cft_flag++;
	if(eco_cft_flag > 2){
		eco_cft_flag = 0;
	}
	eco_cft_display_handler();
}

void battery_low_handler(void)
{
	static uint8_t blink = 0;
	if(battery_level < 1){
		if(blink == 0){
			blink = 1;
			ui_battery_level_display(battery_level);
		}
		else{
			blink = 0;
			ui_battery_clear();
		}
		ui_show();
	}

}
/*********************************************************************/
/*********************************************************************/
//AUTO
static uint8_t auto_temperature_display_delay = 0;
static uint8_t auto_eco_delay_timer = 0;
void auto_state_enter(void)
{
	auto_temperature_display_delay = 0;
	auto_manu_flag = AUTO_ON;
	struct tm ttime;
	ttime = rtc_time_get_time();
	ui_time_display(ttime.tm_hour, ttime.tm_min);
	ui_week_display(ttime.tm_wday);
	function_icon_dispaly();
	ui_auto_display();
	ui_manu_clear();
	ui_battery_level_display(battery_level);
	ui_display_temperature(measurement_temperaturn->integer, measurement_temperaturn->decimal);
	ui_program_display(ttime.tm_hour+1);
	ui_show();
	control_timer_force();
}
void auto_state_exit(void)
{
}

void auto_state_key_eco_short_handler(void)
{
	eco_switch();
	switch (eco_cft_flag) {
	case ECO_CFT_OFF:
		eco_cft_timer_stop();
		ui_display_temperature(measurement_temperaturn->integer, measurement_temperaturn->decimal);
		break;

	case CFT_ON:
		eco_cft_timer_start();
		ui_display_temperature(comfortable_temperature->integer, comfortable_temperature->decimal);
		break;

	case ECO_ON:
		eco_cft_timer_start();
		ui_display_temperature(economic_temperature->integer, economic_temperature->decimal);
		break;
	default:
		break;
	}
	ui_show();
	auto_temperature_display_delay = 1;
	control_timer_force();
}

void auto_state_key_eco_long_handler(void)
{
	state_jump_to_state(&TIME_DATE_STATE_STRUCT);
}

void auto_state_key_boost_short_handler(void)
{
	boost_switch();
	control_timer_force();
}

void auto_state_key_boost_long_handler(void)
{
	paired_switch();
	control_timer_force();
}

void auto_state_key_auto_short_handler(void)
{
	state_jump_to_state(&MANU_STATE_STRUCT);
	control_timer_force();
}

void auto_state_key_auto_long_handler(void)
{
	LAST_STATE = &AUTO_STATE_STRUCT;
	state_jump_to_state(&CONFIG_STATE_STRUCT);
	control_timer_force();
}

void auto_state_key_all_long_handler(void)
{
	LAST_STATE = &AUTO_STATE_STRUCT;
	state_jump_to_state(&LOCK_STATE_STRUCT);
}

void auto_state_rtc_1sec_handler(void)
{
	if(auto_temperature_display_delay >= 1){
		auto_temperature_display_delay++;
	}
	if(auto_temperature_display_delay >= 4){
		auto_temperature_display_delay = 0;
		ui_display_temperature(measurement_temperaturn->integer, measurement_temperaturn->decimal);
		ui_show();
	}
	battery_low_handler();
}

STATE_EVENT_HANDLER auto_event_handler_table[] ={
	auto_state_key_eco_short_handler,
	auto_state_key_eco_long_handler,
	auto_state_key_boost_short_handler,
	auto_state_key_boost_long_handler,
	auto_state_key_auto_short_handler,
	auto_state_key_auto_long_handler,
	auto_state_key_all_long_handler,
	0,
	0,
	auto_state_rtc_1sec_handler
};

STATE_ST AUTO_STATE_STRUCT ={
	auto_state_enter,
	auto_state_exit,
	auto_event_handler_table,
	sizeof(auto_event_handler_table)/sizeof(STATE_EVENT_HANDLER)
};


/*********************************************************************/
/*********************************************************************/
//MANU
static uint8_t manu_temperature_display_delay = 0;
void manu_state_enter(void)
{
	manu_temperature_display_delay = 0;
	auto_manu_flag = MANU_ON;
	struct tm ttime;
	ttime = rtc_time_get_time();
	ui_time_display(ttime.tm_hour, ttime.tm_min);
	ui_week_display(ttime.tm_wday);
	function_icon_dispaly();
	ui_auto_clear();
	ui_manu_display();
	ui_battery_level_display(battery_level);
	ui_display_temperature(measurement_temperaturn->integer, measurement_temperaturn->decimal);
	ui_program_clear();
	ui_show();
	control_timer_force();
}

void manu_state_exit(void)
{
}

void manu_state_key_eco_short_handler(void)
{
	eco_switch();
	eco_cft_display_handler();
	switch (eco_cft_flag) {
	case ECO_CFT_OFF:
		eco_cft_timer_stop();
		ui_display_temperature(measurement_temperaturn->integer, measurement_temperaturn->decimal);
		break;

	case CFT_ON:
		eco_cft_timer_start();
		ui_display_temperature(comfortable_temperature->integer, comfortable_temperature->decimal);
		break;

	case ECO_ON:
		eco_cft_timer_start();
		ui_display_temperature(economic_temperature->integer, economic_temperature->decimal);
		break;
	default:
		break;
	}
	ui_show();
	manu_temperature_display_delay = 1;
	control_timer_force();
}

void manu_state_key_eco_long_handler(void)
{
	LAST_STATE = &MANU_STATE_STRUCT;
	state_jump_to_state(&HOLIDAY_STATE_STRUCT);
}

void manu_state_key_boost_short_handler(void)
{
	boost_switch();
	control_timer_force();
}

void manu_state_key_boost_long_handler(void)
{
	paired_switch();
	control_timer_force();
}

void manu_state_key_auto_short_handler(void)
{
	state_jump_to_state(&AUTO_STATE_STRUCT);
	control_timer_force();
}

void manu_state_key_auto_long_handler(void)
{
	LAST_STATE = &MANU_STATE_STRUCT;
	state_jump_to_state(&CONFIG_STATE_STRUCT);
}

void manu_state_key_all_long_handler(void)
{
	LAST_STATE = &MANU_STATE_STRUCT;
	state_jump_to_state(&LOCK_STATE_STRUCT);
}

void lcm_sleep(void);
void lcm_weakup(void);

void temperature_set_add(uint8_t* temperature_Integer, uint8_t* temperature_decimal)
{
	if (*temperature_Integer < 40) {
		if (*temperature_decimal) {
			(*temperature_Integer)++;
			*temperature_decimal = 0;
		} else {
			*temperature_decimal = 0x80;
		}
	}
}

void temperature_set_sub(uint8_t* temperature_Integer, uint8_t* temperature_decimal)
{
	if (*temperature_Integer > 0) {
		if (*temperature_decimal) {
			*temperature_decimal = 0;
		} else {
			(*temperature_Integer)--;
			*temperature_decimal = 0x80;
		}
	} else {
		if (*temperature_decimal) {
			*temperature_decimal = 0x00;
		}
	}
}

void manu_state_rotary_encoder_cw_handler(void)
{
	manu_temperature_display_delay = 1;
	eco_cft_flag = ECO_CFT_OFF;
	eco_cft_display_handler();
	temperature_set_add(&(manu_temperature->integer), &(manu_temperature->decimal));
	ui_display_temperature((manu_temperature->integer), (manu_temperature->decimal));
	ui_show();
	control_timer_force();
	//lcm_sleep();
}

void manu_state_rotary_encoder_ccw_handler(void)
{
	manu_temperature_display_delay = 1;
	eco_cft_flag = ECO_CFT_OFF;
	eco_cft_display_handler();
	temperature_set_sub(&(manu_temperature->integer), &(manu_temperature->decimal));
	ui_display_temperature((manu_temperature->integer), (manu_temperature->decimal));
	ui_show();
	control_timer_force();
	//lcm_weakup();
}

void manu_state_rtc_1sec_handler(void)
{
	if(manu_temperature_display_delay >= 1){
		manu_temperature_display_delay++;
	}
	if(manu_temperature_display_delay >= 4){
		manu_temperature_display_delay = 0;
		ui_display_temperature(measurement_temperaturn->integer, measurement_temperaturn->decimal);
		ui_show();
	}
	battery_low_handler();
}

STATE_EVENT_HANDLER manu_event_handler_table[] ={
	manu_state_key_eco_short_handler,
	manu_state_key_eco_long_handler,
	manu_state_key_boost_short_handler,
	manu_state_key_boost_long_handler,
	manu_state_key_auto_short_handler,
	manu_state_key_auto_long_handler,
	manu_state_key_all_long_handler,
	manu_state_rotary_encoder_cw_handler,
	manu_state_rotary_encoder_ccw_handler,
	manu_state_rtc_1sec_handler
};

STATE_ST MANU_STATE_STRUCT ={
	manu_state_enter,
	manu_state_exit,
	manu_event_handler_table,
	sizeof(manu_event_handler_table)/sizeof(STATE_EVENT_HANDLER)
};


/*********************************************************************/
static void childlock_icon_blink(void)
{
	uint8_t i;
 	for(i = 0; i < 3; i++){
 		ui_childlock_clear();
 		ui_show();
		delay_ms(100);
		ui_childlock_display();
		ui_show();
		delay_ms(100);
	}
}

void lock_state_enter(void)
{
	ui_childlock_display();
	ui_show();
	control_timer_force();
}

void lock_state_exit(void)
{
	ui_childlock_clear();
}

void lock_state_key_eco_short_handler(void)
{
	childlock_icon_blink();
}

void lock_state_key_boost_short_handler(void)
{
	childlock_icon_blink();
}

void lock_state_key_auto_short_handler(void)
{
	childlock_icon_blink();
}

void lock_state_key_all_long_handler(void)
{
	state_jump_to_state(LAST_STATE);
}

void lock_state_rotary_encoder_cw_handler(void)
{
	childlock_icon_blink();
}

void lock_state_rotary_encoder_ccw_handler(void)
{
	childlock_icon_blink();
}

void lock_state_rtc_1sec_handler(void)
{
	battery_low_handler();
}

STATE_EVENT_HANDLER lock_event_handler_table[] ={
	lock_state_key_eco_short_handler,
	0,
	lock_state_key_boost_short_handler,
	0,
	lock_state_key_auto_short_handler,
	0,
	lock_state_key_all_long_handler,
	lock_state_rotary_encoder_cw_handler,
	lock_state_rotary_encoder_ccw_handler,
	lock_state_rtc_1sec_handler
};

STATE_ST LOCK_STATE_STRUCT ={
	lock_state_enter,
	lock_state_exit,
	lock_event_handler_table,
	sizeof(lock_event_handler_table)/sizeof(STATE_EVENT_HANDLER)
};


/*********************************************************************/
void holiday_state_enter(void)
{
	manu_temperature_display_delay = 0;
	eco_cft_flag = ECO_CFT_OFF;
	hld_flag = HLD_ON;
	struct tm ttime;
	ttime = rtc_time_get_time();
	ui_time_display(ttime.tm_hour, ttime.tm_min);
	ui_week_display(ttime.tm_wday);
	function_icon_dispaly();
	ui_auto_clear();
	ui_manu_clear();
	ui_battery_level_display(battery_level);
	ui_display_temperature(measurement_temperaturn->integer, measurement_temperaturn->decimal);
	ui_program_clear();
	ui_show();
	control_timer_force();
}

void holiday_state_exit(void)
{
}

void holiday_state_key_eco_short_handler(void)
{
}

void holiday_state_key_boost_short_handler(void)
{
}

void holiday_state_key_auto_short_handler(void)
{
	hld_flag = HLD_OFF;
	state_jump_to_state(LAST_STATE);
}

void holiday_state_key_all_long_handler(void)
{
	LAST_STATE = &HOLIDAY_STATE_STRUCT;
	state_jump_to_state(&LOCK_STATE_STRUCT);
}

void holiday_state_rotary_encoder_cw_handler(void)
{
}

void holiday_state_rotary_encoder_ccw_handler(void)
{
}

void holiday_state_rtc_1sec_handler(void)
{
	battery_low_handler();
}

STATE_EVENT_HANDLER holiday_event_handler_table[] ={
	0,
	0,
	0,
	0,
	holiday_state_key_auto_short_handler,
	0,
	holiday_state_key_all_long_handler,
	0,
	0,
	holiday_state_rtc_1sec_handler
};

STATE_ST HOLIDAY_STATE_STRUCT ={
	holiday_state_enter,
	holiday_state_exit,
	holiday_event_handler_table,
	sizeof(holiday_event_handler_table)/sizeof(STATE_EVENT_HANDLER)
};

/*********************************************************************/
#define SET_TIMEOUT_DEF 120
static uint8_t time_date_set_pionter = 0;
static uint8_t time_date_set_timeout = 0;
static struct tm time_date_set_ttime;
void time_date_set_display(uint8_t pionter)
{
	switch(pionter){
		case 0:{
			ui_year_display(time_date_set_ttime.tm_year+1900);
		}
		break;
		case 1:{
			ui_month_display(time_date_set_ttime.tm_mon+1);
		}
		break;
		case 2:{
			ui_month_day_display(time_date_set_ttime.tm_mday);
		}
		break;
		case 3:{
			ui_hour_display(time_date_set_ttime.tm_hour);
		}
		break;
		case 4:{
			ui_minute_display(time_date_set_ttime.tm_min);
		}
		break;
		default:
			break;
	}
	ui_show();
}

void time_date_set_state_enter(void)
{
	time_date_set_timeout = 0;
	software_timer_disable();
	ui_clear_all();
	time_date_set_ttime = rtc_time_get_time();
	time_date_set_pionter = 0;
	time_date_set_display(time_date_set_pionter);
}

void time_date_set_state_exit(void)
{
	software_timer_enable();
	ui_clear_all();
}

void time_date_set_state_key_eco_short_handler(void)
{
	if(time_date_set_pionter > 0){
		time_date_set_pionter--;
	}
	ui_clear_all();
	time_date_set_display(time_date_set_pionter);
}

void time_date_set_state_key_boost_short_handler(void)
{
	rtc_time_set_time(time_date_set_ttime);
	time_date_set_pionter++;
	if(time_date_set_pionter >= 5){
		time_date_set_pionter = 0;
		state_jump_to_state(LAST_STATE);
	}
	else{
		ui_clear_all();
		time_date_set_display(time_date_set_pionter);
	}
}

void time_date_set_state_key_auto_short_handler(void)
{
	state_jump_to_state(LAST_STATE);
}

int daysOfMonth(int year, int month)
{
    switch(month)
    {
        case 4:
        case 6:
        case 9:
        case 11:
            return 30;
        case 2:
            if (year % 400 == 0 || year % 4 == 0 && year % 100 != 0)
                return 29;
            else
                return 28;
        default:
            return 31;
    }
}

void time_date_set_state_rotary_encoder_cw_handler(void)
{
	switch (time_date_set_pionter) {
	case 0: {
		time_date_set_ttime.tm_year++;
		if (time_date_set_ttime.tm_year > (2100-1900)) {
			time_date_set_ttime.tm_year = (2000-1900);
		}

	}
		break;
	case 1: {
		time_date_set_ttime.tm_mon++;
		if (time_date_set_ttime.tm_mon > 11) {
			time_date_set_ttime.tm_mon = 0;
		}
	}
		break;
	case 2: {
		time_date_set_ttime.tm_mday++;
		if (time_date_set_ttime.tm_mday > daysOfMonth(time_date_set_ttime.tm_year+1900, time_date_set_ttime.tm_mon+1)) {
			time_date_set_ttime.tm_mday = 1;
		}
	}
		break;
	case 3: {
		time_date_set_ttime.tm_hour++;
		if (time_date_set_ttime.tm_hour > 23) {
			time_date_set_ttime.tm_hour = 0;
		}
	}
		break;
	case 4: {
		time_date_set_ttime.tm_min++;
		if (time_date_set_ttime.tm_min > 59) {
			time_date_set_ttime.tm_min = 0;
		}
	}
		break;
	default:
		break;
	}
	time_date_set_display(time_date_set_pionter);
}

void time_date_set_state_rotary_encoder_ccw_handler(void)
{
	switch (time_date_set_pionter) {
	case 0: {
		time_date_set_ttime.tm_year--;
		if (time_date_set_ttime.tm_year < (2000-1900)) {
			time_date_set_ttime.tm_year = (2100-1900);
		}
	}
		break;
	case 1: {
		time_date_set_ttime.tm_mon--;
		if (time_date_set_ttime.tm_mon < 0) {
			time_date_set_ttime.tm_mon = 11;
		}
	}
		break;
	case 2: {
		time_date_set_ttime.tm_mday--;
		if (time_date_set_ttime.tm_mday < 1) {
			time_date_set_ttime.tm_mday = daysOfMonth(time_date_set_ttime.tm_year+1900, time_date_set_ttime.tm_mon+1);
		}
	}
		break;
	case 3: {
		time_date_set_ttime.tm_hour--;
		if (time_date_set_ttime.tm_hour < 0) {
			time_date_set_ttime.tm_hour = 23;
		}

	}
		break;
	case 4: {
		time_date_set_ttime.tm_min--;
		if (time_date_set_ttime.tm_min < 0) {
			time_date_set_ttime.tm_min = 59;
		}
	}
		break;
	default:
		break;
	}
	time_date_set_display(time_date_set_pionter);
}

void time_date_set_state_rtc_1sec_handler(void)
{
	time_date_set_timeout++;
	if(time_date_set_timeout >= SET_TIMEOUT_DEF){
		time_date_set_timeout = 0;
		state_jump_to_state(LAST_STATE);
	}
}

STATE_EVENT_HANDLER time_date_set_event_handler_table[] ={
	time_date_set_state_key_eco_short_handler,
	0,
	time_date_set_state_key_boost_short_handler,
	0,
	time_date_set_state_key_auto_short_handler,
	0,
	0,
	time_date_set_state_rotary_encoder_cw_handler,
	time_date_set_state_rotary_encoder_ccw_handler,
	time_date_set_state_rtc_1sec_handler
};

STATE_ST TIME_DATE_STATE_STRUCT ={
	time_date_set_state_enter,
	time_date_set_state_exit,
	time_date_set_event_handler_table,
	sizeof(time_date_set_event_handler_table)/sizeof(STATE_EVENT_HANDLER)
};

/*********************************************************************/
static uint8_t config_set_pionter = 0;
static uint8_t config_set_timeout = 0;
void config_set_display_icon(uint8_t pionter)
{
	switch (pionter) {
	case 0: {
		ui_comfortable_display();
	}
		break;
	case 1: {
		ui_economic_display();
	}
		break;
	case 2: {
		ui_holiday_display();
	}
		break;
	case 3:
	case 4: {
		ui_openwindow_display();
	}
		break;
	case 5:
	case 6: {
		ui_frost_display();
	}
		break;
	default:
		break;
	}
	ui_show();
}

void pram_ON_OFF_switch(uint8_t* flag)
{
	if(*flag == 0){
		*flag = 1;
	}
	else{
		*flag = 0;
	}
}

void pram_ON_OFF_dislay(uint8_t flag)
{
	if(flag == 1){
		ui_OFF_clear();
		ui_ON_display();
	}
	else{
		ui_OFF_clear();
		ui_OFF_display();
	}
}

void config_set_set_parm_add(uint8_t pionter, uint8_t pram[][2])
{
	if(config_set_pram[pionter][1] == 0xff){
		pram_ON_OFF_switch(&pram[pionter][0]);
	}
	else{
		if(pionter == 4){
			if(pram[pionter][0] < 60){
				pram[pionter][0]++;
			}
		}
		else{
			if(pram[pionter][0] < 30){
				temperature_set_add(&pram[pionter][0], &pram[pionter][1]);
			}
		}
	}
}

void config_set_set_parm_sub(uint8_t pionter, uint8_t pram[][2])
{
	if(config_set_pram[pionter][1] == 0xff){
		pram_ON_OFF_switch(&pram[pionter][0]);
	}
	else{
		if(pionter == 4){
			if(pram[pionter][0] > 1){
				pram[pionter][0]--;
			}
		}
		else{
			if(pram[pionter][0] > 7){
				temperature_set_sub(&pram[pionter][0], &pram[pionter][1]);
			}
		}
	}
}

void config_set_display_parm(uint8_t pionter, uint8_t pram[][2])
{
	switch (pionter) {
	case 0:
		ui_display_temperature(pram[pionter][0], pram[pionter][1]);
		break;
	case 1:
		ui_display_temperature(pram[pionter][0], pram[pionter][1]);
		break;
	case 2:
		ui_display_temperature(pram[pionter][0], pram[pionter][1]);
		break;
	case 3:
		pram_ON_OFF_dislay(pram[pionter][0]);
		break;
	case 4:
		ui_num_2_display(pram[pionter][0]);
		break;
	case 5:
		pram_ON_OFF_dislay(pram[pionter][0]);
		break;
	case 6:
		ui_display_temperature(pram[pionter][0], pram[pionter][1]);
		break;
	default:
		break;
	}
	ui_show();
}

void config_set_state_enter(void)
{
	config_set_timeout = 0;
	software_timer_disable();
	ui_clear_all();
	config_set_pionter = 0;
	config_set_display_icon(config_set_pionter);
	config_set_display_parm(config_set_pionter, config_set_pram);
}

void config_set_state_exit(void)
{
	ui_clear_all();
	software_timer_enable();
}

void config_set_state_key_eco_short_handler(void)
{
	if(config_set_pionter > 0){
		config_set_pionter--;
	}
	ui_clear_all();
	config_set_display_icon(config_set_pionter);
	config_set_display_parm(config_set_pionter, config_set_pram);
}

void config_set_state_key_boost_short_handler(void)
{
	config_set_pionter++;
	if(config_set_pionter >= 7){
		config_set_pionter = 0;
		state_jump_to_state(LAST_STATE);
	}
	else{
		ui_clear_all();
		config_set_display_icon(config_set_pionter);
		config_set_display_parm(config_set_pionter, config_set_pram);
	}
}

void config_set_state_key_auto_short_handler(void)
{
	state_jump_to_state(LAST_STATE);
}

void config_set_state_rotary_encoder_cw_handler(void)
{
	config_set_set_parm_add(config_set_pionter, config_set_pram);
	config_set_display_parm(config_set_pionter, config_set_pram);
}

void config_set_state_rotary_encoder_ccw_handler(void)
{
	config_set_set_parm_sub(config_set_pionter, config_set_pram);
	config_set_display_parm(config_set_pionter, config_set_pram);
}

void config_set_state_rtc_1sec_handler(void)
{
	config_set_timeout++;
	if(config_set_timeout >= SET_TIMEOUT_DEF){
		config_set_timeout = 0;
		state_jump_to_state(LAST_STATE);
	}
}

STATE_EVENT_HANDLER config_set_event_handler_table[] ={
	config_set_state_key_eco_short_handler,
	0,
	config_set_state_key_boost_short_handler,
	0,
	config_set_state_key_auto_short_handler,
	0,
	0,
	config_set_state_rotary_encoder_cw_handler,
	config_set_state_rotary_encoder_ccw_handler,
	config_set_state_rtc_1sec_handler
};

STATE_ST CONFIG_STATE_STRUCT ={
	config_set_state_enter,
	config_set_state_exit,
	config_set_event_handler_table,
	sizeof(config_set_event_handler_table)/sizeof(STATE_EVENT_HANDLER)
};
