///*
// * stts751.c
// *
// *  Created on: 2017��4��5��
// *      Author: zengjie
// */

///***************************************************************************//**
// * @file
// * @brief Driver for the STTS7510 temperature sensor
// * @author Fan Ma
// * @date 20170204
// * @version 1.0
// * @company Leedarson Lighting
// *
// ******************************************************************************/


#include <stdint.h>
#include "i2c.h"

//define Register                    	//|Device register name  |Size  |Type |Power-up default value(binary)
#define STTS751_TemperatureH_Addr 		0x00 	//Temperature-value-high-byte 8 R undefined
#define STTS751_Status_Addr 			0x01 	//Status 8 R undefined
#define STTS751_TemperatureL_Addr 		0x02 	//Temperature-value-low-byte 8 R undefined
#define STTS751_Configuration_Addr 		0x03 	//Configuration 8 R/W 0000 0000
#define STTS751_Conversion_Addr 		0x04 	//Conversion-rate 8 R/W 0000 0100
#define STTS751_HighLimitH_Addr 		0x05 	//Temperature-high-limit-high-byte 8 R/W 0101 0101 (85 ��C)
#define STTS751_HighLimitL_Addr 		0x06 	//Temperature-high-limit-low-byte 8 R/W 0000 0000
#define STTS751_LowLimitH_Addr  		0x07 	//Temperature-low-limit-high-byte 8 R/W 0000 0000 (0 ��C)
#define STTS751_LowLimitL_Addr  		0x08 	//Temperature-low-limit-low-byte 8 R/W 0000 0000
#define STTS751_OneShot_Addr    		0x0F 	//One-shot 8 W N/A
#define STTS751_THERM_Limit_Addr 		0x20 	//THERM-limit 8 R/W 0101 0101 (85 ��C)
#define STTS751_THERM_Hysteresis_Addr 	0x21 	//THERM-hysteresis 8 R/W 0000 1010 (10 ��C)
#define STTS751_SMBusTimeoutEnable_Addr 0x22 	//SMBus-timeout-enable 8 R/W 1000 0000 (Enabled)
#define STTS751_ProductID_Addr 			0xFD 	//Product-ID-register 8 R STTS751-0[0000 0000]STTS751-1[0000 0001]
#define STTS751_ManufacturerID_Addr 	0xFE 	//Manufacturer-ID 8 R 0101 0011 (53h)
#define STTS751_RevisionNumber_Addr 	0xFF 	//Revision-number 8 R 0000 0001

// Reg conf
#define STTS751_EVENT_ENABLE				0<<7
#define STTS751_EVENT_DISABLE				1<<7

#define STTS751_MODE_RUN					0<<6
#define STTS751_MODE_STOP					1<<6

#define STTS751_CONVERSION_RESOLUTION_10B	0<<2
#define STTS751_CONVERSION_RESOLUTION_11B	1<<2
#define STTS751_CONVERSION_RESOLUTION_9B	2<<2
#define STTS751_CONVERSION_RESOLUTION_12B	3<<2
// Reg end

#define STTS751_ADDR_READ 		0x71//20k ~ 0111 000b
#define STTS751_ADDR_WRITE 		0x70//20k ~ 0111 000b
//
///** Available registers in STTS7510 sensor device */
//typedef enum
//{
//  temperatureH = 0,
//  status = 1,
//  temperatureL = 2,
//  configuration =  3,
//  conversion = 4,
//  highLimitH = 5,
//  highLimitL = 6,
//  lowLimitH = 7,
//  lowLimitL = 8,
//  oneShot = 0x0f,
//  tHERM_Limit = 0x20,
//  tHERM_Hysteresis = 0x21,
//  sMBusTimeoutEnable = 0x22,
//  productID = 0xfd,
//  manufacturerID = 0xfe,
//  revisionNumber = 0xff
//} STTS7510_Register_TypeDef;

///**************************************************************************//**
// * @brief
// *  basic configure the temperature measurement sensor(configuration register).
//#define Configuration 		0x03
//Configuration 8 R/W 0000 0000
//
//MASK1: 		[bit 7]
//					0: EVENT is enabled. Any out-of-limit condition asserts the EVENT pin (active low).
//					1: EVENT is disabled.
//RUN/STOP: 	[bit 6]
//					0: Device is running in continuous conversion mode.
//					1: Device is in standby mode drawing minimum power.
//					The RUN/STOP bit controls temperature conversions by the ADC. When this bit is 0, the
//					ADC converts temperatures in continuous mode, at a rate as selected by the Conversion
//					Rate register (Section 4.7). When the RUN/STOP bit is 1, the ADC will be in standby mode,
//					thus reducing current supply significantly.
//
//Tres1:Tres0 [bits 3 and 2]
//					These bits select one of the four programmable resolutions for temperature data on the
//					STTS751 providing resolutions down to 0.0625 C/LSB. The default resolution is 10 bits,
//					0.25 C/LSB.
//
//RFU: 		[bit 410] Not used - reserved
//
//Conversion resolution:
//--------------------------------------------------------
//Tres1:Tres0 |Temperature resolution |LSB step size (C)|
//--------------------------------------------------------|
//0		0 	|		10 bits 		|	0.25(default)	|
//0		1 	|		11 bits 		|	0.125			|
//1		1 	|		12 bits 		|	0.0625			|
//1		0 	|		9 bits 			|	0.5				|
//---------------------------------------------------------
// *****************************************************************************/
static int32_t stts751_set_configuration(void)
{
	uint8_t data = 0;
	int32_t ret;
	//data = STTS751_EVENT_DISABLE | STTS751_MODE_STOP | STTS751_CONVERSION_RESOLUTION_9B;
	data = STTS751_EVENT_DISABLE | STTS751_MODE_STOP | STTS751_CONVERSION_RESOLUTION_12B;
	ret = i2c_device_reg_write(I2C0, STTS751_ADDR_WRITE, STTS751_Configuration_Addr, data);
	return ret;
}
//
//
///**************************************************************************//**
// * @brief
// *  conversion configure the temperature measurement sensor(conversion register).
// #define Conversion 			0x04
// Conversion rate 8 R/W 0000 0100
// CONV[3:0](hex) |Conversions per second |Typical current (A) 	|Comment
//--------------------------------------------------------------------------
//	0 				0.0625 					15
//	1 				0.125
//	2 				0.25
//	3				 0.5
//	4 				1 						20
//	5 				2
//	6 				4
//	7 				8 						50
//	8 				16 											9, 10, or 11-bit resolutions only
//	9 				32 						125 				9 or 10-bit resolutions only
//	A-F 			reserved
//---------------------------------------------------------------------------------
// *****************************************************************************/
static int32_t stts751_set_conversion(void)
{
	uint8_t data = 4;//(0~9)
	int32_t ret;
	ret = i2c_device_reg_write(I2C0, STTS751_ADDR_WRITE, STTS751_Conversion_Addr, data);// 1/1sec, will be ignored in STOP mode
	return ret;
}
//
//
//
///**************************************************************************//**
// * @brief
// *  initialize the STTS7510 temperature measurement sensor.
// *****************************************************************************/
void stts751_init(void)
{
	i2c_init();

	//Disable Event PIN ; Stop Mode; Set Conversion Resolution 10Bit
	stts751_set_configuration();

	//Set Conversions per second 1, will be ignored in Stop mode
	stts751_set_conversion();
}
//
///**************************************************************************//**
// * @brief
// *  write one-shot register to start one measurement.
// #define OneShot    			0x0F
// One-shot 8 W N/A
// *****************************************************************************/
static int32_t stts751_set_oneshot(void) {
	int32_t ret = i2c_device_reg_write(I2C0, STTS751_ADDR_WRITE, STTS751_OneShot_Addr, 0); // write any data(e.g. 0) to one-shot register will start one measurement.
	return ret;
}

/**************************************************************************//**
 * @brief
 *  get product id from the sensor.
 * @param[in] data
 *   The data read from the sensor.
 * @return
 *   Returns 0 when read on success. Otherwise returns error codes
 *   based on the I2CDRV.
 *****************************************************************************/
int32_t stts751_get_manufacturerID(uint8_t *data)
{
	int32_t ret = i2c_device_reg_read(I2C0, STTS751_ADDR_READ, STTS751_ManufacturerID_Addr, data);
	if(ret != 0)
	{
		*data = 0;
	}
	return ret;
}
//
//
///**************************************************************************//**
// * @brief
// *  get configuration from the sensor(configuration register).
// * @param[in] data
// *   The data read from the sensor.
// * @return
// *   Returns 0 when read on success. Otherwise returns error codes
// *   based on the I2CDRV.
// *****************************************************************************/
//int32_t get_configuration(uint8_t *data)
//{
//	int32_t ret = i2c_reg_read(I2C0, Configuration_Addr, data);
//	if(ret != 0)
//	{
//		*data = 0;
//	}
//	return ret;
//}
//
//
///**************************************************************************//**
// * @brief
// *  get conversion from the sensor(conversion register).
// * @param[in] data
// *   The data read from the sensor.
// * @return
// *   Returns 0 when read on success. Otherwise returns error codes
// *   based on the I2CDRV.
// *****************************************************************************/
//int32_t get_conversion(uint8_t *data)
//{
//	int32_t ret= i2c_reg_read(I2C0, Conversion_Addr, data);
//	if(ret != 0)
//	{
//		*data = 0;
//	}
//	return ret;
//}
//
///**************************************************************************//**
// * @brief
// *  start one time temperature measure.
// * @return
// *   Returns 0 when read on success. Otherwise returns error codes
// *   based on the I2CDRV.
// *****************************************************************************/
int32_t stts751_start_one_temperature_measure(void) {
	int32_t ret = stts751_set_oneshot();
	if (ret != 0) {
		ret = stts751_set_oneshot(); // restart one time in case start failure.
	}

	return ret;
}


///**************************************************************************//**
// * @brief
// *  get measured temperature from the sensor(temperatureH register and temperatureL register).
// * @param[in] data
// *   The data read from the sensor, two byte: data[0] = high; data[1] = low;
// * @return
// *   Returns 0 when read on success. Otherwise returns error codes
// *   based on the I2CDRV.
// *****************************************************************************/
int32_t stts751_get_measured_temperature(uint8_t *data) {
	int32_t ret1, ret2;
	ret1 = i2c_device_reg_read(I2C0, STTS751_ADDR_READ, STTS751_TemperatureH_Addr, data);
	ret2 = i2c_device_reg_read(I2C0, STTS751_ADDR_READ, STTS751_TemperatureL_Addr, (data + 1));
	if (ret1 != 0) {
		ret1 = i2c_device_reg_read(I2C0, STTS751_ADDR_READ, STTS751_TemperatureH_Addr, data);
		if (ret1 != 0) {
			*data = 0;
			return ret1;
		}
	}

	if (ret2 != 0) {
		ret2 = i2c_device_reg_read(I2C0, STTS751_ADDR_READ, STTS751_TemperatureH_Addr, (data + 1));
		if (ret2 != 0) {
			*data = 0;
			return ret2;
		}
	}

	return 0; //measured successful
}

//e.g.
//stts751_start_one_temperature_measure();
//delay_ms(1);
//stts751_get_measured_temperature(&t);

