
#include "hal.h"

void WakeUp_Timer_init(void)
{
//-----------------------------------------------------
//	WKT initial 	
//-----------------------------------------------------	
	set_WKPS2;  //101 -> 512 Pre-scale = 1/1024;
	clr_WKPS1;  
	set_WKPS0;   

	RWK = 60; //60 -> 10s
	set_EWKT;													// enable WKT interrupt
	set_WKTR; 												// Wake-up timer run 
	
}

/***********************************************************************
	WDT CONFIG enable 
	warning : this macro is only when ICP not enable CONFIG WDT function
	copy this marco code to you code to enable WDT reset.
************************************************************************/
/*
void Enable_WDT_Reset_Config(void)
{
	  set_IAPEN;
    IAPAL = 0x04;
    IAPAH = 0x00;
    IAPFD = 0x0F;
    IAPCN = 0xE1;
    set_CFUEN;
    set_IAPGO;                                  //trigger IAP
		while((CHPCON&SET_BIT6)==SET_BIT6);          //check IAPFF (CHPCON.6)
    clr_CFUEN;
    clr_IAPEN;
}

void wdt_init(void)
{
	TA=0xAA;TA=0x55;WDCON=0x07;						//Setting WDT prescale 
	set_WDCLR;														//Clear WDT timer
	while((WDCON|~SET_BIT6)==0xFF);				//confirm WDT clear is ok before into power down mode
//	EA = 1;
	set_WDTR;	
}
*/
/*
void delay_20us(volatile uint8_t nCount)	//20us
{
	volatile uint8_t i;
	for(; nCount != 0; nCount--){
		for(i = 0; i < 11; i++);
	}
}


void delay_1ms(volatile uint8_t nCount)
{
	volatile uint8_t i;
	for(; nCount != 0; nCount--){
		for(i = 0; i < 250; i++);
		for(i = 0; i < 250; i++);
		for(i = 0; i < 115; i++);
	}
}
*/
void gpio_mode_config(uint8_t port, uint8_t pin, uint8_t mode)
{
	switch(port){
		case 0:
			switch(mode){
				case GPIO_MODE_BD:
					reset_bit(P0M1, pin);
					reset_bit(P0M2, pin);
					break;
				case GPIO_MODE_PP:
					reset_bit(P0M1, pin);
					set_bit(P0M2, pin);
					break;
				case GPIO_MODE_HI:
					set_bit(P0M1, pin);
					reset_bit(P0M2, pin);
					break;
				case GPIO_MODE_OD:
					set_bit(P0M1, pin);
					set_bit(P0M2, pin);
					break;
				default:
					break;
			}
			break;

		case 1:
			switch(mode){
				case GPIO_MODE_BD:
					reset_bit(P1M1, pin);
					reset_bit(P1M2, pin);
					break;
				case GPIO_MODE_PP:
					reset_bit(P1M1, pin);
					set_bit(P1M2, pin);
					break;
				case GPIO_MODE_HI:
					set_bit(P1M1, pin);
					reset_bit(P1M2, pin);
					break;
				case GPIO_MODE_OD:
					set_bit(P1M1, pin);
					set_bit(P1M2, pin);
					break;
				default:
					break;
			}
			break;

//		case 2:
//			switch(mode){
//				case GPIO_MODE_BD:
//					reset_bit(P2M1, pin);
//					reset_bit(P2M2, pin);
//					break;
//				case GPIO_MODE_PP:
//					reset_bit(P2M1, pin);
//					set_bit(P2M2, pin);
//					break;
//				case GPIO_MODE_HI:
//					set_bit(P2M1, pin);
//					reset_bit(P2M2, pin);
//					break;
//				case GPIO_MODE_OD:
//					set_bit(P2M1, pin);
//					set_bit(P2M2, pin);
//					break;
//				default:
//					break;
//			}
//			break;
		
		case 3:
			switch(mode){
				case GPIO_MODE_BD:
					reset_bit(P3M1, pin);
					reset_bit(P3M2, pin);
					break;
				case GPIO_MODE_PP:
					reset_bit(P3M1, pin);
					set_bit(P3M2, pin);
					break;
				case GPIO_MODE_HI:
					set_bit(P3M1, pin);
					reset_bit(P3M2, pin);
					break;
				case GPIO_MODE_OD:
					set_bit(P3M1, pin);
					set_bit(P3M2, pin);
					break;
				default:
					break;
			}
			break;
		default:
			break;
	}
}
/*
//H, R = 1; L, F = 0;
//level = 1; edge = 0;
void gpio_int_config(uint8_t port, uint8_t pin, uint8_t level_edge, uint8_t H_L_R_F)
{
	switch(port){
	case 0:
		Enable_INT_Port0;
		break;
	case 1:
		Enable_INT_Port1;
		break;
	case 2:
		Enable_INT_Port2;
		break;
	case 3:
		Enable_INT_Port3;
		break;
	default:
		break;
	}
	
	if(level_edge == 0){
		if(H_L_R_F == 0){
			switch(pin){
			case 0:
				Enable_BIT0_FallEdge_Trig;
				break;
			case 1:
				Enable_BIT1_FallEdge_Trig;
				break;
			case 2:
				Enable_BIT2_FallEdge_Trig;
				break;
			case 3:
				Enable_BIT3_FallEdge_Trig;
				break;
			case 4:
				Enable_BIT4_FallEdge_Trig;
				break;
			case 5:
				Enable_BIT5_FallEdge_Trig;
				break;
			case 6:
				Enable_BIT6_FallEdge_Trig;
				break;
			case 7:
				Enable_BIT7_FallEdge_Trig;
				break;
			default:
				break;
			}	
		}
		else{
			switch(pin){
			case 0:
				Enable_BIT0_RasingEdge_Trig;
				break;
			case 1:
				Enable_BIT1_RasingEdge_Trig;
				break;
			case 2:
				Enable_BIT2_RasingEdge_Trig;
				break;
			case 3:
				Enable_BIT3_RasingEdge_Trig;
				break;
			case 4:
				Enable_BIT4_RasingEdge_Trig;
				break;
			case 5:
				Enable_BIT5_RasingEdge_Trig;
				break;
			case 6:
				Enable_BIT6_RasingEdge_Trig;
				break;
			case 7:
				Enable_BIT7_RasingEdge_Trig;
				break;
			default:
				break;
			}	
		}
	}	
	
	if(level_edge == 1){
		if(H_L_R_F == 0){
			switch(pin){
			case 0:
				Enable_BIT0_LowLevel_Trig;
				break;
			case 1:
				Enable_BIT1_LowLevel_Trig;
				break;
			case 2:
				Enable_BIT2_LowLevel_Trig;
				break;
			case 3:
				Enable_BIT3_LowLevel_Trig;
				break;
			case 4:
				Enable_BIT4_LowLevel_Trig;
				break;
			case 5:
				Enable_BIT5_LowLevel_Trig;
				break;
			case 6:
				Enable_BIT6_LowLevel_Trig;
				break;
			case 7:
				Enable_BIT7_LowLevel_Trig;
				break;
			default:
				break;
			}				
		}
		else{
			switch(pin){
			case 0:
				Enable_BIT0_HighLevel_Trig;
				break;
			case 1:
				Enable_BIT1_HighLevel_Trig;
				break;
			case 2:
				Enable_BIT2_HighLevel_Trig;
				break;
			case 3:
				Enable_BIT3_HighLevel_Trig;
				break;
			case 4:
				Enable_BIT4_HighLevel_Trig;
				break;
			case 5:
				Enable_BIT5_HighLevel_Trig;
				break;
			case 6:
				Enable_BIT6_HighLevel_Trig;
				break;
			case 7:
				Enable_BIT7_HighLevel_Trig;
				break;
			default:
				break;
			}	
		}
	}
}

void gpio_int_config_clean(void)
{
	PICON=0x00;PINEN=0x00;PIPEN=0x00;
}


//#define PWN_P 0X03e7 //0X03e7 -> 2k	//0X0063 20k //0X02D6 -> 2.75K // 0X01f3 -> 4k
**********************************************************************
 PWM frequency = Fpwm/((PWMPH,PWMPL) + 1) <Fpwm = Fsys/PWM_CLOCK_DIV>
  = (16MHz/8)/(0x7CF + 1)
  = 1KHz (1ms)
***********************************************************************
void pwm_init(uint16_t PWN_P)
{
	PWM_IMDEPENDENT_MODE;
	
	PWM_CLOCK_DIV_8;
	
	PWMPH = PWN_P/256;
	PWMPL = PWN_P%256;
	
	set_LOAD;
    set_PWMRUN;
}


void pwm_set(uint8_t channel, uint8_t pct)
{
	uint32_t temp = 0;
	
	if(pct > 100){
		return;
	}
	
	temp = PWMPH;
	temp <<= 8;
	temp |= PWMPL;
	

	temp = (temp*pct)/100;
	
	set_SFRPAGE;	
	switch(channel){
		case 0:
			PWM0H = temp/256;
			PWM0L = temp%256;
			break;
		case 1:
			PWM1H = temp/256;
			PWM1L = temp%256;
			break;
		case 2:
			PWM2H = temp/256;
			PWM2L = temp%256;
			break;
		case 3:
			PWM3H = temp/256;
			PWM3L = temp%256;
			break;	
		case 4:
			PWM4H = temp/256;
			PWM4L = temp%256;
			break;	
		case 5:
			PWM5H = temp/256;
			PWM5L = temp%256;
			break;	
	
	}
	clr_SFRPAGE;	
	
	set_LOAD;
}
*/
void tim3_init_10ms(void)
{
	//111 1/128分频 16Mhz/128 -> 0.125Mhz
	set_T3PS2;
	set_T3PS1;
	set_T3PS0;	
	
	//0.125Mhz -> 目标时间 1=8us ... 125=1ms ...
    RH3 = (65536-1250)/256;		//RELOAD_VALUE_H; 
	RL3 = (65536-1250)%256;		//RELOAD_VALUE_L;    
	
	set_ET3;                                    //enable Timer3 interrupt
//    set_EA;                                     //enable interrupts
//	set_TR3;                                    //Timer3 run
}

void tim3_set_10ms(uint16_t time)
{
	time *= 1250;
	
	//1/8 -> 0.125Mhz -> 目标时间 1=8us ... 125=1ms ...
    RH3 = (65536-time)/256;		//RELOAD_VALUE_H; 
	RL3 = (65536-time)%256;		//RELOAD_VALUE_L;    
}

/*
void timer0_init_20us(void)
{
	TIMER0_MODE2_ENABLE;
    clr_T0M;	//时钟源 Fsys/12 -> 16Mhz/12 -> 4/3Mhz
	
    TH0 = (256-27);            
    TL0 = (256-27);    
    
    set_ET0;                                    //enable Timer0 interrupt
}
*/

void timer0_init_40us(void)
{
	TIMER0_MODE2_ENABLE;
    clr_T0M;	//时钟源 Fsys/12 -> 16Mhz/12 -> 4/3Mhz
	
    //TH0 = (256-27);  	//20us          
	//TL0 = (256-27);    
    	
    TH0 = (256-53);            
	TL0 = (256-53);    
    
    set_ET0;                                    //enable Timer0 interrupt
}

/*
#define BANDGAP_VOLT	122
//TARGET_v = TARGET_ad * BANDGAP_v / BANDGAP_ad
uint8_t adc_get_volt(uint8_t channel)
{
	uint16_t BandGapAD;
	uint32_t targetAD;
	
	Enable_ADC_BandGap;
	clr_ADCF;
	set_ADCS;																	// Each time ADC start trig signal
	while(ADCF == 0);
	BandGapAD =  ADCRH;
	BandGapAD <<=  8;
	BandGapAD |=  ADCRL<<4;	
	BandGapAD >>= 4;
	
	adc_disable();		//结束一次采样要关闭adc，否则会影响下一次不同通道的采样结果
	
	switch(channel){
		case 0:
			Enable_ADC_AIN0;
			break;
		case 1:
			Enable_ADC_AIN1;
			break;
		case 2:
			Enable_ADC_AIN2;
			break;
		case 3:
			Enable_ADC_AIN3;
			break;
		case 4:
			Enable_ADC_AIN4;
			break;
		case 5:
			Enable_ADC_AIN5;
			break;
		case 6:
			Enable_ADC_AIN6;
			break;
		case 7:
			Enable_ADC_AIN7;
			break;
		default:
			Enable_ADC_BandGap;
			break;
	}
	clr_ADCF;
	set_ADCS;																	// Each time ADC start trig signal
	while(ADCF == 0);
	targetAD =  ADCRH;
	targetAD <<=  8;
	targetAD |=  ADCRL<<4;	
	targetAD >>= 4;
	
	adc_disable();
	
	targetAD = targetAD*BANDGAP_VOLT;
	targetAD = targetAD/BandGapAD;
	
	return targetAD;
}
*/
/*
void write_DATAFLASH_BYTE(UINT16 u16_addr,UINT8 u8_data)
{
	UINT8 looptmp=0,u8_addrl_r;
	unsigned char code *cd_longaddr;
	unsigned char xdata *xd_tmp;
	
//Check page start address
	u8_addrl_r = u16_addr;
	if (u8_addrl_r<0x80)
	{
		u8_addrl_r = 0;
	}
	else 
	{
		u8_addrl_r = 0x80;
	}
//Save APROM data to XRAM
	xd_tmp = 0x80;
	cd_longaddr = (u16_addr&0xff00)+u8_addrl_r;	
	while (xd_tmp !=0x100)
	{
		*xd_tmp = *cd_longaddr;
		looptmp++;
		xd_tmp++;
		cd_longaddr++;
	}
// Modify customer data in XRAM
	u8_addrl_r = u16_addr;
	if (u8_addrl_r<0x80)
	{
		xd_tmp = u8_addrl_r+0x80;
	}
	else
	{
		xd_tmp = u8_addrl_r+0;
	}
	*xd_tmp = u8_data;
//Erase APROM DATAFLASH page
	IAPAL = u16_addr;
	IAPAH = u16_addr>>8;
	IAPFD = 0xFF;
	set_IAPEN; 
	set_APUEN;
    IAPCN = 0x22; 		
	set_IAPGO; 
//Save changed RAM data to APROM DATAFLASH
	u8_addrl_r = u16_addr;
	if (u8_addrl_r<0x80)
	{
		u8_addrl_r =0;
	}
	else
	{
		u8_addrl_r = 0x80;
	}
	xd_tmp = 0x80;
	IAPAL = u8_addrl_r;
    IAPAH = u16_addr>>8;
	set_IAPEN; 
	set_APUEN;
	IAPCN = 0x21;
	while (xd_tmp !=0xFF)
	{
		IAPFD = *xd_tmp;
		set_IAPGO;
		IAPAL++;
		xd_tmp++;
	}
	clr_APUEN;
	clr_IAPEN;
}	

UINT8 read_APROM_BYTE(UINT16 code *u16_addr)
{
	UINT8 rdata;
	rdata = *u16_addr>>8;
	return rdata;
}
*/
