#ifndef __HAL_H
#define __HAL_H

#include "type.h"
#include "N76E003.h"
#include "Common.h"
#include "SFR_Macro.h"
#include <string.h>

#define GPIO_MODE_BD 0	//BIDIRECTIONAL
#define GPIO_MODE_PP 1
#define GPIO_MODE_HI 2	//HIGH IMPEDANCE
#define GPIO_MODE_OD 3

/******************
M1 	0	0	1	1
M2 	0	1	0	1
	准	推	高	开
******************/

#define set_bit(byte, bit)			byte |= 1<<bit
#define reset_bit(byte, bit)		byte &= ~(1<<bit)
#define get_bit(byte, bit)			((byte & (1<<bit))>>bit)

/*******************************************************
#if(HW_VER == 5)
#define PIN_NC_MODE				GPIO_MODE_PP
#define PIN_NC0					P20		//推挽输出 输出1（悬空脚处理）
#define PIN_NC0_PORT			2
#define PIN_NC0_PIN				0

#define PIN_NC1					P02		//推挽输出 输出1（悬空脚处理）
#define PIN_NC1_PORT			0
#define PIN_NC1_PIN				2

#define PIN_NC2					P10		//推挽输出 输出1（悬空脚处理）
#define PIN_NC2_PORT			1
#define PIN_NC2_PIN				0

#define PIN_NC3					P16		//推挽输出 输出1（悬空脚处理）
#define PIN_NC3_PORT			1
#define PIN_NC3_PIN				6

#define PIN_LCD_DATA			P12		//推挽输出
#define PIN_LCD_DATA_PORT		1
#define PIN_LCD_DATA_PIN		2
#define PIN_LCD_DATA_MODE		GPIO_MODE_PP

#define PIN_LCD_WR				P13		//推挽输出
#define PIN_LCD_WR_PORT			1
#define PIN_LCD_WR_PIN			3
#define PIN_LCD_WR_MODE			GPIO_MODE_PP

#define PIN_LCD_CS				P14		//推挽输出
#define PIN_LCD_CS_PORT			1
#define PIN_LCD_CS_PIN			4
#define PIN_LCD_CS_MODE			GPIO_MODE_PP

#define PIN_LCD_BL				P11		//推挽输出
#define PIN_LCD_BL_PORT			1
#define PIN_LCD_BL_PIN			1
#define PIN_LCD_BL_MODE			GPIO_MODE_PP

#define PIN_KEY_MODE			P04		//准双向 输出1（上拉）
#define PIN_KEY_MODE_PORT		0
#define PIN_KEY_MODE_PIN		4
#define PIN_KEY_MODE_MODE		GPIO_MODE_BD

#define PIN_KEY_SEND			P07		//准双向 输出1（上拉）
#define PIN_KEY_SEND_PORT		0
#define PIN_KEY_SEND_PIN		7
#define PIN_KEY_SEND_MODE		GPIO_MODE_BD

#define PIN_KEY_UP				P00		//准双向 输出1（上拉）
#define PIN_KEY_UP_PORT			0
#define PIN_KEY_UP_PIN			0
#define PIN_KEY_UP_MODE			GPIO_MODE_BD

#define PIN_KEY_DOWN			P01		//准双向 输出1（上拉）
#define PIN_KEY_DOWN_PORT		0
#define PIN_KEY_DOWN_PIN		1
#define PIN_KEY_DOWN_MODE		GPIO_MODE_BD

#define PIN_KEY_DOG				P03		//准双向 输出1（上拉）
#define PIN_KEY_DOG_PORT		0
#define PIN_KEY_DOG_PIN			3
#define PIN_KEY_DOG_MODE		GPIO_MODE_BD

#define PIN_LED_SEND			P17		//推挽输出
#define PIN_LED_SEND_PORT		1
#define PIN_LED_SEND_PIN		7
#define PIN_LED_SEND_MODE		GPIO_MODE_PP

#define PIN_BEEP_KEY			PIN_NC1		//推挽输出
#define PIN_BEEP_KEY_PORT		PIN_NC1_PORT
#define PIN_BEEP_KEY_PIN		PIN_NC1_PIN
#define PIN_BEEP_KEY_MODE		GPIO_MODE_PP

#define PIN_RF_SEND_DATA		P15		//推挽输出
#define PIN_RF_SEND_DATA_PORT	1
#define PIN_RF_SEND_DATA_PIN	5
#define PIN_RF_SEND_DATA_MODE	GPIO_MODE_PP

#define PIN_RF_EN				P05		//推挽输出
#define PIN_RF_EN_PORT			0
#define PIN_RF_EN_PIN			5
#define PIN_RF_EN_MODE			GPIO_MODE_PP

#define PIN_RF_VOLT				PIN_NC1		//高阻输入
#define PIN_RF_VOLT_PORT		PIN_NC1_PORT
#define PIN_RF_VOLT_PIN			PIN_NC1_PIN
#define PIN_RF_VOLT_MODE		GPIO_MODE_HI

#define PIN_DC_VOLT				P06		//高阻输入
#define PIN_DC_VOLT_PORT		0
#define PIN_DC_VOLT_PIN			6
#define PIN_DC_VOLT_MODE		GPIO_MODE_HI

#define PIN_BATT_VOLT			P30		//高阻输入
#define PIN_BATT_VOLT_PORT		3
#define PIN_BATT_VOLT_PIN		0
#define PIN_BATT_VOLT_MODE		GPIO_MODE_HI

#define ADC_CHANNEL_BATT		1
#define ADC_CHANNEL_DC			3
#endif

/*******************************************************
#if(HW_VER == 6)
#define PIN_NC_MODE				GPIO_MODE_PP
#define PIN_NC0					P20		//推挽输出 输出1（悬空脚处理）
#define PIN_NC0_PORT			2
#define PIN_NC0_PIN				0

#define PIN_NC1					P15		//推挽输出 输出1（悬空脚处理）
#define PIN_NC1_PORT			1
#define PIN_NC1_PIN				5

#define PIN_NC2					P16		//推挽输出 输出1（悬空脚处理）
#define PIN_NC2_PORT			1
#define PIN_NC2_PIN				6

#define PIN_NC3					P17		//推挽输出 输出1（悬空脚处理）
#define PIN_NC3_PORT			1
#define PIN_NC3_PIN				7

#define PIN_LCD_DATA			P13		//推挽输出
#define PIN_LCD_DATA_PORT		1
#define PIN_LCD_DATA_PIN		3
#define PIN_LCD_DATA_MODE		GPIO_MODE_PP

#define PIN_LCD_WR				P12		//推挽输出
#define PIN_LCD_WR_PORT			1
#define PIN_LCD_WR_PIN			2
#define PIN_LCD_WR_MODE			GPIO_MODE_PP

#define PIN_LCD_CS				P10		//推挽输出
#define PIN_LCD_CS_PORT			1
#define PIN_LCD_CS_PIN			0
#define PIN_LCD_CS_MODE			GPIO_MODE_PP

#define PIN_LCD_BL				P30		//推挽输出
#define PIN_LCD_BL_PORT			3
#define PIN_LCD_BL_PIN			0
#define PIN_LCD_BL_MODE			GPIO_MODE_PP

#define PIN_KEY_MODE			P00		//准双向 输出1（上拉）
#define PIN_KEY_MODE_PORT		0
#define PIN_KEY_MODE_PIN		0
#define PIN_KEY_MODE_MODE		GPIO_MODE_BD

#define PIN_KEY_SEND			P01		//准双向 输出1（上拉）
#define PIN_KEY_SEND_PORT		0
#define PIN_KEY_SEND_PIN		1
#define PIN_KEY_SEND_MODE		GPIO_MODE_BD

#define PIN_KEY_UP				P05		//准双向 输出1（上拉）
#define PIN_KEY_UP_PORT			0
#define PIN_KEY_UP_PIN			5
#define PIN_KEY_UP_MODE			GPIO_MODE_BD

#define PIN_KEY_DOWN			P06		//准双向 输出1（上拉）
#define PIN_KEY_DOWN_PORT		0
#define PIN_KEY_DOWN_PIN		6
#define PIN_KEY_DOWN_MODE		GPIO_MODE_BD

#define PIN_KEY_DOG				P02		//准双向 输出1（上拉）
#define PIN_KEY_DOG_PORT		0
#define PIN_KEY_DOG_PIN			2
#define PIN_KEY_DOG_MODE		GPIO_MODE_BD

#define PIN_LED_SEND			P04		//推挽输出
#define PIN_LED_SEND_PORT		0
#define PIN_LED_SEND_PIN		4
#define PIN_LED_SEND_MODE		GPIO_MODE_PP

#define PIN_BEEP_KEY			PIN_NC1		//推挽输出
#define PIN_BEEP_KEY_PORT		PIN_NC1_PORT
#define PIN_BEEP_KEY_PIN		PIN_NC1_PIN
#define PIN_BEEP_KEY_MODE		GPIO_MODE_PP

#define PIN_RF_SEND_DATA		P03		//推挽输出
#define PIN_RF_SEND_DATA_PORT	0
#define PIN_RF_SEND_DATA_PIN	3
#define PIN_RF_SEND_DATA_MODE	GPIO_MODE_PP

#define PIN_RF_EN				P14		//推挽输出
#define PIN_RF_EN_PORT			1
#define PIN_RF_EN_PIN			4
#define PIN_RF_EN_MODE			GPIO_MODE_PP

#define PIN_RF_VOLT				PIN_NC1		//高阻输入
#define PIN_RF_VOLT_PORT		PIN_NC1_PORT
#define PIN_RF_VOLT_PIN			PIN_NC1_PIN
#define PIN_RF_VOLT_MODE		GPIO_MODE_HI

#define PIN_DC_VOLT				P07		//高阻输入
#define PIN_DC_VOLT_PORT		0
#define PIN_DC_VOLT_PIN			7
#define PIN_DC_VOLT_MODE		GPIO_MODE_HI

#define PIN_BATT_VOLT			P11	//高阻输入
#define PIN_BATT_VOLT_PORT		1
#define PIN_BATT_VOLT_PIN		1
#define PIN_BATT_VOLT_MODE		GPIO_MODE_HI

#define ADC_CHANNEL_BATT		7
#define ADC_CHANNEL_DC			2

#endif

/*******************************************************/
void WakeUp_Timer_init(void);

void write_DATAFLASH_BYTE(UINT16 u16_addr,UINT8 u8_data);
UINT8 read_APROM_BYTE(UINT16 code *u16_addr);

void delay_20us(volatile uint8_t nCount);
void delay_1ms(volatile uint8_t nCount);
	
void gpio_init(void);
void pwm_init(uint16_t PWN_P);
void pwm_set(uint8_t channel, uint8_t pct);

void timer0_init_40us(void);
void tim3_init_10ms(void);
void tim3_set_10ms(uint16_t time);

uint8_t adc_get_volt(uint8_t channel);

void Enable_WDT_Reset_Config(void);
void wdt_init(void);

void gpio_mode_config(uint8_t port, uint8_t pin, uint8_t mode);
void gpio_int_config(uint8_t port, uint8_t pin, uint8_t level_edge, uint8_t H_L_R_F);
void gpio_int_config_clean(void);


#define tim3_start()			set_ET3; set_TR3                               
#define tim3_stop()				clr_ET3; clr_TR3   

#define gpio_int_en()			set_EPI;                               
#define gpio_int_dis()			clr_EPI; 

#define sleep()					set_PD;
#define IDL()					set_IDL;


#define adc_enable()			set_ADCEN
#define adc_disable()			clr_ADCEN

#endif
