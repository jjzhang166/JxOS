
#ifndef __HAL_ADC_H
#define __HAL_ADC_H

#include "../lib/type.h"

#define ADC_TypeDef void

#define 	ADC0 		0
#define    	ADC1		1
#define    	ADC2		2
#define    	ADC3		3
#define    	ADC4		4
#define    	ADC5		5
#define    	ADC6		6

typedef enum
{
    ENABLE = 1,
    DISABLE,
}FunctionalState;

typedef enum
{
    RESET = 0,
    SET = !RESET
} FlagStatus;

/**
  * @brief  ADC Init structure definition
  */

typedef struct
{
  uint32_t ADC_Mode;                      /*!< Configures the ADC to operate in independent or
                                               dual mode.
                                               This parameter can be a value of @ref ADC_mode */

  FunctionalState ADC_ContinuousConvMode; /*!< Specifies whether the conversion is performed in
                                               Continuous or Single mode.
                                               This parameter can be set to ENABLE or DISABLE. */
}ADC_InitTypeDef;


void ADC_Init(ADC_TypeDef* ADCx, ADC_InitTypeDef* ADC_InitStruct);
void ADC_StartCalibration(ADC_TypeDef* ADCx);
FlagStatus ADC_GetCalibrationStatus(ADC_TypeDef* ADCx);
void ADC_Cmd(ADC_TypeDef* ADCx, FunctionalState NewState);
void ADC_ITConfig(ADC_TypeDef* ADCx, uint16_t ADC_IT, FunctionalState NewState);
void ADC_SoftwareStartConvCmd(ADC_TypeDef* ADCx, FunctionalState NewState);
FlagStatus ADC_GetSoftwareStartConvStatus(ADC_TypeDef* ADCx);
uint16_t ADC_GetConversionValue(ADC_TypeDef* ADCx);

#endif
