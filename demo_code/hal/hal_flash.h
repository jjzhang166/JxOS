
#ifndef __HAL_FLASH_H
#define __HAL_FLASH_H

#include "../lib/type.h"

typedef enum
{ 
  FLASH_BUSY = 1,
  FLASH_ERROR_PG,
  FLASH_ERROR_WRP,
  FLASH_COMPLETE,
  FLASH_TIMEOUT
}FLASH_Status;

FLASH_Status FLASH_ErasePage(uint32_t Page_Address);
FLASH_Status FLASH_ProgramWord(uint32_t Address, uint32_t Data);
FLASH_Status FLASH_ReadWord(uint32_t Address, uint32_t* Data);
FLASH_Status FLASH_ProgramByte(uint32_t Address, uint32_t Data);
FLASH_Status FLASH_ReadByte(uint32_t Address, uint32_t* Data);

#endif
