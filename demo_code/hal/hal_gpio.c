
#include "hal_gpio.h"
#include "N76E003.h"
/******************
M1 	0	0	1	1
M2 	0	1	0	1
	׼	��	��	��
******************/
void GPIO_Init(GPIO_TypeDef* GPIOx, GPIO_InitTypeDef* GPIO_InitStruct)
{
	switch((uint8_t)GPIOx){
		case 0:
			switch(GPIO_InitStruct->GPIO_Mode){
				case GPIO_Mode_BD:
					P0M1 &= ~(GPIO_InitStruct->GPIO_Pin);
					P0M2 &= ~(GPIO_InitStruct->GPIO_Pin);
					break;
				case GPIO_Mode_Out_PP:
					P0M1 &= ~(GPIO_InitStruct->GPIO_Pin);
					P0M2 |= (GPIO_InitStruct->GPIO_Pin);
					break;
				case GPIO_Mode_IN_FLOATING :
					P0M1 |= (GPIO_InitStruct->GPIO_Pin);
					P0M2 &= ~(GPIO_InitStruct->GPIO_Pin);
					break;
				case GPIO_Mode_Out_OD:
					P0M1 |= (GPIO_InitStruct->GPIO_Pin);
					P0M2 |= (GPIO_InitStruct->GPIO_Pin);
					break;
				default:
					break;
			}
			break;

		case 1:
			switch(GPIO_InitStruct->GPIO_Mode){
				case GPIO_Mode_BD:
					P1M1 &= ~(GPIO_InitStruct->GPIO_Pin);
					P1M2 &= ~(GPIO_InitStruct->GPIO_Pin);
					break;
				case GPIO_Mode_Out_PP:
					P1M1 &= ~(GPIO_InitStruct->GPIO_Pin);
					P1M2 |= (GPIO_InitStruct->GPIO_Pin);
					break;
				case GPIO_Mode_IN_FLOATING :
					P1M1 |= (GPIO_InitStruct->GPIO_Pin);
					P1M2 &= ~(GPIO_InitStruct->GPIO_Pin);
					break;
				case GPIO_Mode_Out_OD:
					P1M1 |= (GPIO_InitStruct->GPIO_Pin);
					P1M2 |= (GPIO_InitStruct->GPIO_Pin);
					break;
				default:
					break;
			}
			break;
/***
		case 2:
			switch(GPIO_InitStruct->GPIO_Mode){
				case GPIO_Mode_BD:
					P2M1 &= ~(GPIO_InitStruct->GPIO_Pin);
					P2M2 &= ~(GPIO_InitStruct->GPIO_Pin);
					break;
				case GPIO_Mode_Out_PP:
					P2M1 &= ~(GPIO_InitStruct->GPIO_Pin);
					P2M2 |= (GPIO_InitStruct->GPIO_Pin);
					break;
				case GPIO_Mode_IN_FLOATING :
					P2M1 |= (GPIO_InitStruct->GPIO_Pin);
					P2M2 &= ~(GPIO_InitStruct->GPIO_Pin);
					break;
				case GPIO_Mode_Out_OD:
					P2M1 |= (GPIO_InitStruct->GPIO_Pin);
					P2M2 |= (GPIO_InitStruct->GPIO_Pin);
					break;
				default:
					break;
			}
			break;
***/
		case 3:
			switch(GPIO_InitStruct->GPIO_Mode){
				case GPIO_Mode_BD:
					P3M1 &= ~(GPIO_InitStruct->GPIO_Pin);
					P3M2 &= ~(GPIO_InitStruct->GPIO_Pin);
					break;
				case GPIO_Mode_Out_PP:
					P3M1 &= ~(GPIO_InitStruct->GPIO_Pin);
					P3M2 |= (GPIO_InitStruct->GPIO_Pin);
					break;
				case GPIO_Mode_IN_FLOATING :
					P3M1 |= (GPIO_InitStruct->GPIO_Pin);
					P3M2 &= ~(GPIO_InitStruct->GPIO_Pin);
					break;
				case GPIO_Mode_Out_OD:
					P3M1 |= (GPIO_InitStruct->GPIO_Pin);
					P3M2 |= (GPIO_InitStruct->GPIO_Pin);
					break;
				default:
					break;
			}
			break;
		default:
			break;
	}
}

uint8_t GPIO_ReadInputDataBit(GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin)
{
	uint8_t ret = 0;
	switch((uint8_t)GPIOx){
		case 0:
			ret = P0&(GPIO_Pin);
			break;
		case 1:
			ret = P1&(GPIO_Pin);
			break;
		case 2:
			ret = P2&(GPIO_Pin);
			break;
		case 3:
			ret = P3&(GPIO_Pin);
			break;
		default:
			break;
	}
//    return ((ret != 0)? 1:0) ;	
	return (ret != 0);
}


uint16_t GPIO_ReadInputData(GPIO_TypeDef* GPIOx)
{
	uint16_t ret = 0;
	switch((uint8_t)GPIOx){
		case 0:
			ret = P0;
			break;
		case 1:
			ret = P1;
			break;
		case 2:
			ret = P2;
			break;
		case 3:
			ret = P3;
			break;
		default:
			break;
	}
	return (ret&0x00ff);
}

uint8_t GPIO_ReadOutputDataBit(GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin){return 0;}
uint16_t GPIO_ReadOutputData(GPIO_TypeDef* GPIOx){return 0;}

void GPIO_SetBits(GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin)
{
	switch((uint8_t)GPIOx){
		case 0:
			P0 |= (GPIO_Pin);
			break;
		case 1:
			P1 |= (GPIO_Pin);
			break;
		case 2:
			P2 |= (GPIO_Pin);
			break;
		case 3:
			P3 |= (GPIO_Pin);
			break;
		default:
			break;
	}
}

void GPIO_ResetBits(GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin)
{
	switch((uint8_t)GPIOx){
		case 0:
			P0 &= ~(GPIO_Pin);
			break;
		case 1:
			P1 &= ~(GPIO_Pin);
			break;
		case 2:
			P2 &= ~(GPIO_Pin);
			break;
		case 3:
			P3 &= ~(GPIO_Pin);
			break;
		default:
			break;
	}
}

void GPIO_WriteBit(GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin, BitAction BitVal){}
void GPIO_Write(GPIO_TypeDef* GPIOx, uint16_t PortVal){}
