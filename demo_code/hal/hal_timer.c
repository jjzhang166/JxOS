
#include "hal_timer.h"
#include "N76E003.h"
#include "SFR_Macro.h"

/*********************************
#include<Windows.h>
#include<stdio.h>
//参数一表示 需要等待的时间 微秒为单位
int UsSleep(int us)
{
    //储存计数的联合
    LARGE_INTEGER fre;
    //获取硬件支持的高精度计数器的频率
    if (QueryPerformanceFrequency(&fre))
    {
        LARGE_INTEGER run,priv,curr;
        run.QuadPart = fre.QuadPart * us / 1000000;//转换为微妙级
        //获取高精度计数器数值
        QueryPerformanceCounter(&priv);
        do
        {
            QueryPerformanceCounter(&curr);
        } while (curr.QuadPart - priv.QuadPart < run.QuadPart);
        curr.QuadPart -= priv.QuadPart;
        int nres = (curr.QuadPart * 1000000 / fre.QuadPart);//实际使用微秒时间
        return nres;
    }
    return -1;//
}

static uint16_t timer0_p = 0;
void TIM0_IRQHandler(void);
DWORD WINAPI TIMER0_INT(LPVOID p)
{
	while(1){
		TIM0_IRQHandler();
		Sleep(timer0_p);
	}
    return 0;
}
static uint16_t timer1_p = 0;
void TIM1_IRQHandler(void);
DWORD WINAPI TIMER1_INT(LPVOID p)
{
	int us;
	while(1){
		TIM1_IRQHandler();
//		Sleep(timer1_p);

		us = UsSleep(20);
		if(us == -1){
			printf("us err\r\n");
		}
		else{
//			printf("us: %d\r\n",us);
		}

	}
    return 0;
}
**********************************/

void TIM_TimeBaseInit(TIM_TypeDef* TIMx, TIM_TimeBaseInitTypeDef* TIM_TimeBaseInitStruct)
{
	if((uint16_t)TIMx == TIM0){
//		timer0_p = TIM_TimeBaseInitStruct->TIM_Period;
//		CreateThread(NULL, 0, TIMER0_INT, 0, 0, NULL);
		TIMER0_MODE2_ENABLE;
		clr_T0M;	//??? Fsys/12 -> 16Mhz/12 -> 4/3Mhz
		
		//TH0 = (256-27);  	//20us          
		//TL0 = (256-27);    
			
		TH0 = (256-53);            
		TL0 = (256-53);    
		
		set_ET0;                                    //enable Timer0 interrupt
	}
	else if((uint16_t)TIMx == TIM1){
//		timer1_p = TIM_TimeBaseInitStruct->TIM_Period;
//		CreateThread(NULL, 0, TIMER1_INT, 0, 0, NULL);
	}
	else if((uint16_t)TIMx == TIM3){
		//111 1/128?? 16Mhz/128 -> 0.125Mhz
		set_T3PS2;
		set_T3PS1;
		set_T3PS0;	
		
		//0.125Mhz -> ???? 1=8us ... 125=1ms ...
		RH3 = (65536-1250)/256;		//RELOAD_VALUE_H; 
		RL3 = (65536-1250)%256;		//RELOAD_VALUE_L;    
		
		set_ET3;                                    //enable Timer3 interrupt
	}
}
void TIM_Cmd(TIM_TypeDef* TIMx, FunctionalState NewState)
{
	if((uint16_t)TIMx == TIM0){
		set_TR0;                                    //Timer3 run			
	}
	else if((uint16_t)TIMx == TIM1){
	}
	else if((uint16_t)TIMx == TIM3){
		set_TR3;                                    //Timer3 run	
	}
	set_EA;                                     //enable interrupts
}
void TIM_ITConfig(TIM_TypeDef* TIMx, uint16_t TIM_IT, FunctionalState NewState)
{

}
