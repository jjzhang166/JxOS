
#include "hal_adc.h"

void ADC_Init(ADC_TypeDef* ADCx, ADC_InitTypeDef* ADC_InitStruct){}
void ADC_StartCalibration(ADC_TypeDef* ADCx){}
FlagStatus ADC_GetCalibrationStatus(ADC_TypeDef* ADCx){}
void ADC_Cmd(ADC_TypeDef* ADCx, FunctionalState NewState){}
void ADC_ITConfig(ADC_TypeDef* ADCx, uint16_t ADC_IT, FunctionalState NewState){}
void ADC_SoftwareStartConvCmd(ADC_TypeDef* ADCx, FunctionalState NewState){}
FlagStatus ADC_GetSoftwareStartConvStatus(ADC_TypeDef* ADCx){}
uint16_t ADC_GetConversionValue(ADC_TypeDef* ADCx){}