
#include "hal_flash.h"

FLASH_Status FLASH_ErasePage(uint32_t Page_Address){}
FLASH_Status FLASH_ProgramWord(uint32_t Address, uint32_t Data){}
FLASH_Status FLASH_ReadWord(uint32_t Address, uint32_t* Data){}
FLASH_Status FLASH_ProgramByte(uint32_t Address, uint32_t Data){}
FLASH_Status FLASH_ReadByte(uint32_t Address, uint32_t* Data){}