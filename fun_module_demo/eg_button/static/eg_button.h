
#ifndef __BUTTON__H
#define __BUTTON__H


#include "type.h"

typedef struct __BUTTON_STRUCT__ BUTTON_STRUCT;
struct __BUTTON_STRUCT__{
	/**********************************************************/
	//private_attribute
	uint8_t last_state;
	uint8_t press_tick_count;
	/**********************************************************/
	//public_attribute
};

//public_action
void button_tick_scan_handler(BUTTON_STRUCT* button, uint8_t button_index, uint8_t tick);
void button_new(BUTTON_STRUCT* button);

#endif
