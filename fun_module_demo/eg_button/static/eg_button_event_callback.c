
#include "type.h"

//every project using this module has their own config files
//the config filse inchulde this files:
//module_config.h, module_config_callback.c, module_event_callback.c

/***********************************************************/
//event_callback
void button_press_event_callback(uint8_t button_index)
{
	LED_ON();
}
	
void button_release_event_callback(uint8_t button_index)
{
	LED_OFF();
}

void button_long_press_event_callback(uint8_t button_index)
{
	LED_TOGGLE();
}
