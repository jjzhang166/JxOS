
#ifndef __BSP_ASYNC_COMM__H
#define __BSP_ASYNC_COMM__H

#include "../lib/type.h"

/*****************************************************************
hal_rec_data --> bsp_async_comm_rec_data_handler --> bsp_async_comm_receive_ok_call_back --> payload data
payload data --> bsp_async_comm_pack_send_data --> packaged_send_data ..> hal_send
*****************************************************************/

void bsp_async_comm_init(void (*bsp_async_comm_receive_ok_call_back_handler)(uint8_t* unpackaged_rec_data, uint8_t unpackaged_rec_data_len));
uint8_t bsp_async_comm_pack_send_data(uint8_t* packaged_send_data, uint8_t* packaged_send_data_len,
									uint8_t* original_send_data, uint8_t original_send_data_len);
void bsp_async_comm_rec_data_handler(uint8_t* original_rec_data, uint8_t original_rec_data_len);

#endif //
