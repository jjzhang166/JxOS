
#ifndef _HT1621_H_
#define _HT1621_H_

#include "../lib/type.h"

void bsp_ht1621_write_cmd(uint8_t cmd);
void bsp_ht1621_write_data(uint8_t addr, uint8_t dat);
void bsp_ht1621_read_data(uint8_t addr, uint8_t* dat);
void bsp_ht1621_write_data_series(uint8_t addr, uint8_t *dat, uint8_t dat_len);
void bsp_ht1621_init(void);

#endif
