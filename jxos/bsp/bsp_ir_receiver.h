
#ifndef __BSP_IR_RECEIVER__H
#define __BSP_IR_RECEIVER__H


#include "../lib/type.h"

extern uint8_t bsp_ir_receive_finish_receive_falg;
extern uint8_t bsp_ir_receive_buff[];
extern uint8_t bsp_ir_receive_buff_len;
extern void (*bsp_ir_receive_receive_finish_callbakc)(void);

void bsp_ir_receive_tick_handler(void);
void bsp_ir_receive_init(void (*bsp_ir_receive_hal_init_callbakc_handler)(void),
						uint8_t (*bsp_ir_receive_hal_read_level_callbakc_handler)(void));



#endif