
#ifndef _BYTE_BIT_H_
#define _BYTE_BIT_H_

#include "../lib/type.h"

extern uint8_t bsp_ir_send_busy_falg;

uint8_t bsp_ir_send(uint8_t* send_data, uint8_t send_data_len);
void bsp_ir_send_tick_handler(void);
void bsp_ir_send_init(void (*bsp_ir_send_hal_init_callbakc_handler)(void),
						void (*bsp_ir_send_hal_set_level_pulse_callbakc_handler)(void),
						void (*bsp_ir_send_hal_set_level_idle_callbakc_handler)(void));
						
void bsp_ir_send_hal_set_level_pulse_callback_register(void (*bsp_ir_send_hal_set_level_pulse_callback_handler)(void));
void bsp_ir_send_hal_set_level_idle_callback_register(void (*bsp_ir_send_hal_set_level_idle_callback_handler)(void));


#endif