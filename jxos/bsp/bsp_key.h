
#ifndef __KEY__H
#define __KEY__H

#include "../lib/type.h"

void bsp_key_init(void (*bsp_key_hal_init_callback_handler)(void),
				uint8_t (*bsp_key_hal_read_pin_level_callbakc_handler)(uint8_t key_num),
				void (*bsp_key_free_callback_handler)(uint8_t key_num),
				void (*bsp_key_press_callback_handler)(uint8_t key_num),
				void (*bsp_key_release_callback_handler)(uint8_t key_num),
				void (*bsp_key_long_press_callback_handler)(uint8_t key_num));
void bsp_key_scan_handler(void);
uint8_t bsp_key_read_press_release(uint8_t key_mun);	//press 1; release 0
uint8_t bsp_key_all_release(void);
void bsp_key_long_press_repeat_callback_register(void (*bsp_key_long_press_repeat_callback_handler)(uint8_t key_num));

				
#endif //
