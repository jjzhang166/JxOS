#ifndef __LED_H
#define __LED_H

#include "../lib/type.h"

void bsp_led_init(void (*bsp_led_hal_init_callback_handler)(void),
				void (*bsp_led_hal_on_callback)(uint8_t led_num),
				void (*bsp_led_hal_off_callback)(uint8_t led_num));

void bsp_led_on(uint8_t led_num);
void bsp_led_off(uint8_t led_num);
void bsp_led_toggle(uint8_t led_num);
uint8_t bsp_led_get_state(uint8_t led_num);

void bsp_led_blink_stop(uint8_t led_num);
uint8_t bsp_led_blink(uint8_t led_num,uint8_t period, 
				uint8_t duty_ration, uint8_t blink_times);//blink_times = 0xff blink forever
uint8_t bsp_led_is_blink(uint8_t led_num);
uint8_t bsp_led_blink_check_blink_all(void);

void bsp_led_blink_handler(uint8_t interval_time);


#endif
