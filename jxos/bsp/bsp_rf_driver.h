

#ifndef _RF_DROVER_
#define _RF_DROVER_


#include <bsp_rf_driver_config.h>
/********************************************************/
/********************************************************/
//SEND
extern void (*bsp_rf_send_ok_call_back)(void);
extern uint8_t bsp_rf_send_finish_send_falg;	//clr by user

void bsp_rf_send_init(void);
void bsp_rf_send_tick_handler(void);
uint8_t bsp_rf_send(uint8_t* send_data, uint8_t data_len);
uint8_t bsp_rf_send_busy_check(void);		//1:busy 0:idle

/********************************************************/
/********************************************************/
//RECEIVE
extern void (*bsp_rf_receive_wait_call_back)(void);
extern void (*bsp_rf_receive_ing_call_back)(void);
extern void (*bsp_rf_receive_ok_call_back)(void);

#define BSP_RF_REC_STATE_WAITTING		0
#define BSP_RF_REC_STATE_RECING_SYNC	1
#define BSP_RF_REC_STATE_RECING_DATA	2
#define BSP_RF_REC_STATE_FINISH			3
extern uint8_t bsp_rf_receive_state;
extern uint8_t bsp_rf_receive_finish_receive_falg;	//clr by user
extern uint8_t bsp_rf_receive_buff[DATA_FLOW_MAX_LEN];
extern uint8_t bsp_rf_receive_buff_len;

void bsp_rf_receive_init(void);
void bsp_rf_receive_tick_handler(void);

#endif // _RF_DROVER_

