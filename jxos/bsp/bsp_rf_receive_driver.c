/********************************************************************************
文  件： rf_receive_driver.c
作  者:	JeremyCeng
版  本:	V1.0.0
时  间:	2017-12-21
说  明:	RF 433 315 模块驱动
历  史: V1.0.0 2017-07-05:	初版
	    V1.0.1 2017-12-21:	性能优化
********************************************************************************/
#include <string.h>
#include "bsp_rf_driver.h"
#include <bsp_rf_driver_config.h>

//数据：大端模式，高字节在前 0x55aa --> 1:0x55, 2.0xaa;
//字节：LSB, 低位在前 --> 0x55:B01010101 ->发送 1,0,1,0,1,0,1,0
/********************************************************
比特流格式：
|起始位(1bit)|数据位(8/16/24/32bit)(低位在前)|结束位(1bit)|
start d0(lsb) d1 d2 ... dn(msb) stop
********************************************************/
//接收模块 信号 高电平最长保持时间 20ms；低电平最长保持时间 300ms
//接受模块 杂波 高电平最长时间 4ms；低电平最长时间 2ms
//165us 490us

#ifndef DATA_FLOW_MAX_LEN
#define DATA_FLOW_MAX_LEN 	32UL	//byte
#endif // DATA_FLOW_MAX_LEN
#ifndef SYNC_H_TIME
#define SYNC_H_TIME		40UL
#endif // SYNC_H_TIME
#ifndef SYNC_L_TIME
#define SYNC_L_TIME		40UL
#endif // SYNC_L_TIME
#ifndef BIT0_H_TIME
#define BIT0_H_TIME		20UL
#endif // BIT0_H_TIME
#ifndef BIT0_L_TIME
#define BIT0_L_TIME		20UL
#endif // BIT0_L_TIME
#ifndef BIT1_H_TIME
#define BIT1_H_TIME		10UL
#endif // BIT1_H_TIME
#ifndef BIT1_L_TIME
#define BIT1_L_TIME		10UL
#endif // BIT1_L_TIME
#ifndef END_H_TIME
#define END_H_TIME		80UL
#endif // END_H_TIME
#ifndef END_L_TIME
#define END_L_TIME		5UL
#endif // END_L_TIME
#ifndef ALLOW_ERR_PERCENT
#define ALLOW_ERR_PERCENT	30UL
#endif // ALLOW_ERR_PERCENT
#ifndef SEND_SYNC_REPEAT
#define SEND_SYNC_REPEAT	1
#endif // SEND_SYNC_REPEAT

/********************************************************/
/********************************************************/
//RECEIVE

#define H_TIME_MAX		255
#define L_TIME_MAX		255

#define RECEIVE_NONE	0
#define RECEIVE_SYNC	1
#define RECEIVE_BIT0	2
#define RECEIVE_BIT1	3
#define RECEIVE_END		4
#define RECEIVE_ERR		5

#define EDGE_NONE		0
#define EDGE_FALL		1
#define EDGE_RISE		2

//void (*bsp_rf_receive_wait_call_back)(void) = 0;
void (*bsp_rf_receive_ing_call_back)(void) = 0;
void (*bsp_rf_receive_ok_call_back)(void) = 0;

static uint8_t sync_rec_ok_falg = 0;
static uint8_t rf_receive_buff_p = 0;
static uint8_t hight_count = 0;
static uint8_t low_count = 0;
static uint8_t last_pin_level = 0;
static uint8_t bit_i = 0;
static uint8_t receive_byte = 0;

uint8_t bsp_rf_receive_state = BSP_RF_REC_STATE_WAITTING;
uint8_t bsp_rf_receive_finish_receive_falg = 0;
uint8_t bsp_rf_receive_buff[DATA_FLOW_MAX_LEN] = {0};
uint8_t bsp_rf_receive_buff_len = 0;
void bsp_rf_receive_init(void)
{
	sync_rec_ok_falg = 0;
	rf_receive_buff_p = 0;
	hight_count = 0;
	low_count = 0;
	last_pin_level = 0;
	bit_i = 0;
	receive_byte = 0;

	bsp_rf_receive_state = BSP_RF_REC_STATE_WAITTING;
	bsp_rf_receive_finish_receive_falg = 0;
	memset(bsp_rf_receive_buff, 0, DATA_FLOW_MAX_LEN);
	bsp_rf_receive_buff_len = 0;

	bsp_rf_receive_pin_hal_init();
}

void bsp_rf_receive_tick_handler(void)		//10us
{
   	uint8_t temp_pin_level ;
	uint8_t edge;
	uint16_t bit_time;

	if(bsp_rf_receive_finish_receive_falg == 1){
		return;
	}
	
	temp_pin_level = bsp_rf_receive_pin_hal_read_level();
	edge = EDGE_NONE;
	if(temp_pin_level == 0){
		if(low_count <= L_TIME_MAX){
			low_count++;
		}
		if(last_pin_level  == 1){
			edge = EDGE_FALL;
		}
	}
	else{
		if(hight_count <= H_TIME_MAX){
			hight_count++;
		}
		if(last_pin_level == 0){
			edge = EDGE_RISE;
		}
	}
	last_pin_level = temp_pin_level;

	if(edge == EDGE_FALL){
		if((sync_rec_ok_falg == 1)
			&&(rf_receive_buff_p != 0)
			&&(hight_count >= END_H_TIME - ((END_H_TIME*ALLOW_ERR_PERCENT)/100))){//ALLOW_ERR_TIME)){
//			bit_state = RECEIVE_END;
			sync_rec_ok_falg = 0;
			bsp_rf_receive_buff_len = rf_receive_buff_p;
			rf_receive_buff_p = 0;
			bsp_rf_receive_finish_receive_falg = 1;
			bsp_rf_receive_state = BSP_RF_REC_STATE_FINISH;
			if(bsp_rf_receive_ok_call_back != 0){
				bsp_rf_receive_ok_call_back();
			}
		}
	}
	else if(edge == EDGE_RISE){
		bit_time = low_count + hight_count;
		if((bit_time >= SYNC_L_TIME + SYNC_H_TIME - (((SYNC_L_TIME + SYNC_H_TIME)*ALLOW_ERR_PERCENT)/100))//ALLOW_ERR_TIME)
		&&(bit_time <= SYNC_L_TIME + SYNC_H_TIME + (((SYNC_L_TIME + SYNC_H_TIME)*ALLOW_ERR_PERCENT)/100))){//ALLOW_ERR_TIME)){
			//rec sync head
//			bit_state = RECEIVE_SYNC;
			bit_i = 0;
			receive_byte = 0;
			rf_receive_buff_p = 0;
			sync_rec_ok_falg = 1;
			bsp_rf_receive_state = BSP_RF_REC_STATE_RECING_SYNC;
			if(bsp_rf_receive_ing_call_back != 0){
				bsp_rf_receive_ing_call_back();
			}
		}
		else if((bit_time >= BIT0_L_TIME + BIT0_H_TIME - (((BIT0_L_TIME + BIT0_H_TIME)*ALLOW_ERR_PERCENT)/100))//ALLOW_ERR_TIME)
		&&(bit_time <= BIT0_L_TIME + BIT0_H_TIME + (((BIT0_L_TIME + BIT0_H_TIME)*ALLOW_ERR_PERCENT)/100))){//ALLOW_ERR_TIME)){
			//rec bit 0
//			bit_state = RECEIVE_BIT0;
			if(sync_rec_ok_falg == 1){
				bit_i++;
			}
			bsp_rf_receive_state = BSP_RF_REC_STATE_RECING_DATA;
//			if(bsp_rf_receive_ing_call_back != 0){
//				bsp_rf_receive_ing_call_back();
//			}
		}
		else if((bit_time >= BIT1_L_TIME + BIT1_H_TIME - (((BIT1_L_TIME + BIT1_H_TIME)*ALLOW_ERR_PERCENT)/100))//ALLOW_ERR_TIME)
		&&(bit_time <= BIT1_L_TIME + BIT1_H_TIME + (((BIT1_L_TIME + BIT1_H_TIME)*ALLOW_ERR_PERCENT)/100))){//ALLOW_ERR_TIME)){
			//rec bit 1
//			bit_state = RECEIVE_BIT1;
			if(sync_rec_ok_falg == 1){
				receive_byte |= 0x01<<bit_i; //the 8 bit cpu was allowed move data in16 bit only, if receive_byte is more then 16 bit, there will be a error happened
				bit_i++;
			}
			bsp_rf_receive_state = BSP_RF_REC_STATE_RECING_DATA;
//			if(bsp_rf_receive_ing_call_back != 0){
//				bsp_rf_receive_ing_call_back();
//			}
		}
		else{
			bit_i = 0;
			receive_byte = 0;
			rf_receive_buff_p = 0;
			sync_rec_ok_falg = 0;
			bsp_rf_receive_state = BSP_RF_REC_STATE_WAITTING;
//			if(bsp_rf_receive_wait_call_back != 0){
//				bsp_rf_receive_wait_call_back();
//			}
		}
		hight_count = 0;
		low_count = 0;
/**************************************************************
//optimize 
		if(bit_state == RECEIVE_SYNC){
			i = 0;
			receive_byte = 0;
			rf_receive_buff_p = 0;
			sync_rec_ok_falg = 1;
		}
		else if((sync_rec_ok_falg == 1)
			&&(bit_state == RECEIVE_BIT0)){
			i++;
		}
		else if((sync_rec_ok_falg == 1)
			&&(bit_state == RECEIVE_BIT1)){
			receive_byte |= 0x01<<i;
			i++;
		}
		else{
			i = 0;
			receive_byte = 0;
			rf_receive_buff_p = 0;
			sync_rec_ok_falg = 0;
		}
***************************************************************/
		if(bit_i == 8){
			if(rf_receive_buff_p < DATA_FLOW_MAX_LEN){
				bsp_rf_receive_buff[rf_receive_buff_p] = receive_byte;
				rf_receive_buff_p++;
			}
			bit_i = 0;
			receive_byte = 0;
		}
	}
}


