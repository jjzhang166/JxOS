/********************************************************************************
作  者:	JeremyCeng
版  本:	V1.0.0
时  间:	2017-12-21
说  明:	RF 433 315 模块驱动
历  史: V1.0.0 2017-07-05:	初版
	    V1.0.1 2017-12-21:	性能优化
********************************************************************************/
#include <string.h>
#include "bsp_rf_driver.h"
#include <bsp_rf_driver_config.h>

//数据：大端模式，高字节在前 0x55aa --> 1:0x55, 2.0xaa;
//字节：LSB, 低位在前 --> 0x55:B01010101 ->发送 1,0,1,0,1,0,1,0
/********************************************************
比特流格式：
|起始位(1bit)|数据位(8/16/24/32bit)(低位在前)|结束位(1bit)|
start d0(lsb) d1 d2 ... dn(msb) stop
********************************************************/
//接收模块 信号 高电平最长保持时间 20ms；低电平最长保持时间 300ms
//接受模块 杂波 高电平最长时间 4ms；低电平最长时间 2ms
//165us 490us

#ifndef DATA_FLOW_MAX_LEN
#define DATA_FLOW_MAX_LEN 	32UL	//byte
#endif // DATA_FLOW_MAX_LEN
#ifndef SYNC_H_TIME
#define SYNC_H_TIME		40UL
#endif // SYNC_H_TIME
#ifndef SYNC_L_TIME
#define SYNC_L_TIME		40UL
#endif // SYNC_L_TIME
#ifndef BIT0_H_TIME
#define BIT0_H_TIME		20UL
#endif // BIT0_H_TIME
#ifndef BIT0_L_TIME
#define BIT0_L_TIME		20UL
#endif // BIT0_L_TIME
#ifndef BIT1_H_TIME
#define BIT1_H_TIME		10UL
#endif // BIT1_H_TIME
#ifndef BIT1_L_TIME
#define BIT1_L_TIME		10UL
#endif // BIT1_L_TIME
#ifndef END_H_TIME
#define END_H_TIME		80UL
#endif // END_H_TIME
#ifndef END_L_TIME
#define END_L_TIME		5UL
#endif // END_L_TIME
#ifndef ALLOW_ERR_PERCENT
#define ALLOW_ERR_PERCENT	30UL
#endif // ALLOW_ERR_PERCENT
#ifndef SEND_SYNC_REPEAT
#define SEND_SYNC_REPEAT	1
#endif // SEND_SYNC_REPEAT

/********************************************************/
/********************************************************/
//SEND

#define SEND_STATE_SYNC		0
#define SEND_STATE_DATA		1
#define SEND_STATE_END		2
#define SEND_STATE_STOP		3

void (*bsp_rf_send_ok_call_back)(void) = 0;

static uint8_t send_state = 0;
static uint8_t send_sync_count = 0;
static uint8_t send_buff_p = 0;
static uint8_t send_byte_i = 0;
static uint8_t send_delay_time = 0;
static uint8_t send_delay_lable_id = 0xff;
static uint8_t send_byte = 0;

static uint8_t send_buff[DATA_FLOW_MAX_LEN] = {0};
static uint8_t send_buff_len = 0;
static uint8_t send_start_falg = 0;
uint8_t bsp_rf_send_finish_send_falg = 0;

void bsp_rf_send_init(void)
{
	bsp_rf_send_ok_call_back = 0;

	send_state = SEND_STATE_SYNC;
	send_sync_count = 0;
	send_buff_p = 0;
	send_byte_i = 0;
	send_delay_time = 0;
	send_delay_lable_id = 0xff;
	send_byte = 0;

    memset(send_buff, 0, DATA_FLOW_MAX_LEN);
    send_buff_len = 0;
	send_start_falg = 0;
	bsp_rf_send_finish_send_falg = 0;
	
	bsp_rf_send_pin_hal_init();
}

void bsp_rf_send_tick_handler(void)
{
    if(send_start_falg == 0){
        return;
    }

    if(send_delay_time > 0){
        send_delay_time--;
        return;
    }
    else{
        switch(send_delay_lable_id){
			case 0:
				goto send_delay_lable_0;
				break;

			case 1:
				goto send_delay_lable_1;
				break;

			case 2:
				goto send_delay_lable_2;
				break;

			case 3:
				goto send_delay_lable_3;
				break;

			default:
				break;
        }
    }

    if(send_state == SEND_STATE_SYNC){
        bsp_rf_send_pin_hal_hight();
        send_delay_time = SYNC_H_TIME-1;
        send_delay_lable_id = 0;
        return;
send_delay_lable_0:
        bsp_rf_send_pin_hal_low();
        send_delay_time = SYNC_L_TIME-1;
        send_delay_lable_id = 0xff;
		send_sync_count++;
		if(send_sync_count >= SEND_SYNC_REPEAT){
			send_state = SEND_STATE_DATA;
		}
    }

    else if(send_state == SEND_STATE_DATA){
        if(send_byte_i == 0){
            send_byte = send_buff[send_buff_p];
            send_buff_p++;
        }
        if ((send_byte & 0x01) == 0){
            bsp_rf_send_pin_hal_hight();
            send_delay_time = BIT0_H_TIME-1;
            send_delay_lable_id = 2;
            return;
send_delay_lable_2:
            bsp_rf_send_pin_hal_low();
            send_delay_time = BIT0_L_TIME-1;
            send_delay_lable_id = 0xff;
        }
        else{
            bsp_rf_send_pin_hal_hight();
            send_delay_time = BIT1_H_TIME-1;
            send_delay_lable_id = 3;
            return;
send_delay_lable_3:
            bsp_rf_send_pin_hal_low();
            send_delay_time = BIT1_L_TIME-1;
            send_delay_lable_id = 0xff;
        }
        send_byte >>= 1;
        send_byte_i++;

        if(send_byte_i >= 8){
            if(send_buff_p >= send_buff_len){
                send_buff_p = 0;
                send_state = SEND_STATE_END;
            }
            else{
                send_byte_i = 0;
            }
        }
    }

    else if(send_state == SEND_STATE_END){
        bsp_rf_send_pin_hal_hight();
        send_delay_time = END_H_TIME-1;
        send_delay_lable_id = 1;
        return;
send_delay_lable_1:
        bsp_rf_send_pin_hal_low();
        send_delay_time = END_L_TIME-1;
        send_delay_lable_id = 0xff;
		send_state = SEND_STATE_STOP;
    }

	else if(send_state == SEND_STATE_STOP){
		send_start_falg = 0;
		bsp_rf_send_finish_send_falg = 1;
		if(bsp_rf_send_ok_call_back != 0){
			bsp_rf_send_ok_call_back();
		}
	}

	else{
		send_start_falg = 0;
    }
}

uint8_t bsp_rf_send(uint8_t* send_data, uint8_t data_len)
{
    if((send_start_falg == 0)
		&&(data_len <= DATA_FLOW_MAX_LEN)){
        send_state = SEND_STATE_SYNC;
		send_sync_count = 0;
        send_buff_p = 0;
        send_byte_i = 0;
        send_delay_time = 0;
		send_delay_lable_id = 0xff;

        memcpy(send_buff, send_data, data_len);
        send_buff_len = data_len;
        send_start_falg = 1;
        return 1;
    }
    else{
        return 0;
    }
}

uint8_t bsp_rf_send_busy_check(void)
{
	return send_start_falg;
}
