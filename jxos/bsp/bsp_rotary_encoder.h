#ifndef __ROTARY_ENCODER_H
#define __ROTARY_ENCODER_H

#include "../lib/type.h"

extern void (*bsp_rotary_encoder_clockwise_callback)(uint32_t num);
extern void (*bsp_rotary_encoder_counter_clockwise_callback)(uint32_t num);

void bsp_rotary_encoder_init(void);
void bsp_rotary_encoder_pin_a_edge_handler(uint32_t num);

#endif // __ROTARY_ENCODER_H
