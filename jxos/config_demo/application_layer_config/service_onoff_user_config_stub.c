
#include "../../../lib/type.h"
#include <application_layer_config.h>

#include "../../../bsp/bsp_led.h"

#if (SERVICE_ONOFF_SLAVE_ENABLE == 1)

void onoff_light_rec_toggle_cmd_handler(void);
void service_onoff_rec_cmd_on_callback(uint8_t serila_mun)
{
    printf("cmd on\r\n");
    bsp_led_on(0);
}

void service_onoff_rec_cmd_off_callback(uint8_t serila_mun)
{
    printf("cmd off\r\n");
    bsp_led_off(0);
}

void service_onoff_rec_cmd_toggle_callback(uint8_t serila_mun)
{
	uint8_t led_state = bsp_led_get_state(0);
	if(led_state == 0){
		bsp_led_on(0);
	}
	else{
		bsp_led_off(0);
	}
	onoff_light_rec_toggle_cmd_handler();
    printf("toggle\r\n");
}

#endif
