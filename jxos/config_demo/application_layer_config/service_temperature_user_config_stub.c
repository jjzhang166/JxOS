
#include "../../../lib/type.h"
#include "application_layer_config.h"

#if (SERVICE_TEMPERATURE_SLAVE_ENABLE == 1)

void service_temperature_rec_cmd_report_val(uint8_t serila_mun, uint16_t report_temperature_val)
{
    printf("temperature report %d\r\n", report_temperature_val);
}

#endif

