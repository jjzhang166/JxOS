

#include "stdint.h"
#include "bsp_sw_i2c.h"

void bh1750_hal_init(void)
{
 	bsp_sw_i2c_init();
}

uint8_t	bh1750_hal_i2c_write_device(uint8_t addr, uint8_t *data, uint8_t len)
{
	bsp_sw_i2c_burst_write(addr, len, data);
	return 1;
	//return halI2cWriteBytes(addr<<1, data, len);
}

uint8_t	bh1750_hal_i2c_read_device(uint8_t addr, uint8_t *data, uint8_t len)
{
  	bsp_sw_i2c_burst_read(addr, len, data);
	return 1;
//	return halI2cReadBytes(addr<<1, data, len);
}