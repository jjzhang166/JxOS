
#include <bsp_rf_driver_config.h>
#include "../hal/hal_gpio.h"

/********************************************************/
/********************************************************/
//SEND
void bsp_rf_send_pin_hal_init(void)
{
    GPIO_InitTypeDef GPIO_InitStruct;
    GPIO_InitStruct.GPIO_Mode = GPIO_Mode_Out_PP;
    GPIO_InitStruct.GPIO_Pin = GPIO_Pin_3;
	GPIO_Init(GPIOA, &GPIO_InitStruct);

	GPIO_ResetBits(GPIOA, GPIO_Pin_3);
}
void bsp_rf_send_pin_hal_hight(void)
{
    GPIO_SetBits(GPIOA, GPIO_Pin_3);
}

void bsp_rf_send_pin_hal_low(void)
{
    GPIO_ResetBits(GPIOA, GPIO_Pin_3);
}

/********************************************************/
/********************************************************/
//RECEIVE
void bsp_rf_receive_pin_hal_init(void)
{
    GPIO_InitTypeDef GPIO_InitStruct;
    GPIO_InitStruct.GPIO_Mode = GPIO_Mode_IN_FLOATING;
    GPIO_InitStruct.GPIO_Pin = GPIO_Pin_4;
	GPIO_Init(GPIOA, &GPIO_InitStruct);

	GPIO_ResetBits(GPIOA, GPIO_Pin_4);
}

uint8_t bsp_rf_receive_pin_hal_read_level(void)
{
    return GPIO_ReadInputDataBit(GPIOA, GPIO_Pin_4);
}

