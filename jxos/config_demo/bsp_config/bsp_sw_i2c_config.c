#include <bsp_sw_i2c_config.h>

/*******************************************************************************/
void bsp_sw_i2c_delay_half(void) 
{		//50us		10kbps
	volatile unsigned char i;
	for(i = 0; i < 5; i++);
}

void bsp_sw_i2c_hal_init(void) 
{
	halGpioSetConfig(PORTB_PIN(2),GPIOCFG_OUT);
	halGpioSet(PORTB_PIN(2));
	
	halGpioSetConfig(PORTB_PIN(1),GPIOCFG_OUT);	
	halGpioSet(PORTB_PIN(1));
}