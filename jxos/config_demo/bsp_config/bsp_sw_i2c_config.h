#ifndef __BSP_SW_I2C_CONFIG_H
#define __BSP_SW_I2C_CONFIG_H

#include "app/framework/include/af.h"
#include "stdint.h"

#define bsp_sw_i2c_sda_pin_hight()	halGpioSet(PORTB_PIN(2))
#define bsp_sw_i2c_sda_pin_low()	halGpioClear(PORTB_PIN(2))
#define bsp_sw_i2c_sda_pin_rec()	halGpioRead(PORTB_PIN(2))

#define bsp_sw_i2c_scl_pin_hight()	halGpioSet(PORTB_PIN(1))
#define bsp_sw_i2c_scl_pin_hlow()	halGpioClear(PORTB_PIN(1))

#define bsp_sw_i2c_sda_pin_out_mode()	halGpioSetConfig(PORTB_PIN(2),GPIOCFG_OUT);halGpioSet(PORTB_PIN(2))
#define bsp_sw_i2c_sda_pin_in_mode()	halGpioSetConfig(PORTB_PIN(2),GPIOCFG_IN_PUD)

//void bsp_sw_i2c_sda_pin_hight(void);
//void bsp_sw_i2c_sda_pin_low(void);
//uint8_t bsp_sw_i2c_sda_pin_rec(void);
//
//void bsp_sw_i2c_scl_pin_hight(void);
//void bsp_sw_i2c_scl_pin_hlow(void);

//void bsp_sw_i2c_sda_pin_out_mode(void);
//void bsp_sw_i2c_sda_pin_in_mode(void);

void bsp_sw_i2c_delay_half(void);
void bsp_sw_i2c_hal_init(void);

#endif