#include "em_gpio.h"
#include "em_cmu.h"

void e93196_delay_us(uint32_t udelayus)
{
    volatile uint32_t u32Delay = 0;
    volatile uint8_t u8Delay = 0;
    for(u32Delay = 0;u32Delay < udelayus; u32Delay++)
    {
		for(u8Delay = 0;u8Delay < 3;u8Delay++);
    }
}

void e93196_hal_init(void)
{
	CMU_ClockEnable(cmuClock_GPIO, true);
}
