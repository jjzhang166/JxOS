
#ifndef __MOTION_E93196_CONFIG_H__
#define __MOTION_E93196_CONFIG_H__	

#include "em_gpio.h"

void e93196_delay_us(uint32_t udelayus);
void e93196_hal_init(void);

//#define  GPIO_SENSOR_PIN  			2
//#define  GPIO_SENSOR_PORT         	gpioPortF

#define	E93196_SERIN_PORT       gpioPortF
#define	E93196_SERIN_PIN        7
#define	E93196_INT_PORT         gpioPortF
#define	E93196_INT_PIN          2

#define	e93196_hal_serin_pin_out_mod()			GPIO_PinModeSet(E93196_SERIN_PORT, E93196_SERIN_PIN, gpioModePushPull, 0)
#define	e93196_hal_serin_pin_in_mod()           GPIO_PinModeSet(E93196_SERIN_PORT, E93196_SERIN_PIN, gpioModeInput, 0)
#define	e93196_hal_serin_pin_lowpower_mod()		GPIO_PinModeSet(E93196_SERIN_PORT, E93196_SERIN_PIN, gpioModeDisabled, 0)
#define	e93196_hal_serin_pin_hight()			GPIO_PinOutSet(E93196_SERIN_PORT,E93196_SERIN_PIN)
#define	e93196_hal_serin_pin_low()              GPIO_PinOutClear(E93196_SERIN_PORT,E93196_SERIN_PIN)
#define	e93196_hal_serin_pin_level_get()		GPIO_PinInGet(E93196_SERIN_PORT, E93196_SERIN_PIN)

#define	e93196_hal_int_pin_out_mod()			GPIO_PinModeSet(E93196_INT_PORT, E93196_INT_PIN, gpioModePushPull, 0)
#define	e93196_hal_int_pin_in_mod()             GPIO_PinModeSet(E93196_INT_PORT, E93196_INT_PIN, gpioModeInput, 0) 
#define	e93196_hal_int_pin_hight()              GPIO_PinOutSet(E93196_INT_PORT,E93196_INT_PIN)
#define	e93196_hal_int_pin_low()                GPIO_PinOutClear(E93196_INT_PORT,E93196_INT_PIN)
#define	e93196_hal_int_pin_level_get()			GPIO_PinInGet(E93196_INT_PORT, E93196_INT_PIN)

#define	e93196_hal_int_pin_disable_int_mod()	GPIO_IntConfig(E93196_INT_PORT, E93196_INT_PIN, true, true, false)
#define	e93196_hal_int_pin_enable_int_mod()     GPIO_IntConfig(E93196_INT_PORT, E93196_INT_PIN, true, true, true)

#endif
