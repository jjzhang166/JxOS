
#ifndef _SOFTWARE_TIMER_TASK_CONFIG_H
#define _SOFTWARE_TIMER_TASK_CONFIG_H

#include "../hal/hal_interrupt.h"

#define SWTIMER_MAX 		3

#define sys_software_timer_config_hal_timer_isr_callback    TIM0_IRQHandler_callback


#endif
