
#ifndef __SIM_PWM__H
#define __SIM_PWM__H

#include "../lib/type.h"

void sim_pwm_init(void);
void sim_pwm_tick_handler(void);

void sim_pwm_start(uint8_t pwm_num);
void sim_pwm_stop(uint8_t pwm_num);
void sim_pwm_set_period(uint8_t pwm_num, uint16_t period);  //1~ffff ticks
void sim_pwm_set_duty(uint8_t pwm_num, uint16_t duty);      //0~ffff ticks

#endif
