#ifndef __PWM_MOVE__H
#define __PWM_MOVE__H

#include "type.h"

void value_move_init(void);
void value_move_tick_handler(void);

//move_ticks: how many ticks will take to move "current_value" to "target_value"
void value_move_to_value_by_tick(uint8_t value_move_num, uint16_t set_target_value, uint16_t move_ticks);
//set_step_value: how many steps pre tick
void value_move_to_value_by_step(uint8_t value_move_num, uint16_t set_target_value, uint16_t set_step_value);
//the "current_value" move to the "begin_value" in the first round
//period_ticks: how many ticks will take to move "begin_value" to "end_value"
void value_move_circle_move_by_tick(uint8_t value_move_num, uint16_t set_begin_value, uint16_t set_end_value, uint16_t period_ticks);
//the "current_value" move to the "begin_value" in the first round
//set_step_value: how many steps pre tick
void value_move_circle_move_by_step(uint8_t value_move_num, uint16_t set_begin_value, uint16_t set_end_value, uint16_t set_step_value);
void value_move_stop_move(uint8_t value_move_num);

#endif