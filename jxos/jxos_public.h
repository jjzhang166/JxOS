#ifndef __JXOS_PUBLIC_H
#define __JXOS_PUBLIC_H

#include "jxos_config.h"
#include "type.h"


void jxos_run(void);

/****************************************************************************/
#if (JXOS_TASK_ENABLE == 1)
typedef void* JXOS_TASK_HANDLE;
#if (JXOS_Compiler_optimization_1 == 1)
typedef void (*TASK_FUNCTION)(void);
#else
typedef void (*TASK_FUNCTION)(uint8_t task_id, void * parameter);
#endif
JXOS_TASK_HANDLE jxos_task_create(TASK_FUNCTION function, const char* name, void* parameter);
//jxos_task_delete
uint8_t jxos_task_resume(JXOS_TASK_HANDLE h);
uint8_t jxos_task_suspended(JXOS_TASK_HANDLE h);
uint8_t* jxos_task_get_name(JXOS_TASK_HANDLE h);
#if (JXOS_REGISTRY_ENABLE == 1)
JXOS_TASK_HANDLE jxos_task_get_handle(const char* name);
#endif
#endif

/****************************************************************************/
#if (JXOS_ENENT_ENABLE == 1)
typedef uint8_t JXOS_EVENT_HANDLE;
uint8_t jxos_event_check_empty_all(void);
JXOS_EVENT_HANDLE jxos_event_create(void);
//jxos_event_delete
uint8_t jxos_event_set(JXOS_EVENT_HANDLE event_h);
uint8_t jxos_event_wait(JXOS_EVENT_HANDLE event_h);	//if wait event success, the event will reset automatically
#endif

/****************************************************************************/
#if (JXOS_MESSAGE_PIPE_ENABLE == 1)
typedef void* JXOS_MESSAGE_PIPE_HANDLE;
uint8_t jxos_message_pipe_check_empty_all(void);
JXOS_MESSAGE_PIPE_HANDLE jxos_message_pipe_create(uint8_t* space,
						uint16_t space_len,
						uint8_t data_size,              //data_size  >= 3
						char* message_pipe_name);
uint8_t jxos_message_pipe_send(JXOS_MESSAGE_PIPE_HANDLE message_pipe, char* sender_name,
							uint8_t* data_to_send, uint8_t data_to_send_len);
uint8_t jxos_message_pipe_receive(JXOS_MESSAGE_PIPE_HANDLE message_pipe, char* receiver_name,
							uint8_t* receive_buffer, uint8_t* receive_data_len);
uint8_t jxos_message_pipe_check_empty(JXOS_MESSAGE_PIPE_HANDLE message_pipe);		//err:0xff; true:1; false:0;
uint8_t jxos_message_pipe_check_full(JXOS_MESSAGE_PIPE_HANDLE message_pipe);		//err:0xff; true:1; false:0;
uint8_t jxos_message_pipe_clean_up(JXOS_MESSAGE_PIPE_HANDLE message_pipe,
								char* sender_name_or_receiver_name);
uint8_t jxos_message_pipe_sender_name_register(JXOS_MESSAGE_PIPE_HANDLE message_pipe, char* sender_name);
uint8_t jxos_message_pipe_receiver_name_register(JXOS_MESSAGE_PIPE_HANDLE message_pipe, char* receiver_name);
uint8_t jxos_message_pipe_sender_name_cancel(JXOS_MESSAGE_PIPE_HANDLE message_pipe, char* sender_name);
uint8_t jxos_message_pipe_receiver_name_cancel(JXOS_MESSAGE_PIPE_HANDLE message_pipe, char* receiver_name);
#if (JXOS_REGISTRY_ENABLE == 1)
JXOS_MESSAGE_PIPE_HANDLE jxos_message_pipe_get_handle(char* message_pipe_name);
#endif
#endif

/****************************************************************************/
#if (JXOS_BULLETIN_BOARD_ENABLE == 1)
typedef void* JXOS_BULLETIN_BOARD_HANDLE;
uint8_t jxos_bulletin_board_check_empty_all(void);
JXOS_BULLETIN_BOARD_HANDLE jxos_bulletin_board_create(uint8_t* space,
						uint16_t space_len,
						uint8_t data_size,                    //data_size  >= 3
						char* bulletin_board_name);
uint8_t jxos_bulletin_board_send(JXOS_BULLETIN_BOARD_HANDLE bulletin_board, char* sender_name,
							uint8_t* data_to_send, uint8_t data_to_send_len);
uint8_t jxos_bulletin_board_receive(JXOS_BULLETIN_BOARD_HANDLE bulletin_board,
							uint8_t bulletin_serial_num, uint8_t* receive_buffer, uint8_t* receive_data_len);
uint8_t jxos_bulletin_board_receive_latest(JXOS_BULLETIN_BOARD_HANDLE bulletin_board,
							uint8_t* receive_buffer, uint8_t* receive_data_len);
uint8_t jxos_bulletin_board_check_empty(JXOS_BULLETIN_BOARD_HANDLE bulletin_board);		//err:0xff; true:1; false:0;
uint8_t jxos_bulletin_board_check_full(JXOS_BULLETIN_BOARD_HANDLE bulletin_board);		//err:0xff; true:1; false:0;
uint8_t jxos_bulletin_board_check_num(JXOS_BULLETIN_BOARD_HANDLE bulletin_board);	//return 0XFF when err
uint8_t jxos_bulletin_board_del_earliest(JXOS_BULLETIN_BOARD_HANDLE bulletin_board,
							char* sender_name);
uint8_t jxos_bulletin_board_clean_up(JXOS_BULLETIN_BOARD_HANDLE bulletin_board,
							char* sender_name);
uint8_t jxos_bulletin_board_sender_name_register(JXOS_BULLETIN_BOARD_HANDLE bulletin_board, char* sender_name);
uint8_t jxos_bulletin_board_sender_name_cancel(JXOS_BULLETIN_BOARD_HANDLE bulletin_board, char* sender_name);
#if (JXOS_REGISTRY_ENABLE == 1)
JXOS_BULLETIN_BOARD_HANDLE jxos_bulletin_board_get_handle(char* bulletin_board_name);
#endif
#if (JXOS_BULLETIN_BOARD_INDEX_ENABLE == 1)
//index: 0~255
uint8_t jxos_bulletin_board_read_index(JXOS_BULLETIN_BOARD_HANDLE bulletin_board,
							uint8_t bulletin_serial_num, uint8_t* index);
uint8_t jxos_bulletin_board_get_next_index(JXOS_BULLETIN_BOARD_HANDLE bulletin_board, uint8_t* local_index);
uint8_t jxos_bulletin_board_send_with_index(JXOS_BULLETIN_BOARD_HANDLE bulletin_board, char* sender_name,
							uint8_t* data_to_send, uint8_t data_to_send_len);
uint8_t jxos_bulletin_board_receive_by_index(JXOS_BULLETIN_BOARD_HANDLE bulletin_board,
							uint8_t index, uint8_t* receive_buffer, uint8_t* receive_data_len);
#endif
#endif

/****************************************************************************/
#if (JXOS_MAIL_BOX_ENABLE == 1)
typedef void* JXOS_MAIL_BOX_HANDLE;
uint8_t jxos_mail_box_check_empty_all(void);
JXOS_MAIL_BOX_HANDLE jxos_mail_box_create(uint8_t* space,
						uint16_t space_len,
						uint8_t data_size,              //data_size  >= 3
						char* mail_box_name);
uint8_t jxos_mail_box_send(JXOS_MAIL_BOX_HANDLE mail_box,
							uint8_t* data_to_send, uint8_t data_to_send_len);
uint8_t jxos_mail_box_receive(JXOS_MAIL_BOX_HANDLE mail_box, char* receiver_name,
							uint8_t* receive_buffer, uint8_t* receive_data_len);
uint8_t jxos_mail_box_check_empty(JXOS_MAIL_BOX_HANDLE mail_box);		//err:0xff; true:1; false:0;
uint8_t jxos_mail_box_check_full(JXOS_MAIL_BOX_HANDLE mail_box);		//err:0xff; true:1; false:0;
uint8_t jxos_mail_box_clean_up(JXOS_MAIL_BOX_HANDLE mail_box,
								char* receiver_name);
uint8_t jxos_mail_box_receiver_name_register(JXOS_MAIL_BOX_HANDLE mail_box, char* receiver_name);
uint8_t jxos_mail_box_receiver_name_cancel(JXOS_MAIL_BOX_HANDLE mail_box, char* receiver_name);
#if (JXOS_REGISTRY_ENABLE == 1)
JXOS_MAIL_BOX_HANDLE jxos_mail_box_get_handle(char* mail_box_name);
#endif
#endif

/****************************************************************************/
#if (JXOS_MSG_ENABLE == 1)
typedef void* JXOS_MSG_HANDLE;
uint8_t jxos_msg_check_empty_all(void);
JXOS_MSG_HANDLE jxos_msg_create(uint8_t* queue_space,
						uint16_t queue_space_len,
						uint8_t item_size,
						const char* name);
//delete
uint8_t jxos_msg_send(JXOS_MSG_HANDLE queue_h, const void* item_to_queue);
uint8_t jxos_msg_receive(JXOS_MSG_HANDLE queue_h, void* item_buffer);
uint8_t jxos_msg_check_empty(JXOS_MSG_HANDLE queue_h);
void jxos_msg_clean_up(JXOS_MSG_HANDLE queue_h);
uint8_t* jxos_msg_get_name(JXOS_MSG_HANDLE queue_h);
#if (JXOS_REGISTRY_ENABLE == 1)
JXOS_MSG_HANDLE jxos_msg_get_handle(const char* name);
#endif
#endif

/****************************************************************************/
#if (JXOS_SYS_SERVICE_POWER_MGR_TASK_ENABLE == 1)
uint8_t sys_power_mgr_task_keep_wake_mark_get_id(void);
void sys_power_mgr_task_keep_wake_mark_set(uint8_t mark_id);
void sys_power_mgr_task_keep_wake_mark_reset(uint8_t mark_id);
void sys_power_mgr_task_interrupt_happened_falg(void);
extern void (*sys_power_mgr_task_hal_init_callback)(void);
extern void (*sys_power_mgr_task_prepare_to_sleep_callback)(void);
extern void (*sys_power_mgr_task_go_to_sleep_callback)(void);
extern void (*sys_power_mgr_task_recover_to_wake_callback)(void);
#endif

/****************************************************************************/
#if (JXOS_SYS_SERVICE_SOFTWARE_TIMER_TASK_ENABLE == 1)
//even the timer was overtime, it will keep running until it stopped by user
typedef void* swtime_type;
bool_t software_timer_task_get_tick_happened(void);
void software_timer_task_reset_tick_happened(void);
swtime_type sys_software_timer_task_new_timer(void);
/******************************/
//when the hw timer not support to get the passed time, use this following function very carefully
//this following function must be called when swt tick just happened, or one of the swt was time out
//otherwise the timimg may not correct
//
//if(software_timer_task_get_tick_happened()  == 1)
//||(sys_software_timer_task_check_overtime_timer(swtE) == 1){
//		sys_software_timer_task_set_timer(swtA)
//		sys_software_timer_task_stop_timer(swtB)
//		sys_software_timer_task_set_timer(swtC)
//		sys_software_timer_task_restart_timer(swtD);
//}
void sys_software_timer_task_set_timer(swtime_type swtime, uint16_t time);
void sys_software_timer_task_start_timer(swtime_type swtime);	//just start timer, without reset the counter
void sys_software_timer_task_stop_timer(swtime_type swtime);	//reset the counter, then start timer
void sys_software_timer_task_restart_timer(swtime_type swtime);
/******************************/
bool_t sys_software_timer_task_check_running_timer(swtime_type swtime);
bool_t sys_software_timer_task_check_overtime_timer(swtime_type swtime);
uint32_t sys_software_timer_task_get_tick_ms(void);

#if (JXOS_ENENT_ENABLE == 1)&&(JXOS_Compiler_optimization_2 == 0)
void software_timer_task_tick_handler(void);
#else
extern bool_t software_timer_task_tick_handler_flag;
#define software_timer_task_tick_handler() software_timer_task_tick_handler_flag = true
#endif

#if(JXOS_Compiler_optimization_3 != 1)
extern void (*sys_software_timer_task_hal_init_callback)(void);
extern void (*sys_software_timer_task_hal_start_callback)(void);
extern void (*sys_software_timer_task_hal_stop_callback)(void);
#else
void sys_software_timer_task_hal_init_config(void);
void sys_software_timer_task_hal_start_config(void);
void sys_software_timer_task_hal_stop_config(void);
#endif

#endif

/****************************************************************************/
#if (JXOS_SYS_SERVICE_DEBUG_PRINT_TASK_ENABLE == 1)
void sys_debug_print_task_clr_buff(void);
void sys_debug_print_task_print_str(char* str, uint8_t is_blocked);
void sys_debug_print_task_print_int(int32_t value, uint8_t is_blocked);
void sys_debug_print_task_print_uint(uint32_t value, uint8_t is_blocked);
void sys_debug_print_task_print_hex(uint32_t value, uint8_t is_blocked);
void sys_debug_print_task_print_bin(uint32_t value, uint8_t is_blocked);
void sys_debug_print_task_print_data_stream_in_hex(uint8_t* data_stream,
												uint8_t data_stream_len, uint8_t is_blocked);
extern void (*sys_debug_print_task_hal_init_callback)(void);
extern uint8_t (*sys_debug_print_task_send_finish_check_callback)(void);
extern void (*sys_debug_print_task_send_byte_callback)(uint8_t byte);
#endif

#if (JXOS_SYS_SERVICE_DEBUG_PRINT_BLOCKED_PRINT_ENABLE == 1)
#define sys_debug_print_task_blocked_print_str(s)						sys_debug_print_task_print_str(s,1)
#define sys_debug_print_task_blocked_print_int(v)						sys_debug_print_task_print_int(v,1)
#define sys_debug_print_task_blocked_print_uint(v)						sys_debug_print_task_print_uint(v,1)
#define sys_debug_print_task_blocked_print_hex(v)						sys_debug_print_task_print_hex(v,1)
#define sys_debug_print_task_blocked_print_bin(v)						sys_debug_print_task_print_bin(v,1)
#define sys_debug_print_task_blocked_print_data_stream_in_hex(p,l)		sys_debug_print_task_print_data_stream_in_hex(p,l,1)
#else
#define sys_debug_print_task_blocked_print_str(s)
#define sys_debug_print_task_blocked_print_int(v)
#define sys_debug_print_task_blocked_print_uint(v)
#define sys_debug_print_task_blocked_print_hex(v)
#define sys_debug_print_task_blocked_print_bin(v)
#define sys_debug_print_task_blocked_print_data_stream_in_hex(p,l)
#endif
#if (JXOS_SYS_SERVICE_DEBUG_PRINT_KERNEL_PRINT_ENABLE == 1)
#define sys_debug_print_task_kernel_print_str(s)						sys_debug_print_task_print_str(s,0)
#define sys_debug_print_task_kernel_print_int(v)						sys_debug_print_task_print_int(v,0)
#define sys_debug_print_task_kernel_print_uint(v)						sys_debug_print_task_print_uint(v,0)
#define sys_debug_print_task_kernel_print_hex(v)						sys_debug_print_task_print_hex(v,0)
#define sys_debug_print_task_kernel_print_bin(v)						sys_debug_print_task_print_bin(v,0)
#define sys_debug_print_task_kernel_print_data_stream_in_hex(p,l)		sys_debug_print_task_print_data_stream_in_hex(p,l,0)
#else
#define sys_debug_print_task_kernel_print_str(s)
#define sys_debug_print_task_kernel_print_int(v)
#define sys_debug_print_task_kernel_print_uint(v)
#define sys_debug_print_task_kernel_print_hex(v)
#define sys_debug_print_task_kernel_print_bin(v)
#define sys_debug_print_task_kernel_print_data_stream_in_hex(p,l)
#endif
#if (JXOS_SYS_SERVICE_DEBUG_PRINT_LIB_PRINT_ENABLE == 1)
#define sys_debug_print_task_lib_print_str(s)							sys_debug_print_task_print_str(s,0)
#define sys_debug_print_task_lib_print_int(v)							sys_debug_print_task_print_int(v,0)
#define sys_debug_print_task_lib_print_uint(v)							sys_debug_print_task_print_uint(v,0)
#define sys_debug_print_task_lib_print_hex(v)							sys_debug_print_task_print_hex(v,0)
#define sys_debug_print_task_lib_print_bin(v)							sys_debug_print_task_print_bin(v,0)
#define sys_debug_print_task_lib_print_data_stream_in_hex(p,l)			sys_debug_print_task_print_data_stream_in_hex(p,l,0)
#else
#define sys_debug_print_task_lib_print_str(s)
#define sys_debug_print_task_lib_print_int(v)
#define sys_debug_print_task_lib_print_uint(v)
#define sys_debug_print_task_lib_print_hex(v)
#define sys_debug_print_task_lib_print_bin(v)
#define sys_debug_print_task_lib_print_data_stream_in_hex(p,l)
#endif
#if (JXOS_SYS_SERVICE_DEBUG_PRINT_HAL_PRINT_ENABLE == 1)
#define sys_debug_print_task_hal_print_str(s)							sys_debug_print_task_print_str(s,0)
#define sys_debug_print_task_hal_print_int(v)							sys_debug_print_task_print_int(v,0)
#define sys_debug_print_task_hal_print_uint(v)							sys_debug_print_task_print_uint(v,0)
#define sys_debug_print_task_hal_print_hex(v)							sys_debug_print_task_print_hex(v,0)
#define sys_debug_print_task_hal_print_bin(v)							sys_debug_print_task_print_bin(v,0))
#define sys_debug_print_task_hal_print_data_stream_in_hex(p,l)			sys_debug_print_task_print_data_stream_in_hex(p,l,0)
#else
#define sys_debug_print_task_hal_print_str(s)
#define sys_debug_print_task_hal_print_int(v)
#define sys_debug_print_task_hal_print_uint(v)
#define sys_debug_print_task_hal_print_hex(v)
#define sys_debug_print_task_hal_print_bin(v)
#define sys_debug_print_task_hal_print_data_stream_in_hex(p,l)
#endif
#if (JXOS_SYS_SERVICE_DEBUG_PRINT_BSP_PRINT_ENABLE == 1)
#define sys_debug_print_task_bsp_print_str(s)							sys_debug_print_task_print_str(s,0)
#define sys_debug_print_task_bsp_print_int(v)							sys_debug_print_task_print_int(v,0)
#define sys_debug_print_task_bsp_print_uint(v)							sys_debug_print_task_print_uint(v,0)
#define sys_debug_print_task_bsp_print_hex(v)							sys_debug_print_task_print_hex(v,0)
#define sys_debug_print_task_bsp_print_bin(v)							sys_debug_print_task_print_bin(v,0)
#define sys_debug_print_task_bsp_print_data_stream_in_hex(p,l)			sys_debug_print_task_print_data_stream_in_hex(p,l,0)
#else
#define sys_debug_print_task_bsp_print_str(s)
#define sys_debug_print_task_bsp_print_int(v)
#define sys_debug_print_task_bsp_print_uint(v)
#define sys_debug_print_task_bsp_print_hex(v)
#define sys_debug_print_task_bsp_print_bin(v)
#define sys_debug_print_task_bsp_print_data_stream_in_hex(p,l)
#endif
#if (JXOS_SYS_SERVICE_DEBUG_PRINT_SYS_SER_PRINT_ENABLE == 1)
#define sys_debug_print_task_sys_ser_print_str(s)						sys_debug_print_task_print_str(s,0)
#define sys_debug_print_task_sys_ser_print_int(v)						sys_debug_print_task_print_int(v,0)
#define sys_debug_print_task_sys_ser_print_uint(v)						sys_debug_print_task_print_uint(v,0)
#define sys_debug_print_task_sys_ser_print_hex(v)						sys_debug_print_task_print_hex(v,0)
#define sys_debug_print_task_sys_ser_print_bin(v)						sys_debug_print_task_print_bin(v,0)
#define sys_debug_print_task_sys_ser_print_data_stream_in_hex(p,l)		sys_debug_print_task_print_data_stream_in_hex(p,l,0)
#else
#define sys_debug_print_task_sys_ser_print_str(s)
#define sys_debug_print_task_sys_ser_print_int(v)
#define sys_debug_print_task_sys_ser_print_uint(v)
#define sys_debug_print_task_sys_ser_print_hex(v)
#define sys_debug_print_task_sys_ser_print_bin(v)
#define sys_debug_print_task_sys_ser_print_data_stream_in_hex(p,l)
#endif
#if (JXOS_SYS_SERVICE_DEBUG_PRINT_STD_APP_PRINT_ENABLE == 1)
#define sys_debug_print_task_std_app_print_str(s)						sys_debug_print_task_print_str(s,0)
#define sys_debug_print_task_std_app_print_int(v)						sys_debug_print_task_print_int(v,0)
#define sys_debug_print_task_std_app_print_uint(v)						sys_debug_print_task_print_uint(v,0)
#define sys_debug_print_task_std_app_print_hex(v)						sys_debug_print_task_print_hex(v,0)
#define sys_debug_print_task_std_app_print_bin(v)						sys_debug_print_task_print_bin(v,0)
#define sys_debug_print_task_std_app_print_data_stream_in_hex(p,l)		sys_debug_print_task_print_data_stream_in_hex(p,l,0)
#else
#define sys_debug_print_task_std_app_print_str(s)
#define sys_debug_print_task_std_app_print_int(v)
#define sys_debug_print_task_std_app_print_uint(v)
#define sys_debug_print_task_std_app_print_hex(v)
#define sys_debug_print_task_std_app_print_bin(v)
#define sys_debug_print_task_std_app_print_data_stream_in_hex(p,l)
#endif
#if (JXOS_SYS_SERVICE_DEBUG_PRINT_USER_PRINT_ENABLE == 1)
#define sys_debug_print_task_user_print_str(s)							sys_debug_print_task_print_str(s,0)
#define sys_debug_print_task_user_print_int(v)							sys_debug_print_task_print_int(v,0)
#define sys_debug_print_task_user_print_uint(v)							sys_debug_print_task_print_uint(v,0)
#define sys_debug_print_task_user_print_hex(v)							sys_debug_print_task_print_hex(v,0)
#define sys_debug_print_task_user_print_bin(v)							sys_debug_print_task_print_bin(v,0)
#define sys_debug_print_task_user_print_data_stream_in_hex(p,l)			sys_debug_print_task_print_data_stream_in_hex(p,l,0)
#else
#define sys_debug_print_task_user_print_str(s)
#define sys_debug_print_task_user_print_int(v)
#define sys_debug_print_task_user_print_uint(v)
#define sys_debug_print_task_user_print_hex(v)
#define sys_debug_print_task_user_print_bin(v)
#define sys_debug_print_task_user_print_data_stream_in_hex(p,l)
#endif

/****************************************************************************/
#if (JXOS_STD_APP_KEY_TASK_ENABLE == 1)
void std_app_key_task_key_press_interrupt_handler(void);
extern void (*std_app_key_task_hal_init_callback)(void);
extern uint8_t (*std_app_key_task_hal_read_pin_level_callback)(uint8_t key_num);
//output msg name: "std_app_key_msg"
#endif

#if (JXOS_STD_APP_KEY_MULTI_TASK_ENABLE == 1)
void std_app_key_multi_task_key_press_interrupt_handler(void);
extern void (*std_app_key_multi_task_hal_init_callback)(void);
extern uint8_t (*std_app_multi_key_task_hal_read_pin_level_callback)(uint8_t key_num);
//output msg name: "std_app_key_multi_msg"
#endif

#if (JXOS_STD_APP_LED_TASK_ENABLE == 1)
extern void (*std_app_led_task_hal_init_callback)(void);
extern void (*std_app_led_task_hal_led_on_callback)(uint8_t led_num);
extern void (*std_app_led_task_hal_led_off_callback)(uint8_t led_num);

typedef struct {
    uint8_t led_num;
    uint16_t period;
    uint16_t on_time;
	uint8_t blink_times;
	uint8_t on_off;
} LED_MSG_STRUCT;
//input msg name: "std_app_led_msg"
#endif


#if (JXOS_STD_APP_BUTTON_SCAN_TASK_ENABLE == 1)
#define STD_APP_BUTTON_SCAN_EVENT_PRESS				0
#define STD_APP_BUTTON_SCAN_EVENT_RELEASE			1
#define STD_APP_BUTTON_SCAN_EVENT_LONG_PRESS		2
#define STD_APP_BUTTON_SCAN_EVENT_LONG_PRESS_REPEAT	3
#if (JXOS_MSG_ENABLE == 1)
typedef struct {
    uint8_t button_num;
    uint8_t button_event;
} STD_APP_BUTTON_SCAN_MSG_STRUCT;
extern JXOS_MSG_HANDLE std_app_button_scan_msg;
#endif
extern uint8_t std_app_button_event_msg[];
#endif

#if (JXOS_STD_APP_BUTTON_TASK_ENABLE == 1)
void std_app_button_task_button_press_interrupt_handler(void);
extern void (*std_app_button_task_hal_init_callback)(void);
extern uint8_t (*std_app_button_task_hal_read_button_state_callback)(uint8_t button_num);

typedef struct {
    uint8_t button_num;
    uint8_t multi_click_count;
} BUTTON_MSG_STRUCT;
//output msg name: "std_app_button_msg"
#endif

#if (JXOS_STD_APP_FRAME_SEND_TASK_ENABLE == 1)
extern void (*std_app_frame_send_task_hal_send_data_callback)(uint8_t* send_data, uint8_t send_data_len);
void std_app_frame_send_task_stop_repeat(void);
uint8_t std_app_frame_send_task_send_data(uint8_t* frame_send, uint8_t frame_send_len);
uint8_t std_app_frame_send_task_send_data_no_repeat(uint8_t* frame_send, uint8_t frame_send_len);
//msg pipe name: "frame_send_data_pipe"
#endif

#if (JXOS_STD_APP_COMPOSITE_KEY_TASK_ENABLE == 1)
typedef uint8_t COMPOSITE_KEY_HANDLE;
COMPOSITE_KEY_HANDLE* std_app_composite_key_create(uint8_t* composite_key_list, uint8_t composite_key_list_len,
					uint16_t long_press_time);
//msg pipe name: "cp_key_pipe"
#endif

#if (JXOS_STD_APP_S_COMM_TASK_ENABLE == 1)
uint8_t std_app_s_byte_comm_send(uint8_t send_byte);
void std_app_s_byte_comm_rec_isr(uint8_t rec_byte);
extern void (*s_byte_comm_send_byte_blocked_config_callback)(uint8_t send_byte);
extern uint8_t (*s_byte_comm_rec_byte_event_callback)(void);
#endif

#if (JXOS_STD_APP_VALUE_MOVE_TASK_ENABLE == 1)
void std_app_value_move_task_move_to(uint8_t num, uint16_t val, uint16_t move_time_ms);
void std_app_value_move_task_move_to_by_step(uint8_t num, uint16_t val, uint16_t steps);
//move to the "max_val" in the first round
void std_app_value_move_task_circle_move(uint8_t num, uint16_t max_val, uint16_t min_val, uint16_t circle_time_ms);
void std_app_value_move_task_circle_move_by_step(uint8_t num, uint16_t max_val, uint16_t min_val, uint16_t steps);
void std_app_value_move_task_stop_move(uint8_t num);
#endif

#endif
