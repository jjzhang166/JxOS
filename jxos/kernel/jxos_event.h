#ifndef __JXOS_EVENT_H
#define __JXOS_EVENT_H

#include <jxos_config.h>
#include "../lib/type.h"

#if (JXOS_ENENT_ENABLE == 1)

typedef uint8_t JXOS_EVENT_HANDLE;

void jxos_event_init(void);
uint8_t jxos_event_check_empty_all(void);

JXOS_EVENT_HANDLE jxos_event_create(void);
//jxos_event_delete
uint8_t jxos_event_set(JXOS_EVENT_HANDLE event_h);
uint8_t jxos_event_wait(JXOS_EVENT_HANDLE event_h);


#endif // JXOS_ENENT_ENABLE

#endif // __JXOS_EVENT_H
