
#ifndef __JXOS_MAIL_BOX_H
#define __JXOS_MAIL_BOX_H

#include <jxos_config.h>
#include "../lib/type.h"

#if (JXOS_MAIL_BOX_ENABLE == 1)

typedef void* JXOS_MAIL_BOX_HANDLE;

void jxos_mail_box_init(void);
uint8_t jxos_mail_box_check_empty_all(void);

JXOS_MAIL_BOX_HANDLE jxos_mail_box_create(uint8_t* space,
						uint16_t space_len,
						uint8_t data_size,
						char* mail_box_name);

uint8_t jxos_mail_box_send(JXOS_MAIL_BOX_HANDLE mail_box,
							uint8_t* data_to_send, uint8_t data_to_send_len);
uint8_t jxos_mail_box_receive(JXOS_MAIL_BOX_HANDLE mail_box, char* receiver_name,
							uint8_t* receive_buffer, uint8_t* receive_data_len);
uint8_t jxos_mail_box_check_empty(JXOS_MAIL_BOX_HANDLE mail_box);		//err:0xff; true:1; false:0;
uint8_t jxos_mail_box_check_full(JXOS_MAIL_BOX_HANDLE mail_box);		//err:0xff; true:1; false:0;
uint8_t jxos_mail_box_clean_up(JXOS_MAIL_BOX_HANDLE mail_box,
								char* receiver_name);
uint8_t jxos_mail_box_receiver_name_register(JXOS_MAIL_BOX_HANDLE mail_box, char* receiver_name);
uint8_t jxos_mail_box_receiver_name_cancel(JXOS_MAIL_BOX_HANDLE mail_box, char* receiver_name);
#if (JXOS_REGISTRY_ENABLE == 1)
JXOS_MAIL_BOX_HANDLE jxos_mail_box_get_handle(char* mail_box_name);
#endif

#endif

#endif // __JXOS_MAIL_BOX_H
