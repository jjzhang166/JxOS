#include "jxos_malloc.h"

JXOS_MALLOC_MEM_POOL_STRUCT* jxos_malloc_mem_pool;// = 0;
uint8_t jxos_malloc_set_mem_pool(JXOS_MALLOC_MEM_POOL_STRUCT* mem_pool)
{
	if(mem_pool != 0){
		jxos_malloc_mem_pool = mem_pool;
		return 1;
	}
	else{
		return 0;
	}
}

void* jxos_malloc(uint8_t size)
{
	void* p = 0;
	if((size != 0)&&
    (jxos_malloc_mem_pool->offset <= (jxos_malloc_mem_pool->len)-size)){
		p = (void*)((jxos_malloc_mem_pool->mem_pool)+(jxos_malloc_mem_pool->offset));
        jxos_malloc_mem_pool->offset += size;
	}
	return p;
}
