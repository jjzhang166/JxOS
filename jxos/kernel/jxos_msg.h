
#ifndef __JXOS_MSG_H
#define __JXOS_MSG_H

#include <jxos_config.h>
#include "../lib/type.h"

#if (JXOS_MSG_ENABLE == 1)

typedef void* JXOS_MSG_HANDLE;

void jxos_msg_init(void);
uint8_t jxos_msg_check_empty_all(void);

JXOS_MSG_HANDLE jxos_msg_create(uint8_t* queue_space,
						uint16_t queue_space_len,
						uint8_t item_size,
						const char* name);
//delete
uint8_t jxos_msg_send(JXOS_MSG_HANDLE queue_h, const void* item_to_queue);
uint8_t jxos_msg_receive(JXOS_MSG_HANDLE queue_h, void* item_buffer);
uint8_t jxos_msg_check_empty(JXOS_MSG_HANDLE queue_h);
void jxos_msg_clean_up(JXOS_MSG_HANDLE queue_h);
uint8_t* jxos_msg_get_name(JXOS_MSG_HANDLE queue_h);
#if (JXOS_REGISTRY_ENABLE == 1)
JXOS_MSG_HANDLE jxos_msg_get_handle(const char* name);
#endif



#endif

#endif // __JXOS_MSG_H
