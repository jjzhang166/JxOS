
#ifndef __JXOS_H
#define __JXOS_H

#include <jxos_config.h>
#include "../lib/type.h"

#if (JXOS_REGISTRY_ENABLE == 1)

uint8_t jxos_registry_register(const char* name, void* handle);
void* jxos_registry_find(const char* name);

#endif

#endif
