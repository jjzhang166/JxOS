
#ifndef __JXOS_TASK_H
#define __JXOS_TASK_H

#include <jxos_config.h>
#include "../lib/type.h"

#if (JXOS_TASK_ENABLE == 1)

typedef void* JXOS_TASK_HANDLE ;

#if (JXOS_Compiler_optimization_1 == 1)
typedef void (*TASK_FUNCTION)(void);
#else
typedef void (*TASK_FUNCTION)(uint8_t task_id, void * parameter);
#endif

void jxos_task_init(void);
void jxos_task_schedule(void);

JXOS_TASK_HANDLE jxos_task_create(TASK_FUNCTION function, const char* name, void* parameter);
//jxos_task_delete
uint8_t jxos_task_resume(JXOS_TASK_HANDLE h);
uint8_t jxos_task_suspended(JXOS_TASK_HANDLE h);
uint8_t* jxos_task_get_name(JXOS_TASK_HANDLE h);
#if (JXOS_REGISTRY_ENABLE == 1)
JXOS_TASK_HANDLE jxos_task_get_handle(const char* name);
#endif

#endif

#endif // __JXOS_TASK_H
