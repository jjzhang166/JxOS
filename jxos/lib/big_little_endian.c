

unsigned char is_big_endian(void)
{
	unsigned int test;
	unsigned char *p;
	test = 0x1234;
	p = (unsigned char *)&test;

	if(*p == 0x34){
//		printf("this cpu is LITTLE endian\r\n");
		return 0;
	}
	else{
//		printf("this cpu is BIG endian\r\n");
		return 1;
	}
}

void int_to_little_endian_buffer(unsigned char* buffer, unsigned long int_data, unsigned int copy_byte_num)
{
    unsigned int i;
    for (i = 0; i < copy_byte_num; i++)
    {
        buffer[i] = (unsigned char)(int_data & 0x00000000000000ff);
        int_data >>= 8;
    }
}

void int_to_big_endian_buffer(unsigned char* buffer, unsigned long int_data, unsigned int copy_byte_num)
{
    unsigned int i;
    for (i = 0; i < copy_byte_num; i++)
    {
        buffer[copy_byte_num-i-1] = (unsigned char)(int_data & 0x00000000000000ff);
        int_data >>= 8;
    }
}

unsigned long little_endian_buffer_to_int(unsigned char* buffer, unsigned int copy_byte_num)
{
    unsigned long int_data = 0;
    unsigned int i;
    for (i = 0; i < copy_byte_num; i++)
    {
        int_data <<= 8;
        int_data += buffer[copy_byte_num-i-1];
    }
    return int_data;
}

unsigned long big_endian_buffer_to_int(unsigned char* buffer, unsigned int copy_byte_num)
{
    unsigned long int_data = 0;
    unsigned int i;
    for (i = 0; i < copy_byte_num; i++)
    {
        int_data <<= 8;
        int_data += buffer[i];
    }
    return int_data;
}