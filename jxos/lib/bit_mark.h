
#ifndef __BIT_MARK_H
#define __BIT_MARK_H

#include "type.h"

typedef struct
{
	uint8_t* bit_mark_buffer;
    uint16_t bit_mark_count;	//0~0xffff
} BIT_MARK_STRUCT;

uint8_t bit_mark_init(BIT_MARK_STRUCT* bit_mark,
				uint8_t* buffer, uint8_t buffer_len, uint16_t bit_mark_count);	//bit_mark_count 0~0xffff
uint8_t bit_mark_set(BIT_MARK_STRUCT* bit_mark, uint16_t bits);		//bits 0~0xffff
uint8_t bit_mark_reset(BIT_MARK_STRUCT* bit_mark, uint16_t bits);	//bits 0~0xffff
uint8_t bit_mark_get(BIT_MARK_STRUCT* bit_mark, uint16_t bits);		//bits 0~0xffff
void bit_mark_reset_all(BIT_MARK_STRUCT* bit_mark);
uint8_t bit_mark_check_all_reset(BIT_MARK_STRUCT* bit_mark);		//1:is all reset; 0:not all reset

#endif // __BIT_MARK_H

