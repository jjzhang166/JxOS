
#ifndef __BUTTON__H
#define __BUTTON__H

#include "type.h"

#define BUTTON_STATE_RELEASE				0
#define BUTTON_STATE_PRESS					1
#define BUTTON_STATE_LONG_PRESS				2

typedef struct __BUTTON_STRUCT__ BUTTON_STRUCT;
struct __BUTTON_STRUCT__{
	/************************************************/
	/******************attribute*********************/
	//config_attribute
	bool_t pin_press_level_config;
	uint16_t jitter_tick_config;
	uint16_t long_press_tick_config;
	uint16_t long_press_repeat_tick_config;

	//public_attribute	
	uint8_t state;

	//private_attribute
	uint16_t press_tick_count;

	/************************************************/
	/******************action************************/
	//private_action

	//public_action
	//void button_scan_tick_handler(BUTTON_STRUCT* button, uint8_t tick);
	
	/************************************************/
	/******************callback**********************/
	//config_callbakc
	bool_t (*read_pin_level_config_callback)(BUTTON_STRUCT* button);

	//event_callback
	void (*press_event_callback)(BUTTON_STRUCT* button);
	void (*release_event_callback)(BUTTON_STRUCT* button);
	void (*long_press_event_callback)(BUTTON_STRUCT* button);
	void (*long_press_repeat_event_callback)(BUTTON_STRUCT* button);
};

//public_action
void button_scan_tick_handler(BUTTON_STRUCT* button, uint8_t tick);



#endif //
