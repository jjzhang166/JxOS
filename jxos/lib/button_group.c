
#include "button_group.h"
#include "string.h"

void button_group_scan_tick_handler(BUTTON_GROUP_STRUCT* button_group, uint16_t ticks)
{
    uint8_t i;
    if(ticks > 0){
        for (i = 0; i < button_group->button_num; i++) {
			button_scan_tick_handler(&(button_group->button[i]), ticks);
		}
	}
}

void button_group_init(BUTTON_GROUP_STRUCT* button_group,
						uint8_t button_group_button_num,
						BUTTON_STRUCT* button_space)
{
	memset(button_space, 0, sizeof(BUTTON_STRUCT)*button_group_button_num);
	button_group->button_num = 0;
	button_group->button_num_max = button_group_button_num;
	button_group->button = button_space;
}

BUTTON_STRUCT* button_new(BUTTON_GROUP_STRUCT* button_group, BUTTON_STRUCT* new_button)
{
    BUTTON_STRUCT* button = 0;
   	if(button_group->button_num < button_group->button_num_max){
	    button = &(button_group->button[button_group->button_num]);
		memcpy((uint8_t*)button, (uint8_t*)new_button, sizeof(BUTTON_STRUCT));
		button_group->button_num++;
        // button = button_group->button;
		// button[button_group->button_num].jitter_tick = new_button.jitter_tick;
		// button[button_group->button_num].long_press_tick = new_button.long_press_tick;
		// button[button_group->button_num].long_press_repeat_tick = long_press_repeat_tick;
		// button[button_group->button_num].read_pin_level_config_callback = read_pin_level_config_handler;
		// button[button_group->button_num].press_event_callback = press_event_handler;
		// button[button_group->button_num].release_event_callback = release_event_handler;
		// button[button_group->button_num].long_press_event_callback = long_press_event_handler;
		// button[button_group->button_num].press_tick_count = 0;
		// button[button_group->button_num].state = BUTTON_RELEASE;
        // button = &(button[button_group->button_num]);
        return button;
    }
    else{
        return 0;
    }
}
