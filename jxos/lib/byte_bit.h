
#ifndef _BYTE_BIT_H_
#define _BYTE_BIT_H_

#include "lib/type.h"

//if input_bit was not 0 or 1,the bit_to_byte will reset to rec frist bit
//when bit_to_byte was rec 8 bits, the return val is 1, otherwise return 0;
//LSB: Least Significant Bit(right bit, 0 bit), MSB: Most Significant Bit(lift bit, 8 bit)
uint8_t bit_to_byte_msb_frist(uint8_t* output_byte, uint8_t* count, uint8_t input_bit);
uint8_t bit_to_byte_lsb_frist(uint8_t* output_byte, uint8_t* count, uint8_t input_bit);

uint8_t byte_to_bit_msb_frist(uint8_t* output_bit, uint8_t* count, uint8_t input_byte);
uint8_t byte_to_bit_lsb_frist(uint8_t* output_bit, uint8_t* count, uint8_t input_byte);

#endif