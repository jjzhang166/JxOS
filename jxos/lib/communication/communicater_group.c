

#include "communicater_group.h"

void communicater_group_tick_handler(COMMUNICATER_GROUP_STRUCT* communicater_group, uint16_t ticks)
{
    uint8_t i;
    if(ticks > 0){
        for (i = 0; i < communicater_group->communicater_num_max; i++) {
            if(communicater_group->communicater_init_flag[i] == true){
                communicater_tick_handler(&(communicater_group->communicater[i]), ticks);
            }
        }
    }
}

void communicater_group_match_handler(COMMUNICATER_GROUP_STRUCT* communicater_group, uint8_t* response_match_info, uint8_t response_match_info_len)
{
    uint8_t i;
    for (i = 0; i < communicater_group->communicater_num_max; i++) {
        if(communicater_group->communicater_init_flag[i] == true){
            communicater_match_handler(&(communicater_group->communicater[i]),response_match_info, response_match_info_len);
        }
    }
}

void communicater_group_init(COMMUNICATER_GROUP_STRUCT* communicater_group,
                                    uint8_t communicater_group_max_num,
                                    uint8_t* communicater_init_flag_space,
                                    COMMUNICATER_STRUCT* communicater_space)
{
      memset(communicater_space, 0, sizeof(COMMUNICATER_STRUCT)*communicater_group_max_num);
      memset(communicater_init_flag_space, false, communicater_group_max_num);
      communicater_group->communicater_num_max = communicater_group_max_num;
      communicater_group->communicater_num = 0;
      communicater_group->communicater_init_flag = communicater_init_flag_space;
      communicater_group->communicater = communicater_space;
}

COMMUNICATER_STRUCT* communicater_new(COMMUNICATER_GROUP_STRUCT* communicater_group)
{
    COMMUNICATER_STRUCT* communicater = 0;
    uint8_t i = 0;
      if(communicater_group->communicater_num < communicater_group->communicater_num_max){
        for(i = 0; i < communicater_group->communicater_num_max; i++){
            if(communicater_group->communicater_init_flag[i] == false){
                communicater = &(communicater_group->communicater[i]);
                communicater_group->communicater_num++;
                communicater_group->communicater_init_flag[i] = true;
                return communicater;
            }
        }
    }
    return 0;
}

bool_t communicater_del(COMMUNICATER_GROUP_STRUCT* communicater_group, COMMUNICATER_STRUCT* del_communicater)
{
    uint8_t i = 0;

   for(i = 0; i < communicater_group->communicater_num_max; i++){
        if(del_communicater == &(communicater_group->communicater[i])){
            communicater_group->communicater_init_flag[i] = false;
            communicater_group->communicater_num--;
            return true;
        }
    }

    return false;
}


/********************************************************************/
////e.g./////
/********************************************************************/
#include "communicater_group.h"

COMMUNICATER_GROUP_STRUCT communicater_group_test;
bool_t communicater_group_test_init_flag_space[8];
COMMUNICATER_STRUCT communicater_group_test_space[8];

#include<Windows.h>
#include<stdio.h>
DWORD WINAPI communicater_task(LPVOID p)
{
    while(1){
        communicater_group_tick_handler(&communicater_group_test, 100);
        Sleep(100);
    }

    return 0;
}

DWORD WINAPI receivie_task(LPVOID p)
{
    char c;
    while(1){
        c = getchar();
        switch (c){
        case '1':
            communicater_group_match_handler(&communicater_group_test, "AAA", 3);
            break;

        case '2':
            communicater_group_match_handler(&communicater_group_test, "BBB", 3);
            break;

        default:
            break;
        }
        Sleep(100);
    }

    return 0;
}


/******************************/
void send_print(uint8_t* send_msg, uint8_t send_msg_len)
{
    printf("send_print: %s\r\n", send_msg);
}
DWORD WINAPI usr_task1(LPVOID p)
{
    COMMUNICATER_STRUCT* cmt_test;
    uint8_t rep_state;

    cmt_test = communicater_new(&communicater_group_test);
    if(cmt_test == 0){
        printf("usr_task1 communicater_new error\r\n");
        return 0;
    }
    communicater_send(cmt_test, "usr_task1 send", 9, 5, 1000, send_print);
    communicater_set_response_macth_info(cmt_test, "AAA", 3);

    if(communicater_wait_response_blocked(cmt_test)){
        printf("usr_task1 get rsp: %s\r\n", cmt_test->matcher.match_info_config);
    }
    else{
        printf("usr_task1 get rsp overtime\r\n");
    }

    communicater_del(&communicater_group_test, cmt_test);

    return 0;
}

DWORD WINAPI usr_task2(LPVOID p)
{
    COMMUNICATER_STRUCT* cmt_test;
    uint8_t rep_state;

    cmt_test = communicater_new(&communicater_group_test);
    if(cmt_test == 0){
        printf("usr_task2 communicater_new error\r\n");
        return 0;
    }
    communicater_send(cmt_test, "usr_task2 send", 9, 5, 1000, send_print);
    communicater_set_response_macth_info(cmt_test, "BBB", 3);

    if(communicater_wait_response_blocked(cmt_test)){
        printf("usr_task2 get rsp: %s\r\n", cmt_test->matcher.match_info_config);
    }
    else{
        printf("usr_task2 get rsp overtime\r\n");
    }

    communicater_del(&communicater_group_test, cmt_test);

    return 0;
}

/******************************/
void communicater_group_eg(void)
{
    communicater_group_init(&communicater_group_test, 8, communicater_group_test_init_flag_space, communicater_group_test_space);

    CreateThread(NULL, 0, communicater_task, 0, 0, NULL);
    CreateThread(NULL, 0, receivie_task, 0, 0, NULL);

    CreateThread(NULL, 0, usr_task1, 0, 0, NULL);
    Sleep(300);
    CreateThread(NULL, 0, usr_task2, 0, 0, NULL);

    while(1);
}
/******************************/

