

#ifndef __COMMUNICATER_GROUP_H__
#define __COMMUNICATER_GROUP_H__

#include "communicater.h"

typedef struct
{
      uint8_t communicater_num_max;
      uint8_t communicater_num;
    bool_t* communicater_init_flag;
      COMMUNICATER_STRUCT* communicater;
} COMMUNICATER_GROUP_STRUCT;

void communicater_group_init(COMMUNICATER_GROUP_STRUCT* communicater_group,
                                    uint8_t communicater_group_max_num,
                                    uint8_t* communicater_init_flag_space,
                                    COMMUNICATER_STRUCT* communicater_space);

void communicater_group_tick_handler(COMMUNICATER_GROUP_STRUCT* communicater_group, uint16_t ticks);
void communicater_group_match_handler(COMMUNICATER_GROUP_STRUCT* communicater_group, uint8_t* response_match_info, uint8_t response_match_info_len);

COMMUNICATER_STRUCT* communicater_new(COMMUNICATER_GROUP_STRUCT* communicater_group);
bool_t communicater_del(COMMUNICATER_GROUP_STRUCT* communicater_group, COMMUNICATER_STRUCT* del_communicater);

#endif
