

#include "matcher.h"

#define MATCHER_STATE_NO                        0
#define MATCHER_STATE_YES                       1

bool_t matcher_match(MATCHER_STRUCT* mch, uint8_t* match_info, uint8_t match_info_len)
{
    if(match_info_len == mch->match_info_len_config){
        if(memcmp(match_info, mch->match_info_config, match_info_len) == 0){
            mch->state = MATCHER_STATE_YES;
            return true;
        }
    }
    mch->state = MATCHER_STATE_NO;
    return false;
}
