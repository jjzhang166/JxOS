
#ifndef __MATCHER_H__
#define __MATCHER_H__

#include "type.h"

#define MATCHER_STATE_NO                        0
#define MATCHER_STATE_YES                       1

typedef struct __MATCHER_STRUCT__ MATCHER_STRUCT;
struct __MATCHER_STRUCT__{
    uint8_t* match_info_config;
    uint8_t match_info_len_config;
    uint8_t state;
};

bool_t matcher_match(MATCHER_STRUCT* mch, uint8_t* match_info, uint8_t match_info_len);
bool_t matcher_check(MATCHER_STRUCT* mch);

#endif // __MATCHER_H__
