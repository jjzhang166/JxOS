

#include "retransmiter.h"

void retransmiter_tick_handler(RETRANSMITER_STRUCT* rpt, uint16_t tick)
{
    if(rpt->state == RETRANSMITER_STATE_REPEATING){
        if(rpt->repeat_counter >= rpt->repeat_max_config){
            rpt->repeat_counter = 0;
            rpt->state = RETRANSMITER_STATE_OVERTIME;
        }
        else{
            rpt->repeat_tick_counter += tick;
            if(rpt->repeat_tick_counter >= rpt->repeat_cycle_tick_config){
                rpt->repeat_tick_counter = 0;
                rpt->repeat_counter++;
                rpt->send_msg_config_callback(rpt->msg, rpt->msg_len);
            }
        }
    }
}

void retransmiter_send(RETRANSMITER_STRUCT* rpt)
{
    rpt->repeat_counter = 0;
    rpt->repeat_tick_counter = 0;
    rpt->state = RETRANSMITER_STATE_REPEATING;
    rpt->send_msg_config_callback(rpt->msg, rpt->msg_len);
}

void retransmiter_stop_repeat(RETRANSMITER_STRUCT* rpt)
{
    rpt->state = RETRANSMITER_STATE_STOP;
}
