
#ifndef __RETRANSMITER_H__
#define __RETRANSMITER_H__

#include "type.h"

#define RETRANSMITER_STATE_REPEATING                    0
#define RETRANSMITER_STATE_STOP                         1
#define RETRANSMITER_STATE_OVERTIME                     2

typedef struct __RETRANSMITER_STRUCT__ RETRANSMITER_STRUCT;
struct __RETRANSMITER_STRUCT__{
    uint8_t* msg;
    uint8_t msg_len;
    uint8_t repeat_counter;
    uint16_t repeat_tick_counter;
    uint8_t state;
    uint8_t repeat_max_config;
    uint16_t repeat_cycle_tick_config;
    void (*send_msg_config_callback)(uint8_t* send_msg, uint8_t send_msg_len);
};

void retransmiter_tick_handler(RETRANSMITER_STRUCT* rpt, uint16_t tick);
void retransmiter_send(RETRANSMITER_STRUCT* rpt);
void retransmiter_stop_repeat(RETRANSMITER_STRUCT* rpt);

#endif // __RETRANSMITER_H__
