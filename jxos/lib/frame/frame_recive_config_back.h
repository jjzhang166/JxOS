
#ifndef __FRAME_RECIVE_CONFIG_H
#define __FRAME_RECIVE_CONFIG_H


/*******************************************/
#define FRAME_RECIVE_BUFFER_LEN     48

/*******************************************/
#define FRAME_FINISH_CHECK_BY_TAIL      0
#define FRAME_FINISH_CHECK_BY_LEN	    1

#define FRAME_FINISH_CHECK_BY	FRAME_FINISH_CHECK_BY_TAIL

#define FRAME_HEADER		{'s','t','a','r','t'}
#define FRAME_TAIL			{'s','t','o','p'}
//if use the frame len mode, the frame len data(1byte) must be just behind the frame head

/*******************************************/
#define FRAME_VERIFY_CHECK_BY_XOR       0
#define FRAME_VERIFY_CHECK_BY_CRC16	    1

#define FRAME_VERIFY_CHECK_BY	FRAME_VERIFY_CHECK_BY_XOR

#define FRAME_VERIFY_CHECK_INCLUDE_FRAME_HEAD	true

#endif
