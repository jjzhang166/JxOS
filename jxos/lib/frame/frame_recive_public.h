#ifndef __FRAME_RECIVE_PUBLIC_H
#define __FRAME_RECIVE_PUBLIC_H

#include "../../lib/type.h"

void frame_recive_start_check_callback_reg(uint8_t (*frame_recive_start_check_callback)(uint8_t rec_byte));
void frame_recive_start_check_init_callback_reg(void (*frame_recive_start_check_init_callback)(void));
//copy the frame head to *frame_head, and return frame_head_len
void frame_recive_get_frame_head_callback_reg(void (*frame_recive_get_frame_head_callback)(uint8_t* frame_head, uint8_t* frame_head_len));
void frame_recive_finish_check_callback_reg(uint8_t (*frame_recive_finish_check_callback)(uint8_t rec_byte));
void frame_recive_finish_check_init_callback_reg(void (*frame_recive_finish_check_init_callback)(void));
void frame_recice_verify_check_callback_reg(uint8_t (*frame_recice_verify_check_callback)(uint8_t* recive_frame, uint8_t recive_frame_len));
void frame_recice_success_callback_reg(void (*frame_recice_success_callback)(uint8_t* recive_frame, uint8_t recive_frame_len));
void frame_recive_init(void);
void frame_recive_handler(uint8_t rec_byte);

void frame_recive_std_init(void);
/**
  	frame_recive_start_check_callback_reg(frame_recive_start_check);
	frame_recive_start_check_init_callback_reg(frame_recive_start_check_init);
	frame_recive_get_frame_head_callback_reg(frame_recive_get_frame_head);
	frame_recive_finish_check_callback_reg(frame_recive_finish_check);
	frame_recive_finish_check_init_callback_reg(frame_recive_finish_check_init);
	frame_recive_init();
**/

#endif // __FRAME_RECIVE_PUBLIC_H
