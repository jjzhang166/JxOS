

#include "../../lib/type.h"

#include "frame_recive_config.h"

#if (FRAME_VERIFY_CHECK_BY == FRAME_VERIFY_CHECK_BY_XOR)
static uint8_t XOR_Check_Sum(uint8_t *dat,uint8_t length)
{
	uint8_t result;
	result = 0;
	for(; length > 0; length--)
	{
		result = result ^ *dat;
		dat++;
	}
	return result;
}
#endif

#if (FRAME_VERIFY_CHECK_BY == FRAME_VERIFY_CHECK_BY_CRC16)
//==[sys_nfc_crc16]=========================================
//  Abstract	: 
//  Param 		: Len->number of protocol bytes without CRC
//				  pBuf->data buffer need to CRC calcute
//  Return		: crc result
//  Detail  	: CRC16:x16+x12+x5+1, polynom:0x1021, reverse polynom:0x8408
//  Attention	: low bit crc first.
//  Author   	: 2018/10/23 Created by Ray Lei
//==========================================================
static uint16_t sys_nfc_crc16(uint8_t* pBuf, uint8_t Len)
{
	uint16_t Polynom= 0x8408;
	uint16_t CrcCalc= 0xFFFF; 
	uint8_t* pTmp 	= pBuf;
	uint8_t ByteNum, BitNum;
	
	for( ByteNum = 0; ByteNum < Len; ByteNum++ ){ 
		CrcCalc ^= *pTmp++; 

		for( BitNum = 0; BitNum < 8; BitNum++ ){ 
			if( CrcCalc & 0x01 ){ 
				CrcCalc >>= 1; 
				CrcCalc ^= Polynom; 
			}else{ 
				CrcCalc >>= 1; 
			}
		}
	}
	
	return CrcCalc;
}
#endif

uint8_t frame_recice_verify_check_callback(uint8_t* recive_frame, uint8_t recive_frame_len)
{
 //   FRAME_VERIFY_CHECK_INCLUDE_FRAME_HEAD
    return 1;
}
