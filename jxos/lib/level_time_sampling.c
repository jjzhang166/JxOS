
#include "level_time_sampling.h"

void level_time_sampling_init(LEVEL_TIME_SAMPLING_STRUCT* level_time_sampling,
							void (*finish_callback)(struct LEVEL_TIME_SAMPLING_STRUCT* level_time_sampling))
{
    level_time_sampling->state.last_level = LEVEL_TIME_SAMPLING_LOW_LEVEL;
    level_time_sampling->state.current_level = LEVEL_TIME_SAMPLING_LOW_LEVEL;
    level_time_sampling->count = 0;
	level_time_sampling->finish_callback = finish_callback;
}

void level_time_sampling_reset_counter(LEVEL_TIME_SAMPLING_STRUCT* level_time_sampling)
{
   level_time_sampling->count = 0;
}

void level_time_sampling_tick_handler(LEVEL_TIME_SAMPLING_STRUCT* level_time_sampling, 
                                        uint8_t new_level)
{
	level_time_sampling->count++;
	if((new_level != level_time_sampling->state.current_level)
		||(level_time_sampling->count == 0XFFFF)){
		if((new_level == LEVEL_TIME_SAMPLING_HIGH_LEVEL)
			||(new_level == LEVEL_TIME_SAMPLING_LOW_LEVEL)){
			level_time_sampling->state.last_level = level_time_sampling->state.current_level;
			level_time_sampling->state.current_level = new_level;
		}
		if(level_time_sampling->finish_callback != 0){
			level_time_sampling->finish_callback(level_time_sampling);
		}
		level_time_sampling->count = 0;
	}
}

