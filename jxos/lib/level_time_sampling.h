

#ifndef _LEVEL_TIME_SAMPLING_H_
#define _LEVEL_TIME_SAMPLING_H_

#include "lib/type.h"

/******************************************
when "finish_callback()" called, 
the "count" was the tick times record of "last_level";
and if "last_level" was not the same as "current_level",
that means an edge has be detected
******************************************/

#define LEVEL_TIME_SAMPLING_HIGH_LEVEL  1
#define LEVEL_TIME_SAMPLING_LOW_LEVEL   0

typedef struct{ 
 uint8_t b7	:1;
 uint8_t b6	:1;
 uint8_t b5	:1;
 uint8_t b4	:1;
 uint8_t b3	:1;
 uint8_t b2	:1;
 uint8_t current_level  :1;
 uint8_t last_level	    :1;
} LEVEL_TIME_SAMPLING_STATE_STRUCT;	

struct __LEVEL_TIME_SAMPLING_STRUCT__{
	uint16_t count;
	LEVEL_TIME_SAMPLING_STATE_STRUCT state;
    void (*finish_callback)(struct __LEVEL_TIME_SAMPLING_STRUCT__* level_time_sampling);
};
typedef struct __LEVEL_TIME_SAMPLING_STRUCT__ LEVEL_TIME_SAMPLING_STRUCT;

void level_time_sampling_init(LEVEL_TIME_SAMPLING_STRUCT* level_time_sampling,
					void (*finish_callback)(struct LEVEL_TIME_SAMPLING_STRUCT* level_time_sampling));
void level_time_sampling_tick_handler(LEVEL_TIME_SAMPLING_STRUCT* level_time_sampling, 
                                        uint8_t new_level);
void level_time_sampling_reset_counter(LEVEL_TIME_SAMPLING_STRUCT* level_time_sampling);

#endif