
#ifndef __LIB_H
#define __LIB_H

#include "type.h"

uint32_t __ratio(uint32_t a_reference,
					uint32_t a_start, uint32_t a_end,
					uint32_t b_start, uint32_t b_end);
		
void __itoa(uint8_t* string, uint8_t* string_len, 
					int32_t value, uint8_t radix, uint8_t is_signed);

uint8_t __include_check(uint8_t* buff, uint8_t buff_len, uint8_t obj);

uint8_t __cycle_increase(uint8_t begin, uint8_t add,
						uint8_t range_start, uint8_t range_end);
						
#endif