/*
 * rtc_time.h
 *
 *  Created on: 2017��4��10��
 *      Author: zengjie
 */

#ifndef SRC_RTC_TIME_H_
#define SRC_RTC_TIME_H_

#include <time.h>

#define RTC_START_TIME_SEC		0
#define RTC_START_TIME_MIN		22
#define RTC_START_TIME_HOUR		11
#define RTC_START_TIME_MDAY		13
#define RTC_START_TIME_MON		8
#define RTC_START_TIME_YEAR		2017

void rtc_time_init(void);
void rtc_time_tick(void);

struct tm rtc_time_get_time(void);
void rtc_time_set_time(struct tm set_time);


#endif /* SRC_RTC_TIME_H_ */
