#ifndef __IO_SOFT_SPI_H__
#define __IO_SOFT_SPI_H__

#include "stdint.h"

typedef struct _spi_sw SPI_SW;

typedef struct {
	uint8_t cpol;	//1:空闲时钟为高电平 0:空闲时钟为低电平
	uint8_t cpha;	//1:第二边沿采样	 0:第一边沿采样
} SPI_SW_PRARM;		//spi_sw 参数

typedef struct {
	void (*pin_init)(void);

	void (*sclk_out_h)(void);
	void (*sclk_out_l)(void);

	void (*mosi_out_h)(void);
	void (*mosi_out_l)(void);

	uint8_t (*miso_in)(void);

	void (*delay)(void);
} SPI_SW_INTERFACE;	//用户重定义接口

typedef struct {
	void (*spi_sw_init)(SPI_SW *spi_sw);

	uint8_t (*spi_sw_master_transfer_byte)(SPI_SW *spi_sw, uint8_t byte);
} SPI_SW_API;		//API

struct _spi_sw {
	SPI_SW_PRARM prarm;			//spi_sw 参数
	SPI_SW_INTERFACE intface;	//用户重定义接口
	SPI_SW_API api;				//API
};

void SpiSwStructInit(SPI_SW* spi_sw, SPI_SW_PRARM prm, SPI_SW_INTERFACE intface);

#endif
