/*
 * software_timer.h
 *
 *  Created on: 2017��4��10��
 *      Author: zengjie
 */

#ifndef SRC_SOFTWATE_TIMER_H_
#define SRC_SOFTWATE_TIMER_H_

#include "type.h"

typedef struct
{
	uint16_t count;
	uint16_t timeout;
	uint8_t running;
	uint8_t overtime;
} SoftwareTimer_STRUCT;

typedef struct
{
	uint8_t len;
	uint8_t count;
	SoftwareTimer_STRUCT* software_timer;
} SoftwareTimer_Group_STRUCT;

void software_timer_group_init(SoftwareTimer_Group_STRUCT* software_timer_group,
                               uint8_t software_timer_group_len,
                               SoftwareTimer_STRUCT* software_timer_group_space);
void software_timer_group_handler(SoftwareTimer_Group_STRUCT* software_timer_group,
                                  uint16_t tick_time);
uint16_t software_timer_group_find_minimum_time(SoftwareTimer_Group_STRUCT* software_timer_group);
uint8_t software_timer_group_check_running_all(SoftwareTimer_Group_STRUCT* software_timer_group);


SoftwareTimer_STRUCT*  software_timer_create(SoftwareTimer_Group_STRUCT* software_timer_group,
                              uint16_t timeout);
uint8_t software_timer_check_running(SoftwareTimer_STRUCT* software_timer);
uint8_t software_timer_check_overtime(SoftwareTimer_STRUCT* software_timer);
uint8_t software_timer_start(SoftwareTimer_STRUCT* software_timer);
uint8_t software_timer_stop(SoftwareTimer_STRUCT* software_timer);
uint8_t software_timer_restart(SoftwareTimer_STRUCT* software_timer);
uint8_t software_timer_set_timeout(SoftwareTimer_STRUCT* software_timer, uint16_t timeout);


#endif /* SRC_SOFTWATE_TIMER_H_ */
