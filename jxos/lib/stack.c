
#include "stack.h"

//标准接口
bool stack_empty(typestack* stack)					//判断栈是否为空
{
    bool ret = 0;
    if(stack->top > 0){
        ret = 0;
    }
    else{
        ret = 1;
    }
    return ret;
}

uint32_t stack_size(typestack* stack)				//获得栈中元素个数
{
    return (stack->top);
}

typeval* stack_top(typestack* stack)				//获取栈顶元素（返回指针 引用）
{
    return &(stack->buff[(stack->top)-1]);
}

void stack_push(typestack* stack, typeval value)	//入栈
{
    if(stack->top < stack->len){
        stack->buff[(stack->top)] = value;
        stack->top++;
    }
}

void stack_pop(typestack *stack)					//出栈，弹出栈顶元素（对比top() 移除栈顶元素, 注意不返回元素的引用）
{
    if(stack->top > 0){
        stack->top--;
    }
}

//bool stack_operator(typestack *stack)				//判断两个栈是否相等

//追加接口
bool stack_create(typestack* stack,
				void* space, uint32_t space_len)	//初始化栈
{
    if((stack != 0)&&(space != 0)&&(space_len != 0)){
        stack->buff = space;
        stack->len = space_len;
        stack->top = 0;
        return 1;
    }
    else{
        return 0;
    }
}

uint32_t stack_space_len(typestack *stack)			//获得栈总空间大小
{
    return stack->len;
}