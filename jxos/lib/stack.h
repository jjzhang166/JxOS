
#ifndef _STACK_H
#define _STACK_H

////////////////////////////////////////////////////////////////////////////////
// stack是一种先进后出(First In Last Out, FILO)的数据结构, 其只有一个出口
// 支持对栈顶元素的追加, 弹出, 获取, 但是不提供对其它位置元素的访问
////////////////////////////////////////////////////////////////////////////////
// 以下为使用deque时的布局
//
// 栈底                      当前栈顶                         预留的内存边界
// ↓                            ↓                                 ↓
// --------------------------------------------------------------------
// |   |   | ...... |   |   |   |   |   |   |   | ......  |   |   | X |
// --------------------------------------------------------------------
//                              ↑   ↑                             ↑
//                              |   |                             |
//                              |   -------------------------------
//                              |    这里是尚未使用的预留内存, 可能为0
//                              |
//                       仅支持在这里进行push(), pop(), top()操作
////////////////////////////////////////////////////////////////////////////////

#include "stdint.h"
#include "stdbool.h"

#define typeval uint8_t
typedef struct {
    uint32_t top;
    uint32_t len;
    typeval* buff;
}typestack;

//标准接口
bool stack_empty(typestack* stack);					//判断栈是否为空
uint32_t stack_size(typestack* stack);				//获得栈中元素个数
typeval* stack_top(typestack* stack);				//获取栈顶元素（返回指针 引用）
void stack_push(typestack* stack, typeval value);	//入栈
void stack_pop(typestack* stack);					//出栈，弹出栈顶元素（对比top() 移除栈顶元素, 注意不返回元素的引用）
//bool stack_operator(typestack*s tack);			//判断两个栈是否相等

//追加接口
bool stack_create(typestack* stack,
				void* space, uint32_t space_len);	//初始化栈
uint32_t stack_space_len(typestack* stack);			//获得栈总空间大小

#endif
