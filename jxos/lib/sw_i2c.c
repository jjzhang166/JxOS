#include <bsp_sw_i2c_config.h>

typedef enum
{
	I2C_ACK  = 0,
	I2C_NACK  = 1
} IC2_ACK_TypeDef;


#define i2c_delay()		bsp_sw_i2c_delay_half();bsp_sw_i2c_delay_half()
/*****************************************************************************/

void bsp_sw_i2c_reset(void) {
	bsp_sw_i2c_sda_pin_hight();
	bsp_sw_i2c_scl_pin_hight();
	i2c_delay();
}

void bsp_sw_i2c_init(void) 
{
	bsp_sw_i2c_hal_init();
	bsp_sw_i2c_reset();
}

//原则：每个步骤的初始状态由上一个步骤准备好，并在结束时为下一个步骤准备好正确的状态
static void i2c_send_start(void) {
	bsp_sw_i2c_sda_pin_out_mode();
	
	bsp_sw_i2c_sda_pin_low();
	bsp_sw_i2c_scl_pin_hight();
	i2c_delay();

	bsp_sw_i2c_sda_pin_low();
	bsp_sw_i2c_scl_pin_hight();
	i2c_delay();

	bsp_sw_i2c_sda_pin_low();
	bsp_sw_i2c_scl_pin_hlow();
}


static void i2c_send_byte(unsigned char ch) {
	unsigned char i;

	bsp_sw_i2c_sda_pin_out_mode();
	
	i2c_delay();
	i2c_delay();

	for (i = 0; i < 8; i++) {

		bsp_sw_i2c_scl_pin_hlow();
		bsp_sw_i2c_delay_half();

		if ((ch << i) & 0x80)	//send bit 1
		{
			bsp_sw_i2c_sda_pin_hight();
		} 
		else //send bit 0
		{
			bsp_sw_i2c_sda_pin_low();
		}
		bsp_sw_i2c_scl_pin_hlow();
		bsp_sw_i2c_delay_half();

		bsp_sw_i2c_scl_pin_hight();
		bsp_sw_i2c_delay_half();
		bsp_sw_i2c_delay_half();
		bsp_sw_i2c_delay_half();
	}

	bsp_sw_i2c_scl_pin_hlow();
}

static unsigned char i2c_read_byte(void) {
	unsigned char b = 0;
	unsigned char i;

	bsp_sw_i2c_sda_pin_in_mode();

	i2c_delay();
	i2c_delay();

	bsp_sw_i2c_sda_pin_hight();
	for (i = 0; i < 8; i++) {
		b <<= 1;
		bsp_sw_i2c_scl_pin_hlow();
		bsp_sw_i2c_delay_half();
		bsp_sw_i2c_delay_half();
		bsp_sw_i2c_delay_half();

		bsp_sw_i2c_scl_pin_hight();
		bsp_sw_i2c_delay_half();
		if (bsp_sw_i2c_sda_pin_rec()) {
			b |= 0x01;
		}
		bsp_sw_i2c_delay_half();

	}

	bsp_sw_i2c_scl_pin_hlow();
	return b;
}

static void i2c_send_ack(IC2_ACK_TypeDef ACK) {

	bsp_sw_i2c_sda_pin_out_mode();
		
	bsp_sw_i2c_delay_half();

	if (ACK == I2C_ACK) {
		bsp_sw_i2c_sda_pin_low();
	} else {
		bsp_sw_i2c_sda_pin_hight();
	}
	bsp_sw_i2c_delay_half();

	bsp_sw_i2c_scl_pin_hight();
	bsp_sw_i2c_delay_half();
	bsp_sw_i2c_delay_half();
	bsp_sw_i2c_delay_half();

	bsp_sw_i2c_scl_pin_hlow();
}


static IC2_ACK_TypeDef i2c_check_ack(void) {

	IC2_ACK_TypeDef ack = I2C_NACK;
	
	bsp_sw_i2c_sda_pin_in_mode();
	
	bsp_sw_i2c_delay_half();
	bsp_sw_i2c_sda_pin_hight();
	bsp_sw_i2c_delay_half();
	bsp_sw_i2c_delay_half();
	bsp_sw_i2c_scl_pin_hight();
	bsp_sw_i2c_delay_half();
	if (!bsp_sw_i2c_sda_pin_rec()) {
		ack = I2C_ACK;
	} else {
		ack = I2C_NACK;
	}
	bsp_sw_i2c_delay_half();
	
	bsp_sw_i2c_scl_pin_hlow();
	return ack;
}

static void i2c_send_stop(void) {

	bsp_sw_i2c_sda_pin_out_mode();
		
	i2c_delay();
	i2c_delay();

	bsp_sw_i2c_sda_pin_low();
	i2c_delay();
	bsp_sw_i2c_scl_pin_hight();
	i2c_delay();
	bsp_sw_i2c_sda_pin_hight();
}

static void i2c_send_repeated_start(void) {
	
	bsp_sw_i2c_sda_pin_out_mode();
		
	i2c_send_stop();
	i2c_delay();
	i2c_delay();
	i2c_send_start();
}

/*
 bool    i2c1_send_byte_with_check(unsigned char ch)
 {
 u8  i;
 for(i = 0; i < 8; i++)
 {
 if((ch << i) & 0x80)
 {
 OP_I2C1_SDA_H();
 i2c_delay();
 if(!IN_I2C1_SDA())
 return FALSE;

 OP_I2C1_SCL_H();
 i2c_delay();
 OP_I2C1_SCL_L();
 i2c_delay();
 }
 else
 {
 OP_I2C1_SDA_L();
 i2c_delay();
 if(IN_I2C1_SDA())
 return FALSE;

 OP_I2C1_SCL_H();
 i2c_delay();
 OP_I2C1_SCL_L();
 i2c_delay();
 }
 }
 return  TRUE;
 }
 */
/****************************************************************************/
typedef enum {
	I2C_WRITE_BIT = 0, I2C_RADE_BIT = 1
} IC2_RW_BIT_TypeDef;

static unsigned char slave_id_addr_rw_bit(unsigned char slave_id,
		IC2_RW_BIT_TypeDef rw_bit) 
{
	unsigned char ret = 0;

	switch (rw_bit) {
	case I2C_WRITE_BIT:
		ret = slave_id << 1;
		break;
	case I2C_RADE_BIT:
		ret = slave_id << 1;
		ret |= 0x01;
		break;
	default:
		break;
	}
	return ret;
}

void bsp_sw_i2c_burst_read(unsigned char slave_id,
		unsigned int read_reg_len,
		unsigned char *read_buf) 
{
	i2c_send_start();

	i2c_send_byte(slave_id_addr_rw_bit(slave_id, I2C_RADE_BIT));//read
	i2c_check_ack();

	i2c_delay();
	i2c_delay();
	i2c_delay();
	i2c_delay();
	
	for (; read_reg_len > 0; read_reg_len--) {
		*read_buf = i2c_read_byte();	//read reg
		read_buf++;
		if (read_reg_len == 1)	//last reg
				{
			i2c_send_ack(I2C_NACK);
		} else {
			i2c_send_ack(I2C_ACK);
		}
	}

	i2c_send_stop();
}

void bsp_sw_i2c_burst_write(unsigned char slave_id,
		unsigned int write_reg_len,
		unsigned char *write_buf) 
{
	i2c_send_start();

	i2c_send_byte(slave_id_addr_rw_bit(slave_id, I2C_WRITE_BIT));//write
	i2c_check_ack();

	for (; write_reg_len > 0; write_reg_len--) {
		i2c_send_byte(*write_buf);	//read reg
		write_buf++;
		i2c_check_ack();
	}

	i2c_send_stop();
}

void bsp_sw_i2c_burst_read_reg(unsigned char slave_id,
		unsigned char start_reg_addr, unsigned int read_reg_len,
		unsigned char *read_buf) 
{
	i2c_send_start();

	i2c_send_byte(slave_id_addr_rw_bit(slave_id, I2C_WRITE_BIT));//write
	i2c_check_ack();

	i2c_send_byte(start_reg_addr);	//send reg addr
	i2c_check_ack();

	i2c_send_repeated_start();

	i2c_send_byte(slave_id_addr_rw_bit(slave_id, I2C_RADE_BIT));//read
	i2c_check_ack();

	for (; read_reg_len > 0; read_reg_len--) {
		*read_buf = i2c_read_byte();	//read reg
		read_buf++;
		if (read_reg_len == 1)	//last reg
				{
			i2c_send_ack(I2C_NACK);
		} else {
			i2c_send_ack(I2C_ACK);
		}
	}

	i2c_send_stop();
}

void bsp_sw_i2c_burst_write_reg(unsigned char slave_id,
		unsigned char start_reg_addr, unsigned int write_reg_len,
		unsigned char *write_buf) 
{
	i2c_send_start();

	i2c_send_byte(slave_id_addr_rw_bit(slave_id, I2C_WRITE_BIT));//write
	i2c_check_ack();

	i2c_send_byte(start_reg_addr);	//send reg addr
	i2c_check_ack();

	for (; write_reg_len > 0; write_reg_len--) {
		i2c_send_byte(*write_buf);	//read reg
		write_buf++;
		i2c_check_ack();
	}

	i2c_send_stop();
}




//void i2c_rw_test(void)
//{
//	unsigned char wr_buf = 0x3f;
//	while(1){
//		GCC_CLRWDT();
//		wr_buf++;
//		i2c_burst_write(0x39, 0x42, 1, &wr_buf);
//		i2c_delay();
//		i2c_delay();
//		i2c_delay();
//		i2c_delay();
//		wr_buf = 0;
//		i2c_burst_read(0x39, 0x42, 1, &wr_buf);
//		i2c_delay();
//		i2c_delay();
//		i2c_delay();
//		i2c_delay();
//	}
//}

unsigned short bsp_sw_i2c_read_16bit_data_lsb(unsigned char slave_id, unsigned char start_reg_addr) 
{
	unsigned char buf[2] = { 0 };
	unsigned short ret = 0;

	bsp_sw_i2c_burst_read_reg(slave_id, start_reg_addr, 2, buf);

	ret = buf[1];
	ret <<= 8;
	ret |= buf[0];

	return ret;
}
