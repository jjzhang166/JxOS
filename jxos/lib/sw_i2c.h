#ifndef __SW_I2C_H__
#define __SW_I2C_H__

void bsp_sw_i2c_init(void);
void bsp_sw_i2c_reset(void);

void bsp_sw_i2c_burst_read(unsigned char slave_id,
		unsigned int read_reg_len,
		unsigned char *read_buf);
void bsp_sw_i2c_burst_write(unsigned char slave_id,
		unsigned int write_reg_len,
		unsigned char *write_buf);

void bsp_sw_i2c_burst_read_reg(unsigned char slave_id, \
                    unsigned char start_reg_addr, unsigned int read_reg_len, \
                    unsigned char *read_buf);
void bsp_sw_i2c_burst_write_reg(unsigned char slave_id, \
                    unsigned char start_reg_addr, unsigned int write_reg_len, \
                    unsigned char *write_buf);

unsigned short bsp_sw_i2c_read_16bit_data_lsb(unsigned char slave_id, \
  	unsigned char start_reg_addr);


#endif
