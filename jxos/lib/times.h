#ifndef _TIME_H_
#define _TIME_H_

#include "type.h"

typedef struct{
	uint8_t hour;
	uint8_t min;
	uint8_t sec;
} TIME_STRUCT;

void time_print(TIME_STRUCT time);
uint8_t time_init(TIME_STRUCT* time,\
				uint8_t hour, uint8_t min, uint8_t sec);
uint8_t time_add_one_secs(TIME_STRUCT* time);	//return 1: over day

#endif
