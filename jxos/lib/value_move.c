

#include "value_move.h"

void value_move_stop_move(VALUE_MOVE_STRUCT* value_move)
{
	value_move->target_value = value_move->current_value;
}

void value_move_init_current_value(VALUE_MOVE_STRUCT* value_move, uint16_t set_current_value)
{
	value_move->target_value = set_current_value;
	value_move->current_value = set_current_value;
}

uint8_t value_move_check_active(VALUE_MOVE_STRUCT* value_move)
{
	if(value_move->current_value == value_move->target_value){
		return 0;
	}
	else{
		return 1;
	}
}

void compute_interval_setp_and_interval_tick(uint16_t* interval_setps, uint16_t* interval_ticks,
												uint16_t totle_setps, uint16_t totle_ticks)
{
	if((totle_setps == 0)||(totle_ticks == 0)){
		return;
	}
	if(totle_setps > totle_ticks){
		*interval_setps = totle_setps/totle_ticks;
		*interval_ticks = 1;
	}
	else{
		*interval_ticks = totle_ticks/totle_setps;
		*interval_setps = 1;
	}
}

//move_ticks: how many ticks will take to move "current_value" to "target_value"
void value_move_to_value_by_tick(VALUE_MOVE_STRUCT* value_move, uint16_t set_target_value, uint16_t move_ticks)
{
	uint16_t totle_setps;

	value_move_stop_move(value_move);

	if(value_move->current_value == set_target_value){
		return;
	}
	if(move_ticks > 0){
		if(value_move->current_value > set_target_value){
			totle_setps = value_move->current_value - set_target_value;
		}
		else if(value_move->current_value < set_target_value){
			totle_setps = set_target_value - value_move->current_value;
		}
		compute_interval_setp_and_interval_tick(&(value_move->interval_setps), &(value_move->interval_ticks),
													totle_setps, move_ticks);
        value_move->target_value = set_target_value;
        value_move->interval_tick_counter = 0;
	}
	else{
        value_move->interval_setps = 0;
        value_move->interval_ticks = 0;
        value_move->target_value = set_target_value;
        value_move->interval_tick_counter = 0;

		value_move->current_value = set_target_value;
		value_move->current_value_changed_event_callbakc(value_move);
	}
}

//set_step_value: how many steps to move pre tick
void value_move_to_value_by_step(VALUE_MOVE_STRUCT* value_move, uint16_t set_target_value, uint16_t set_step_value)
{
	value_move_stop_move(value_move);

	if(value_move->current_value == set_target_value){
		return;
	}
	if(set_step_value > 0){
		if(value_move->current_value > set_target_value){
			if(set_step_value > (value_move->current_value - set_target_value)){
				set_step_value = value_move->current_value - set_target_value;
			}
		}
		else if(value_move->current_value < set_target_value){
			if(set_step_value > (set_target_value - value_move->current_value)){
				set_step_value = set_target_value - value_move->current_value;
			}
		}

        value_move->interval_setps = set_step_value;
        value_move->interval_ticks = 1;
        value_move->target_value = set_target_value;
        value_move->interval_tick_counter = 0;
	}
}

void value_move_tick_handler(VALUE_MOVE_STRUCT* value_move)
{
	if(value_move_check_active(value_move) == 0){
		return;
	}
	if(value_move->interval_setps == 0){
		value_move_stop_move(value_move);
		return;
	}
	if(value_move->interval_ticks == 0){
		value_move_stop_move(value_move);
		return;
	}

	value_move->interval_tick_counter++;
	if(value_move->interval_tick_counter < value_move->interval_ticks){
		return;
	}
	else{
        value_move->interval_tick_counter = 0;
	}

	if(value_move->current_value > value_move->target_value){
		if(value_move->current_value - value_move->target_value > value_move->interval_setps){
			value_move->current_value -= value_move->interval_setps;
		}
		else{
			value_move->current_value = value_move->target_value;
		}
		value_move->current_value_changed_event_callbakc(value_move);
	}
	else if(value_move->current_value < value_move->target_value){
		if(value_move->target_value - value_move->current_value > value_move->interval_setps){
			value_move->current_value += value_move->interval_setps;
		}
		else{
			value_move->current_value = value_move->target_value;
		}
		value_move->current_value_changed_event_callbakc(value_move);
	}
}

