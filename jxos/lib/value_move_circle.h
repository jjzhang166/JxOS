
#ifndef __VALUE_MOVE_CIRCLE__H
#define __VALUE_MOVE_CIRCLE__H

#include "value_move.h"

typedef struct __VALUE_MOVE_CIRCLE_STRUCT__ VALUE_MOVE_CIRCLE_STRUCT;
struct __VALUE_MOVE_CIRCLE_STRUCT__{ 
	/************************************************/
	/******************attribute*********************/
	//config_attribute

	//public_attribute	

	//private_attribute
	VALUE_MOVE_STRUCT value_move;
	uint16_t begin_value;
	uint16_t end_value;

	/************************************************/
	/******************action************************/
	//private_action

	//public_action
	//void value_move_circle_tick_handler(VALUE_MOVE_CIRCLE_STRUCT* value_move_circle);
	
	/************************************************/
	/******************callback**********************/
	//config_callbakc

	//event_callback
};   

void value_move_circle_stop_move(VALUE_MOVE_CIRCLE_STRUCT* value_move_circle);
void value_move_circle_init_current_value(VALUE_MOVE_CIRCLE_STRUCT* value_move_circle, uint16_t set_current_value);
uint16_t value_move_circle_get_current_value(VALUE_MOVE_CIRCLE_STRUCT* value_move_circle);
uint8_t value_move_circle_check_active(VALUE_MOVE_CIRCLE_STRUCT* value_move_circle);

//the "current_value" move to the "begin_value" in the first round
//period_ticks: how many ticks will take to move "begin_value" to "end_value"
void value_move_circle_move_by_tick(VALUE_MOVE_CIRCLE_STRUCT* value_move_circle, uint16_t set_begin_value, uint16_t set_end_value, uint16_t period_ticks);

//the "current_value" move to the "begin_value" in the first round
//set_step_value: how many steps pre tick
void value_move_circle_move_by_step(VALUE_MOVE_CIRCLE_STRUCT* value_move_circle, uint16_t set_begin_value, uint16_t set_end_value, uint16_t set_step_value);

void value_move_circle_tick_handler(VALUE_MOVE_CIRCLE_STRUCT* value_move_circle);

#endif

