
#ifndef __VALUE_MOVE_CIRCLE_GROUP__H
#define __VALUE_MOVE_CIRCLE_GROUP__H

#include "value_move_circle.h"

typedef struct
{
	uint8_t value_move_circle_num_max;
	uint8_t value_move_circle_count;
	VALUE_MOVE_CIRCLE_STRUCT* value_move_circle;
} VALUE_MOVE_CIRCLE_GROUP_STRUCT;

void value_move_circle_group_tick_handler(VALUE_MOVE_CIRCLE_GROUP_STRUCT* value_move_circle_group);
uint8_t value_move_circle_group_check_active(VALUE_MOVE_CIRCLE_GROUP_STRUCT* value_move_circle_group);
void value_move_circle_group_init(VALUE_MOVE_CIRCLE_GROUP_STRUCT* value_move_circle_group,
						uint8_t value_move_circle_group_value_move_circle_num,
						VALUE_MOVE_CIRCLE_STRUCT* value_move_circle_space);
VALUE_MOVE_CIRCLE_STRUCT* value_move_circle_new(VALUE_MOVE_CIRCLE_GROUP_STRUCT* value_move_circle_group, VALUE_MOVE_CIRCLE_STRUCT* new_value_move_circle);

#endif