

#include "value_move_group.h"
#include "string.h"

void value_move_group_tick_handler(VALUE_MOVE_GROUP_STRUCT* value_move_group)
{
	uint8_t i;
	for (i = 0; i < value_move_group->value_move_count; i++) {
			value_move_tick_handler(&(value_move_group->value_move[i]));
	}
}

uint8_t value_move_group_check_active(VALUE_MOVE_GROUP_STRUCT* value_move_group)
{
	uint8_t i;
	for (i = 0; i < value_move_group->value_move_count; i++) {
			if(value_move_check_active(&(value_move_group->value_move[i])) == 1){
				return 1;
			}
	}
	return 0;
}

void value_move_group_init(VALUE_MOVE_GROUP_STRUCT* value_move_group,
						uint8_t value_move_group_value_move_num,
						VALUE_MOVE_STRUCT* value_move_space)
{
	memset(value_move_space, 0, sizeof(VALUE_MOVE_STRUCT)*value_move_group_value_move_num);
	value_move_group->value_move_count = 0;
	value_move_group->value_move_num_max = value_move_group_value_move_num;
	value_move_group->value_move = value_move_space;
}


VALUE_MOVE_STRUCT* value_move_new(VALUE_MOVE_GROUP_STRUCT* value_move_group, VALUE_MOVE_STRUCT* new_value_move)
{
	VALUE_MOVE_STRUCT* value_move = 0;
	if(value_move_group->value_move_count < value_move_group->value_move_num_max){
		value_move = &(value_move_group->value_move[value_move_group->value_move_count]);
		memcpy((uint8_t*)value_move, (uint8_t*)new_value_move, sizeof(VALUE_MOVE_STRUCT));
		value_move_group->value_move_count++;
		return value_move;
	}
	else{
		return 0;
	}
}


/**********************
e.g.
#include "value_move_group.h"

#define VALUE_MOVE_EVENT_NUM_MAX        8
static VALUE_MOVE_GROUP_STRUCT value_group;
static VALUE_MOVE_STRUCT values[VALUE_MOVE_EVENT_NUM_MAX];

static uint8_t obj_to_mun(VALUE_MOVE_STRUCT* value_move)
{
    uint8_t i;
    for(i = 0; i < VALUE_MOVE_EVENT_NUM_MAX; i++){
        if(value_move == &(values[i])){
            return i;
        }
    }
    return 0xff;
}

static void value_changed(VALUE_MOVE_STRUCT* value_move)
{
    uint8_t v_num = obj_to_mun(value_move);
    printf("value_move %d changed to %d\r\n", v_num, value_move->current_value);
}

void main(void)
{
    uint8_t i;
    VALUE_MOVE_STRUCT* value_move_p;
    VALUE_MOVE_STRUCT value_move_n;

    value_move_n.begin_value = 0;
    value_move_n.end_value = 0;
    value_move_n.current_value = 0;
    value_move_n.target_value = 0;
    value_move_n.current_value_changed_event_callbakc = value_changed;

    value_move_group_init(&value_group, VALUE_MOVE_EVENT_NUM_MAX, values);

    for(i = 0; i < VALUE_MOVE_EVENT_NUM_MAX; i++){
        value_move_new(&value_group, &value_move_n);
    }

    value_move_p = &(values[0]);
    value_move_init_current_value(value_move_p, 0);
    value_move_to_value_by_tick(value_move_p, 100, 250);

    while(1){
        value_move_group_tick_handler(&value_group);
        delay(1000);
    }
}

*****/


