
#include <string.h>
#include "jxos_public.h"

#if (JXOS_STD_APP_FRAME_SEND_TASK_ENABLE == 1)
void (*std_app_frame_send_task_hal_send_data_callback)(uint8_t* send_data, uint8_t send_data_len) = 0;

enum
{
	SEND_STATE_IDLE = 0,
	SEND_STATE_SENDING,
	SEND_STATE_WAIT_RSP,
};

static uint8_t send_state = SEND_STATE_IDLE;
static const uint16_t sending_time_ms = FRAME_SEND_SENDING_TIME_MS;
static const uint16_t wait_rsp_time_ms = FRAME_SEND_WAIT_RSP_TIME_MS;
static const uint8_t send_repeat_count = FRAME_SEND_REPEAT_COUNT;
static swtime_type send_swt;


#if (FRAME_SEND_USE_MSG_PIPE == true)
static uint8_t send_buff_msg_pipe_space[FRAME_SEND_BUFF_FRAME_NUM*FRAME_SEND_FRAME_MAX_LEN];
static JXOS_MESSAGE_PIPE_HANDLE frame_send_data_pipe;

static uint8_t send_buff_msg_pipe_space_no_repeat[FRAME_SEND_BUFF_FRAME_NUM*FRAME_SEND_FRAME_MAX_LEN];
static JXOS_MESSAGE_PIPE_HANDLE frame_send_data_pipe_no_repeat;

static uint8_t send_buff[FRAME_SEND_FRAME_MAX_LEN];
static uint8_t send_buff_len = 0;
static uint8_t send_buff_no_repeat[FRAME_SEND_FRAME_MAX_LEN];
static uint8_t send_buff_len_no_repeat = 0;
#else
static uint8_t send_buff[FRAME_SEND_FRAME_MAX_LEN];
static uint8_t send_buff_len = 0;
//static uint8_t send_buff_no_repeat[FRAME_SEND_FRAME_MAX_LEN];
//static uint8_t send_buff_len_no_repeat = 0;
static uint8_t send_buff_new_data_falg = 0;
#endif

static void frame_send_task(uint8_t task_id, void * parameter)
{
    static uint8_t repeat = 0;

#if (FRAME_SEND_USE_MSG_PIPE == true)
//    if(1 == jxos_message_pipe_receive(frame_send_ctrl_msg, "f_send", ctrl_msg)){
//    sending_time_ms = set_sending_time_ms;
//    wait_rsp_time_ms = set_wait_rsp_time_ms;
//    send_repeat_count = set_send_repeat_count;
//    frame_send_task_stop_repeat
//    }

    if(jxos_message_pipe_receive(frame_send_data_pipe_no_repeat, "f_rver",
                            send_buff_no_repeat, &send_buff_len_no_repeat) == 1){
        if(std_app_frame_send_task_hal_send_data_callback != 0){
            std_app_frame_send_task_hal_send_data_callback(send_buff_no_repeat, send_buff_len_no_repeat);
        }
        return;
    }

    if(jxos_message_pipe_check_empty(frame_send_data_pipe) == 0){
        if(send_state == SEND_STATE_IDLE){
            if(jxos_message_pipe_receive(frame_send_data_pipe, "f_rver",
									send_buff, &send_buff_len) == 1){
                repeat = 0;
                if(std_app_frame_send_task_hal_send_data_callback != 0){
                    std_app_frame_send_task_hal_send_data_callback(send_buff, send_buff_len);
                }
                send_state = SEND_STATE_SENDING;
                if(sending_time_ms != 0){
                    sys_software_timer_task_set_timer(send_swt, sending_time_ms);
                }
                else{
                    sys_software_timer_task_set_timer(send_swt, 1);
                }
                sys_software_timer_task_restart_timer(send_swt);
            }
        }
    }
#endif

    if(sys_software_timer_task_check_overtime_timer(send_swt) == 1){
        sys_software_timer_task_stop_timer(send_swt);
        switch(send_state){
            case SEND_STATE_IDLE:
#if (FRAME_SEND_USE_MSG_PIPE == true)
            	if(jxos_message_pipe_receive(frame_send_data_pipe, "f_rver",
									send_buff, &send_buff_len) == 1){
#else
                if(send_buff_new_data_falg == 1){
                    send_buff_new_data_falg = 0;
#endif
                    repeat = 0;
					if(std_app_frame_send_task_hal_send_data_callback != 0){
						std_app_frame_send_task_hal_send_data_callback(send_buff, send_buff_len);
					}
                    send_state = SEND_STATE_SENDING;
                    if(sending_time_ms != 0){
                        sys_software_timer_task_set_timer(send_swt, sending_time_ms);
                    }
                    else{
                        sys_software_timer_task_set_timer(send_swt, 1);
                    }
                    sys_software_timer_task_restart_timer(send_swt);
                }
            break;

            case SEND_STATE_SENDING:
                send_state = SEND_STATE_WAIT_RSP;
                if((wait_rsp_time_ms != 0)&&(repeat < send_repeat_count)){
                    sys_software_timer_task_set_timer(send_swt, wait_rsp_time_ms);
                }
                else{
                    sys_software_timer_task_set_timer(send_swt, 1);
                }
                sys_software_timer_task_restart_timer(send_swt);
                break;

            case SEND_STATE_WAIT_RSP:
                if(repeat < send_repeat_count){
                    repeat++;
					if(std_app_frame_send_task_hal_send_data_callback != 0){
						std_app_frame_send_task_hal_send_data_callback(send_buff, send_buff_len);
					}
                    send_state = SEND_STATE_SENDING;
                    if(sending_time_ms != 0){
                        sys_software_timer_task_set_timer(send_swt, sending_time_ms);
                    }
                    else{
                        sys_software_timer_task_set_timer(send_swt, 1);
                    }
                    sys_software_timer_task_restart_timer(send_swt);
                }
                else{
                   send_state = SEND_STATE_IDLE;
                   repeat = 0;
#if (FRAME_SEND_USE_MSG_PIPE == false)
                   send_buff_new_data_falg = 0;
#endif
                }

            break;
        }
    }
}

void std_app_frame_send_task_init(void)
{
#if (FRAME_SEND_USE_MSG_PIPE == true)
    frame_send_data_pipe = jxos_message_pipe_create(send_buff_msg_pipe_space, FRAME_SEND_BUFF_FRAME_NUM*FRAME_SEND_FRAME_MAX_LEN,
													FRAME_SEND_FRAME_MAX_LEN, "frame_send_data_pipe");
	jxos_message_pipe_sender_name_register(frame_send_data_pipe, "f_sder");
	jxos_message_pipe_receiver_name_register(frame_send_data_pipe, "f_rver");

    frame_send_data_pipe_no_repeat = jxos_message_pipe_create(send_buff_msg_pipe_space_no_repeat, FRAME_SEND_BUFF_FRAME_NUM*FRAME_SEND_FRAME_MAX_LEN,
													FRAME_SEND_FRAME_MAX_LEN, "frame_send_data_pipe_no_repeat");
	jxos_message_pipe_sender_name_register(frame_send_data_pipe_no_repeat, "f_sder");
	jxos_message_pipe_receiver_name_register(frame_send_data_pipe_no_repeat, "f_rver");
#endif

    send_swt = sys_software_timer_task_new_timer();
	jxos_task_create(frame_send_task, "f_send", 0);
}

//void frame_send_task_set_sending_time(uint16_t set_sending_time_ms)
//{
//    sending_time_ms = set_sending_time_ms;
//}
//
//void frame_send_task_set_wait_rsp_time(uint16_t set_wait_rsp_time_ms)
//{
//    wait_rsp_time_ms = set_wait_rsp_time_ms;
//}
//
//void frame_send_task_set_send_repeat_count(uint8_t set_send_repeat_count)
//{
//    send_repeat_count = set_send_repeat_count;
//}

void std_app_frame_send_task_stop_repeat(void)
{
    send_state = SEND_STATE_IDLE;
}

uint8_t std_app_frame_send_task_send_data(uint8_t* frame_send, uint8_t frame_send_len)
{
#if (FRAME_SEND_USE_MSG_PIPE == true)
	return jxos_message_pipe_send(frame_send_data_pipe, "f_sder",
									frame_send, frame_send_len);
#else
    if(send_state == SEND_STATE_IDLE){
        send_buff_len = frame_send_len;
        memcpy(send_buff, frame_send, frame_send_len);
        send_buff_new_data_falg = 1;
        sys_software_timer_task_set_timer(send_swt, 1);
        sys_software_timer_task_restart_timer(send_swt);
        return 1;
    }
    else{
        return 0;
    }
#endif
}

uint8_t std_app_frame_send_task_send_data_no_repeat(uint8_t* frame_send, uint8_t frame_send_len)
{
#if (FRAME_SEND_USE_MSG_PIPE == true)
	return jxos_message_pipe_send(frame_send_data_pipe_no_repeat, "f_sder",
									frame_send, frame_send_len);
#else
    if(std_app_frame_send_task_hal_send_data_callback != 0){
        std_app_frame_send_task_hal_send_data_callback(frame_send, frame_send_len);
    }
    return 1;
#endif
}

#endif

