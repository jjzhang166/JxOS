
#ifndef __std_app_FRAME_SEND_TASK_H
#define __std_app_FRAME_SEND_TASK_H

#include "../lib/type.h"

void std_app_frame_send_task_init(void);
//void frame_send_task_set_sending_time(uint16_t set_sending_time_ms);
//void frame_send_task_set_wait_rsp_time(uint16_t set_wait_rsp_time_ms);
//void frame_send_task_set_send_repeat_count(uint8_t set_send_repeat_count);
void std_app_frame_send_task_stop_repeat(void);
uint8_t std_app_frame_send_task_send_data(uint8_t* frame_send, uint8_t frame_send_len);
uint8_t std_app_frame_send_task_send_data_no_repeat(uint8_t* frame_send, uint8_t frame_send_len);

extern void (*std_app_frame_send_task_hal_send_data_callback)(uint8_t* send_data, uint8_t send_data_len);


#endif

