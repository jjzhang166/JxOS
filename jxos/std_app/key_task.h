
#ifndef __std_app_KEY_TASK_H
#define __std_app_KEY_TASK_H

#include "../lib/type.h"

void std_app_key_task_init(void);

extern void (*std_app_key_task_hal_init_callback)(void);

//output msg name: "std_app_key_msg"

#endif
