
#ifndef __std_app_value_move_task_H
#define __std_app_value_move_task_H

#include "type.h"

void std_app_value_move_task_init(void);
void std_app_value_move_task(void);

#endif
