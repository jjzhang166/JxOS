
#ifndef _DEBUG_PRINT_TASK_H
#define _DEBUG_PRINT_TASK_H

#include "../lib/type.h"
#include <jxos_config.h>

void sys_debug_print_task_init(void);

#if (JXOS_SYS_SERVICE_DEBUG_PRINT_TASK_ENABLE == 1)
void sys_debug_print_task_clr_buff(void);
void sys_debug_print_task_print_str(char* str, uint8_t is_blocked);
void sys_debug_print_task_print_int(int32_t value, uint8_t is_blocked);
void sys_debug_print_task_print_uint(uint32_t value, uint8_t is_blocked);
void sys_debug_print_task_print_hex(uint32_t value, uint8_t is_blocked);
void sys_debug_print_task_print_bin(uint32_t value, uint8_t is_blocked);
void sys_debug_print_task_print_data_stream_in_hex(uint8_t* data_stream,
												uint8_t data_stream_len, uint8_t is_blocked);
extern void (*sys_debug_print_task_hal_init_callback)(void);
extern uint8_t (*sys_debug_print_task_send_finish_check_callback)(void);
extern void (*sys_debug_print_task_send_byte_callback)(uint8_t byte);
#endif

#if (JXOS_SYS_SERVICE_DEBUG_PRINT_BLOCKED_PRINT_ENABLE == 1)
#define sys_debug_print_task_blocked_print_str(s)						sys_debug_print_task_print_str(s,1)
#define sys_debug_print_task_blocked_print_int(v)						sys_debug_print_task_print_int(v,1)
#define sys_debug_print_task_blocked_print_uint(v)						sys_debug_print_task_print_uint(v,1)
#define sys_debug_print_task_blocked_print_hex(v)						sys_debug_print_task_print_hex(v,1)
#define sys_debug_print_task_blocked_print_bin(v)						sys_debug_print_task_print_bin(v,1)
#define sys_debug_print_task_blocked_print_data_stream_in_hex(p,l)		sys_debug_print_task_print_data_stream_in_hex(p,l,1)
#else
#define sys_debug_print_task_blocked_print_str(s)
#define sys_debug_print_task_blocked_print_int(v)
#define sys_debug_print_task_blocked_print_uint(v)
#define sys_debug_print_task_blocked_print_hex(v)
#define sys_debug_print_task_blocked_print_bin(v)
#define sys_debug_print_task_blocked_print_data_stream_in_hex(p,l)
#endif
#if (JXOS_SYS_SERVICE_DEBUG_PRINT_KERNEL_PRINT_ENABLE == 1)
#define sys_debug_print_task_kernel_print_str(s)						sys_debug_print_task_print_str(s,0)
#define sys_debug_print_task_kernel_print_int(v)						sys_debug_print_task_print_int(v,0)
#define sys_debug_print_task_kernel_print_uint(v)						sys_debug_print_task_print_uint(v,0)
#define sys_debug_print_task_kernel_print_hex(v)						sys_debug_print_task_print_hex(v,0)
#define sys_debug_print_task_kernel_print_bin(v)						sys_debug_print_task_print_bin(v,0)
#define sys_debug_print_task_kernel_print_data_stream_in_hex(p,l)		sys_debug_print_task_print_data_stream_in_hex(p,l,0)
#else
#define sys_debug_print_task_kernel_print_str(s)
#define sys_debug_print_task_kernel_print_int(v)
#define sys_debug_print_task_kernel_print_uint(v)
#define sys_debug_print_task_kernel_print_hex(v)
#define sys_debug_print_task_kernel_print_bin(v)
#define sys_debug_print_task_kernel_print_data_stream_in_hex(p,l)
#endif
#if (JXOS_SYS_SERVICE_DEBUG_PRINT_LIB_PRINT_ENABLE == 1)
#define sys_debug_print_task_lib_print_str(s)							sys_debug_print_task_print_str(s,0)
#define sys_debug_print_task_lib_print_int(v)							sys_debug_print_task_print_int(v,0)
#define sys_debug_print_task_lib_print_uint(v)							sys_debug_print_task_print_uint(v,0)
#define sys_debug_print_task_lib_print_hex(v)							sys_debug_print_task_print_hex(v,0)
#define sys_debug_print_task_lib_print_bin(v)							sys_debug_print_task_print_bin(v,0)
#define sys_debug_print_task_lib_print_data_stream_in_hex(p,l)			sys_debug_print_task_print_data_stream_in_hex(p,l,0)
#else
#define sys_debug_print_task_lib_print_str(s)
#define sys_debug_print_task_lib_print_int(v)
#define sys_debug_print_task_lib_print_uint(v)
#define sys_debug_print_task_lib_print_hex(v)
#define sys_debug_print_task_lib_print_bin(v)
#define sys_debug_print_task_lib_print_data_stream_in_hex(p,l)
#endif
#if (JXOS_SYS_SERVICE_DEBUG_PRINT_HAL_PRINT_ENABLE == 1)
#define sys_debug_print_task_hal_print_str(s)							sys_debug_print_task_print_str(s,0)
#define sys_debug_print_task_hal_print_int(v)							sys_debug_print_task_print_int(v,0)
#define sys_debug_print_task_hal_print_uint(v)							sys_debug_print_task_print_uint(v,0)
#define sys_debug_print_task_hal_print_hex(v)							sys_debug_print_task_print_hex(v,0)
#define sys_debug_print_task_hal_print_bin(v)							sys_debug_print_task_print_bin(v,0))
#define sys_debug_print_task_hal_print_data_stream_in_hex(p,l)			sys_debug_print_task_print_data_stream_in_hex(p,l,0)
#else
#define sys_debug_print_task_hal_print_str(s)
#define sys_debug_print_task_hal_print_int(v)
#define sys_debug_print_task_hal_print_uint(v)
#define sys_debug_print_task_hal_print_hex(v)
#define sys_debug_print_task_hal_print_bin(v)
#define sys_debug_print_task_hal_print_data_stream_in_hex(p,l)
#endif
#if (JXOS_SYS_SERVICE_DEBUG_PRINT_BSP_PRINT_ENABLE == 1)
#define sys_debug_print_task_bsp_print_str(s)							sys_debug_print_task_print_str(s,0)
#define sys_debug_print_task_bsp_print_int(v)							sys_debug_print_task_print_int(v,0)
#define sys_debug_print_task_bsp_print_uint(v)							sys_debug_print_task_print_uint(v,0)
#define sys_debug_print_task_bsp_print_hex(v)							sys_debug_print_task_print_hex(v,0)
#define sys_debug_print_task_bsp_print_bin(v)							sys_debug_print_task_print_bin(v,0)
#define sys_debug_print_task_bsp_print_data_stream_in_hex(p,l)			sys_debug_print_task_print_data_stream_in_hex(p,l,0)
#else
#define sys_debug_print_task_bsp_print_str(s)
#define sys_debug_print_task_bsp_print_int(v)
#define sys_debug_print_task_bsp_print_uint(v)
#define sys_debug_print_task_bsp_print_hex(v)
#define sys_debug_print_task_bsp_print_bin(v)
#define sys_debug_print_task_bsp_print_data_stream_in_hex(p,l)
#endif
#if (JXOS_SYS_SERVICE_DEBUG_PRINT_SYS_SER_PRINT_ENABLE == 1)
#define sys_debug_print_task_sys_ser_print_str(s)						sys_debug_print_task_print_str(s,0)
#define sys_debug_print_task_sys_ser_print_int(v)						sys_debug_print_task_print_int(v,0)
#define sys_debug_print_task_sys_ser_print_uint(v)						sys_debug_print_task_print_uint(v,0)
#define sys_debug_print_task_sys_ser_print_hex(v)						sys_debug_print_task_print_hex(v,0)
#define sys_debug_print_task_sys_ser_print_bin(v)						sys_debug_print_task_print_bin(v,0)
#define sys_debug_print_task_sys_ser_print_data_stream_in_hex(p,l)		sys_debug_print_task_print_data_stream_in_hex(p,l,0)
#else
#define sys_debug_print_task_sys_ser_print_str(s)
#define sys_debug_print_task_sys_ser_print_int(v)
#define sys_debug_print_task_sys_ser_print_uint(v)
#define sys_debug_print_task_sys_ser_print_hex(v)
#define sys_debug_print_task_sys_ser_print_bin(v)
#define sys_debug_print_task_sys_ser_print_data_stream_in_hex(p,l)
#endif


#endif
