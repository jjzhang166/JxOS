
#ifndef _SOFTWARE_TIMER_TASK_H
#define _SOFTWARE_TIMER_TASK_H

#include "../lib/type.h"

typedef void* swtime_type;

void sys_software_timer_task_init(void);

swtime_type sys_software_timer_task_new_timer(void);
void sys_software_timer_task_set_timer(swtime_type swtime, uint16_t time);
void sys_software_timer_task_start_timer(swtime_type swtime);
void sys_software_timer_task_stop_timer(swtime_type swtime);
void sys_software_timer_task_restart_timer(swtime_type swtime);
uint8_t sys_software_timer_task_check_running_timer(swtime_type swtime);
uint8_t sys_software_timer_task_check_overtime_timer(swtime_type swtime);

#endif

