

#include "jxos_public.h"


static swtime_type test_swt;
void test_app(void)
{
	if(sys_software_timer_task_check_overtime_timer(test_swt) == 1){
		printf_string("----------------test_app\r\n");
	}
}

void test_app_init(void)
{
	uint8_t i;
	test_swt = sys_software_timer_task_new_timer();
	i = (uint8_t)test_swt;
	printf_byte_hex(i);
	printf_string(" test_swt\r\n");
	sys_software_timer_task_set_timer(test_swt, 1000);
	sys_software_timer_task_restart_timer(test_swt);
}
