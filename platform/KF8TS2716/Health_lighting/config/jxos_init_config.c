
#include "jxos_public.h"

void mcu_init(void);
void jxos_hal_init(void)
{
	mcu_init();
}

void button_handler_task_init(void);
void attribute_handler_task_init(void);
void tick_handler_task_init(void);

void test_app_init(void);
void jxos_user_task_init(void)
{
	button_handler_task_init();
	attribute_handler_task_init();
	tick_handler_task_init();

	test_app_init();
}


void mcu_run(void);
void jxos_prepare_to_run(void)
{
	mcu_run();
}


void button_handler_task(void);
void attribute_handler_task(void);
void tick_handler_task(void);

void test_app(void);
void jxos_user_task_run(void)
{
	button_handler_task();
	attribute_handler_task();
	tick_handler_task();

	test_app();
}

