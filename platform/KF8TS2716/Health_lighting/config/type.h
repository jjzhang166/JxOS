
#ifndef __JXOS_TYPE_H
#define __JXOS_TYPE_H

#define bool_t     unsigned char

#define uint8_t     unsigned char
#define uint16_t    unsigned int
#define uint32_t    unsigned long int
#define uint64_t    unsigned long long

#define int8_t     char
#define int16_t    int
#define int32_t    long int
#define int64_t    long long


#define true        1
#define false       0

/********
typedef unsigned char         uint8_t;
typedef unsigned int          uint16_t;
typedef unsigned long         uint32_t;
*********/

//void print_hex(uint8_t* hex, uint8_t len);

#endif
