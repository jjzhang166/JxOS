
#ifndef _FLASH_H_
#define _FLASH_H_

#define			FLASH_BUFFER_MAX	16		// 数据的大小与单次操作量相关，这里int型，16,32，除非不用写操作或不关心一些数据。

extern unsigned int 	FLASH_BUFFER[];
extern unsigned int    FLASH_READ_BUF;	//当单字读函数使用非全嵌汇编模式时，使用该值获取读取结果值

unsigned int 	FLASH_READ_ONE	(unsigned int address);	//读一个字
void 			FLASH_READ_FUN	(unsigned int address,unsigned char length);//读多个数据到数据缓存区，建议最多量为1个页即32个数据，同时建议不跨页操作，与写函数对应
void 			FLASH_WRITE_ONE	(unsigned int address,unsigned int value);  // 过程封装
void 			FLASH_WRITE_FUN	(unsigned int address,unsigned char length); //将缓存区数据写到对应位置，从第一个开始

#endif
