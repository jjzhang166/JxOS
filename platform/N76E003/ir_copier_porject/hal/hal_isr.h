#ifndef _HAL_ISR_H
#define _HAL_ISR_H

extern void (*exint0_ISR_callback)(void);
extern void (*WakeUp_Timer_ISR_callback)(void);
extern void (*Timer0_ISR_callback)(void);
extern void (*Timer3_ISR_callback)(void);
extern void (*SerialPort0_RX_ISR_callback)(char byte);

#endif
