
#include <string.h>
#include "jxos_public.h"
#include "bsp/bsp_ir_receiver.h"

static JXOS_EVENT_HANDLE ir_rec_finish_evt;
static uint8_t ir_rce_buff[10] = {0};
static uint8_t ir_rce_buff_len = 0;

void ir_rec_pin_init(void);
uint8_t ir_rec_pin_read(void);
void ir_timer_init(void);
void ir_timer_run(void);

static void ir_rx_task(uint8_t task_id, void * parameter)
{
	if(jxos_event_wait(ir_rec_finish_evt) == 1){
		bsp_ir_receive_finish_receive_falg = 0;
		ir_rce_buff_len = bsp_ir_receive_buff_len;
		memcpy(ir_rce_buff, bsp_ir_receive_buff, ir_rce_buff_len);
		bsp_ir_receive_finish_receive_falg = 0;
		sys_debug_print_task_user_print_str("ir_rx_task rec data:");
		sys_debug_print_task_user_print_data_stream_in_hex(ir_rce_buff, ir_rce_buff_len);
		sys_debug_print_task_user_print_str("\r\n");
	}
}

static void bsp_ir_receive_receive_finish_callbakc_handler(void)
{
	jxos_event_set(ir_rec_finish_evt);
}

void  ir_rx_task_init(void)
{
	bsp_ir_receive_init(0, ir_rec_pin_read);
	bsp_ir_receive_receive_finish_callbakc = bsp_ir_receive_receive_finish_callbakc_handler;

	jxos_task_create(ir_rx_task, "ir_rx", 0);
	ir_rec_finish_evt = jxos_event_create();
	
	ir_rec_pin_init();
	ir_timer_init();
	ir_timer_run();
}

