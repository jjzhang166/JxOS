
#include "jxos_public.h"
#include "bsp/bsp_ir_sender.h"
#include "bsp/bsp_key.h"

#define STATE_WAIT_TO_TIMMING	0
#define STATE_TIMMING_WAKE		1
#define STATE_TIMMING_SLEEP		2

void ir_send_pin_init(void);
void ir_send_pin_idle(void);
void ir_send_pin_pulse(void);
void ir_send_power_on(void);
void ir_send_power_off(void);
void ir_timer_init(void);
void ir_timer_run(void);
void key_pin_init(void);
uint8_t key_pin_read(uint8_t num);
void led_init(void);
void led_on(void);
void led_off(void);

uint8_t software_timer_task_tick_handler_flag = 0;

static swtime_type key_scan_timer;
static swtime_type led_blink_timer;
static swtime_type timming_timer;

static uint8_t keep_wake;
static uint8_t send_data_temp[6] = {0x97, 0x68, 0x6e, 0x91, 0x2a, 0xd5};
static uint8_t state;
static uint8_t send_delay_time = 0;

static void short_press(uint8_t key_num)
{
	if(bsp_ir_send_busy_falg != 1){
		bsp_ir_send(send_data_temp, 6);
		sys_debug_print_task_user_print_str("ir_rx_task send data:");
		sys_debug_print_task_user_print_data_stream_in_hex(send_data_temp, 6);
		sys_debug_print_task_user_print_str("\r\n");
	}
}

static void long_press(uint8_t key_num)
{
	if(state == STATE_WAIT_TO_TIMMING){
		state = STATE_TIMMING_WAKE;
		sys_software_timer_task_restart_timer(timming_timer);
		
		sys_software_timer_task_set_timer(led_blink_timer, 250);
		sys_software_timer_task_restart_timer(led_blink_timer);
		send_delay_time = 60+5;	//60min
		
		ir_send_power_off();
	}
	else if(state == STATE_TIMMING_WAKE){
		state = STATE_TIMMING_SLEEP;
		sys_software_timer_task_set_timer(led_blink_timer, 1000);
		sys_software_timer_task_restart_timer(led_blink_timer);
		send_delay_time = 90+5;	//90min
		
		sys_power_mgr_task_keep_wake_mark_reset(keep_wake);
		sys_software_timer_task_stop_timer(key_scan_timer);
	}
}

static void ir_send_task(uint8_t task_id, void * parameter)
{
	static uint8_t led_blink = 0;

	if(software_timer_task_tick_handler_flag == 1){
		software_timer_task_tick_handler_flag = 0;
		software_timer_task_tick_handler();
	}
	if(sys_software_timer_task_check_overtime_timer(key_scan_timer) == 1){
		bsp_key_scan_handler();
	}
	if(sys_software_timer_task_check_overtime_timer(led_blink_timer) == 1){
		if(led_blink == 0){
			led_blink = 1;
			led_on();
		}
		else{
			led_blink = 0;
			led_off();
		}
	}
	if(sys_software_timer_task_check_overtime_timer(timming_timer) == 1){
		if(state == STATE_TIMMING_WAKE){
			state = STATE_TIMMING_SLEEP;
			sys_power_mgr_task_keep_wake_mark_reset(keep_wake);
			ir_send_power_off();
			sys_software_timer_task_stop_timer(key_scan_timer);
		}
		if(send_delay_time > 0){
			send_delay_time--;
			if((send_delay_time <= 5)&&(send_delay_time > 0)){
				sys_power_mgr_task_keep_wake_mark_set(keep_wake);
				ir_send_power_on();
				
				sys_software_timer_task_stop_timer(led_blink_timer);
				led_off();

				sys_software_timer_task_set_timer(timming_timer, 1000);
				sys_software_timer_task_restart_timer(timming_timer);
				bsp_ir_send(send_data_temp, 6);
			}
			if(send_delay_time == 0){
				sys_power_mgr_task_keep_wake_mark_reset(keep_wake);
				ir_send_power_off();
				sys_software_timer_task_stop_timer(timming_timer);
			}
		}
	}
}

void ir_send_task_init(void)
{
	key_scan_timer = sys_software_timer_task_new_timer();
	sys_software_timer_task_set_timer(key_scan_timer, 25);
	sys_software_timer_task_restart_timer(key_scan_timer);
	
	timming_timer = sys_software_timer_task_new_timer();
	sys_software_timer_task_set_timer(timming_timer, 60000);
		
	led_blink_timer = sys_software_timer_task_new_timer();
		
	keep_wake = sys_power_mgr_task_keep_wake_mark_get_id();
	sys_power_mgr_task_keep_wake_mark_set(keep_wake);
	
	led_init();
	led_on();
	bsp_key_init(key_pin_init, key_pin_read,
				0,
				0,
				short_press,
				long_press
				);
	bsp_key_long_press_repeat_callback_register(short_press);
	
 	bsp_ir_send_init(ir_send_pin_init, 0, 0);
	bsp_ir_send_hal_set_level_pulse_callback_register(ir_send_pin_pulse);
	bsp_ir_send_hal_set_level_idle_callback_register(ir_send_pin_idle);
	ir_timer_init();
	ir_timer_run();
	ir_send_power_on();
	
	jxos_task_create(ir_send_task, "ir_send", 0);
	
	state = STATE_WAIT_TO_TIMMING;
}
