

#include <kernel/jxos_task.h>
#include <kernel/jxos_msg.h>

static JXOS_MSG_HANDLE msg;
static void app_test1_task(uint8_t task_id, void * parameter)
{
    uint8_t buff[16];
   if(jxos_msg_receive(msg, buff) == 1){
//		printf("app_test1_task rec msg:");
//		printf(" %s\r\n", buff);
   }
}

void app_test1_task_init(void)
{
	jxos_task_create(app_test1_task, "test1", 0);

    msg = jxos_msg_get_handle("test_msg");
}

