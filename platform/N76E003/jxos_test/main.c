
#include <kernel/jxos.h>
#include <sys_service/power_mgr_task.h>
#include <sys_service/software_timer_task.h>

void app_test_task_init(void);
void app_test1_task_init(void);
int main (void)
{
	//OS init
	jxos_init();

	//Task init
	sys_software_timer_task_init();
	/*********************/
	//user_task
	app_test_task_init();
	app_test1_task_init();

	/*********************/
	sys_power_mgr_task_init();

	//OS run
	jxos_run();
    return 0;
}

void print_hex(uint8_t* hex, uint8_t len)
{
	uint8_t i;
	for(i = 0; i < len; i++){
//		printf("0x%x ", *hex);
		hex++;
	}
//	printf("\r\n");
}

/***
char putchar(char c)
{
//	Send_Data_To_UART0(c);
	TI = 0;
	SBUF = c;
	while(TI==0);
	return 0;
}
***/
//	while(1){
//		set_WDCLR;
//		sleep();
//		if(KEY == 1){
//			sleep();
//			delay_1ms(25);
//			KEY = 0;
//		}
//		IDL();
//	}

