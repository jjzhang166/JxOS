
#include <bsp_rf_driver_config.h>
#include <hal.h>

/********************************************************/
/********************************************************/
//SEND
void bsp_rf_send_pin_hal_init(void)
{
	gpio_mode_config(RFTXDATA_PORT, RFTXDATA_PIN, GPIO_MODE_PP);
	RFTXDATA = 0;
}

/********************************************************/
/********************************************************/
//RECEIVE
void bsp_rf_receive_pin_hal_init(void)
{
	gpio_mode_config(RFRXDATA_PORT, RFRXDATA_PIN, GPIO_MODE_HI);	
	RFRXDATA = 0;
}


