
#include "hal_isr.h"
#include "jxos_public.h"
#include "../SRTnet/srtnet_public.h"

void jxos_prepare_to_run_callback_handler(void);

void sys_debug_print_task_hal_init_callback_handler(void);
uint8_t sys_debug_print_task_send_finish_check_callback_handler(void);
void sys_debug_print_task_send_byte_callback_handler(uint8_t byte);

void sys_software_timer_task_hal_init_callback_handler(void);

void std_app_led_task_hal_init_callback_handler(void);
void std_app_led_task_hal_led_on_callback_handler(void);
void std_app_led_task_hal_led_off_callback_handler(void);
void std_app_button_task_hal_init_callback_handler(void);
uint8_t std_app_button_task_hal_read_button_state_callback_handler(uint8_t num);

uint8_t srtnet_app_layer_binding_table_store_callback_handler(uint8_t* binding_table, uint8_t binding_table_len);
uint8_t srtnet_app_layer_binding_table_recover_callback_handler(uint8_t* binding_table, uint8_t binding_table_len);
	
void jxos_hal_init(void)
{
	jxos_prepare_to_run_callback = jxos_prepare_to_run_callback_handler;
#if	(JXOS_SYS_SERVICE_DEBUG_PRINT_TASK_ENABLE == 1)
	sys_debug_print_task_hal_init_callback = sys_debug_print_task_hal_init_callback_handler;
	sys_debug_print_task_send_finish_check_callback = sys_debug_print_task_send_finish_check_callback_handler;
	sys_debug_print_task_send_byte_callback = sys_debug_print_task_send_byte_callback_handler;
#endif
	sys_software_timer_task_hal_init_callback = sys_software_timer_task_hal_init_callback_handler;
//	std_app_led_task_hal_init_callback = std_app_led_task_hal_init_callback_handler;
//	std_app_led_task_hal_led_on_callback = std_app_led_task_hal_led_on_callback_handler;
//	std_app_led_task_hal_led_off_callback = std_app_led_task_hal_led_off_callback_handler;
//	std_app_button_task_hal_init_callback = std_app_button_task_hal_init_callback_handler;
//	std_app_button_task_hal_read_button_state_callback = std_app_button_task_hal_read_button_state_callback_handler;

	srtnet_app_layer_binding_table_store_callback = srtnet_app_layer_binding_table_store_callback_handler;
	srtnet_app_layer_binding_table_recover_callback = srtnet_app_layer_binding_table_recover_callback_handler;
}

void  onoff_light_task_init(void);
void jxos_user_task_init(void)
{
	srtnet_comm_layer_task_init();
	srtnet_app_layer_task_init();
	onoff_light_task_init();
}
