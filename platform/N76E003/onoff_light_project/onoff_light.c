
#include "jxos_public.h"
#include "../SRTnet/srtnet_public.h"
#include "bsp/bsp_led.h"
#include "bsp_led_config.h"
#include "bsp/bsp_key.h"
#include "lib/bit.h"

void led_init(void);
void led_on(uint8_t led_num);
void led_off(uint8_t led_num);
void led_state_store(uint8_t led_state_mark);
uint8_t led_state_recover(void);
void key_init(void);
uint8_t key_read_pin_level(uint8_t num);
void feed_watchdog(void);

#define onoff_light_state_inactive_bind		0
#define onoff_light_state_active_bind		1
#define onoff_light_state_activing			2

static uint8_t rec_toggle_cmd_count = 0;
static uint8_t onoff_light_state = 0;

static swtime_type onoff_light_task_active_bind_swt;
static swtime_type onoff_light_task_led_state_store_swt;
static swtime_type onoff_light_task_led_blink_swt;
static swtime_type onoff_light_task_key_scan_swt;

uint8_t software_timer_task_tick_handler_flag = 0;

static void button_short_press(uint8_t key_num)
{
	if(bsp_led_is_blink(key_num) != 1){
		bsp_led_toggle(key_num);
		sys_software_timer_task_restart_timer(onoff_light_task_led_state_store_swt);
	}	
}

static void button_long_press(uint8_t key_num)
{
	if(onoff_light_state != onoff_light_state_active_bind){
		onoff_light_state = onoff_light_state_active_bind;
		srtnet_app_layer_service_set_active_service(key_num);
		sys_software_timer_task_restart_timer(onoff_light_task_active_bind_swt);
		bsp_led_blink(key_num, 8, 4, 5);
	}
	else{
		if(bsp_led_is_blink(key_num) == 1){
			onoff_light_state = onoff_light_state_inactive_bind;
			srtnet_app_layer_service_no_active_service();
			srtnet_app_layer_binding_table_clear(key_num);
			sys_software_timer_task_stop_timer(onoff_light_task_active_bind_swt);
			bsp_led_blink_stop(key_num);
			bsp_led_on(key_num);
		}
	}
}

static void onoff_light_task(uint8_t task_id, void * parameter)
{
	uint8_t i;
	uint8_t led_state_mark;

	feed_watchdog();
	
	if(software_timer_task_tick_handler_flag == 1){
		software_timer_task_tick_handler_flag = 0;
		software_timer_task_tick_handler();
	}

	if(sys_software_timer_task_check_overtime_timer(onoff_light_task_active_bind_swt) == 1){
		sys_software_timer_task_stop_timer(onoff_light_task_active_bind_swt);
		if(SERVICE_CONFIG_TABLE_LEN == 1){
			if(onoff_light_state == onoff_light_state_activing){
				if(rec_toggle_cmd_count == 5){							//rec toggle cmd 5 times
					onoff_light_state = onoff_light_state_active_bind;
					srtnet_app_layer_service_set_active_service(0);			//active service num 0
					sys_software_timer_task_set_timer(onoff_light_task_active_bind_swt, 10000);//actived bind in 10s
					sys_software_timer_task_restart_timer(onoff_light_task_active_bind_swt);
					bsp_led_blink(0, 4, 2, 3);
				}
				else if(rec_toggle_cmd_count == 8){					//rec toggle cmd 10 times
					onoff_light_state = onoff_light_state_active_bind;
					srtnet_app_layer_binding_table_clear(0);								//clr bind table
					srtnet_app_layer_service_set_active_service(0);			//active service num 0
					sys_software_timer_task_set_timer(onoff_light_task_active_bind_swt, 10000);//actived bind in 10s
					sys_software_timer_task_restart_timer(onoff_light_task_active_bind_swt);
					bsp_led_blink(0, 4, 2, 3);
				}
				else{
					onoff_light_state = onoff_light_state_inactive_bind;
					srtnet_app_layer_service_no_active_service();					//bind succes, inactive service
				}
				rec_toggle_cmd_count = 0;
			}
			else{
				if(srtnet_app_layer_binding_table_is_empty() == 1){
					onoff_light_state = onoff_light_state_active_bind;
					srtnet_app_layer_service_set_active_service(0);
					sys_software_timer_task_set_timer(onoff_light_task_active_bind_swt, 10000);//actived bind in 10s
					sys_software_timer_task_restart_timer(onoff_light_task_active_bind_swt);
				}
				else{
					onoff_light_state = onoff_light_state_inactive_bind;
					srtnet_app_layer_service_no_active_service();
				}
			}
		}
		else if(SERVICE_CONFIG_TABLE_LEN > 1){
			onoff_light_state = onoff_light_state_inactive_bind;
			srtnet_app_layer_service_no_active_service();
		}
	}

	if(sys_software_timer_task_check_overtime_timer(onoff_light_task_led_state_store_swt) == 1){
		sys_software_timer_task_stop_timer(onoff_light_task_led_state_store_swt);
		srtnet_app_layer_binding_table_store();
		led_state_mark = 0;
		for(i = 0; i < LED_NUM_MAX; i++){
			if(bsp_led_get_state(i) == 1){
				set_bit(led_state_mark, i);
			}
		}
		led_state_store(led_state_mark);
	}

	if(sys_software_timer_task_check_overtime_timer(onoff_light_task_led_blink_swt) == 1){
		bsp_led_blink_handler(1);
	}

	if(sys_software_timer_task_check_overtime_timer(onoff_light_task_key_scan_swt) == 1){
		bsp_key_scan_handler();
	}
}

static void srtnet_app_layer_service_id_onoff_rec_cmd_off_callback_handler(uint8_t service_seri_num)
{
	bsp_led_off(service_seri_num);
	sys_software_timer_task_restart_timer(onoff_light_task_led_state_store_swt);
}
static void srtnet_app_layer_service_id_onoff_rec_cmd_toggle_callback_handler(uint8_t service_seri_num)
{
	if(SERVICE_CONFIG_TABLE_LEN == 1){
		onoff_light_state = onoff_light_state_activing;
		srtnet_app_layer_service_set_active_service(0);
		sys_software_timer_task_set_timer(onoff_light_task_active_bind_swt, 2000);			//rec toggle cmd in 2s
		sys_software_timer_task_restart_timer(onoff_light_task_active_bind_swt);
		rec_toggle_cmd_count++;
	}
	bsp_led_toggle(service_seri_num);
	sys_software_timer_task_restart_timer(onoff_light_task_led_state_store_swt);
}
static void srtnet_app_layer_service_id_binding_rec_cmd_bind_callback_handler(SRTNET_BINDING_STRUCT* p)
{
	bsp_led_blink(p->slave_service_serila_mun, 3, 1, 4);
	onoff_light_state = onoff_light_state_inactive_bind;
	srtnet_app_layer_service_no_active_service();					//bind succes, inactive service
}


void  onoff_light_task_init(void)
{
	uint8_t i;

	onoff_light_task_active_bind_swt = sys_software_timer_task_new_timer();
	sys_software_timer_task_set_timer(onoff_light_task_active_bind_swt, 10000);
	if(SERVICE_CONFIG_TABLE_LEN == 1){
		sys_software_timer_task_restart_timer(onoff_light_task_active_bind_swt);
	}

	onoff_light_task_led_state_store_swt = sys_software_timer_task_new_timer();
	sys_software_timer_task_set_timer(onoff_light_task_led_state_store_swt, 5000);

	onoff_light_task_led_blink_swt = sys_software_timer_task_new_timer();
	sys_software_timer_task_set_timer(onoff_light_task_led_blink_swt, 250);	
	sys_software_timer_task_restart_timer(onoff_light_task_led_blink_swt);

	onoff_light_task_key_scan_swt = sys_software_timer_task_new_timer();
	sys_software_timer_task_set_timer(onoff_light_task_key_scan_swt, 50);
	sys_software_timer_task_restart_timer(onoff_light_task_key_scan_swt);

	srtnet_app_layer_service_id_binding_rec_cmd_bind_callback = srtnet_app_layer_service_id_binding_rec_cmd_bind_callback_handler;
	srtnet_app_layer_service_id_onoff_rec_cmd_toggle_callback = srtnet_app_layer_service_id_onoff_rec_cmd_toggle_callback_handler;
	srtnet_app_layer_service_id_onoff_rec_cmd_off_callback	= srtnet_app_layer_service_id_onoff_rec_cmd_off_callback_handler;

	srtnet_comm_layer_request_local_frame_head_write(0xF1);

	bsp_key_init(key_init,
				key_read_pin_level,
				0,
				0,
				button_short_press,
				button_long_press);

	bsp_led_init(led_init, led_on, led_off);
	onoff_light_state = led_state_recover();
	for(i = 0; i < LED_NUM_MAX; i++){
		if(get_bit(onoff_light_state, i))
			bsp_led_on(i);
		else{
			bsp_led_off(i);
		}
	}

	if(SERVICE_CONFIG_TABLE_LEN == 1){
		onoff_light_state = onoff_light_state_active_bind;
		srtnet_app_layer_service_set_active_service(0);							//active service num 0
	}
	else if(SERVICE_CONFIG_TABLE_LEN > 1){
		onoff_light_state = onoff_light_state_inactive_bind;
		srtnet_app_layer_service_no_active_service();
	}


	jxos_task_create(onoff_light_task, "onoff_light", 0);
}

