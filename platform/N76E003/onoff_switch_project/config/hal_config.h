#ifndef __HAL_CONFIG_H
#define __HAL_CONFIG_H

#define SW_DELAY_ENABLE 		1
#define UART0_USE_TIMER1_ENABLE 0
#define WKT_ENABLE				0
#define WDT_ENABLE				0
#define GPIO_ENABLE				1
#define GPIO_INT_ENABLE			1
#define PWM_ENABLE				0
#define ADC_ENABLE				1
#define INTERNAL_FLASH_ENABLE	0

#endif