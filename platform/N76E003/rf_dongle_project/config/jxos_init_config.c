
#include "jxos_public.h"

void jxos_prepare_to_run_callback_handler(void);
void jxos_hal_init(void)
{
	jxos_prepare_to_run_callback = jxos_prepare_to_run_callback_handler;
}

void  rf_dongle_task_init(void);
void  uart_task_init(void);
void jxos_user_task_init(void)
{
	rf_dongle_task_init();
	uart_task_init();
}
