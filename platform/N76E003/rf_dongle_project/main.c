
#include "N76E003.h"
#include "Common.h"
#include "SFR_Macro.h"
#include "jxos_public.h"
#include "hal.h"

int main (void)
{
	jxos_run();
    return 0;
}

void jxos_prepare_to_run_callback_handler(void)
{
	set_EA;
}

void led_hal_on(void)
{
	P05 = 1;
}

void led_hal_off(void)
{
	P05 = 0;
}

void led_init(void)
{
//	gpio_mode_config(1, 7, GPIO_MODE_PP);
	gpio_mode_config(0, 5, GPIO_MODE_PP);
	led_hal_on();
	delay_1ms(250);
	delay_1ms(250);
	delay_1ms(250);
	delay_1ms(250);
	delay_1ms(250);
	delay_1ms(250);
	delay_1ms(250);
	delay_1ms(250);
	led_hal_off();
	delay_1ms(250);
	delay_1ms(250);
	delay_1ms(250);
	delay_1ms(250);
	delay_1ms(250);
	delay_1ms(250);
	delay_1ms(250);
	delay_1ms(250);
}

void rf_power_init(void)
{
	gpio_mode_config(0, 1, GPIO_MODE_PP);	//rf rx
}

void rf_tx_enable(void)
{
	P00 = 1;
}

void rf_tx_disable(void)
{
	P00 = 0;
}

void rf_rx_enable(void)
{
	P01 = 1;
}

void rf_rx_disable(void)
{
	P01 = 0;
}