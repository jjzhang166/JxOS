#include <string.h>
#include "jxos_public.h"
#include "lib/ring_buff.h"
#include "bsp/bsp_rf_driver.h"
#include "bsp/bsp_async_comm.h"
#include "bsp_async_comm_config.h"
#include "hal_isr.h"
#include "hal.h"

void led_hal_on(void);
void led_hal_off(void);
void rf_rx_disable(void);

uint8_t uart_rx_isr_rec_data[32] = {0};
uint8_t uart_rx_isr_rec_data_len = 0;
uint8_t uart_rx_isr_handler_flag = 0;
uint8_t uart_tx_isr_handler_flag = 0;

static uint8_t uart_rx_buffer[32] = {0};
static uint8_t uart_rx_buffer_len = 0;

static uint8_t uart_tx_buffer_space[16] = {0};
RINGBUFF_MGR uart_tx_buffer_mgr;

static JXOS_MSG_HANDLE rf_rec_data_msg;
static swtime_type uart_rx_delay_timer = 0;

static void bsp_async_comm_receive_ok_call_back_handler(uint8_t* unpackaged_rec_data, uint8_t unpackaged_rec_data_len)
{
	rf_rx_disable();
	bsp_rf_send(unpackaged_rec_data, unpackaged_rec_data_len);
	sys_debug_print_task_blocked_print_str("bsp_async_comm_receive_ok_call_back\r\n");
	sys_debug_print_task_blocked_print_data_stream_in_hex(unpackaged_rec_data, unpackaged_rec_data_len);
	sys_debug_print_task_blocked_print_str("\r\n");
}

static void uart_rx_isr_handler(void)
{
	sys_software_timer_task_restart_timer(uart_rx_delay_timer);
}

static void uart_tx_isr_handler(void)
{
	uint8_t tx_data;
	if(1 == ringbuff_read_data(&uart_tx_buffer_mgr, 1, &tx_data)){
		SBUF = tx_data;
		led_hal_off();
	}
}

void uart_send_data_blocked(uint8_t* send_data, uint8_t len)
{
	uint8_t i;
	for(i = 0; i < len; i++){
		uart0_send_data_blocked(send_data[i]);
	}
}
//#include "N76E003.h"
//#include "SFR_Macro.h"
//uint8_t test_buff[] = "0123456789";
static void uart_task(uint8_t task_id, void * parameter)
{
	uint8_t rf_rec_data_buff[FRAME_PAYLOAD_LEN_MAX];
	uint8_t rf_rec_data_len;
	uint8_t uart_tx_data;
	if(1 == sys_software_timer_task_check_overtime_timer(uart_rx_delay_timer)){
		sys_software_timer_task_stop_timer(uart_rx_delay_timer);
		uart_rx_buffer_len = uart_rx_isr_rec_data_len;
		memcpy(uart_rx_buffer, uart_rx_isr_rec_data, uart_rx_buffer_len);
		uart_rx_isr_rec_data_len = 0;
		bsp_async_comm_rec_data_handler(uart_rx_buffer, uart_rx_buffer_len);
	}
	if(uart_rx_isr_handler_flag == 1){
		uart_rx_isr_handler_flag = 0;
		uart_rx_isr_handler();
	}
	if(uart_tx_isr_handler_flag == 1){
		uart_tx_isr_handler_flag = 0;
		uart_tx_isr_handler();
	}
	
//	while(1){
//		ringbuff_write_data(&uart_tx_buffer_mgr, 10, test_buff);
//		uart_send_data_blocked("testt\r\n", 7);
//		clr_EA;
//		ringbuff_read_data(&uart_tx_buffer_mgr, 10, uart_rec_data_buff);
//		set_EA;
//		if(memcmp(test_buff, uart_rec_data_buff, 10) != 0){
//			uart_send_data_blocked("error\r\n", 7);
//		}
//	}
	
	rf_rec_data_len = 0;
	while(jxos_msg_receive(rf_rec_data_msg, &rf_rec_data_buff[rf_rec_data_len]) == 1){
		rf_rec_data_len++;
	}
	if(rf_rec_data_len > 0){
		led_hal_off();
		bsp_async_comm_pack_send_data(uart_rx_buffer, &uart_rx_buffer_len, rf_rec_data_buff, rf_rec_data_len);
		ringbuff_write_data(&uart_tx_buffer_mgr, uart_rx_buffer_len, uart_rx_buffer);
		sys_debug_print_task_blocked_print_str("write to uart tx buff\r\n");
		sys_debug_print_task_blocked_print_data_stream_in_hex(uart_rx_buffer, uart_rx_buffer_len);
		sys_debug_print_task_blocked_print_str("\r\n");
		if(1 == ringbuff_read_data(&uart_tx_buffer_mgr, 1, &uart_tx_data)){
			SBUF = uart_tx_data;
			led_hal_on();
		}
	}
}

void  uart_task_init(void)
{
	bsp_async_comm_init(bsp_async_comm_receive_ok_call_back_handler);

	uart0_use_timer1_init(38400);
//	clr_ES;           	//disable UART interrupt
//	clr_REN;			//disable uart0 rx
    RingBuff(&uart_tx_buffer_mgr, 16, uart_tx_buffer_space);

    uart_rx_delay_timer = sys_software_timer_task_new_timer();
	sys_software_timer_task_set_timer(uart_rx_delay_timer, 50);

	rf_rec_data_msg = jxos_msg_get_handle("rf_rec_data_msg");
	jxos_task_create(uart_task, "u_t", 0);
}
