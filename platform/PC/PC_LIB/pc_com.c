
#include <stdio.h>
#include <windows.h>
#include <string.h>

#define COM_PORT	"COM2"

static void (*com_rec_callback)(unsigned char* str, unsigned char len) = 0;
static HANDLE hCom;
static unsigned char str[256];
static DWORD WINAPI COM_SIM(LPVOID p)
{
    DWORD wCount = 256;//读取的字节数
    DWORD wCount1;

    while(1){
        memset(str, 0, 256);
        if(ReadFile(hCom, str, wCount, &wCount1, NULL))
        {
//            printf("com rec %d: ", wCount1);
//            printf("%s\r\n", str);
            if(com_rec_callback != 0){
				com_rec_callback(str, (unsigned char)wCount1);
            }
        }
        Sleep(1);
    }

    return 0;
}

void com_init(void)
{
	WCHAR wszClassName[8];
	memset(wszClassName, 0, sizeof(wszClassName));
	MultiByteToWideChar(CP_ACP, 0, COM_PORT, strlen(COM_PORT) + 1, wszClassName,
		sizeof(wszClassName) / sizeof(wszClassName[0]));

    hCom = CreateFile(COM_PORT,		//COM2口
        GENERIC_READ | GENERIC_WRITE,	//允许读和写
        0,//独占方式
        NULL,
        OPEN_EXISTING,//打开而不是创建
        0,//同步方式
        NULL);
    if(hCom == (HANDLE)-1)
    {
        printf("com open faile!\n");
        return;
    }
    else
    {
        printf("com open ok!\n");
    }
    SetupComm(hCom, 20480, 20480);//输入缓冲区和输出缓冲区的大小都是1024

    COMMTIMEOUTS TimeOuts;//设定读超时
    TimeOuts.ReadIntervalTimeout = 2;
    TimeOuts.ReadTotalTimeoutMultiplier = 1;
    TimeOuts.ReadTotalTimeoutConstant = 2;
    TimeOuts.WriteTotalTimeoutMultiplier = 5;//设定写超时
    TimeOuts.WriteTotalTimeoutConstant = 20;
    SetCommTimeouts(hCom, &TimeOuts);//设置超时

    DCB dcb1;
    GetCommState(hCom, &dcb1);
    dcb1.BaudRate = 115200;//波特率为9600
    dcb1.ByteSize = 8;//每个字节有8位
    dcb1.Parity = NOPARITY;//无奇偶校验位
    dcb1.StopBits = TWOSTOPBITS;//两个停止位
    dcb1.fParity = FALSE;
    dcb1.fNull = FALSE;
    SetCommState(hCom, &dcb1);
    PurgeComm(hCom, PURGE_TXCLEAR|PURGE_RXCLEAR);//清空缓冲区

	CreateThread(NULL, 0, COM_SIM, 0, 0, NULL);
/*
    while(1)
    {
        int i = 0;
        FILE *fp1;
        unsigned char str[256];
        if(!ReadFile(hCom, str, wCount, &wCount1, NULL))
        {
            printf("读串口失败!");
            return FALSE;
        }
        fp1 = fopen("串口发送的数.txt", "a+");
        if(wCount1 > 0)
        {
            printf("wCount1:%d\n", wCount1);
        }
        printf("读串口成功!读取数据为:\n");
        for(i=0; i< wCount1; i++)
        {
            printf("%02X ", str[i]);
            fprintf(fp1, "%02X ", str[i]);
        }
        printf("\n");
        fclose(fp1);
    }

    CloseHandle(hCom);
*/
}

void com_send_data(unsigned char* data, unsigned char len)
{
    DWORD wCount1;
    WriteFile(hCom, data, (DWORD)len, &wCount1, NULL);
}

void com_rec_callback_reg(void (*com_rec_callback_handler)(unsigned char* str, unsigned char len))
{
	com_rec_callback = com_rec_callback_handler;
}

/*************************************************

#include <Windows.h>
#include <stdio.h>

HANDLE hCom;

int main(void)
{
	hCom = CreateFile(TEXT("com6"),//COM1口
		GENERIC_READ, //允许读
		0, //指定共享属性，由于串口不能共享，所以该参数必须为0
		NULL,
		OPEN_EXISTING, //打开而不是创建

		FILE_ATTRIBUTE_NORMAL, //属性描述，该值为FILE_FLAG_OVERLAPPED，表示使用异步I/O，该参数为0，表示同步I/O操作
		NULL);

	if (hCom == INVALID_HANDLE_VALUE)
	{
		printf("打开COM失败!\n");
		return FALSE;
	}
	else
	{
		printf("COM打开成功！\n");
	}

	SetupComm(hCom, 1024, 1024); //输入缓冲区和输出缓冲区的大小都是1024

	/*********************************超时设置**************************************
	COMMTIMEOUTS TimeOuts;
	//设定读超时
	TimeOuts.ReadIntervalTimeout = MAXDWORD;//读间隔超时
	TimeOuts.ReadTotalTimeoutMultiplier = 0;//读时间系数
	TimeOuts.ReadTotalTimeoutConstant = 0;//读时间常量
	//设定写超时
	TimeOuts.WriteTotalTimeoutMultiplier = 1;//写时间系数
	TimeOuts.WriteTotalTimeoutConstant = 1;//写时间常量
	SetCommTimeouts(hCom, &TimeOuts); //设置超时

	/*****************************************配置串口***************************
	DCB dcb;
	GetCommState(hCom, &dcb);
	dcb.BaudRate = 9600; //波特率为9600
	dcb.ByteSize = 8; //每个字节有8位
	dcb.Parity = NOPARITY; //无奇偶校验位
	dcb.StopBits = ONESTOPBIT; //一个停止位
	SetCommState(hCom, &dcb);

	DWORD wCount;//实际读取的字节数
	bool bReadStat;

	char str[2] = { 0 };

	while (1)
	{
		//PurgeComm(hCom, PURGE_TXCLEAR | PURGE_RXCLEAR); //清空缓冲区
		bReadStat = ReadFile(hCom, str, sizeof(str), &wCount, NULL);

		if (!bReadStat)
		{
			printf("读串口失败!");
			return FALSE;
		}
		else
		{
			//str[1] = '\0';
			printf("%c\n", str[0]);
		}
		Sleep(100);
	}

	CloseHandle(hCom);
}


#include<stdio.h>
#include<windows.h>

int main()
{
	FILE *fp;
	if ((fp = fopen("com6", "r")) == NULL)
	{
		printf("cannot open com!\n");
	}
	else
		printf("open com successful!\n");

	char str;

	while (1)
	{
		fscanf(fp, "%c", &str);
		printf("%c ", str);

		Sleep(100);
	}

	return 0;
}



打开COM1
HANDLE hCom = CreateFile(TEXT("COM1"), GENERIC_READ|GENERIC_WRITE, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
if( hCom==INVALID_HANDLE_VALUE ) return false;

设置 波特率、奇偶校验、停止位 等等
DCB CommDCB;
GetCommState( hCom, &CommDCB );
CommDCB.BaudRate = BaudRate;
CommDCB.Parity = EVENPARITY;
CommDCB.StopBits = ONESTOPBIT;
CommDCB.ByteSize = 8;
CommDCB.fBinary = 1;
CommDCB.fParity = 1;
CommDCB.fOutxCtsFlow = 0;
CommDCB.fOutxDsrFlow = 0;
CommDCB.fDtrControl = 0;
CommDCB.fDsrSensitivity = 0;
CommDCB.fTXContinueOnXoff = 0;
CommDCB.fOutX = 0;
CommDCB.fInX = 0;
CommDCB.fErrorChar = 0;
CommDCB.fNull = 0;
CommDCB.fRtsControl = RTS_CONTROL_TOGGLE;
CommDCB.fAbortOnError = 0;
SetCommState( hCom, &CommDCB );

设置缓冲大小
SetupComm( hCom, 100, 100 );

设置超时时间
COMMTIMEOUTS CommTimeouts;
GetCommTimeouts( hCom, &CommTimeouts );
CommTimeouts.ReadIntervalTimeout = MAXDWORD;
CommTimeouts.ReadTotalTimeoutMultiplier = 0;
CommTimeouts.ReadTotalTimeoutConstant = 0;
SetCommTimeouts( hCom, &CommTimeouts );

/////////////// 这期间就是对COM进行读写了 ///////////////
COMSTAT ComStat; ClearCommError(hCom,&Errors,&ComStat); 后 ComStat.cbInQue 保存着一个数值，指明还有多少字节已经获得但没有用ReadFile取走。
读用 ReadFile，写用 WriteFile。
/////////////////////////////////////////////////////////

关闭COM
CloseHandle( hCom );

基本上就这些，详见 MFC

***************************************************************/


