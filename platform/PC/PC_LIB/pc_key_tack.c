
#include "jxos_public.h"

#include <stdio.h>
#include <windows.h>

#define SENDER_NAME "pcksnd"
static uint8_t pc_key_msg_pipe_buff[32];
static JXOS_MESSAGE_PIPE_HANDLE pc_key_msg_pipe;

static volatile char key_code = 0XFF;

static DWORD WINAPI PC_KEY_TASK_SIM(LPVOID p)
{
    while(1){
        key_code = getch();
        Sleep(50);
    }
    return 0;
}


static void pc_key_task(uint8_t task_id, void * parameter)
{
	if((unsigned char)key_code != 0xff){
        jxos_message_pipe_send(pc_key_msg_pipe, SENDER_NAME, &key_code, 1);
        key_code = 0xff;
	}
}

void pc_key_task_init(void)
{
    key_code = 0xff;

	//OS
	jxos_task_create(pc_key_task, "pc_key", 0);
    //data_size  >= 3
	pc_key_msg_pipe = jxos_message_pipe_create(pc_key_msg_pipe_buff, 32, 3, "pc_key_pipe");
    jxos_message_pipe_sender_name_register(pc_key_msg_pipe, SENDER_NAME);

	CreateThread(NULL, 0, PC_KEY_TASK_SIM, 0, 0, NULL);
}


