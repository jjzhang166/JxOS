

#include<Windows.h>
#include<stdio.h>
//参数一表示 需要等待的时间 微秒为单位
int UsSleep(int us)
{
    //储存计数的联合
    LARGE_INTEGER fre;
    //获取硬件支持的高精度计数器的频率
    if (QueryPerformanceFrequency(&fre))
    {
        LARGE_INTEGER run,priv,curr;
        run.QuadPart = fre.QuadPart * us / 1000000;//转换为微妙级
        //获取高精度计数器数值
        QueryPerformanceCounter(&priv);
        do
        {
            QueryPerformanceCounter(&curr);
        } while (curr.QuadPart - priv.QuadPart < run.QuadPart);
        curr.QuadPart -= priv.QuadPart;
        int nres = (curr.QuadPart * 1000000 / fre.QuadPart);//实际使用微秒时间
        return nres;
    }
    return -1;//
}

static void (*pc_timer0_isr)(void);
static unsigned short int timer0_p_ms = 0;
static DWORD WINAPI TIMER0_INT(LPVOID p)
{
	while(1){
		if(timer0_p_ms != 0){
 //           printf("TIMER0_INT\r\n");
			Sleep(timer0_p_ms);
			if(pc_timer0_isr != 0){
				pc_timer0_isr();
			}
		}
		else{
			Sleep(10);
		}
	}

    return 0;
}

/***
static unsigned short int timer1_p = 0;
static DWORD WINAPI TIMER1_INT(LPVOID p)
{
	int us;
	while(1){
//		printf("TIMER1_INT\r\n");s
		us = UsSleep(500);
		if(us == -1){
			printf("us err\r\n");
		}
		else{
//			printf("us: %d\r\n",us);
		}
	}
    return 0;
}
**/

void pc_timer0_init(unsigned short int int_time_ms)
{
	timer0_p_ms = int_time_ms;
	CreateThread(NULL, 0, TIMER0_INT, 0, 0, NULL);
}

void pc_timer0_isr_reg(void (*pc_timer0_isr_handler)(void))
{
	pc_timer0_isr = pc_timer0_isr_handler;
}

