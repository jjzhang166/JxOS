
#include "jxos_public.h"

static JXOS_MSG_HANDLE r_msg;
static JXOS_MESSAGE_PIPE_HANDLE r_pipe = 0;
static JXOS_BULLETIN_BOARD_HANDLE r_bin = 0;

static void app_test1_task(uint8_t task_id, void * parameter)
{
	/*********************************************/
//    uint8_t buff[16] = {0};
//    uint8_t buff_len;
//   if(jxos_msg_receive(r_msg, buff) == 1){
//		printf("app_test1_task rec msg:");
//		printf(" %s\r\n", buff);
//  }
//   if(jxos_message_pipe_check_empty(r_pipe) == 0){
//	   if(jxos_message_pipe_receive(r_pipe, "tps222", buff, &buff_len) == 1){
//			printf("app_test1_task rec pipe:");
//			printf(" %s\r\n", buff);
//			printf("len %d\r\n", buff_len);
//	   }
//   }
	/********************************************/
	/*********************************************
	uint8_t last_index_temp;
	static uint8_t last_index = 0xff;

    uint8_t buff[30];
  	uint8_t len;

    if(jxos_bulletin_board_check_empty(r_bin) == 1){
//		printf("empty\r\n");
        return;
	}
	last_index_temp = last_index;
	jxos_bulletin_board_get_next_index(r_bin, &last_index_temp);
	if(jxos_bulletin_board_receive_by_index(r_bin, last_index_temp, buff, &len) == 1){
		last_index = last_index_temp;
        printf("buff rec OK: %s, len %d\r\n", buff, len);
	}
	else{
//		printf("no new msg\r\n");
	}
	********************************************/
}

void app_test1_task_init(void)
{
	jxos_task_create(app_test1_task, "test1", 0);
    r_msg = jxos_msg_get_handle("test_msg");
    r_pipe = jxos_message_pipe_get_handle("test_pipe");
    if(r_pipe != 0){
        printf("jxos_message_pipe_get_handle OK!\r\n");
        if(jxos_message_pipe_receiver_name_register(r_pipe, "tps222")){
            printf("jxos_message_pipe_receiver_name_register OK!\r\n");
        }
    }

    r_bin = jxos_bulletin_board_get_handle("test_bin");
}

