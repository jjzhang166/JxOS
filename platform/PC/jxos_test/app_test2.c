
#include "..\PC_LIB\pc_com.h"
#include "jxos_public.h"
#include "lib/frame/frame_recive_public.h"

//#include "hal/hal_exti.h"
//#include "hal/hal_interrupt.h"

#include "sys_service/software_timer_task.h"

static swtime_type app_test_swt = 0;

static JXOS_EVENT_HANDLE test_event;

static uint8_t test_msg[128];
static JXOS_MSG_HANDLE test_msg_h;

static JXOS_MSG_HANDLE key_msg_h;

static uint8_t test_pipe[128];
static JXOS_MESSAGE_PIPE_HANDLE test_pipe_h;

static uint8_t test_bin[128];
static JXOS_MESSAGE_PIPE_HANDLE test_bin_h;

static void app_test_task(uint8_t task_id, void * parameter)
{
    uint8_t msg_itme;
    uint8_t key_num;
    uint8_t key_code;
    uint8_t frame_send_s[30];
    static uint8_t frame_send_count = 0;

//    if(jxos_event_wait(test_event) == 1){
//    }
    if(sys_software_timer_task_check_overtime_timer(app_test_swt) == 1){
		memcpy(frame_send_s, "frame_send test", 15);
        frame_send_s[14] = frame_send_count+'0';
        frame_send_count++;
//		printf("app_test_task send \"t_tick!\" -->%s\r\n", frame_send_s);
//        jxos_msg_send(test_msg_h, "t_tick!");
//        jxos_message_pipe_send(test_pipe_h, "tps111", frame_send_s, 15);

//        if(std_app_frame_send_task_send_data(frame_send_s, 15) != 1){
//            printf("frame_send busy\r\n");
//        }
    }
    if(jxos_msg_receive(key_msg_h, &msg_itme)){
        key_num = msg_itme>>4;
        key_code = msg_itme&0x0f;
        printf("key_msg num:%d, code:%d\r\n", key_num, key_code);
 //       memcpy(frame_send_s, "test", 5);
 //       frame_send_s[4] = frame_send_count+'0';
 //       frame_send_count++;
 //       if(jxos_bulletin_board_check_full(test_bin_h) == 1){
 //           jxos_bulletin_board_del_earliest(test_bin_h, "tps111");
  //      }
//		jxos_bulletin_board_send_with_index(test_bin_h, "tps111", frame_send_s, 6);
    }
}

static void app_test_exti_handler(void)
{
    jxos_event_set(test_event);
}

void app_test_task_init(void)
{
//	EXTI_InitTypeDef EXTI_InitStruct;
//	EXTI_InitStruct.EXTI_Line = EXTI_Line0;
//    EXTI_Init(&EXTI_InitStruct);
//    EXTI0_IRQHandler_callback = app_test_exti_handler;

	jxos_task_create(app_test_task, "test", 0);

	test_pipe_h = jxos_message_pipe_create(test_pipe, 128, 32, "test_pipe");
    jxos_message_pipe_sender_name_register(test_pipe_h, "tps111");

	test_bin_h = jxos_bulletin_board_create(test_bin, 128, 32, "test_bin");
    jxos_bulletin_board_sender_name_register(test_bin_h, "tps111");

    test_msg_h = jxos_msg_create(test_msg, 128, 32, "test_msg");

    key_msg_h = jxos_msg_get_handle("std_app_key_msg");

    test_event =jxos_event_create();

	app_test_swt = sys_software_timer_task_new_timer();
	sys_software_timer_task_set_timer(app_test_swt, 5000);
	sys_software_timer_task_start_timer(app_test_swt);
}

