
#include "../hal_pc/pc_com.h"
#include "jxos_public.h"
#include "lib/frame/frame_recive_public.h"
#include "sys_service/software_timer_task.h"

#include "stdio.h"
#include <string.h>

void print_data(uint8_t* data, uint8_t len);

void frame_decode(uint8_t *dat,uint8_t length);
void com_rec_callback_handler(uint8_t *dat,uint8_t length);

static swtime_type mcu_to_zigbee_swt = 0;
static JXOS_MSG_HANDLE key_msg_h;

static void mcu_to_zigbee_task(uint8_t task_id, void * parameter)
{
    uint8_t msg_itme;
    uint8_t key_num;
    uint8_t key_code;

//    uint8_t frame_send_s[30];
//    static uint8_t frame_send_count = 0;

    if(sys_software_timer_task_check_overtime_timer(mcu_to_zigbee_swt) == 1){

//		memcpy(frame_send_s, "frame_send test", 15);
//        frame_send_s[14] = frame_send_count+'0';
//        frame_send_count++;
//		printf("mcu_to_zigbee_task send \"t_tick!\" -->%s\r\n", frame_send_s);
//        jxos_msg_send(test_msg_h, "t_tick!");
//        jxos_message_pipe_send(test_pipe_h, "tps111", frame_send_s, 15);

//        if(std_app_frame_send_task_send_data(frame_send_s, 15) != 1){
//            printf("frame_send busy\r\n");
//        }
    }
    if(jxos_msg_receive(key_msg_h, &msg_itme)){
        key_num = msg_itme>>4;
        key_code = msg_itme&0x0f;
        printf("key_msg num:%d, code:%d\r\n", key_num, key_code);

		if(key_num == 1){
			std_app_frame_send_task_send_data((uint8_t*)":SMD1563412907856341249\r\n", 25);
		}
		else if(key_num == 2){
			std_app_frame_send_task_send_data((uint8_t*)":SMD1000000000000000049\r\n", 25);
		}
		else if(key_num == 9){
			std_app_frame_send_task_send_data_no_repeat((uint8_t*)":LST\r\n", 6);
		}
		else if(key_num == 0){
			std_app_frame_send_task_send_data_no_repeat((uint8_t*)":RST\r\n", 6);
		}
   }
}

void mcu_to_zigbee_task_init(void)
{
	jxos_task_create(mcu_to_zigbee_task, "mcu_to_zigbee", 0);
    key_msg_h = jxos_msg_get_handle("std_app_key_msg");

	mcu_to_zigbee_swt = sys_software_timer_task_new_timer();
	sys_software_timer_task_set_timer(mcu_to_zigbee_swt, 10000);
	sys_software_timer_task_start_timer(mcu_to_zigbee_swt);

	frame_recive_std_init();
	frame_recice_success_callback_reg(frame_decode);

	com_init();
	com_rec_callback_reg(com_rec_callback_handler);

	std_app_frame_send_task_hal_send_data_callback = com_send_data;
}

#define EUI64_SIZE 16
typedef uint8_t EmberEUI64[EUI64_SIZE];

static EmberEUI64 end_device_list[10] =
{
	"1122334455667780",
	"1122334455667781",
	"FFFFFFFFFFFFFFFF",
	"FFFFFFFFFFFFFFFF",
	"FFFFFFFFFFFFFFFF",
	"FFFFFFFFFFFFFFFF",
	"FFFFFFFFFFFFFFFF",
	"FFFFFFFFFFFFFFFF",
	"FFFFFFFFFFFFFFFF",
	"FFFFFFFFFFFFFFFF",
};

static uint8_t XOR_Check_Sum(uint8_t *dat,uint8_t length)
{
	uint8_t result;
	result = 0;
	for(; length > 0; length--)
	{
		result = result ^ *dat;
		dat++;
	}
	return result;
}

/*
void cmd_send_data_pack(uint8_t* cmd, uint8_t node_id, uint8_t* payloud, uint8_t payloud_len, uint8_t* data_buff)
{
	uint8_t Check_Sum;
	uint8_t temp;
	memset(data_buff, 0, MAX_CMD_LEN);
	data_buff[0] = ':';
	memcpy(&(data_buff[1]), cmd, 3);
	data_buff[4] = node_id;
	memcpy(&(data_buff[5]), payloud, payloud_len);

	Check_Sum = XOR_Check_Sum(data_buff, 5+payloud_len);

	temp = Check_Sum>>4;
	if(temp <= 0x09){
		temp += '0';
	}
	else{
		temp += 'A';
	}
	data_buff[5+payloud_len] = temp;

	temp = Check_Sum&0x0f;
	if(temp <= 0x09){
		temp += '0';
	}
	else{
		temp += 'A';
	}
	data_buff[5+payloud_len+1] = temp;

	data_buff[5+payloud_len+2] = '\r';
	data_buff[5+payloud_len+3] = '\n';
}
*/

static void send_mac(uint8_t id)
{
	uint8_t data_buff[30];

	uint8_t Check_Sum;
	uint8_t temp;

	memset(data_buff, 0, 30);
	data_buff[0] = ':';
	memcpy(&(data_buff[1]), "SMU", 3);	//SMU:rsp back to zigbee; SMD:set zigbee mac
	data_buff[4] = id;
	memcpy(&(data_buff[5]), end_device_list[id-'0'], EUI64_SIZE);

	Check_Sum = XOR_Check_Sum(data_buff, 5+EUI64_SIZE);

	temp = Check_Sum>>4;
	if(temp <= 0x09){
		temp += '0';
	}
	else{
		temp -= 0x0A;
		temp += 'A';
	}
	data_buff[5+EUI64_SIZE] = temp;

	temp = Check_Sum&0x0f;
	if(temp <= 0x09){
		temp += '0';
	}
	else{
		temp -= 0x0A;
		temp += 'A';
	}
	data_buff[5+EUI64_SIZE+1] = temp;

	data_buff[5+EUI64_SIZE+2] = '\r';
	data_buff[5+EUI64_SIZE+3] = '\n';

	com_send_data(data_buff, EUI64_SIZE+9);
	printf("mcu->zigbee: %s", data_buff);
}

static void cmd_send_ack(uint8_t* cmd, uint8_t id)
{
	uint8_t data_buff[10];
	uint8_t Check_Sum;
	uint8_t temp;

	memset(data_buff, 0, 10);
	data_buff[0] = ':';

	memcpy(&(data_buff[1]), cmd, 3);
	data_buff[3] = 'D';

	data_buff[4] = id;
	data_buff[5] = '1';
	Check_Sum = XOR_Check_Sum(data_buff, 6);

	temp = Check_Sum>>4;
	if(temp <= 0x09){
		temp += '0';
	}
	else{
		temp -= 0x0A;
		temp += 'A';
	}
	data_buff[6] = temp;

	temp = Check_Sum&0x0f;
	if(temp <= 0x09){
		temp += '0';
	}
	else{
		temp -= 0x0A;
		temp += 'A';
	}
	data_buff[6+1] = temp;

	data_buff[6+2] = '\r';
	data_buff[6+3] = '\n';

	std_app_frame_send_task_send_data_no_repeat(data_buff, 10);
	printf("mcu->zigbee:");
	print_data(data_buff, 10);
}

void frame_decode(uint8_t *dat,uint8_t length)
{
	if(memcmp(&(dat[1]), "SMD", 3) == 0)		//SMD:zigbee ask mac
	{
		send_mac(dat[4]);
	}
	else if(memcmp(&(dat[1]), "SMU", 3) == 0){  //SMU:zigbee ACK FOR SET MAC
        std_app_frame_send_task_stop_repeat();
	}
	else if(dat[3] == 'U'){                                       //other zigbee to mcu cmd
        //send ack
        cmd_send_ack(&(dat[1]), dat[4]);
	}
}

void com_rec_callback_handler(uint8_t *buff,uint8_t buff_len)
{
//	uint8_t buff[30];
//	uint8_t buff_len;
//	uint8_t i = 0;
//	read_com_buff(buff, &len);

//	uint8_t buff[] = ":SMD1AA\r\n";
//	uint8_t buff_len = 9;

//	if(buff_len > 0)
//		printf("zigbee -> mcu\r\n");

	while(1){
		if(buff_len == 0){
			break;
		}
		printf("%c", *buff);
		frame_recive_handler(*buff);
		buff++;
		buff_len--;
	}
}
