
#include "jxos_public.h"

uint8_t ck_0[] = {0, 3, 1};
uint8_t ck_1[] = {0, 3, 1, 4};
uint8_t ck_2[] = {6, 2, 9};

static JXOS_MESSAGE_PIPE_HANDLE mb_key_msg_pipe = 0;

static void mb_test_task(uint8_t task_id, void * parameter)
{
    uint8_t key_code;
    uint8_t len;

    if(jxos_message_pipe_check_empty(mb_key_msg_pipe) == 0){
        if(jxos_message_pipe_receive(mb_key_msg_pipe, "recpck", &key_code, &len) == 1){
            printf("mb_test_task rec key_code: %d\r\n", key_code);
        }
    }
}

void mb_test_task_init(void)
{
	std_app_composite_key_create(ck_0, 3, 1);
	std_app_composite_key_create(ck_1, 4, 0);
	std_app_composite_key_create(ck_2, 3, 5);

	jxos_task_create(mb_test_task, "mb_test", 0);

	mb_key_msg_pipe = jxos_message_pipe_get_handle("cp_key_pipe");
	jxos_message_pipe_receiver_name_register(mb_key_msg_pipe, "recpck");
}
