#ifndef __KEY_CONGIF_H
#define __KEY_CONGIF_H

#include "lib/type.h"

#define KEY_NUM_MAX 		        1

#define KEY_PRESS_LEVEL				0

#define KEY_JITTER_TIME					0
#define KEY_LONG_PRESS_TIME 		    2000/25	//KEY_TASK_SCAN_TIME 25ms
#define KEY_LONG_PRESS_REPEAT_TIME 		1000/25

#endif
