
/********************************************************************************
文  件：rf_driver.c
作  者:	JeremyCeng
版  本:	V1.0.0
时  间:	2017-07-05
说  明:	RF 433 315 模块驱动
历  史: V1.0.0 2017-07-05:	初版
********************************************************************************/

/********************************************************
比特流格式：
|起始位(1bit)|数据位(8/16/24/32bit)(低位在前)|结束位(1bit)|
start d0(lsb) d1 d2 ... dn(msb) stop
********************************************************/

//接收模块 信号 高电平最长保持时间 20ms；低电平最长保持时间 300ms
//接受模块 杂波 高电平最长时间 4ms；低电平最长时间 2ms
//165us 490us


#ifndef _RF_DROVER_CONFIG_
#define _RF_DROVER_CONFIG_

#include "lib/type.h"

#define DATA_FLOW_MAX_LEN 	32UL	//byte

#define SYNC_H_TIME		30UL
#define SYNC_L_TIME		30UL

#define BIT0_H_TIME		20UL
#define BIT0_L_TIME		20UL

#define BIT1_H_TIME		10UL
#define BIT1_L_TIME		10UL

#define END_H_TIME		40UL
#define END_L_TIME		5UL

#define ALLOW_ERR_TIME	    6UL
#define ALLOW_ERR_PERCENT	30UL

#define SEND_SYNC_REPEAT	1


#endif // _RF_DROVER_CONFIG_




