
#include "jxos_public.h"
#include "hal/hal_timer.h"
#include "hal/hal_exti.h"

int main (void)
{
	jxos_run();
    return 0;
}

void jxos_prepare_to_run_callback_handler(void)
{
	sys_debug_print_task_user_print_str("jxos_prepare_to_run_callback_handler\r\n");
}

#if	(JXOS_SYS_SERVICE_DEBUG_PRINT_TASK_ENABLE == 1)
void sys_debug_print_task_hal_init_callback_handler(void)
{
}

uint8_t sys_debug_print_task_send_finish_check_callback_handler(void)
{
	Sleep(1);
	return true;
}

void sys_debug_print_task_send_byte_callback_handler(uint8_t byte)
{
	printf("%c", byte);
}
#endif

void sys_software_timer_task_hal_init_callback_handler(void)
{
    TIM_TimeBaseInitTypeDef TIM_TimeBaseInitStruct;
    TIM_TimeBaseInitStruct.TIM_CounterMode = TIM_CounterMode_Up;
    TIM_TimeBaseInitStruct.TIM_Period = 5;
    TIM_TimeBaseInit(TIM0, &TIM_TimeBaseInitStruct);

    TIM_ITConfig(TIM0, TIM_IT_Update, ENABLE);

    TIM_Cmd(TIM0, ENABLE);
}

void srtnet_comm_layer_task_hal_timer_init_callback_handler(void)
{
    TIM_TimeBaseInitTypeDef TIM_TimeBaseInitStruct;
    TIM_TimeBaseInitStruct.TIM_CounterMode = TIM_CounterMode_Up;
    TIM_TimeBaseInitStruct.TIM_Period = 5;
    TIM_TimeBaseInit(TIM1, &TIM_TimeBaseInitStruct);

    TIM_ITConfig(TIM1, TIM_IT_Update, ENABLE);

    TIM_Cmd(TIM1, ENABLE);
}

void led_on(uint8_t led_num)
{
	sys_debug_print_task_user_print_str("led on\r\n");
    switch(led_num){
    case 0:
//        GPIO_SetBits(GPIOA, GPIO_Pin_1);
        break;
    default:
        break;
    }
}

void led_off(uint8_t led_num)
{
	sys_debug_print_task_user_print_str("led off\r\n");
    switch(led_num){
    case 0:
//        GPIO_ResetBits(GPIOA, GPIO_Pin_1);
        break;
    default:
        break;
    }
}

void led_init(void)
{
}

void key_init(void)
{
}

uint8_t key_read_pin_level(uint8_t num)
{
	return 0;
}

void Timer0_ISR_callback_handler()
{
	software_timer_task_tick_handler();
}

void Timer1_ISR_callback_handler()
{
	srtnet_comm_layer_tick_handler();
}
