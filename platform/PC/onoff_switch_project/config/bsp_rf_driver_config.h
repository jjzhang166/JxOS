
/********************************************************************************
��  ����rf_driver.c
��  ��:	JeremyCeng
��  ��:	V1.0.0
ʱ  ��:	2017-07-05
˵  ��:	RF 433 315 ģ������
��  ʷ: V1.0.0 2017-07-05:	����
********************************************************************************/

/********************************************************
��������ʽ��
|��ʼλ(1bit)|����λ(8/16/24/32bit)(��λ��ǰ)|����λ(1bit)|
start d0(lsb) d1 d2 ... dn(msb) stop
********************************************************/

//����ģ�� �ź� �ߵ�ƽ�����ʱ�� 20ms���͵�ƽ�����ʱ�� 300ms
//����ģ�� �Ӳ� �ߵ�ƽ�ʱ�� 4ms���͵�ƽ�ʱ�� 2ms
//165us 490us


#ifndef _RF_DROVER_CONFIG_
#define _RF_DROVER_CONFIG_

#include "lib/type.h"

#define DATA_FLOW_MAX_LEN 	16UL	//byte

#define SYNC_H_TIME		40UL
#define SYNC_L_TIME		40UL

#define BIT0_H_TIME		20UL
#define BIT0_L_TIME		20UL

#define BIT1_H_TIME		10UL
#define BIT1_L_TIME		10UL

#define END_H_TIME		80UL
#define END_L_TIME		5UL

#define ALLOW_ERR_PERCENT	15UL

#define SEND_SYNC_REPEAT	1


#endif // _RF_DROVER_CONFIG_




