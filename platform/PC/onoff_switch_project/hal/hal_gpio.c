
#include "hal_gpio.h"


void GPIO_Init(GPIO_TypeDef* GPIOx, GPIO_InitTypeDef* GPIO_InitStruct){}
uint8_t GPIO_ReadInputDataBit(GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin){return 0;}
uint16_t GPIO_ReadInputData(GPIO_TypeDef* GPIOx){return 0;}
uint8_t GPIO_ReadOutputDataBit(GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin){return 0;}
uint16_t GPIO_ReadOutputData(GPIO_TypeDef* GPIOx){return 0;}
void GPIO_SetBits(GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin){}
void GPIO_ResetBits(GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin){}
void GPIO_WriteBit(GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin, BitAction BitVal){}
void GPIO_Write(GPIO_TypeDef* GPIOx, uint16_t PortVal){}
