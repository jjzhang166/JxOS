
#ifndef __HAL_TIMER_H
#define __HAL_TIMER_H

#include "lib/type.h"

#define TIM_TypeDef void

#define 	TIM0 		0
#define    	TIM1		1
#define    	TIM2		2
#define    	TIM3		3
#define    	TIM4		4
#define    	TIM5		5
#define    	TIM6		6

typedef enum
{
  ENABLE = 1,
  DISABLE,
}FunctionalState;

typedef struct
{
  uint16_t TIM_CounterMode;       /*!< Specifies the counter mode.
                                       This parameter can be a value of @ref TIM_Counter_Mode */

  uint16_t TIM_Period;            /*!< Specifies the period value to be loaded into the active
                                       Auto-Reload Register at the next update event.
                                       This parameter must be a number between 0x0000 and 0xFFFF.  */

} TIM_TimeBaseInitTypeDef;

#define TIM_CounterMode_Up                 ((uint16_t)0x0000)
#define TIM_CounterMode_Down               ((uint16_t)0x0010)


#define TIM_IT_Update                      ((uint16_t)0x0001)
#define TIM_IT_Trigger                     ((uint16_t)0x0040)
#define TIM_IT_Break                       ((uint16_t)0x0080)

void TIM_TimeBaseInit(TIM_TypeDef* TIMx, TIM_TimeBaseInitTypeDef* TIM_TimeBaseInitStruct);
void TIM_Cmd(TIM_TypeDef* TIMx, FunctionalState NewState);
void TIM_ITConfig(TIM_TypeDef* TIMx, uint16_t TIM_IT, FunctionalState NewState);

#endif
