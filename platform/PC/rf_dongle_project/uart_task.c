
#include "jxos_public.h"
#include "lib/ring_buff.h"
#include "bsp/bsp_async_comm.h"
#include "bsp_async_comm_config.h"

void rf_rx_disable(void);

static uint8_t uart_rx_buffer_space[64] = {0};
RINGBUFF_MGR uart_rx_buffer_mgr;
static uint8_t uart_tx_buffer_space[64] = {0};
RINGBUFF_MGR uart_tx_buffer_mgr;

static JXOS_MSG_HANDLE rf_rec_data_msg;
static swtime_type uart_rx_delay_timer = 0;

static void bsp_async_comm_receive_ok_call_back_handler(uint8_t* unpackaged_rec_data, uint8_t unpackaged_rec_data_len)
{
	rf_rx_disable();
	bsp_rf_send(unpackaged_rec_data, unpackaged_rec_data_len);
	sys_debug_print_task_blocked_print_str("bsp_async_comm_receive_ok_call_back\r\n");
	sys_debug_print_task_blocked_print_data_stream_in_hex(unpackaged_rec_data, unpackaged_rec_data_len);
	sys_debug_print_task_blocked_print_str("\r\n");
}

static void uart_rx_isr_handler(uint8_t rx_data)
{
	ringbuff_write_data(&uart_rx_buffer_mgr, 1, rx_data);
	sys_software_timer_task_restart_timer(uart_rx_delay_timer);
}

static void uart_tx_isr_handler(void)
{
	uint8_t tx_data;
	if(1 == ringbuff_read_data(&uart_tx_buffer_mgr, 1, &tx_data)){
//		uart_send_data(tx_data);
	}
}

static void uart_task(uint8_t task_id, void * parameter)
{
	uint8_t uart_rec_data_buff[32];
	uint8_t uart_rec_data_len;
	uint8_t rf_rec_data_buff[FRAME_PAYLOAD_LEN_MAX];
	uint8_t rf_rec_data_len;

	if(1 == sys_software_timer_task_check_overtime_timer(uart_rx_delay_timer)){
		uart_rec_data_len = ringbuff_check_used_space(&uart_rx_buffer_mgr);
		ringbuff_read_data(&uart_rx_buffer_mgr, uart_rec_data_len, uart_rec_data_buff);
		bsp_async_comm_rec_data_handler(uart_rec_data_buff, uart_rec_data_len);
	}

	rf_rec_data_len = 0;
	while(jxos_msg_receive(rf_rec_data_msg, &rf_rec_data_buff[rf_rec_data_len]) == 1){
		rf_rec_data_len++;
	}
	if(rf_rec_data_len > 0){
		bsp_async_comm_pack_send_data(uart_rec_data_buff, &uart_rec_data_len, rf_rec_data_buff, rf_rec_data_len);
		ringbuff_write_data(&uart_tx_buffer_mgr, uart_rec_data_len, uart_rec_data_buff);
		sys_debug_print_task_blocked_print_str("write to uart tx buff\r\n");
		sys_debug_print_task_blocked_print_data_stream_in_hex(uart_rec_data_buff, uart_rec_data_len);
		sys_debug_print_task_blocked_print_str("\r\n");
		if(1 == ringbuff_read_data(&uart_tx_buffer_mgr, 1, &uart_rec_data_len)){
	//		uart_send_data(uart_rec_data_len);
		}
	}
}


static uint8_t text1[] = {0xaa,0x0a,0xff,0xab,0xcd,0x4b,0xe1,0x22,0xef,0xaa,0xaa,0x5a,0xaa,0x09,0xff,0xab,0xcd,0x4b,0xe1,0x22,0xef,0xcc,0x5a};
static uint8_t text2[] = {0xaa,0x0a,0xff,0xab,0xcd,0x4b};
static uint8_t text3[] = {0xe1,0x22,0xef,0xaa,0xaa,0x5a,0xaa,0x09,0xff};
static uint8_t text4[] = {0xab,0xcd,0x4b,0xe1,0x22,0xef,0xcc,0x5a};
static uint8_t temp_buff[64];
static uint8_t len;
void  uart_task_init(void)
{
	bsp_async_comm_init(bsp_async_comm_receive_ok_call_back_handler);

    RingBuff(&uart_rx_buffer_mgr, 64, uart_rx_buffer_space);
    RingBuff(&uart_tx_buffer_mgr, 64, uart_tx_buffer_space);

    uart_rx_delay_timer = sys_software_timer_task_new_timer();
	sys_software_timer_task_set_timer(uart_rx_delay_timer, 100/5);

	rf_rec_data_msg = jxos_msg_get_handle("rf_rec_data_msg");
	jxos_task_create(uart_task, "u_t", 0);

	ringbuff_write_data(&uart_rx_buffer_mgr, sizeof(text1), text1);
	len = ringbuff_check_used_space(&uart_rx_buffer_mgr);
	ringbuff_read_data(&uart_rx_buffer_mgr, len, temp_buff);
	bsp_async_comm_rec_data_handler(temp_buff, len);

	ringbuff_write_data(&uart_rx_buffer_mgr, sizeof(text2), text2);
	len = ringbuff_check_used_space(&uart_rx_buffer_mgr);
	ringbuff_read_data(&uart_rx_buffer_mgr, len, temp_buff);
	bsp_async_comm_rec_data_handler(temp_buff, len);

	ringbuff_write_data(&uart_rx_buffer_mgr, sizeof(text3), text3);
	len = ringbuff_check_used_space(&uart_rx_buffer_mgr);
	ringbuff_read_data(&uart_rx_buffer_mgr, len, temp_buff);
	bsp_async_comm_rec_data_handler(temp_buff, len);

	ringbuff_write_data(&uart_rx_buffer_mgr, sizeof(text4), text4);
	len = ringbuff_check_used_space(&uart_rx_buffer_mgr);
	ringbuff_read_data(&uart_rx_buffer_mgr, len, temp_buff);
	bsp_async_comm_rec_data_handler(temp_buff, len);
}
