
#ifndef _APP_LAYER_CONFIG_H
#define _APP_LAYER_CONFIG_H

#include "../../SRTnet/application_layer/application_layer_service.h"

/*
֡�ṹ:
service serila mun
1 byte

service id
1 byte

service cmd
1 byte

service cmd param
n byte(byte[0] == len Big-Endian)
*/

#define BUNDLING_TABLE_MAX						10

//SERVICE_CONFIG_TABLE < 0XF0
#define SERVICE_CONFIG_TABLE_INIT \
	{SERVICE_ONOFF_ID,	SERVICE_SLAVE} \

#define SERVICE_ONOFF_MASTER_ENABLE 		0
#define SERVICE_ONOFF_SLAVE_ENABLE 			0

#define SERVICE_TEMPERATURE_MASTER_ENABLE 	1
#define SERVICE_TEMPERATURE_SLAVE_ENABLE 	0

#endif
