#include <bsp_key_config.h>
#include "../../../hal/hal_gpio.h"

void bsp_key_hal_init(void)
{
    GPIO_InitTypeDef GPIO_InitStruct;
    GPIO_InitStruct.GPIO_Mode = GPIO_Mode_IPU;
    GPIO_InitStruct.GPIO_Pin = GPIO_Pin_0;
	GPIO_Init(GPIOA, &GPIO_InitStruct);
}

/************************************************/
//以下接口由 key_scan() 调用
//如果 key_scan() 在中断中执行，注意代码处理时间等细节
#include<Windows.h>
#include<stdio.h>
uint8_t bsp_key_hal_read_pin_level(uint8_t key_mun)
{
	return ((GetAsyncKeyState(key_mun+0x30) & 0x8000) ? 0 : 1);
 //   return GPIO_ReadInputDataBit(GPIOA, GPIO_Pin_0);
}


