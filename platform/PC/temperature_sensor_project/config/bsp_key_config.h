#ifndef __KEY_CONGIF_H
#define __KEY_CONGIF_H

#include "../../../lib/type.h"

#define KEY_NUM_MAX 		        3

#define KEY_PRESS_LEVEL				0
#define KEY_RELEASE_LEVEL 		    1

#define KEY_JITTER_TIME					0
#define KEY_LONG_PRESS_TIME 		    100
#define KEY_LONG_PRESS_REPEAT_TIME 		30

void bsp_key_hal_init(void);
uint8_t bsp_key_hal_read_pin_level(uint8_t key_mun);

#endif
