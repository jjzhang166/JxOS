
#include "bsp_led_config.h"
#include "../../../hal/hal_gpio.h"

void bsp_led_hal_init(void)
{
    GPIO_InitTypeDef GPIO_InitStruct;
    GPIO_InitStruct.GPIO_Mode = GPIO_Mode_Out_PP;
    GPIO_InitStruct.GPIO_Pin = GPIO_Pin_1;
	GPIO_Init(GPIOA, &GPIO_InitStruct);
}

void bsp_led_hal_on(uint8_t led_num)
{
	printf("led on %d\r\n", led_num);
    switch(led_num){
    case 0:
        GPIO_SetBits(GPIOA, GPIO_Pin_1);
        break;
    default:
        break;
    }
}


void bsp_led_hal_off(uint8_t led_num)
{
	printf("led off %d\r\n", led_num);
    switch(led_num){
    case 0:
        GPIO_ResetBits(GPIOA, GPIO_Pin_1);
        break;
    default:
        break;
    }
}

