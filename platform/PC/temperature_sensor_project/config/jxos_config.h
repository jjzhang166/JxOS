
#ifndef __JXOS_CONFIG_H
#define __JXOS_CONFIG_H

#define JXOS_TASK_ENABLE        1
#define TASK_MAX                6

#define JXOS_ENENT_ENABLE       1
#define EVENT_MAX               6

#define JXOS_MSG_ENABLE         1
#define MSG_QUEUE_MAX           1

#define JXOS_REGISTRY_ENABLE    1

#endif

