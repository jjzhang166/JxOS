
#include "../../../hal/hal_timer.h"

void sys_software_timer_config_hal_timer_init(void)
{
    TIM_TimeBaseInitTypeDef TIM_TimeBaseInitStruct;
    TIM_TimeBaseInitStruct.TIM_CounterMode = TIM_CounterMode_Up;
    TIM_TimeBaseInitStruct.TIM_Period = 10;
    TIM_TimeBaseInit(TIM0, &TIM_TimeBaseInitStruct);

    TIM_ITConfig(TIM0, TIM_IT_Update, ENABLE);

    TIM_Cmd(TIM0, ENABLE);
}
