#ifndef __KEY_CONGIF_H
#define __KEY_CONGIF_H

#include "lib/type.h"

#define KEY_NUM_MAX 		        1

#define KEY_PRESS_LEVEL				0

#define KEY_JITTER_TICK					0
#define KEY_LONG_PRESS_TICK 		    80
#define KEY_LONG_PRESS_REPEAT_TICK 		80

#endif
