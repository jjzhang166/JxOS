
#include "jxos_public.h"

#define I2C_SPEED 100000
#define I2C_SLAVE_ADDRESS7     0xA0

static void iic_task(uint8_t task_id, void * parameter)
{

}
void bsp_sw_i2c_burst_read(unsigned char slave_id,
		unsigned int read_reg_len,
		unsigned char *read_buf);
void bsp_sw_i2c_burst_write(unsigned char slave_id,
		unsigned int write_reg_len,
		unsigned char *write_buf);

void bsp_sw_i2c_burst_read_reg(unsigned char slave_id, \
                    unsigned char start_reg_addr, unsigned int read_reg_len, \
                    unsigned char *read_buf);
void bsp_sw_i2c_burst_write_reg(unsigned char slave_id, \
                    unsigned char start_reg_addr, unsigned int write_reg_len, \
                    unsigned char *write_buf);

	

	
uint8_t iic_task_burst_write(uint8_t slave_addr, uint8_t write_data_len, uint8_t *write_data_buf)
{
	while(I2C_GetFlagStatus(I2C_FLAG_BUSBUSY))
	{
	//    if((sEETimeout--) == 0) return sEE_TIMEOUT_UserCallback();
	}

	/* Send START condition */
	I2C_GenerateSTART( ENABLE);

	/* Test on EV5 and clear it */
	//  sEETimeout = sEE_FLAG_TIMEOUT;
	while(!I2C_CheckEvent(I2C_EVENT_MASTER_MODE_SELECT))
	{
	//    if((sEETimeout--) == 0) return sEE_TIMEOUT_UserCallback();
	}

	I2C_Send7bitAddress((uint8_t)slave_addr, I2C_DIRECTION_TX);

	/* Test on EV6 and clear it */
	//  sEETimeout = sEE_FLAG_TIMEOUT;
	while(!I2C_CheckEvent( I2C_EVENT_MASTER_TRANSMITTER_MODE_SELECTED))
	{
	//    if((sEETimeout--) == 0) return sEE_TIMEOUT_UserCallback();
	}
	
	for (; write_data_len > 0; write_data_len--) {
		I2C_SendData(*write_buf);	//read reg
		write_buf++;
		while(!I2C_CheckEvent( I2C_EVENT_MASTER_BYTE_TRANSMITTED))
		{
		//    if((sEETimeout--) == 0) return sEE_TIMEOUT_UserCallback();
		}
		/* Wait till all data have been physically transferred on the bus */
//		sEETimeout = sEE_LONG_TIMEOUT;
//		while(!I2C_GetFlagStatus( I2C_FLAG_TRANSFERFINISHED))
//		{
//		  if((sEETimeout--) == 0) sEE_TIMEOUT_UserCallback();
//		}
	}
	
   /* Send STOP condition */
    I2C_GenerateSTOP(ENABLE);
	
  return 1;
}

uint8_t iic_task_burst_read(uint8_t slave_addr, uint8_t read_data_len, uint8_t *read_data_buf)
{
	while(I2C_GetFlagStatus(I2C_FLAG_BUSBUSY))
	{
	//    if((sEETimeout--) == 0) return sEE_TIMEOUT_UserCallback();
	}

	/* Send START condition */
	I2C_GenerateSTART( ENABLE);

	/* Test on EV5 and clear it */
	//  sEETimeout = sEE_FLAG_TIMEOUT;
	while(!I2C_CheckEvent(I2C_EVENT_MASTER_MODE_SELECT))
	{
	//    if((sEETimeout--) == 0) return sEE_TIMEOUT_UserCallback();
	}

	I2C_Send7bitAddress((uint8_t)slave_addr, I2C_DIRECTION_RX);	
	while(!I2C_CheckEvent( I2C_EVENT_MASTER_RECEIVER_MODE_SELECTED))
	{
	//    if((sEETimeout--) == 0) return sEE_TIMEOUT_UserCallback();
	}

	while (read_reg_len) {
		while ((I2C_GetFlagStatus(I2C_FLAG_TRANSFERFINISHED) == RESET)); /* Poll on BTF */
		#ifdef SAFE_PROCEDURE
		if (NumByteToRead != 3) /* Receive bytes from first byte until byte N-3 */
		{
		  while ((I2C_GetFlagStatus(I2C_FLAG_TRANSFERFINISHED) == RESET)); /* Poll on BTF */

		  /* Read a byte from the Slave */
		  *read_buf = I2C_ReceiveData();

		  /* Point to the next location where the byte read will be saved */
		  read_buf++;

		  /* Decrement the read bytes counter */
		  read_reg_len--;
		}

		if (read_reg_len == 3)  /* it remains to read three data: data N-2, data N-1, Data N */
		{
		  /* Data N-2 in DR and data N -1 in shift register */
		  while ((I2C_GetFlagStatus(I2C_FLAG_TRANSFERFINISHED) == RESET)); /* Poll on BTF */

		  /* Clear ACK */
		  I2C_AcknowledgeConfig(I2C_ACK_NONE);

		  /* Disable general interrupts */
		  disableInterrupts();

		  /* Read Data N-2 */
			*read_buf = I2C_ReceiveData();

		  /* Point to the next location where the byte read will be saved */
		  read_buf++;

		  /* Program the STOP */
		  I2C_GenerateSTOP(ENABLE);

		  /* Read DataN-1 */
		  *read_buf = I2C_ReceiveData();

		  /* Enable General interrupts */
		  enableInterrupts();

		  /* Point to the next location where the byte read will be saved */
		  read_buf++;

		  while ((I2C_GetFlagStatus(I2C_FLAG_RXNOTEMPTY) == RESET)); /* Poll on RxNE */

		  /* Read DataN */
		  *read_buf = I2C_ReceiveData();

		  /* Reset the number of bytes to be read by master */
		  read_reg_len = 0;

		}
		#else
		if (read_reg_len == 1)
		{
		  /* Disable Acknowledgement */
		  I2C_AcknowledgeConfig(I2C_ACK_NONE);

		  /* Send STOP Condition */
		  I2C_GenerateSTOP(ENABLE);

		  /* Poll on RxNE Flag */
		  while ((I2C_GetFlagStatus(I2C_FLAG_RXNOTEMPTY) == RESET));
		  /* Read a byte from the Slave */
		  *read_buf = I2C_ReceiveData();

		  /* Point to the next location where the byte read will be saved */
		  read_buf++;

		  /* Decrement the read bytes counter */
		  read_reg_len--;
		}

		/* Test on EV7 and clear it */
		if (I2C_CheckEvent(I2C_EVENT_MASTER_BYTE_RECEIVED) )
		{
		  /* Read a byte from the EEPROM */
		  *read_buf = I2C_ReceiveData();

		  /* Point to the next location where the byte read will be saved */
		  read_buf++;

		  /* Decrement the read bytes counter */
		  read_reg_len--;
		}
		#endif /* SAFE_PROCEDURE */
		}
	}

}

void iic_task_init(void)
{
  /* I2C Initialize */
  I2C_Init(I2C_SPEED, I2C_SLAVE_ADDRESS7, I2C_DUTYCYCLE_2, I2C_ACK_CURR, I2C_ADDMODE_7BIT, 16);

  /* Enable Buffer and Event Interrupt*/
//  I2C_ITConfig((I2C_IT_TypeDef)(I2C_IT_EVT | I2C_IT_BUF) , ENABLE);
  
  I2C_Cmd( ENABLE);
  
  /* TXBuffer initialization */
  for (i = 0; i < BUFFERSIZE; i++)
    TxBuffer[i] = i;

  /* Send START condition */
  I2C_GenerateSTART(ENABLE);
  while (I2C_GetFlagStatus(I2C_FLAG_BUSBUSY));

  /* Add a delay to be sure that communication is finished */
  Delay(0xFFFF);

  /*****  reception phase ***/
  /*  Wait while the bus is busy */
  while (I2C_GetFlagStatus(I2C_FLAG_BUSBUSY));

  /* Send START condition */
  I2C_GenerateSTART(ENABLE);

  /* Test on EV5 and clear it */
  while (!I2C_CheckEvent(I2C_EVENT_MASTER_MODE_SELECT));

#ifdef TEN_BITS_ADDRESS

#else
  /* Send slave Address for write */
  I2C_Send7bitAddress(SLAVE_ADDRESS, I2C_DIRECTION_RX);

  /* Test on EV6 and clear it */
  while (!I2C_CheckEvent(I2C_EVENT_MASTER_RECEIVER_MODE_SELECTED));
#endif /* TEN_BITS_ADDRESS */


  
//	key_multi_click_count = 0;
//	key_multi_click_keynum = 0xff;

	//HAL
//	if(std_app_key_task_hal_init_callback != 0){
//		std_app_key_task_hal_init_callback();
//	}

	//BSP


	//OS
//	jxos_task_create(iic_task, "uart", 0);
//	key_press_event = jxos_event_create();
//    key_msg = jxos_msg_create(key_msg_buff, 32, 1, "std_app_key_msg");

	//LIB

	//SYS TASK
//	key_scan_swt = sys_software_timer_task_new_timer();
//	sys_software_timer_task_set_timer(key_scan_swt, KEY_TASK_SCAN_TIME);

//	key_multi_click_swt = sys_software_timer_task_new_timer();
//	sys_software_timer_task_set_timer(key_multi_click_swt, KEY_TASK_MULTI_CLICK_TIME);
}
